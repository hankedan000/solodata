#!/bin/bash
REPO_ROOT=$(pwd)
PROTO_DIR_PY=$REPO_ROOT/src/proto/py
PROTO_DIR_JAVA=$REPO_ROOT/src/proto/java

echo "Exporting environment variables..."

export PYTHONPATH=$REPO_ROOT/src/py:$PROTO_DIR_PY
echo "PYTHONPATH = $PYTHONPATH"

# add utils directory to PATH so that you can use programs like "csv2json" from terminal
export PATH=$PATH:$REPO_ROOT/src/py/utils
echo "PATH = $PATH"
