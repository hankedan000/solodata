#!/bin/bash

# install kernel boot args
if grep -q "SOLODATA" "/boot/config.txt"; then
	# file already modified, skip
	echo "boot.txt already modified. skipping"
else
	echo "modifying boot config"
	sudo cat rpi/boot/config.txt >> /boot/config.txt
fi

# disable UART console
echo "disabling console UART"
sudo sed -i "s/console=serial0,115200 //" /boot/cmdline.txt 

# install wifi switching services
echo "installing WiFi switcher"
sudo ./wifi_switch/install.sh
