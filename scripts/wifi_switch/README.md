# WiFi Switch
This service toggles the WiFi configuration between an end point and access point based on a GPIO pin state.

## Raspberry PI WiFi Access Point Setup
I followed this guide on configuring the Raspberry PI as WiFi hotspot.
https://www.raspberrypi.org/documentation/configuration/wireless/access-point-routed.md

Quickstart:
1. sudo apt install hostapd dnsmasq
2. sudo systemctl unmask hostapd
3. sudo DEBIAN_FRONTEND=noninteractive apt install -y netfilter-persistent iptables-persistent
4. sudo cp -r ./exemplar/* /etc/
  - you can customize the example configs we just installed
  - the hostapd.conf will setup a default WiFi network from the Pi
    * WiFi device: wlan0
    * WiFi SSID: "PINET"
    * WiFi password: "password"
    * Pi's IP address: 10.200.200.1
    * DHCP IP range: 10.200.200.2 to 10.200.200.20

Use config-wlan0-ap.sh to configure the PI as a WiFi access point. config-wlan0-ep.sh will setup the PI as a regular WiFi endpoint. These scripts can be used to toggle the configuration at runtime.

## Install
Run install.sh as root.
Start the service by running ```systemctl start wifi-switch.service```.
To start the service from power on enter ```systemctl enable wifi-switch.service```.

## Debugging
To check if the service is running, run ```systemctl status wifi-switch.service```. The log should have a history of all the WiFi state transitions that occured.

To see the current state of the WiFi configuration you can run ```cat /tmp/wifi_state```. This file will contain "ap" or "ep" if the WiFi is configured as an access point or end point respectively.
