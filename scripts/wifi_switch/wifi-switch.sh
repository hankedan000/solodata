#!/bin/bash
# GPIO pin used for toggling WiFi between 
# end-point and access-point mode
SW_PIN=29

# contains "ap" if wlan0 is an access point or
# "ep" if wlan0 is an end point
CURR_WIFI_STATE_FILE="/tmp/wifi_state"

# initial state of switch is unknown
prev_state="unknown"

# setup pin as input with internal pull up
gpio mode $SW_PIN in
gpio mode $SW_PIN up

while true; do
	curr_state=$(gpio read $SW_PIN)
	
	# check if switch changed state
	if [ $curr_state != $prev_state ]; then
		if [ $curr_state == "1" ]; then
			echo "Configure as end point"
			bash /etc/init.d/wifi_switch/config-wlan0-ep.sh
			echo "ep" > $CURR_WIFI_STATE_FILE
		else
			echo "Configure as access point"
			bash /etc/init.d/wifi_switch/config-wlan0-ap.sh
			echo "ap" > $CURR_WIFI_STATE_FILE
		fi
	fi
	
	# store current state for next time
	prev_state=$curr_state

	sleep 5;
done
