#!/bin/bash
systemctl stop hostapd
cp /etc/dnsmasq.conf.ep /etc/dnsmasq.conf
cp /etc/dhcpcd.conf.ep /etc/dhcpcd.conf
systemctl daemon-reload
systemctl restart dnsmasq
systemctl restart dhcpcd
# restart avahi for *.local mDNS lookup
systemctl restart avahi-daemon.service
