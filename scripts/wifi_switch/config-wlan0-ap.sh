#!/bin/bash
cp /etc/dnsmasq.conf.ap /etc/dnsmasq.conf
cp /etc/dhcpcd.conf.ap /etc/dhcpcd.conf
systemctl daemon-reload
systemctl restart dnsmasq
systemctl restart dhcpcd
systemctl restart hostapd
# restart avahi for *.local mDNS lookup
systemctl restart avahi-daemon.service
