#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# install wifi switching scripts
echo "Installing scripts..."
mkdir -p /etc/init.d/wifi_switch
cp $SCRIPT_DIR/*.sh /etc/init.d/wifi_switch

# install service scripts
echo "Installing service..."
cp $SCRIPT_DIR/*.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable wifi-switch.service
