# mkvcan.sh
Creates a virtual CAN interface named "vcan0".

# rmvcan.sh
Removes the virtual CAN interface named "vcan0".

# vcan-playback.sh
Plays a candump recorded file to CAN interface "vcan0".
