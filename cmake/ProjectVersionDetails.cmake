# This file should contain nothing but the following line
# setting the project version. The variable name must not
# clash with the solodata_VERSION* variables automatically
# defined by the project() command.
set(solodata_VER 0.1.0)