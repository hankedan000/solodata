# if CppUnit is installed, include it's headers
message(STATUS "Detecting if CPPUNIT is installed")
find_package(CppUnit)
message(STATUS "Detecting if CPPUNIT is installed - ${CPPUNIT_FOUND}")
if (CPPUNIT_FOUND)
	include_directories(${CPPUNIT_INCLUDE_DIR})
endif()

# include argparse library
# find_package(Argparse REQUIRED)
# if (ARGPARSE_FOUND)
#     include_directories(${ARGPARSE_INCLUDE_DIR})
# endif()

# if Google Protobuf is installed, include it's headers
find_package(Protobuf REQUIRED)
set(Protobuf_BUILD "NO")
if (Protobuf_FOUND)
    include_directories(${Protobuf_INCLUDE_DIRS})
    if (Protobuf_PROTOC_EXECUTABLE)
        set(Protobuf_BUILD "YES")
    endif()
endif()

# if ZeroMQ is installed, include it's headers
find_package(ZMQ REQUIRED)
if (ZMQ_FOUND)
    include_directories(${ZMQ_INCLUDE_DIR})
endif()

# if libgps is installed, include it's headers
find_package(Gps REQUIRED)
if (GPS_FOUND)
    include_directories(${GPS_INCLUDE_DIR})
endif()

find_package(CppCanParser)
find_package(Reactor)
find_package(LibZip REQUIRED)

# if WiringPi is installed, include it's headers
message(STATUS "Detecting if WiringPi is installed")
find_package(WiringPi)
message(STATUS "Detecting if WiringPi is installed - ${WiringPi_FOUND}")
set(BUILD_PI "OFF")
if (WiringPi_FOUND)
    set(BUILD_PI "ON")
    include_directories(${WiringPi_INCLUDE_DIR})
endif()

# FIXME figure out how to get the path set right
include(/usr/local/lib/cmake/protorecord/protorecord-targets.cmake)
