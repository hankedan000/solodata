#
# Find the ZeroMQ includes and library
#
# This module defines
# ZMQ_INCLUDE_DIR, where to find zmq.h, etc.
# ZMQ_LIBRARIES, the libraries to link against to use ZeroMQ.
# ZMQ_FOUND, If false, do not try to use ZeroMQ.

# also defined, but not for general use are
# ZMQ_LIBRARY, where to find the ZeroMQ library.

find_path(ZMQ_INCLUDE_DIR zmq.h
  /usr/local/include
  /usr/include
)

find_library(ZMQ_LIBRARY zmq
  /usr/local/lib
  /usr/lib)

find_library(ZMQ_STATIC_LIBRARY libzmq.a
  /usr/local/lib
  /usr/lib)

set(ZMQ_FOUND "NO")
if (ZMQ_INCLUDE_DIR)
  if (ZMQ_LIBRARY)
    set(ZMQ_FOUND "YES")
    set(ZMQ_LIBRARIES ${ZMQ_LIBRARY} ${CMAKE_DL_LIBS} pthread)
    set(ZMQ_STATIC_LIBRARIES ${ZMQ_STATIC_LIBRARY} pthread)
  endif()
endif()