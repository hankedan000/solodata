#
# Find the Reactor includes and library
#
# Emitted targets: Reactor
find_path(Reactor_INCLUDE_DIR Reactor.h)
find_library(Reactor_LIBRARY Reactor)

# Handle find_package's QUIET, REQUIRED etc. arguments
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Reactor REQUIRED_VARS
	Reactor_LIBRARY
	Reactor_INCLUDE_DIR
)

# Add imported target to the CMake build
add_library(Reactor STATIC IMPORTED)
# link dependencies
target_link_libraries(
	Reactor INTERFACE
		log4cxx
		Logging
		czmq
		zmq)
set_target_properties(Reactor PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "${Reactor_INCLUDE_DIR}"
	IMPORTED_LOCATION "${Reactor_LIBRARY}"
)
