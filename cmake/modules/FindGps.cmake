#
# Find the libgps includes and library
#
# This module defines
# GPS_INCLUDE_DIR, where to find gps.h, etc.
# GPS_LIBRARIES, the libraries to link against to use libgps.
# GPS_FOUND, If false, do not try to use libgps.

# also defined, but not for general use are
# GPS_LIBRARY, where to find the libgps library.

find_path(GPS_INCLUDE_DIR gps.h
  /usr/local/include
  /usr/include
)

find_library(GPS_LIBRARY gps
  /usr/local/lib
  /usr/lib)

set(GPS_FOUND "NO")
if (GPS_INCLUDE_DIR)
  if (GPS_LIBRARY)
    set(GPS_FOUND "YES")
    set(GPS_LIBRARIES ${GPS_LIBRARY} ${CMAKE_DL_LIBS} pthread)
  endif()
endif()