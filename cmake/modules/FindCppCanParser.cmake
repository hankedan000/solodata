#
# Find the cpp-can-parser includes and library
#
# Emitted targets: CppCanParser
find_path(CPP_CAN_PARSER_INCLUDE_DIR_HEADERS CANDatabase.h PATH_SUFFIXES "cpp-can-parser")
find_library(CPP_CAN_PARSER_LIBRARY cpp-can-parser)

# Handle find_package's QUIET, REQUIRED etc. arguments
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CppCanParser REQUIRED_VARS
	CPP_CAN_PARSER_LIBRARY
	CPP_CAN_PARSER_INCLUDE_DIR_HEADERS
)

# Build include directories list
set(CPP_CAN_PARSER_INCLUDE_DIR
	"${CPP_CAN_PARSER_INCLUDE_DIR_HEADERS}"
)

# Add imported target to the CMake build
add_library(CppCanParser STATIC IMPORTED)
set_target_properties(CppCanParser PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "${CPP_CAN_PARSER_INCLUDE_DIR}"
	IMPORTED_LOCATION "${CPP_CAN_PARSER_LIBRARY}"
)