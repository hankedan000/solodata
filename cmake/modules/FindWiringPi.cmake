#
# Find the WiringPi includes and library
#
# This module defines
# WiringPi_INCLUDE_DIR, where to find zmq.h, etc.
# WiringPi_LIBRARIES, the libraries to link against to use WiringPi.
# WiringPi_FOUND, If false, do not try to use WiringPi.

# also defined, but not for general use are
# WiringPi_LIBRARY, where to find the WiringPi library.

find_path(WiringPi_INCLUDE_DIR wiringPi.h
  /usr/local/include
  /usr/include
)

find_library(WiringPi_LIBRARY wiringPi
  /usr/local/lib
  /usr/lib)

set(WiringPi_FOUND "NO")
if (WiringPi_INCLUDE_DIR)
  if (WiringPi_LIBRARY)
    set(WiringPi_FOUND "YES")
    set(WiringPi_LIBRARIES ${WiringPi_LIBRARY})
  endif()
endif()