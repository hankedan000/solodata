/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.datalogger;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author daniel
 */
public class DetectionGateTest {
    
    public DetectionGateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of detect method, of class DetectionGate.
     */
    @Test
    public void testDetect() {
        System.out.println("detect");
        
        // detect perpendicular intersection
        Vector2D c1 = new Vector2D(0, 0);
        Vector2D c2 = new Vector2D(5, 0);
        DetectionGate gate = new DetectionGate(new Vector2D(1,2), new Vector2D(1,-2));
        assertEquals(true, gate.detect(c1, c2));
        assertEquals(true, gate.detect(c2, c1));// detect both ways
        
        // detect perpendicular but not yet intersecting lines
        c1 = new Vector2D(0, 0);
        c2 = new Vector2D(-5, 0);
        assertEquals(false, gate.detect(c1, c2));
        assertEquals(false, gate.detect(c2, c1));// detect both ways
        
        // parallel intersection
        c1 = new Vector2D(0, 2);
        c2 = new Vector2D(0, -2);
        assertEquals(false, gate.detect(c1, c2));
        assertEquals(false, gate.detect(c2, c1));// detect both ways
    }
    
}
