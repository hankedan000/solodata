/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.logviewer.components;

import com.solodata.tasks.TaskProgressPanel;
import com.solodata.tasks.TaskManager;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author daniel
 */
public class TaskProgressPanelDemo extends javax.swing.JPanel {
    private final TaskProgressPanel tpp = new TaskProgressPanel();
    
    
    /**
     * Creates new form TaskProgressPanelDemo
     */
    public TaskProgressPanelDemo() {
        initComponents();
        
        TaskManager.setTaskManagerListener(tpp);
        bottom.add(tpp, BorderLayout.CENTER);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("TaskProgressPanel Standalone");
                frame.setLayout(new BorderLayout());
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setMinimumSize(new Dimension(600, 400));
                
                // Create and show the JLogChart GUI
                TaskProgressPanelDemo demo = new TaskProgressPanelDemo();
                frame.add(demo, BorderLayout.CENTER);
                
                // Show the GUI
                frame.pack();
                frame.setVisible(true);
            }
        });
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        top = new javax.swing.JPanel();
        addButton = new javax.swing.JButton();
        bottom = new javax.swing.JPanel();

        setLayout(new java.awt.BorderLayout());

        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        addButton.setText("Add Task");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout topLayout = new javax.swing.GroupLayout(top);
        top.setLayout(topLayout);
        topLayout.setHorizontalGroup(
            topLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topLayout.createSequentialGroup()
                .addComponent(addButton)
                .addGap(0, 314, Short.MAX_VALUE))
        );
        topLayout.setVerticalGroup(
            topLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topLayout.createSequentialGroup()
                .addComponent(addButton)
                .addGap(0, 155, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(top);

        bottom.setMinimumSize(new java.awt.Dimension(0, 100));
        bottom.setLayout(new java.awt.BorderLayout());
        jSplitPane1.setRightComponent(bottom);

        add(jSplitPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        TaskManager.startTask("Test Task", null, new TaskManager.Task() {
            @Override
            public void run() {
                final int NUM = 100;
                for (int i=1; i<=NUM; i++) {
                    updateProgress(i, NUM);
                    try {
                        Thread.sleep(10, 0);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(TaskProgressPanelDemo.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }//GEN-LAST:event_addButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JPanel bottom;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JPanel top;
    // End of variables declaration//GEN-END:variables
}
