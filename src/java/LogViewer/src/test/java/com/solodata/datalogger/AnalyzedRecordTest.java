/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.datalogger;

import com.solodata.utils.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author daniel
 */
public class AnalyzedRecordTest {
    private static final File zipFile = new File("/home/daniel/git/solodata/datasets/tests/DataLogger/celeb_fig8.zip");
    private static final SensorDataRates IDEAL_RATES = new SensorDataRates(10.0, 100.0, 52.0);
    private static Record record = null;
    private static AnalyzedRecord ar = null;
    
    public AnalyzedRecordTest() {
        // Extract zipped Record to temporary path
        File tempDir = new File("/tmp/solodata/junit_tests");
        tempDir.mkdirs();
        File recordPath = new File(tempDir,"2022-04-17_21.59.09");
        
        FileUtils.extractAll(zipFile,tempDir);
        record = new Record(recordPath);
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of analyze method, of class AnalyzedRecord.
     */
    @org.junit.Test
    public void testAnalyze() {
        System.out.println("analyze");
        ar = new AnalyzedRecord(record);
        
        try
        {
            // just make sure it doesn't throw anything
            ar.analyze(IDEAL_RATES);
        }
        catch (Exception e)
        {
            fail("analyze() threw an exception");
        }
    }

    /**
     * Test of record method, of class AnalyzedRecord.
     */
    @org.junit.Test
    public void testRecord() {
        System.out.println("record");
        ar = new AnalyzedRecord(record);
        
        assertEquals(record, ar.record());
    }

    /**
     * Test of isAnalyzed method, of class AnalyzedRecord.
     */
    @org.junit.Test
    public void testIsAnalyzed() {
        System.out.println("isAnalyzed");
        ar = new AnalyzedRecord(record);
        
        assertEquals(false, ar.isAnalyzed());
        
        ar.analyze(IDEAL_RATES);
        assertEquals(true, ar.isAnalyzed());
    }

    /**
     * Test of getAvailableSignals method, of class AnalyzedRecord.
     */
    @org.junit.Test
    public void testGetAvailableSignals() {
        System.out.println("getAvailableSignals");
        ar = new AnalyzedRecord(record);
        
        // there should be no signals available until analyze() is called
        Set<String> availSignals = ar.getAvailableSignals();
        assertEquals(0, availSignals.size());
        
        ar.analyze(IDEAL_RATES);
        availSignals = ar.getAvailableSignals();
        assertTrue(availSignals.size() > 0);
    }

    /**
     * Test of getIdealSampleCount method, of class AnalyzedRecord.
     */
    @org.junit.Test
    public void testIdealSampleCount() {
        System.out.println("idealSampleCount");
        ar = new AnalyzedRecord(record);
        
        assertEquals(0, ar.getIdealSampleCount());
        
        ar.analyze(IDEAL_RATES);
        assertEquals(5334, ar.getIdealSampleCount());
    }

    /**
     * Test of hasSignal method, of class AnalyzedRecord.
     */
    @org.junit.Test
    public void testHasSignal() {
        System.out.println("hasSignal");
        ar = new AnalyzedRecord(record);
        
        // won't have these signals until analyze() is called
        assertEquals(false, ar.hasSignal("imu_acc_x"));
        assertEquals(false, ar.hasSignal("bad_signal_name"));
        
        ar.analyze(IDEAL_RATES);
        assertEquals(true, ar.hasSignal("imu_acc_x"));
        assertEquals(false, ar.hasSignal("bad_signal_name"));
    }

    /**
     * Test of getSignalData method, of class AnalyzedRecord.
     */
    @org.junit.Test
    public void testGetSignalData() {
        System.out.println("getSignalData");
        ar = new AnalyzedRecord(record);
        
        ar.analyze(IDEAL_RATES);
        List<Double> data = ar.getImmutableSignalData("imu_acc_x");
        assertEquals(5334, data.size());
        assertEquals(ar.getIdealSampleCount(), data.size());
    }

    /**
     * Test of isDataSkewed method, of class AnalyzedRecord.
     */
    @org.junit.Test
    public void testIsDataSkewed() {
        System.out.println("isDataSkewed");
        ar = new AnalyzedRecord(record);
        
        ar.analyze(IDEAL_RATES);
        assertEquals(false, ar.isDataSkewed());
    }

    @org.junit.Test
    public void testToCSV() {
        System.out.println("testToCSV");
        ar = new AnalyzedRecord(record);
        
        assertEquals("", ar.toCSV_String());
        
        ar.analyze(IDEAL_RATES);
        try {
            ar.toCSV_File(new File("/tmp/celeb_fig8.csv"));
        } catch (IOException ex) {
            fail("toCSV_File() threw an exception");
        }
    }

    @org.junit.Test
    public void newCAND_Samples() {
        System.out.println("newCAND_Samples");
        
        final File zipFile = new File("/home/daniel/git/solodata/datasets/tests/DataLogger/new_cand_samples.zip");
        File tempDir = new File("/tmp/solodata/junit_tests");
        FileUtils.extractAll(zipFile,tempDir);
        File recordPath = new File(tempDir,"2022-11-06_20.33.01");
        record = new Record(recordPath);
        ar = new AnalyzedRecord(record);
        
        ar.analyze(IDEAL_RATES);
    }
    
}
