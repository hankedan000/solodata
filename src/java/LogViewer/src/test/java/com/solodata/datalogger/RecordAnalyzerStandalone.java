/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.datalogger;

import com.solodata.utils.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniel
 */
public class RecordAnalyzerStandalone {
    public static void main(String args[]) {
        File zipFile = new File("/home/daniel/git/solodata/datasets/tests/DataLogger/celeb_fig8.zip");
        
        // Extract zipped Record to temporary path
        File tempDir = new File("/tmp/solodata/junit_tests");
        tempDir.mkdirs();
        FileUtils.extractAll(zipFile,tempDir);
        
        File recordPath = new File(tempDir,"2022-04-17_21.59.09");
        Record record = new Record(recordPath);
        AnalyzedRecord ar = new AnalyzedRecord(record);
        
        SensorDataRates IDEAL_RATES = new SensorDataRates(10.0, 100.0, 52.0);
        ar.analyze(IDEAL_RATES);
        try {
            ar.toCSV_File(new File("/home/daniel/Downloads/data.csv"));
        } catch (IOException ex) {
            Logger.getLogger(RecordAnalyzerStandalone.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
