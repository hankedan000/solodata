/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.utils;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author daniel
 */
public class VectorUtilsTest {
    
    public VectorUtilsTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of lerp method, of class VectorUtils.
     */
    @Test
    public void testLerp() {
        System.out.println("lerp");
        List<Double> V= new ArrayList<>(5);
        V.add(1.0);
        V.add(0.0);
        V.add(0.0);
        V.add(0.0);
        V.add(2.0);
        VectorUtils.lerp(V, 0, 4);
        assertEquals(1.00, V.get(0));
        assertEquals(1.25, V.get(1));
        assertEquals(1.50, V.get(2));
        assertEquals(1.75, V.get(3));
        assertEquals(2.00, V.get(4));
        
        try {
            VectorUtils.lerp(V, 3, 0);
            fail("lerp should throw InvalidParameterException when start > end");
        } catch (InvalidParameterException e) {
        }
        
        // make sure nothing is performed when start == end
        VectorUtils.lerp(V, 0, 0);
        assertEquals(1.00, V.get(0));
        assertEquals(1.25, V.get(1));
        assertEquals(1.50, V.get(2));
        assertEquals(1.75, V.get(3));
        assertEquals(2.00, V.get(4));
        
        // make sure nothing is performed when start is 1 less than end
        VectorUtils.lerp(V, 0, 1);
        assertEquals(1.00, V.get(0));
        assertEquals(1.25, V.get(1));
        assertEquals(1.50, V.get(2));
        assertEquals(1.75, V.get(3));
        assertEquals(2.00, V.get(4));
        
        // check case where we lerp in the middle of the vector
        V.set(0,0.0);
        V.set(1,0.0);
        V.set(2,1.0);
        V.set(3,0.0);
        V.set(4,2.0);
        VectorUtils.lerp(V, 2, 4);
        assertEquals(0.00, V.get(0));
        assertEquals(0.00, V.get(1));
        assertEquals(1.00, V.get(2));
        assertEquals(1.50, V.get(3));
        assertEquals(2.00, V.get(4));
    }
    
}
