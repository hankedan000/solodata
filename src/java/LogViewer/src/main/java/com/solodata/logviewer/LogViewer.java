/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.logviewer;

import com.solodata.tasks.TaskManager;
import com.solodata.utils.TreeUtils;
import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.solodata.datalogger.Record;
import com.solodata.logviewer.ProjectTreeItems.TreeProject;
import com.solodata.logviewer.ProjectTreeItems.TreeRecord;
import com.solodata.logviewer.dialogs.CreateProjectDialog;
import com.solodata.ui.maps.RecordMapMarkers;
import com.solodata.utils.ResourceUtils;
import com.solodata.utils.UserDirUtils;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import solodata.NavDefn;
import solodata.NavSampleOuterClass;

/**
 *
 * @author daniel
 */
public class LogViewer extends javax.swing.JFrame implements
        WindowListener, ComponentListener, TreeSelectionListener {
    public static final Logger logger = Logger.getLogger(LogViewer.class.getName());
    public static final String LOGVIEWER_USER_DIR = "LogViewer";
    
    private final MutableTreeNode projectTreeRoot;
    private final DefaultTreeModel projectTreeModel;
    
    // A map of all imported Projects keyed by their name
    private final Map<String,Project> importedProjectsByName = new HashMap<>();
    
    // The Project that is currently selected in the Project Tree
    private Project activeProject = null;
    // The ProjectView TreeNode for the current active project
    private DefaultMutableTreeNode activeProjectTreeNode = null;

    // map of markers keyed by record name
    private final Map<String,RecordMapMarkers> markersByRecordName = new HashMap<>();
    
    // Current set of markers that are active
    private RecordMapMarkers activeMarkers = null;
    
    private final Timer saveCheckTimer = new Timer(true);

    /**
     * Creates new form LogViewer
     */
    public LogViewer() {
        initComponents();
        setTitle("Solodata Log Viewer");
        setIconImage(ResourceUtils.loadImageIcon("assets/icons/icon128.png").getImage());
        addWindowStateListener(new LV_WindowStateListener());
        addWindowListener(this);
        addComponentListener(this);
        
        TaskManager.setTaskManagerListener(taskProgressPanel);
        
        mapView.setControlPanelVisible(false);
        
        // Init the default start of the Project menu
        onActiveProjectChanged();
        
        // Create user data directory
        UserDirUtils.getUserPath(LOGVIEWER_USER_DIR);
        
        // Init the project tree
        projectTreeRoot = new DefaultMutableTreeNode("ProjectTreeRoot");
        projectTreeModel = new DefaultTreeModel(projectTreeRoot);
        projectTree.setModel(projectTreeModel);
        projectTree.setRootVisible(false);
        projectTree.setShowsRootHandles(true);
        projectTree.addTreeSelectionListener(this);
        projectTree.addMouseListener(new ProjectTreeMouseListener());
        
        // Load all previously imported projects
        ArrayList<String> failedPaths = new ArrayList<>();
        List<String> allProjectPaths = LogViewerSettings.getImportedProjects();
        for (String projectPath : allProjectPaths) {
            try {
                Project project = new Project(new File(projectPath));
                loadProject(project);
            } catch (Exception e) {
                failedPaths.add(projectPath);
            }
        }
        if (failedPaths.size() > 0)
        {
            String projListStr = new String();
            for (String p : failedPaths)
            {
                projListStr += "   " + p + "\n";
                
                // remove failed project so we don't open it next time
                allProjectPaths.remove(p);
            }
            showWarning(String.format(
                    "%d project(s) could not be loaded.\n%s",
                    failedPaths.size(),
                    projListStr));
            
            // update settings with failed projects removed
            LogViewerSettings.setImportedProjects(allProjectPaths);
        }
        
        // Check for save viability every 1000ms
        saveCheckTimer.schedule(new SaveCheckTimerTask(), 1000, 1000);
    }

    public void importProject(Project project) {
        String projectName = project.getProjectName();
        
        // Show a warning if project with same name is already imported
        if (importedProjectsByName.containsKey(projectName)) {
            showWarning(String.format(
                    "Project with name '%s' is already imported!",
                    projectName));
            return;
        }
        
        // Perform all business logic to load the project into LogViewer
        loadProject(project);
        
        // Update settings with new list of imported projects
        ArrayList<String> importedProjectPaths = new ArrayList<>();
        for (Project importedProject : importedProjectsByName.values()) {
            importedProjectPaths.add(importedProject.projectPath.toString());
        }
        LogViewerSettings.setImportedProjects(importedProjectPaths);
        LogViewerSettings.getSettings().save();
    }

    public boolean removeProject(Project project, boolean deleteFromDisk) {
        String projectName = project.getProjectName();
        
        // Show a warning if project with same name is already imported
        if ( ! importedProjectsByName.containsKey(projectName)) {
            showWarning(String.format(
                    "Project '%s' is not imported!",
                    projectName));
            return false;
        }
        
        // Perform all business logic to load the project into LogViewer
//        loadProject(project);

        // remove the project
        importedProjectsByName.remove(projectName);
        
        if (deleteFromDisk) {
            showWarning(String.format(
                    "Project was removed, but not deleted from disk. That's not supported yet"));
        }
        
        // Update settings with new list of imported projects
        ArrayList<String> importedProjectPaths = new ArrayList<>();
        for (Project importedProject : importedProjectsByName.values()) {
            importedProjectPaths.add(importedProject.projectPath.toString());
        }
        LogViewerSettings.setImportedProjects(importedProjectPaths);
        LogViewerSettings.getSettings().save();
        
        return true;
    }
    
    public boolean save() {
        boolean okay = true;
        // Save active Project
        if (activeProject != null) {
            okay = activeProject.save() && okay;
        }
        
        // disable save button
        saveButton.setEnabled(false);
        
        return okay;
    }
    
    public boolean saveAll() {
        boolean okay = true;
        
        // Perform normal save
        okay = save() && okay;
        
        // Save all Projects
        for (Project project : importedProjectsByName.values()) {
            okay = project.save() && okay;
        }
        
        // disable save button
        saveAllButton.setEnabled(false);
        
        return okay;
    }
    
    protected DefaultMutableTreeNode getSelectedProjectTreeNode() {
        DefaultMutableTreeNode node = null;
        Object selectedComp = projectTree.getLastSelectedPathComponent();
        if (selectedComp != null) {
            node = (DefaultMutableTreeNode)selectedComp;
        }
        return node;
    }
    
    private RecordMapMarkers loadMapMarkers(Record record) {
        RecordMapMarkers markers = new RecordMapMarkers(record,mapView.map());
        List<NavSampleOuterClass.NavSample> samples = record.getNavSamples();
        if (samples != null) {
            for (NavSampleOuterClass.NavSample sample : samples) {
                if (sample.hasGps() && sample.getGps().hasPosition()) {
                    NavDefn.Coordinate pos = sample.getGps().getPosition();
                    Coordinate coord = new Coordinate(pos.getLatDd(), pos.getLonDd());
                    markers.route.addPoint(coord);
                }
            }
        }
        
        markers.setVisible(true);
        // TODO add time slider back
//        markers.advToTime(getTimeOffset());
        markers.advToTime(0);
        mapView.setDisplayToFitPolyline(markers.route);
        markersByRecordName.put(record.getName(),markers);
        
        return markers;
    }
    
    private void setActiveMarkers(RecordMapMarkers markers) {
        // Hide the currently active markers
        if (activeMarkers != null) {
            activeMarkers.setVisible(false);
        }
        
        markers.setVisible(true);
        markers.advToTime(0);
        mapView.setDisplayToFitPolyline(markers.route);
        activeMarkers = markers;
    }
    
    private void loadProject(Project project) {
        String projectName = project.getProjectName();
        
        // Add project to imported list
        importedProjectsByName.put(projectName, project);
        
        // Update project tree with imported project
        TreeProject tp = new TreeProject(project);
        DefaultMutableTreeNode projectNode = new DefaultMutableTreeNode(tp);
        projectNode.insert(new DefaultMutableTreeNode("records"), 0);
        
        // Add all the records from Project to its node
        TaskManager.startTask("Loading Project Records", null, new TaskManager.Task() {
            @Override
            public void run() {
                int progress = 1;
                int total = project.getRecords().size();
                for (Record record : project.getRecords()) {
                    String userDefinedName = project.getUserDefinedRecordName(record.getName());
                    updateInformation(String.format("Adding record '%s'.",userDefinedName));
                    addRecordToProjectTreeNode(projectNode, record);
                    updateInformation(String.format("Loading %d nav samples...",record.navSize()));
                    record.getNavSamples();
                    updateProgress(progress++, total);
                }
            }
        }, null);
        
        // Update project view with fully populated Project node
        projectTreeRoot.insert(projectNode,projectTreeRoot.getChildCount());
        projectTreeModel.reload();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        perspectiveButtons = new javax.swing.ButtonGroup();
        jToolBar1 = new javax.swing.JToolBar();
        saveButton = new javax.swing.JButton();
        saveAllButton = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        projectPerspectiveRadioButton = new javax.swing.JRadioButton();
        analyzePerspectiveRadioButton = new javax.swing.JRadioButton();
        topBottomSplit = new javax.swing.JSplitPane();
        perspectivePanel = new javax.swing.JPanel();
        projectPerspective = new javax.swing.JSplitPane();
        projectView = new javax.swing.JScrollPane();
        projectTree = new javax.swing.JTree();
        mapPanel = new javax.swing.JPanel();
        mapView = new com.solodata.ui.maps.BasicMapView();
        mapToolbar = new javax.swing.JToolBar();
        mapViewControlVisibiltyCheckbox = new javax.swing.JCheckBox();
        analyzePerspective = new com.solodata.logviewer.perspectives.AnalyzePerspective();
        taskProgressPanel = new com.solodata.tasks.TaskProgressPanel();
        menubar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        newProjectMenuItem = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        importProjectMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        exitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        darkThemeMenuItem = new javax.swing.JCheckBoxMenuItem();
        restorePositionMenuItem = new javax.swing.JCheckBoxMenuItem();
        projectMenu = new javax.swing.JMenu();
        importRecordsFromFileMenuItem = new javax.swing.JMenuItem();
        importRecordsFromFTP_MenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1200, 600));

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        saveButton.setText("Save");
        saveButton.setEnabled(false);
        saveButton.setFocusable(false);
        saveButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        saveButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(saveButton);

        saveAllButton.setText("Save All");
        saveAllButton.setEnabled(false);
        saveAllButton.setFocusable(false);
        saveAllButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        saveAllButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        saveAllButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAllButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(saveAllButton);
        jToolBar1.add(filler1);

        perspectiveButtons.add(projectPerspectiveRadioButton);
        projectPerspectiveRadioButton.setSelected(true);
        projectPerspectiveRadioButton.setText("Project");
        projectPerspectiveRadioButton.setToolTipText("Change to the \"Project\" perspective.");
        projectPerspectiveRadioButton.setFocusable(false);
        projectPerspectiveRadioButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        projectPerspectiveRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                projectPerspectiveRadioButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(projectPerspectiveRadioButton);

        perspectiveButtons.add(analyzePerspectiveRadioButton);
        analyzePerspectiveRadioButton.setText("Analyze");
        analyzePerspectiveRadioButton.setToolTipText("Change to the \"Analyze\" perspective.");
        analyzePerspectiveRadioButton.setFocusable(false);
        analyzePerspectiveRadioButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        analyzePerspectiveRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                analyzePerspectiveRadioButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(analyzePerspectiveRadioButton);

        getContentPane().add(jToolBar1, java.awt.BorderLayout.NORTH);

        topBottomSplit.setDividerLocation(450);
        topBottomSplit.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        topBottomSplit.setResizeWeight(1.0);

        perspectivePanel.setLayout(new java.awt.CardLayout());

        projectPerspective.setDividerLocation(200);
        projectPerspective.setPreferredSize(new java.awt.Dimension(1200, 600));

        projectView.setMinimumSize(new java.awt.Dimension(200, 20));

        projectTree.setRootVisible(false);
        projectView.setViewportView(projectTree);

        projectPerspective.setLeftComponent(projectView);

        mapPanel.setLayout(new java.awt.BorderLayout());
        mapPanel.add(mapView, java.awt.BorderLayout.CENTER);

        mapToolbar.setFloatable(false);
        mapToolbar.setRollover(true);

        mapViewControlVisibiltyCheckbox.setText("Show Controls");
        mapViewControlVisibiltyCheckbox.setToolTipText("Shows the JMapViews control panel");
        mapViewControlVisibiltyCheckbox.setFocusable(false);
        mapViewControlVisibiltyCheckbox.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        mapViewControlVisibiltyCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mapViewControlVisibiltyCheckboxActionPerformed(evt);
            }
        });
        mapToolbar.add(mapViewControlVisibiltyCheckbox);

        mapPanel.add(mapToolbar, java.awt.BorderLayout.PAGE_START);

        projectPerspective.setRightComponent(mapPanel);

        perspectivePanel.add(projectPerspective, "Project");
        perspectivePanel.add(analyzePerspective, "Analyze");

        topBottomSplit.setTopComponent(perspectivePanel);

        taskProgressPanel.setMinimumSize(new java.awt.Dimension(75, 100));
        topBottomSplit.setBottomComponent(taskProgressPanel);

        getContentPane().add(topBottomSplit, java.awt.BorderLayout.CENTER);

        fileMenu.setText("File");

        newProjectMenuItem.setText("New Project");
        newProjectMenuItem.setToolTipText("Create a new LogViewer project.");
        newProjectMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newProjectMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(newProjectMenuItem);
        fileMenu.add(jSeparator2);

        importProjectMenuItem.setText("Import existing project...");
        importProjectMenuItem.setToolTipText("");
        importProjectMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importProjectMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(importProjectMenuItem);
        fileMenu.add(jSeparator1);

        exitMenuItem.setToolTipText("Exit LogViewer");
        exitMenuItem.setLabel("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menubar.add(fileMenu);

        editMenu.setText("Edit");

        darkThemeMenuItem.setSelected(true);
        darkThemeMenuItem.setText("Dark Theme");
        darkThemeMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                darkThemeMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(darkThemeMenuItem);

        restorePositionMenuItem.setSelected(true);
        restorePositionMenuItem.setText("Restore position on start");
        restorePositionMenuItem.setToolTipText("If set, this will restore the window position on startup.");
        restorePositionMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                restorePositionMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(restorePositionMenuItem);

        menubar.add(editMenu);

        projectMenu.setText("Project");

        importRecordsFromFileMenuItem.setText("Import Records from file...");
        importRecordsFromFileMenuItem.setToolTipText("Opens a dialog to import a DataLogger record from this machine local filesystem.");
        importRecordsFromFileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importRecordsFromFileMenuItemActionPerformed(evt);
            }
        });
        projectMenu.add(importRecordsFromFileMenuItem);

        importRecordsFromFTP_MenuItem.setText("Import Records from FTP Server ...");
        importRecordsFromFTP_MenuItem.setToolTipText("Opens a dialog to import a DataLogger record from a remove filesystem via FTP.");
        importRecordsFromFTP_MenuItem.setEnabled(false);
        importRecordsFromFTP_MenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importRecordsFromFTP_MenuItemActionPerformed(evt);
            }
        });
        projectMenu.add(importRecordsFromFTP_MenuItem);

        menubar.add(projectMenu);

        setJMenuBar(menubar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void newProjectMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newProjectMenuItemActionPerformed
        CreateProjectDialog dialog = new CreateProjectDialog(this, new CreateProjectDialog.Listener() {
            @Override
            public void onCanceled() {
                // do nothing
            }

            @Override
            public void onCreated(Project project) {
                if (project != null) {
                    importProject(project);
                } else {
                    showWarning("Imported project is null!");
                }
            }
        });
        dialog.pack();
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
    }//GEN-LAST:event_newProjectMenuItemActionPerformed

    private void importRecordsFromFileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importRecordsFromFileMenuItemActionPerformed
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setMultiSelectionEnabled(true);
        fc.addChoosableFileFilter(new FileFilter() {
            @Override
            public boolean accept(File file) {
                boolean accept = false;
                if (file.getName().endsWith(".zip")) {
                    accept = true;
                }
                return accept;
            }

            @Override
            public String getDescription() {
                return "";
            }
        });
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File[] files = fc.getSelectedFiles();
            TaskManager.startTask("Record Importing", null, new TaskManager.Task() {
                @Override
                public void run() {
                    int progress = 1;
                    for (File file : files) {
                        logger.log(Level.FINE,"Imporing {0}",file.toString());
                        Record importedRecord = activeProject.importRecordFromZip(file);
                        if (importedRecord != null) {
                            addRecordToProjectTreeNode(activeProjectTreeNode, importedRecord);
                        }
                        updateProgress(progress++,files.length);
                    }
                    // force analyze perspective to pickup new records
                    analyzePerspective.scanForNewRecords();
                }
            }, null);
        }
    }//GEN-LAST:event_importRecordsFromFileMenuItemActionPerformed

    private void darkThemeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_darkThemeMenuItemActionPerformed
        boolean enabled = darkThemeMenuItem.getState();
        LogViewerSettings.setDarkThemeEnabled(enabled);
        showWarning("Restart LogViewer for theme change to take effect.");
    }//GEN-LAST:event_darkThemeMenuItemActionPerformed

    private void restorePositionMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_restorePositionMenuItemActionPerformed
        boolean enabled = restorePositionMenuItem.getState();
        LogViewerSettings.setRestorePositionOnStart(enabled);
    }//GEN-LAST:event_restorePositionMenuItemActionPerformed

    private void mapViewControlVisibiltyCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mapViewControlVisibiltyCheckboxActionPerformed
        mapView.setControlPanelVisible(mapViewControlVisibiltyCheckbox.isSelected());
    }//GEN-LAST:event_mapViewControlVisibiltyCheckboxActionPerformed

    private void projectPerspectiveRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_projectPerspectiveRadioButtonActionPerformed
        CardLayout cl = (CardLayout)perspectivePanel.getLayout();
        cl.show(perspectivePanel, "Project");
    }//GEN-LAST:event_projectPerspectiveRadioButtonActionPerformed

    private void analyzePerspectiveRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_analyzePerspectiveRadioButtonActionPerformed
        CardLayout cl = (CardLayout)perspectivePanel.getLayout();
        cl.show(perspectivePanel, "Analyze");
    }//GEN-LAST:event_analyzePerspectiveRadioButtonActionPerformed

    private void saveAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAllButtonActionPerformed
        saveAll();
    }//GEN-LAST:event_saveAllButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        save();
    }//GEN-LAST:event_saveButtonActionPerformed

    private void importRecordsFromFTP_MenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importRecordsFromFTP_MenuItemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_importRecordsFromFTP_MenuItemActionPerformed

    private void importProjectMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importProjectMenuItemActionPerformed
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setMultiSelectionEnabled(false);
        fc.addChoosableFileFilter(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return Project.isValidProject(file.getAbsoluteFile());
            }

            @Override
            public String getDescription() {
                return "";
            }
        });
        
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            TaskManager.startTask("Project Importing", null, new TaskManager.Task() {
                @Override
                public void run() {
                    Project project = new Project(file.getAbsoluteFile());
                    importProject(project);
                }
            }, null);
        }
    }//GEN-LAST:event_importProjectMenuItemActionPerformed

    private void showWarning(String message) {
        logger.log(Level.WARNING,message);
        JOptionPane.showMessageDialog(
                this,
                (message == null ? "" : message),
                "Log Viewer Warning!",
                JOptionPane.WARNING_MESSAGE);
    }
    
    private void showWarning(String message, Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);

        logger.log(Level.SEVERE,message, e);
        JOptionPane.showMessageDialog(
                this,
                String.format("%s\nCaught %s exception.\n%s",
                    message,
                    e.toString(),
                    sw.toString()),
                "Log Viewer Warning!",
                JOptionPane.WARNING_MESSAGE);
    }
    
    private void setActiveProject(Project project) {
        Project prevActiveProject = activeProject;
        activeProject = project;
        if (prevActiveProject != activeProject) {
            onActiveProjectChanged();
        }
    }
    
    private void onActiveProjectChanged() {
        boolean projectValid = activeProject != null;
        // Only allow opening Project menu when project is valid
        projectMenu.setEnabled(projectValid);
        // Only allow switching to analyze perspective when project is valid
        analyzePerspectiveRadioButton.setEnabled(projectValid);
        
        analyzePerspective.setProject(activeProject);
    }
    
    private void addRecordToProjectTreeNode(
            DefaultMutableTreeNode projectTreeNode,
            Record record) {
        TreeProject treeProject = (TreeProject)projectTreeNode.getUserObject();
        Project project = treeProject.getProject();
        
        // Create and add new record node to project tree
        TreeRecord treeRecord = new TreeRecord(project,record);
        DefaultMutableTreeNode newRecordNode = new DefaultMutableTreeNode(treeRecord);
        DefaultMutableTreeNode projectRecordsNode = (DefaultMutableTreeNode)projectTreeNode.getChildAt(0);
        projectTreeModel.insertNodeInto(newRecordNode, projectRecordsNode, projectRecordsNode.getChildCount());
        
        // Sort records by name
        TreeUtils.sort(projectRecordsNode);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        if (LogViewerSettings.getDarkThemeEnabled()) {
            FlatDarkLaf.install();
        } else {
            FlatLightLaf.install();
        }
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                int x = 0;
                int y = 0;
                int state = Frame.NORMAL;
                
                // Set window position & size to what they were when last ran
                if (LogViewerSettings.getRestorePositionOnStart()) {
                    x = LogViewerSettings.getPreviousWindowPositionX();
                    y = LogViewerSettings.getPreviousWindowPositionY();
                    if (LogViewerSettings.getPreviousWindowMaximized()) {
                        state = Frame.MAXIMIZED_BOTH;
                    }
                    
                    // Make sure it isn't placed outside current screen size
                    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                    if (x > screenSize.getWidth()) {
                        x = 0;
                    }
                    if (y > screenSize.getHeight()) {
                        y = 0;
                    }
                }
                
                // Create and show the LogViewer GUI
                LogViewer lv = new LogViewer();
                lv.setLocation(x, y);
                lv.setExtendedState(state);
                lv.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.solodata.logviewer.perspectives.AnalyzePerspective analyzePerspective;
    private javax.swing.JRadioButton analyzePerspectiveRadioButton;
    private javax.swing.JCheckBoxMenuItem darkThemeMenuItem;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JMenuItem importProjectMenuItem;
    private javax.swing.JMenuItem importRecordsFromFTP_MenuItem;
    private javax.swing.JMenuItem importRecordsFromFileMenuItem;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JPanel mapPanel;
    private javax.swing.JToolBar mapToolbar;
    private com.solodata.ui.maps.BasicMapView mapView;
    private javax.swing.JCheckBox mapViewControlVisibiltyCheckbox;
    private javax.swing.JMenuBar menubar;
    private javax.swing.JMenuItem newProjectMenuItem;
    private javax.swing.ButtonGroup perspectiveButtons;
    private javax.swing.JPanel perspectivePanel;
    private javax.swing.JMenu projectMenu;
    private javax.swing.JSplitPane projectPerspective;
    private javax.swing.JRadioButton projectPerspectiveRadioButton;
    private javax.swing.JTree projectTree;
    private javax.swing.JScrollPane projectView;
    private javax.swing.JCheckBoxMenuItem restorePositionMenuItem;
    private javax.swing.JButton saveAllButton;
    private javax.swing.JButton saveButton;
    private com.solodata.tasks.TaskProgressPanel taskProgressPanel;
    private javax.swing.JSplitPane topBottomSplit;
    // End of variables declaration//GEN-END:variables

    @Override
    public void windowOpened(WindowEvent arg0) {
        darkThemeMenuItem.setState(LogViewerSettings.getDarkThemeEnabled());
        restorePositionMenuItem.setState(LogViewerSettings.getRestorePositionOnStart());
    }

    @Override
    public void windowClosing(WindowEvent arg0) {
        LogViewerSettings.getSettings().save();
        
        for (Project p : importedProjectsByName.values()) {
            if (p.needsSave()) {
                p.save();
            }
        }
    }

    @Override
    public void windowClosed(WindowEvent arg0) {
    }

    @Override
    public void windowIconified(WindowEvent arg0) {
        System.out.println("windowIconified");
    }

    @Override
    public void windowDeiconified(WindowEvent arg0) {
        System.out.println("windowDeiconified");
    }

    @Override
    public void windowActivated(WindowEvent arg0) {
    }

    @Override
    public void windowDeactivated(WindowEvent arg0) {
    }

    @Override
    public void valueChanged(TreeSelectionEvent evt) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                projectTree.getLastSelectedPathComponent();

        // Nothing is elected
        if (node == null) {
            setActiveProject(null);
            return;
        }

        // Find which Project the selected node is a child of
        DefaultMutableTreeNode projectNode = node;
        
        // Traverse up the tree until we find the root
        while (projectNode.getParent() != projectTreeRoot) {
            logger.log(Level.FINER,"Checking {0}",projectNode.toString());
            projectNode = (DefaultMutableTreeNode)projectNode.getParent();
        }
        
        // Get and set the activeProject based on the selected item
        TreeProject treeProject = (TreeProject)projectNode.getUserObject();
        activeProjectTreeNode = projectNode;
        setActiveProject(treeProject.getProject());
        
        // Set to see what kind of tree item we've selected
        Object userObj = node.getUserObject();
        if (userObj instanceof TreeRecord) {
            TreeRecord tr = (TreeRecord)userObj;
            logger.fine("Selected TreeRecord");
        } else if (userObj instanceof TreeProject) {
            TreeProject tp = (TreeProject)userObj;
            logger.fine("Selected TreeProject");
        } else {
            logger.log(Level.FINE,
                    "Unknown ProjectTree selection. {0}",
                    userObj.toString());
        }
    }

    @Override
    public void componentResized(ComponentEvent arg0) {
    }

    @Override
    public void componentMoved(ComponentEvent arg0) {
        LogViewerSettings.setPreviousWindowPositionX(this.getX());
        LogViewerSettings.setPreviousWindowPositionY(this.getY());
    }

    @Override
    public void componentShown(ComponentEvent arg0) {
    }

    @Override
    public void componentHidden(ComponentEvent arg0) {
    }
    
    private class ProjectTreeMouseListener implements MouseListener {
        final private ProjectTreeRenameListener renameListener = new ProjectTreeRenameListener();
        final private ProjectTreeRemoveListener removeListener = new ProjectTreeRemoveListener();
        
        @Override
        public void mouseClicked(MouseEvent e) {
            // Only handle right mouse button clicks right now
            if (SwingUtilities.isLeftMouseButton(e)) {
                // Figure out which row in the Tree was click and select it
                int row = projectTree.getClosestRowForLocation(e.getX(), e.getY());
                projectTree.setSelectionRow(row);
                DefaultMutableTreeNode node = getSelectedProjectTreeNode();
                if (node == null) {
                    return;
                }
                
                Object userObj = node.getUserObject();
                if (userObj instanceof TreeRecord) {
                    TreeRecord tr = (TreeRecord)userObj;
                    String recordName = tr.getRecord().getName();

                    RecordMapMarkers selectedMarkers = null;
                    if (markersByRecordName.containsKey(recordName)) {
                        selectedMarkers = markersByRecordName.get(recordName);
                    } else {
                        selectedMarkers = loadMapMarkers(tr.getRecord());
                    }
                    
                    if (selectedMarkers != activeMarkers) {
                        setActiveMarkers(selectedMarkers);
                    }
                }
            } else if (SwingUtilities.isRightMouseButton(e)) {
                // Figure out which row in the Tree was click and select it
                int row = projectTree.getClosestRowForLocation(e.getX(), e.getY());
                projectTree.setSelectionRow(row);

                // Create a PopupMenu to place where user right clicked
                JMenuItem renameItem = new JMenuItem("Rename");
                renameItem.addActionListener(renameListener);
                JMenuItem removeItem = new JMenuItem("Remove");
                removeItem.addActionListener(removeListener);

                JPopupMenu popupMenu = new JPopupMenu();
                popupMenu.add(renameItem);
                popupMenu.add(removeItem);
                popupMenu.show(e.getComponent(), e.getX(), e.getY());
            }
        }

        @Override
        public void mousePressed(MouseEvent arg0) {
        }

        @Override
        public void mouseReleased(MouseEvent arg0) {
        }

        @Override
        public void mouseEntered(MouseEvent arg0) {
        }

        @Override
        public void mouseExited(MouseEvent arg0) {
        }
    }
    
    private class ProjectTreeRenameListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            DefaultMutableTreeNode node = getSelectedProjectTreeNode();
            if (node == null) {
                logger.log(Level.SEVERE,
                        "Can not perform rename because selected node is null");
                return;
            }
            
            Object userObj = node.getUserObject();
            if (userObj instanceof TreeRecord) {
                TreeRecord tr = (TreeRecord)userObj;
                
                String newName = (String)JOptionPane.showInputDialog(
                        LogViewer.this,
                        "New record name:",
                        "Rename Record",
                        JOptionPane.OK_CANCEL_OPTION,
                        null,// icon
                        null,// selection options
                        (Object)(tr.getProject().getUserDefinedRecordName(tr.getRecord().getName())));
                if (newName != null && newName.length() > 0) {
                    tr.getProject().setUserDefinedRecordName(tr.getRecord().getName(), newName);
                    
                    // Sort records by name
                    DefaultMutableTreeNode projectRecordsNode = (DefaultMutableTreeNode)node.getParent();
                    TreeUtils.sort(projectRecordsNode);
                }
            } else if (userObj instanceof TreeProject) {
                TreeProject tp = (TreeProject)userObj;
                
                String newName = (String)JOptionPane.showInputDialog(
                        LogViewer.this,
                        "New project name:",
                        "Rename Project",
                        JOptionPane.OK_CANCEL_OPTION,
                        null,// icon
                        null,// selection options
                        (Object)(tp.getProject().getProjectName()));
                if (newName != null && newName.length() > 0) {
                    tp.getProject().setProjectName(newName);
                }
            }
        }
    }
    
    private class ProjectTreeRemoveListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            DefaultMutableTreeNode node = getSelectedProjectTreeNode();
            if (node == null) {
                logger.log(Level.SEVERE,
                        "Can not perform remove because selected node is null");
                return;
            }
            
            Object userObj = node.getUserObject();
            if (userObj instanceof TreeRecord) {
                removeRecord(node, (TreeRecord)userObj);
            } else if (userObj instanceof TreeProject) {
                removeProject(node, (TreeProject)userObj);
            } else {
                logger.log(Level.WARNING,
                        "Only support removal of Projects or Records!");
                return;
            }
        }
        
        private void removeProject(DefaultMutableTreeNode node, TreeProject treeProject) {
            // Make a dialog that asks user to confirm removal
            String message = "Are you sure you wanted remove this project?";
            JCheckBox deleteCheckbox = new JCheckBox("Delete project from file system.", false);
            Object[] msgContent = {message,deleteCheckbox};
            int returnVal = JOptionPane.showConfirmDialog(
                    LogViewer.this,
                    msgContent,
                    "Remove project?",
                    JOptionPane.OK_CANCEL_OPTION);
            
            // Handle Record removal from Project if confirmed
            if (returnVal == JOptionPane.OK_OPTION) {
                Project project = treeProject.getProject();
                
                logger.log(Level.FINE,
                        "Removing Project {0}",
                        new Object[]{project.getProjectName()});
                
                boolean deleteFromDisk = deleteCheckbox.isSelected();
                if (LogViewer.this.removeProject(project, deleteFromDisk)) {
                    // Remove record from projectTree is successful
                    projectTreeModel.removeNodeFromParent(node);
                }
            }
        }
        
        private void removeRecord(DefaultMutableTreeNode node, TreeRecord treeRecord) {
            // Make a dialog that asks user to confirm removal
            String message = "Are you sure you wanted remove this record from the project?";
            JCheckBox deleteCheckbox = new JCheckBox("Delete record from file system.", false);
            Object[] msgContent = {message,deleteCheckbox};
            int returnVal = JOptionPane.showConfirmDialog(
                    LogViewer.this,
                    msgContent,
                    "Remove record",
                    JOptionPane.OK_CANCEL_OPTION);
            
            // Handle Record removal from Project if confirmed
            if (returnVal == JOptionPane.OK_OPTION) {
                Record record = treeRecord.getRecord();
                Project project = treeRecord.getProject();
                
                logger.log(Level.FINE,
                        "Removing Record {0} from Project {1}",
                        new Object[]{record.getName(),project.getProjectName()});
                
                boolean delete = deleteCheckbox.isSelected();
                if (project.removeRecordFromProject(record.getName(), delete)) {
                    // Remove record from projectTree is successful
                    projectTreeModel.removeNodeFromParent(node);
                }
            }
        }
    }
    
    private class SaveCheckTimerTask extends TimerTask{
        @Override
        public void run() {
            // If active project needs saving, enable "save" button
            if (activeProject != null && activeProject.needsSave()) {
                saveButton.setEnabled(true);
            }
            
            boolean anyProjectNeedsSave = false;
            for (Project project : importedProjectsByName.values()) {
                // If any project needs saving, enable "save all" button
                if (project.needsSave()) {
                    anyProjectNeedsSave = true;
                    break;
                }
            }
            saveAllButton.setEnabled(anyProjectNeedsSave);
        }
    }
    
    private class LV_WindowStateListener implements WindowStateListener {
        private boolean allBitsSets(int value, int mask) {
            return (value & mask) == mask;
        }
        
        @Override
        public void windowStateChanged(WindowEvent e) {
            boolean prevMax = allBitsSets(e.getOldState(), Frame.MAXIMIZED_BOTH);
            boolean currMax = allBitsSets(e.getNewState(), Frame.MAXIMIZED_BOTH);
            if ( ! prevMax && currMax) {
                logger.log(Level.FINE, "LogViewer maximized");
                LogViewerSettings.setPreviousWindowMaximized(true);
            } else if (prevMax && ! currMax) {
                logger.log(Level.FINE, "LogViewer unmaximized");
                LogViewerSettings.setPreviousWindowMaximized(false);
            }
        }
    }
}
