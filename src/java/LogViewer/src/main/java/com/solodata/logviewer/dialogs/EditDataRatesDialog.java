/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.logviewer.dialogs;

import com.solodata.logviewer.Project;

/**
 *
 * @author daniel
 */
public class EditDataRatesDialog extends javax.swing.JDialog {
    private final Project project;
    private boolean applied = false;
    
    /**
     * Creates new form EditDataRatesDialog
     */
    public EditDataRatesDialog(java.awt.Frame parent, boolean modal, Project project) {
        super(parent, modal);
        initComponents();
        this.project = project;
        gpsRateField.setText(Double.toString(project.getIdealGPS_RateHz()));
        imuRateField.setText(Double.toString(project.getIdealIMU_RateHz()));
        canRateField.setText(Double.toString(project.getIdealCAN_RateHz()));
    }

    public boolean applied() {
        return applied;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        contentPanel = new javax.swing.JPanel();
        gpsLabel = new javax.swing.JLabel();
        gpsRateField = new javax.swing.JTextField();
        imuLabel = new javax.swing.JLabel();
        imuRateField = new javax.swing.JTextField();
        canLabel = new javax.swing.JLabel();
        canRateField = new javax.swing.JTextField();
        buttonPanel = new javax.swing.JPanel();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        cancelButton = new javax.swing.JButton();
        applyButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Sensor Date Rates");
        setMinimumSize(new java.awt.Dimension(270, 120));
        setModal(true);
        setModalExclusionType(java.awt.Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        setPreferredSize(new java.awt.Dimension(270, 120));

        contentPanel.setLayout(new java.awt.GridBagLayout());

        gpsLabel.setText("GPS Rate (Hz):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        contentPanel.add(gpsLabel, gridBagConstraints);

        gpsRateField.setText("jTextField1");
        gpsRateField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gpsRateFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        contentPanel.add(gpsRateField, gridBagConstraints);

        imuLabel.setText("IMU Rate (Hz):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        contentPanel.add(imuLabel, gridBagConstraints);

        imuRateField.setText("jTextField2");
        imuRateField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imuRateFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        contentPanel.add(imuRateField, gridBagConstraints);

        canLabel.setText("CAN Rate (Hz):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        contentPanel.add(canLabel, gridBagConstraints);

        canRateField.setText("jTextField3");
        canRateField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                canRateFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        contentPanel.add(canRateField, gridBagConstraints);

        getContentPane().add(contentPanel, java.awt.BorderLayout.CENTER);

        buttonPanel.setLayout(new javax.swing.BoxLayout(buttonPanel, javax.swing.BoxLayout.LINE_AXIS));
        buttonPanel.add(filler1);

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(cancelButton);

        applyButton.setText("Apply");
        applyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                applyButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(applyButton);

        getContentPane().add(buttonPanel, java.awt.BorderLayout.SOUTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        // Close dialog
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void gpsRateFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gpsRateFieldActionPerformed
        String reason = isValidRateText(gpsRateField.getText());
        gpsRateField.setToolTipText(reason);
        applyButton.setEnabled(reason.length() == 0);
    }//GEN-LAST:event_gpsRateFieldActionPerformed

    private void imuRateFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_imuRateFieldActionPerformed
        String reason = isValidRateText(imuRateField.getText());
        imuRateField.setToolTipText(reason);
        applyButton.setEnabled(reason.length() == 0);
    }//GEN-LAST:event_imuRateFieldActionPerformed

    private void canRateFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_canRateFieldActionPerformed
        String reason = isValidRateText(canRateField.getText());
        canRateField.setToolTipText(reason);
        applyButton.setEnabled(reason.length() == 0);
    }//GEN-LAST:event_canRateFieldActionPerformed

    private void applyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_applyButtonActionPerformed
        // Close dialog
        dispose();
        
        project.setIdealGPS_RateHz(Double.parseDouble(gpsRateField.getText()));
        project.setIdealIMU_RateHz(Double.parseDouble(imuRateField.getText()));
        project.setIdealCAN_RateHz(Double.parseDouble(canRateField.getText()));
        
        applied = true;
    }//GEN-LAST:event_applyButtonActionPerformed

    private String isValidRateText(String rateText) {
        try {
            double rateVal = Double.parseDouble(rateText);
            if (rateVal <= 0) {
                return "Rate must be > 0.0";
            }
        } catch (Exception e) {
            return "Invalid number";
        }
        return "";
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton applyButton;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JLabel canLabel;
    private javax.swing.JTextField canRateField;
    private javax.swing.JButton cancelButton;
    private javax.swing.JPanel contentPanel;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JLabel gpsLabel;
    private javax.swing.JTextField gpsRateField;
    private javax.swing.JLabel imuLabel;
    private javax.swing.JTextField imuRateField;
    // End of variables declaration//GEN-END:variables
}
