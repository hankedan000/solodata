/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.logviewer.misc;

import com.solodata.utils.ComponentUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author daniel
 */
public class ColorCellEditor extends AbstractCellEditor implements TableCellEditor {

    private JButton delegate = new JButton();

    Color savedColor;

    public ColorCellEditor() {
        ActionListener actionListener = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                // center the JColorChooser dialog on the parent frame
                Component parent = ComponentUtils.getParentFrame(delegate);
                if (parent == null) {
                    parent = delegate;
                }
                Color color = JColorChooser.showDialog(parent, "Color Chooser", savedColor);
                ColorCellEditor.this.changeColor(color);
            }
        };
        delegate.addActionListener(actionListener);
    }

    public Object getCellEditorValue() {
        return savedColor;
    }

    private void changeColor(Color color) {
        if (color != null) {
            savedColor = color;
            delegate.setBackground(color);
        }
        stopCellEditing();
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
            int row, int column) {
        changeColor((Color) value);
        return delegate;
    }
}
