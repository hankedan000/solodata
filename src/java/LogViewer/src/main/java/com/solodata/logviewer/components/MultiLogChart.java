/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.logviewer.components;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.hankedan.jlogchart.ChartView;
import com.hankedan.jlogchart.JLogChart;
import com.hankedan.jlogchart.Series;
import com.solodata.datalogger.AnalyzedRecord;
import com.solodata.logviewer.LogViewerSettings;
import com.solodata.logviewer.Project;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author daniel
 */
public class MultiLogChart extends javax.swing.JPanel {
    private final Logger logger = Logger.getLogger(MultiLogChart.class.getName());
    
    private final Map<String,AnalyzedRecord> analyzedRecordsByName = new HashMap();
    private final Map<String,Color> recordColors = new HashMap();
    private Project project = null;
    
    /**
     * Creates new form MultiLogChart
     */
    public MultiLogChart() {
        initComponents();
    }

    public void clear() {
        analyzedRecordsByName.clear();
        recordColors.clear();
        for (LogChart lc : getLogCharts()) {
            lc.clear();
        }
        project = null;
    }
    
    public void setProject(Project p) {
        clear();
        project = p;
    }
    
    public void addAnalyzedRecord(AnalyzedRecord ar, Color c) {
        String recordName = ar.record().getName();
        analyzedRecordsByName.put(recordName, ar);
        recordColors.put(recordName, c);
        for (LogChart lc : getLogCharts()) {
            addRecordToChart(ar, lc);
        }
    }
    
    public void addChart() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = getLogChartCount();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        
        // Create new chart and disable scrolling
        LogChart lc = new LogChart();
        lc.getChart().setScrollbarVisible(false);
        lc.getChart().setMiniMapVisible(false);
        
        // Bind new LogChart scrolling to root LogChart
        ChartView cv = lc.getChart().getChartView();
        cv.bindTo(logChart1.getChart().getChartView());
        
        // Add existing records to new chart
        for (AnalyzedRecord ar : analyzedRecordsByName.values()) {
            addRecordToChart(ar, lc);
        }
        charts.add(lc,gbc);
        
        charts.revalidate();
    }
    
    private void addRecordToChart(AnalyzedRecord ar, LogChart lc) {
        String recordName = ar.record().getName();
        Series s = lc.addAnalyzedRecord(ar);
        s.setVisible(project.getRecordVisible(recordName));
        s.setColor(recordColors.get(recordName));

        // Set series offset from project data
        if (project != null) {
            if (project.hasRecord(recordName)) {
                int offset = project.getRecordDataOffset(recordName);
                s.setOffset(offset);
            } else {
                logger.log(Level.WARNING,
                        "project {0} does not have data offset for record {1}",
                        new Object[]{project.getProjectName(),recordName});
            }
        } else {
            logger.log(Level.WARNING,
                    "unable to set series offsets because project is null");
        }
    }
    
    public void removeChart(int idx) {
        // Always keep at least 1 chart around
        if (getLogChartCount() <= 1) {
            return;
        }
        
        // Remove the designated log chart
        if (idx >= 0 && idx < getLogChartCount()) {
            // remove relative to top
            charts.remove(charts.getComponent(idx));
        } else if (idx < 0 && Math.abs(idx) < getLogChartCount()) {
            // remove relative to bottom
            charts.remove(charts.getComponent(getLogChartCount() + idx));
        }
        
        charts.revalidate();
    }
    
    public void boldRecord(String recordName, boolean unboldOthers) {
        // Bold the selected Record's series across all JLogCharts
        for (LogChart lc : getLogCharts()) {
            JLogChart jlc = lc.getChart();
            for (Series series : jlc.getAllSeries()) {
                if (series.name.compareTo(recordName) == 0) {
                    series.setBolded(true);
                } else if (unboldOthers) {
                    series.setBolded(false);
                }
            }
        }
    }
    
    public void setRecordDataOffset(String recordName, int newOffset) {
        for (LogChart lc : getLogCharts()) {
            Series series = lc.getChart().getSeriesByName(recordName);
            if (series != null) {
                series.setOffset(newOffset);
            }
        }
    }
    
    public void setRecordVisibility(String recordName, boolean visible) {
        for (LogChart lc : getLogCharts()) {
            lc.setRecordVisible(recordName, visible);
        }
    }
    
    public void setRecordColor(String recordName, Color c) {
        for (LogChart lc : getLogCharts()) {
            Series series = lc.getChart().getSeriesByName(recordName);
            if (series != null) {
                series.setColor(c);
            }
        }
    }
    
    public void setTimeMarkerOffset(int absIdx) {
        for (LogChart lc : getLogCharts()) {
            lc.setTimeMarkerOffset(absIdx);
        }
    }
    
    private int getLogChartCount() {
        return charts.getComponentCount();
    }
    
    private List<LogChart> getLogCharts() {
        ArrayList<LogChart> allCharts = new ArrayList<>(getLogChartCount());
        for (Component component : charts.getComponents()) {
            allCharts.add((LogChart)component);
        }
        return allCharts;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        toolbar = new javax.swing.JToolBar();
        removeChartButton = new javax.swing.JButton();
        addChartButton = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        charts = new javax.swing.JPanel();
        logChart1 = new com.solodata.logviewer.components.LogChart();

        setLayout(new java.awt.BorderLayout());

        toolbar.setFloatable(false);
        toolbar.setRollover(true);

        removeChartButton.setText("-");
        removeChartButton.setFocusable(false);
        removeChartButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        removeChartButton.setMaximumSize(new java.awt.Dimension(30, 29));
        removeChartButton.setMinimumSize(new java.awt.Dimension(20, 29));
        removeChartButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        removeChartButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeChartButtonActionPerformed(evt);
            }
        });
        toolbar.add(removeChartButton);

        addChartButton.setText("+");
        addChartButton.setFocusable(false);
        addChartButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        addChartButton.setMaximumSize(new java.awt.Dimension(30, 29));
        addChartButton.setMinimumSize(new java.awt.Dimension(20, 29));
        addChartButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        addChartButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addChartButtonActionPerformed(evt);
            }
        });
        toolbar.add(addChartButton);
        toolbar.add(filler1);

        add(toolbar, java.awt.BorderLayout.NORTH);

        charts.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        charts.add(logChart1, gridBagConstraints);

        add(charts, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void addChartButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addChartButtonActionPerformed
        addChart();
    }//GEN-LAST:event_addChartButtonActionPerformed

    private void removeChartButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeChartButtonActionPerformed
        removeChart(-1);
    }//GEN-LAST:event_removeChartButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addChartButton;
    private javax.swing.JPanel charts;
    private javax.swing.Box.Filler filler1;
    private com.solodata.logviewer.components.LogChart logChart1;
    private javax.swing.JButton removeChartButton;
    private javax.swing.JToolBar toolbar;
    // End of variables declaration//GEN-END:variables
    
    public static void main(String[] args) {
        if (LogViewerSettings.getDarkThemeEnabled()) {
            FlatDarkLaf.install();
        } else {
            FlatLightLaf.install();
        }
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("MultiLogChart Standalone");
                frame.setLayout(new BorderLayout());
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
                // Create and show the JLogChart GUI
                MultiLogChart mlc = new MultiLogChart();
                frame.add(mlc, BorderLayout.CENTER);
                
//                lc.getChart().setSampleRate(100.0);
//                double N_CYCLES = 3;
//                int N_SAMPS = 1000;
//                double radPerSamp = Math.PI * 2 * N_CYCLES / N_SAMPS;
//                List<Double> sinData = new ArrayList();
//                List<Double> cosData = new ArrayList();
//                for (int i=0; i<N_SAMPS; i++) {
//                    sinData.add(Math.sin(i * radPerSamp));
//                    cosData.add(Math.cos(i * radPerSamp));
//                }
//                lc.getChart().addSeries("sin", sinData);
//                lc.getChart().addSeries("cos", cosData);
                
                // Show the GUI
                frame.pack();
                frame.setVisible(true);
            }
        });
    }
}
