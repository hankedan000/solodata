/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.logviewer.components;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.hankedan.jlogchart.LineMarker;
import com.hankedan.jlogchart.Series;
import com.solodata.datalogger.AnalyzedRecord;
import com.solodata.logviewer.LogViewerSettings;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author daniel
 */
public class LogChart extends javax.swing.JPanel {
    private static final Logger logger = Logger.getLogger(LogChart.class.getName());

    private final com.hankedan.jlogchart.JLogChart chart;
    
    private final Map<String,AnalyzedRecord> analyzedRecordsByName = new HashMap<>();
    
    private final Set<String> allAvailSignals = new HashSet<>();
    
    private String prevSelectedSignal = null;
    
    private boolean ignoreSignalChanges = false;
    
    private final LineMarker timeMarker = new LineMarker();
    
    /**
     * Creates new form LogChart2
     */
    public LogChart() {
        initComponents();
        
        timeMarker.setFill(Color.WHITE);
        timeMarker.setOrientation(false);
        
        chart = new com.hankedan.jlogchart.JLogChart();
        chart.setPreferredSize(new Dimension(600, 400));
        chart.setSingleY_Scale(true);
        chart.setMiniMapVisible(true);
        chart.setScrollbarVisible(false);
        chart.addMarker(timeMarker);
        add(chart, BorderLayout.CENTER);
        
        timeMarker.setVisible(timeMarkerVisibiltyCheckbox.isSelected());
    }

    public void clear() {
        for (String seriesName : analyzedRecordsByName.keySet()) {
            chart.removeSeries(seriesName);
        }
        analyzedRecordsByName.clear();
        allAvailSignals.clear();
        prevSelectedSignal = null;
    }
    
    public com.hankedan.jlogchart.JLogChart getChart() {
        return chart;
    }
    
    public Series addAnalyzedRecord(AnalyzedRecord ar) {
        String recordName = ar.record().getName();
        analyzedRecordsByName.put(recordName, ar);
        
        // New record was added, update available signals
        updateAvailableSignals(ar.getAvailableSignals());

        // load the first available signal
        String signal = getSelectedSignal();

        List<Double> signalData = ar.getImmutableSignalData(signal);

        Series series = chart.addSeries(recordName, signalData);
        chart.setSampleRate(ar.getIdealSampleRate());
        prevSelectedSignal = signal;
        
        return series;
    }
    
    public void setRecordVisible(String recordName, boolean visible) {
        chart.setSeriesVisible(recordName, visible);
    }
    
    public void setTimeMarkerVisible(boolean visible) {
        timeMarker.setVisible(visible);
    }
    
    public void setTimeMarkerOffset(int absSampleIdx) {
        timeMarker.setPosition(absSampleIdx,0);
        if (timeMarker.isVisible()) {
            repaint();
        }
    }
    
    public String getSelectedSignal() {
        if (signalComboBox.getSelectedIndex() >= 0) {
            return (String)signalComboBox.getSelectedItem();
        } else {
            return "";
        }
    }
    
    private void updateChartWithSelectedSignal(String nextSignal) {
        for (AnalyzedRecord ar : analyzedRecordsByName.values()) {
            String seriesName = ar.record().getName();
            Series series = chart.getSeriesByName(seriesName);
            if (series == null) {
                logger.log(Level.WARNING,
                        "No series exists for ''{0}''",
                        seriesName);
                continue;
            }
            
            // Get signal data and add it to the chart
            List<Double> yData = new ArrayList();
            if (nextSignal.compareTo("") != 0) {
                try {
                    yData = ar.getImmutableSignalData(nextSignal);
                } catch (UnsupportedOperationException ex) {
                    logger.log(Level.SEVERE, "plotting not supported for signal", ex);
                }
            }

            // Will automatically update and redraw chart
            series.setData(yData);
            series.setSubTitle(nextSignal);
        }
    }
    
    private void updateAvailableSignals(Set<String> signalsToAdd) {
        Set<String> newSignals = new HashSet<>(signalsToAdd);
        newSignals.removeAll(allAvailSignals);
        if (newSignals.isEmpty()) {
            // No new signals to add
            logger.log(Level.FINER, "No new signals to add");
            return;
        }

        logger.log(Level.FINE,
                "Adding {0} signals: {1}",
                new Object[]{newSignals.size(),newSignals});
        
        // Add new signals to available set
        allAvailSignals.addAll(newSignals);
        
        //--------------- BEGIN SIGNAL CHANGE CRITICAL SECTION -----------------
        ignoreSignalChanges = true;
        
        signalComboBox.removeAllItems();
        
        // Sort signals alphabetically
        List<String> sortedSignals = new ArrayList<>(allAvailSignals);
        Collections.sort(sortedSignals);
        
        // Add default "empty" signal and all available signal options
        for (String signal : sortedSignals) {
            signalComboBox.addItem(signal);
        }
        
        ignoreSignalChanges = false;
        //---------------- END SIGNAL CHANGE CRITICAL SECTION ------------------
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        controlPanel = new javax.swing.JPanel();
        signalComboBox = new javax.swing.JComboBox<>();
        decimatingCheckbox = new javax.swing.JCheckBox();
        timeMarkerVisibiltyCheckbox = new javax.swing.JCheckBox();

        setLayout(new java.awt.BorderLayout());

        controlPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        signalComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        signalComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signalComboBoxActionPerformed(evt);
            }
        });
        controlPanel.add(signalComboBox);

        decimatingCheckbox.setSelected(true);
        decimatingCheckbox.setText("Decimate");
        decimatingCheckbox.setToolTipText("If checked, samples will be decimated when the chart's width is less than the total number of visible samples. This can improve draw times when large dataset are being drawn, but can also hide spurious data points.");
        decimatingCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                decimatingCheckboxActionPerformed(evt);
            }
        });
        controlPanel.add(decimatingCheckbox);

        timeMarkerVisibiltyCheckbox.setText("Show Time Marker");
        timeMarkerVisibiltyCheckbox.setToolTipText("Toggle visibilty of the time offset marker line.");
        timeMarkerVisibiltyCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                timeMarkerVisibiltyCheckboxActionPerformed(evt);
            }
        });
        controlPanel.add(timeMarkerVisibiltyCheckbox);

        add(controlPanel, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void decimatingCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_decimatingCheckboxActionPerformed
        boolean enabled = decimatingCheckbox.isSelected();
        chart.setSampleDecimation(enabled);
    }//GEN-LAST:event_decimatingCheckboxActionPerformed

    private void signalComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signalComboBoxActionPerformed
        if (ignoreSignalChanges) {
            return;
        }
        
        String nextSignal = getSelectedSignal();
        logger.log(Level.FINE,"Setting signal to {0}",nextSignal);
        updateChartWithSelectedSignal(nextSignal);
        prevSelectedSignal = nextSignal;
    }//GEN-LAST:event_signalComboBoxActionPerformed

    private void timeMarkerVisibiltyCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_timeMarkerVisibiltyCheckboxActionPerformed
        boolean visible = timeMarkerVisibiltyCheckbox.isSelected();
        timeMarker.setVisible(visible);
        repaint();
    }//GEN-LAST:event_timeMarkerVisibiltyCheckboxActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel controlPanel;
    private javax.swing.JCheckBox decimatingCheckbox;
    private javax.swing.JComboBox<String> signalComboBox;
    private javax.swing.JCheckBox timeMarkerVisibiltyCheckbox;
    // End of variables declaration//GEN-END:variables
    
    public static void main(String[] args) {
        if (LogViewerSettings.getDarkThemeEnabled()) {
            FlatDarkLaf.install();
        } else {
            FlatLightLaf.install();
        }
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("Log Chart");
                frame.setLayout(new BorderLayout());
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
                // Create and show the JLogChart GUI
                LogChart lc = new LogChart();
                frame.add(lc, BorderLayout.CENTER);
                
                lc.getChart().setSampleRate(100.0);
                double N_CYCLES = 3;
                int N_SAMPS = 1000;
                double radPerSamp = Math.PI * 2 * N_CYCLES / N_SAMPS;
                List<Double> sinData = new ArrayList();
                List<Double> cosData = new ArrayList();
                for (int i=0; i<N_SAMPS; i++) {
                    sinData.add(Math.sin(i * radPerSamp));
                    cosData.add(Math.cos(i * radPerSamp));
                }
                lc.getChart().addSeries("sin", sinData);
                lc.getChart().addSeries("cos", cosData);
                
                // Show the GUI
                frame.pack();
                frame.setVisible(true);
            }
        });
    }
}
