/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.logviewer.components;

import com.hankedan.jlogchart.Marker;
import com.solodata.datalogger.DetectionGate;
import java.awt.Color;
import java.awt.Graphics;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

/**
 *
 * @author daniel
 */
public class GateMarker extends Marker {
    private Vector2D b = null;
    
    public GateMarker(Vector2D a) {
        super(a);
    }
    
    public GateMarker(DetectionGate gate) {
        super(gate.a);
        b = gate.b;
    }
    
    public void setA(Vector2D a) {
        this.pos = a;
    }
    
    public void setB(Vector2D b) {
        this.b = b;
    }
    
    public Vector2D getA() {
        return pos;
    }
    
    public Vector2D getB() {
        return b;
    }
    
    public DetectionGate toDetectionGate() {
        return new DetectionGate(getA(), getB());
    }
    
    @Override
    protected void paintMarker(Graphics g, Vector2D viewOrigin, Vector2D pxScale) {
        if (pos == null || b == null) {
            return;
        }
        
        Vector2D aPosPx = pos.subtract(viewOrigin);
        aPosPx = new Vector2D(aPosPx.getX() * pxScale.getX(), aPosPx.getY() * pxScale.getY());
        Vector2D bPosPx = b.subtract(viewOrigin);
        bPosPx = new Vector2D(bPosPx.getX() * pxScale.getX(), bPosPx.getY() * pxScale.getY());
        
        g.setColor(Color.WHITE);
        g.drawLine(
                (int)aPosPx.getX(), (int)aPosPx.getY(),
                (int)bPosPx.getX(), (int)bPosPx.getY());
    }
    
}
