/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.utils;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

/**
 *
 * @author daniel
 */
public class LineSegmentUtils {
    // Given three colinear points p, q, r, the function checks if 
    // point q lies on line segment 'pr' 
    public static boolean onSegment(Vector2D p, Vector2D q, Vector2D r) 
    { 
        if (q.getX() <= Math.max(p.getX(), r.getX()) && q.getX() >= Math.min(p.getX(), r.getX()) && 
            q.getY() <= Math.max(p.getY(), r.getY()) && q.getY() >= Math.min(p.getY(), r.getY())) 
            return true; 

        return false; 
    } 

    // To find orientation of ordered triplet (p, q, r). 
    // The function returns following values 
    // 0 --> p, q and r are colinear 
    // 1 --> Clockwise 
    // 2 --> Counterclockwise 
    public static int orientation(Vector2D p, Vector2D q, Vector2D r) 
    { 
        // See https://www.geeksforgeeks.org/orientation-3-ordered-points/ 
        // for details of below formula. 
        double val = (q.getY() - p.getY()) * (r.getX() - q.getX()) - 
                (q.getX() - p.getX()) * (r.getY() - q.getY()); 

        if (val == 0.0) return 0; // colinear 

        return (val > 0)? 1: 2; // clock or counterclock wise 
    } 

    // The main function that returns true if line segment 'p1q1' 
    // and 'p2q2' intersect. 
    public static boolean doIntersect(Vector2D p1, Vector2D q1, Vector2D p2, Vector2D q2) 
    { 
        // Find the four orientations needed for general and 
        // special cases 
        int o1 = orientation(p1, q1, p2); 
        int o2 = orientation(p1, q1, q2); 
        int o3 = orientation(p2, q2, p1); 
        int o4 = orientation(p2, q2, q1); 

        // General case 
        if (o1 != o2 && o3 != o4) 
            return true; 

        // Special Cases 
        // p1, q1 and p2 are colinear and p2 lies on segment p1q1 
        if (o1 == 0 && onSegment(p1, p2, q1)) return true; 

        // p1, q1 and q2 are colinear and q2 lies on segment p1q1 
        if (o2 == 0 && onSegment(p1, q2, q1)) return true; 

        // p2, q2 and p1 are colinear and p1 lies on segment p2q2 
        if (o3 == 0 && onSegment(p2, p1, q2)) return true; 

        // p2, q2 and q1 are colinear and q1 lies on segment p2q2 
        if (o4 == 0 && onSegment(p2, q1, q2)) return true; 

        return false; // Doesn't fall in any of the above cases 
    }
}
