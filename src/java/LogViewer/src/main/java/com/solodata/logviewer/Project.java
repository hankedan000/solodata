/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.logviewer;

import com.hankedan.jlogchart.util.ColorPalette;
import com.solodata.tasks.TaskManager;
import com.solodata.datalogger.Record;
import com.solodata.datalogger.SensorDataRates;
import com.hankedan.jlogchart.util.ColorUtils;
import com.solodata.datalogger.DetectionGate;
import com.solodata.utils.FileUtils;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author daniel
 */
public class Project {
    private final static Logger logger = Logger.getLogger(Project.class.getName());
    
    public final File projectPath;
    
    /**
     * {
     *  "project_name" : "MyProject",
     *  "ideal_gps_rate_hz" : 10.0,
     *  "ideal_imu_rate_hz" : 200.0,
     *  "ideal_can_rate_hz" : 50.0,
     *  "start_gate" : null | {"a" : {"lat" : 0.0, "lon" : 0.0}, "b" : {"lat" : 0.0, "lon" : 0.0}},
     *  "finish_gate" : null | {"a" : {"lat" : 0.0, "lon" : 0.0}, "b" : {"lat" : 0.0, "lon" : 0.0}},
     *  "records" : {
     *      "2021-05-01_01.41.42" : {
     *          "user_defined_name" : "run1",
     *          "data_offset" : 0,
     *          "removed" : false
     *      }
     *  }
     * }
     */
    private static final String METADATA_FILENAME = "metadata.json";
    private JSONObject metadata = null;
    // Set to true if metadata was modified, but not saved
    private boolean metadataDirty = false;
    // A set of keys that can be stored in the project metadata JSON
    private static final String PROJECT_NAME_KEY = "project_name";
    private static final String PROJECT_RECORDS_KEY = "records";
    private static final String PROJECT_IDEAL_GPS_RATE_KEY = "ideal_gps_rate_hz";
    private static final String PROJECT_IDEAL_IMU_RATE_KEY = "ideal_imu_rate_hz";
    private static final String PROJECT_IDEAL_CAN_RATE_KEY = "ideal_can_rate_hz";
    private static final String PROJECT_START_GATE = "start_gate";
    private static final String PROJECT_FINISH_GATE = "finish_gate";
    
    // A set of keys that can be stored in a metadata Record
    private static final String RECORD_USER_DEFINED_NAME_KEY = "user_defined_name";
    private static final String RECORD_COLOR_KEY = "color";
    private static final String RECORD_DATA_OFFSET_KEY = "data_offset";
    private static final String RECORD_VISIBLE_KEY = "visible";
    private static final String RECORD_REMOVED_KEY = "removed";
    
    // A list of Records that have been loaded for this project
    private final Map<String,Record> recordsByName = new HashMap<>();
    
    public Project(File existingProjectPath) throws RuntimeException {
        if ( ! isValidProject(existingProjectPath)) {
            throw new RuntimeException(String.format(
                    "%s is not a valid LogViewer project!", existingProjectPath.toString()));
        }
        
        projectPath = existingProjectPath;
        
        // Restore metadata from project
        File metadataFile = getProjectMetadataFile(projectPath);
        FileInputStream fis;
        try {
            fis = new FileInputStream(metadataFile);
            JSONTokener tokener = new JSONTokener(fis);
            metadata = new JSONObject(tokener);
            
            // Make sure all project have default empty records
            if ( ! metadata.has(PROJECT_RECORDS_KEY)) {
                metadata.put(PROJECT_RECORDS_KEY, new JSONObject());
            }
            
            // Make sure all projects have default sampling rates
            double DEFAULT_GPS_RATE_HZ = 1.0;
            double DEFAULT_IMU_RATE_HZ = 100.0;
            double DEFAULT_CAN_RATE_HZ = 52.0;
            if ( ! metadata.has(PROJECT_IDEAL_GPS_RATE_KEY)) {
                metadata.put(PROJECT_IDEAL_GPS_RATE_KEY, DEFAULT_GPS_RATE_HZ);
            }
            if ( ! metadata.has(PROJECT_IDEAL_IMU_RATE_KEY)) {
                metadata.put(PROJECT_IDEAL_IMU_RATE_KEY, DEFAULT_IMU_RATE_HZ);
            }
            if ( ! metadata.has(PROJECT_IDEAL_CAN_RATE_KEY)) {
                metadata.put(PROJECT_IDEAL_CAN_RATE_KEY, DEFAULT_CAN_RATE_HZ);
            }
            
            // Make sure all project have default start/finish gates
            if ( ! metadata.has(PROJECT_START_GATE)) {
                metadata.put(PROJECT_START_GATE, JSONObject.NULL);
            }
            if ( ! metadata.has(PROJECT_FINISH_GATE)) {
                metadata.put(PROJECT_FINISH_GATE, JSONObject.NULL);
            }
        } catch (FileNotFoundException ex) {
            // Should never get in here unless isValidProject() is broken
            logger.log(Level.SEVERE, null, ex);
        }
        
        // Load all the non-removed records
        File recordDir = getProjectRecordsDir(projectPath);
        if (recordDir.exists()) {
            for (File recordPath : recordDir.listFiles()) {
                if ( ! isRecordRemoved(recordPath.getName())) {
                    Record record = new Record(recordPath);
                    recordsByName.put(record.getName(),record);
                }
            }
        }
    }
    
    public static Project createNewProject(File newProjectPath, String projectName) throws FileNotFoundException, IOException {
        if (newProjectPath.exists()) {
            throw new RuntimeException(String.format(
                    "Project %s already exists!",newProjectPath.toString()));
        }
        
        // Make project root directory if it doesn't exist already
        newProjectPath.mkdirs();
        
        // Create records directory
        File recordsPath = getProjectRecordsDir(newProjectPath);
        recordsPath.mkdirs();
        
        // Make new project metadata file
        JSONObject newMetadata = new JSONObject();
        newMetadata.put(PROJECT_NAME_KEY,projectName);
        newMetadata.put(PROJECT_RECORDS_KEY, new JSONObject());
        
        // Save metadata to project directory
        saveMetadata(newMetadata, newProjectPath);

        // Construct new project from what we just created, and return it
        return new Project(newProjectPath);
    }
    
    public boolean hasRecord(String recordName) {
        JSONObject metadataRecords = getMetadataRecords();
        return metadataRecords.has(recordName);
    }
    
    public boolean save() {
        boolean okay = true;
        if (saveMetadata(metadata, projectPath)) {
            // Clear dirty flag once save was successful
            metadataDirty = false;
        } else {
            logger.log(Level.SEVERE,
                    "Failed to save metadata for project {0}.",
                    new Object[]{getProjectName()});
            okay = false;
        }
        return okay;
    }
    
    public boolean needsSave() {
        return metadataDirty;
    }
    
    public String getProjectName() {
        return metadata.getString(PROJECT_NAME_KEY);
    }
    
    public boolean setProjectName(String newName) {
        boolean okay = true;
        
        metadata.put(PROJECT_NAME_KEY, newName);
        metadataDirty = true;
        
        return okay;
    }
    
    public SensorDataRates getIdealSensorDataRates() {
        return new SensorDataRates(getIdealGPS_RateHz(), getIdealIMU_RateHz(), getIdealCAN_RateHz());
    }
    
    public void setStartGate(DetectionGate gate) {
        metadata.put(PROJECT_START_GATE, DetectionGate.toJSON(gate));
        metadataDirty = true;
    }
    
    public DetectionGate getStartGate() {
        return getGateFromKey(PROJECT_START_GATE);
    }
    
    public void setFinishGate(DetectionGate gate) {
        metadata.put(PROJECT_FINISH_GATE, DetectionGate.toJSON(gate));
        metadataDirty = true;
    }
    
    public DetectionGate getFinishGate() {
        return getGateFromKey(PROJECT_FINISH_GATE);
    }
    
    private DetectionGate getGateFromKey(String mdKey) {
        if (metadata.isNull(mdKey)) {
            return null;
        } else {
            return DetectionGate.fromJSON(metadata.getJSONObject(mdKey));
        }
    }
    
    public Color getRecordColor(String recordName) {
        Color c = null;
        
        JSONObject metadataRecords = getMetadataRecords();
        if (metadataRecords.has(recordName)) {
            JSONObject metadataRecord = metadataRecords.getJSONObject(recordName);
            try {
                String hexStr = metadataRecord.getString(RECORD_COLOR_KEY);
                c = ColorUtils.fromHexString(hexStr);
            } catch (Exception e) {
                c = Color.BLACK;// default in error
            }
        } else {
            logger.log(Level.WARNING,
                    "getRecordColor() - Record '{0}' does not exist in project {1}",
                    new Object[]{recordName,getProjectName()});
        }
        
        return c;
    }
    
    public boolean setRecordColor(String recordName, Color c) {
        boolean okay = true;
        
        JSONObject metadataRecords = getMetadataRecords();
        if (metadataRecords.has(recordName)) {
            JSONObject metadataRecord = metadataRecords.getJSONObject(recordName);
            metadataRecord.put(RECORD_COLOR_KEY,ColorUtils.toHexString(c));
            metadataDirty = true;
        } else {
            logger.log(Level.WARNING,
                    "setRecordColor() - Record '{0}' does not exist in project {1}",
                    new Object[]{recordName,getProjectName()});
            okay = false;
        }
        
        return okay;
    }
    
    public String getUserDefinedRecordName(String recordName) {
        String userDefinedRecordName = null;
        
        JSONObject metadataRecords = getMetadataRecords();
        if (metadataRecords.has(recordName)) {
            JSONObject metadataRecord = metadataRecords.getJSONObject(recordName);
            userDefinedRecordName = metadataRecord.getString(RECORD_USER_DEFINED_NAME_KEY);
        } else {
            logger.log(Level.WARNING,
                    "Record '{0}' does not exist in project {1}",
                    new Object[]{recordName,getProjectName()});
        }
        
        return userDefinedRecordName;
    }
    
    public boolean setUserDefinedRecordName(String recordName, String userDefinedName) {
        boolean okay = true;
        
        JSONObject metadataRecords = getMetadataRecords();
        if (metadataRecords.has(recordName)) {
            JSONObject metadataRecord = metadataRecords.getJSONObject(recordName);
            metadataRecord.put(RECORD_USER_DEFINED_NAME_KEY,userDefinedName);
            metadataDirty = true;
        } else {
            logger.log(Level.WARNING,
                    "Record '{0}' does not exist in project {1}",
                    new Object[]{recordName,getProjectName()});
            okay = false;
        }
        
        return okay;
    }
    
    public int getRecordDataOffset(String recordName) {
        int offset = 0;
        
        JSONObject metadataRecords = getMetadataRecords();
        if (metadataRecords.has(recordName)) {
            JSONObject metadataRecord = metadataRecords.getJSONObject(recordName);
            if (metadataRecord.has(RECORD_DATA_OFFSET_KEY)) {
                offset = metadataRecord.getInt(RECORD_DATA_OFFSET_KEY);
            }
        } else {
            throw new RuntimeException("Record" + recordName + " does not exist in project " + getProjectName());
        }
        
        return offset;
    }
    
    public boolean setRecordDataOffset(String recordName, int offset) {
        boolean okay = true;
        
        JSONObject metadataRecords = getMetadataRecords();
        if (metadataRecords.has(recordName)) {
            JSONObject metadataRecord = metadataRecords.getJSONObject(recordName);
            metadataRecord.put(RECORD_DATA_OFFSET_KEY,offset);
            metadataDirty = true;
        } else {
            logger.log(Level.WARNING,
                    "Record '{0}' does not exist in project {1}",
                    new Object[]{recordName,getProjectName()});
            okay = false;
        }
        
        return okay;
    }
    
    public boolean getRecordVisible(String recordName) {
        boolean visible = true;
        
        JSONObject metadataRecords = getMetadataRecords();
        if (metadataRecords.has(recordName)) {
            JSONObject metadataRecord = metadataRecords.getJSONObject(recordName);
            if (metadataRecord.has(RECORD_VISIBLE_KEY)) {
                visible = metadataRecord.getBoolean(RECORD_VISIBLE_KEY);
            }
        } else {
            throw new RuntimeException("Record" + recordName + " does not exist in project " + getProjectName());
        }
        
        return visible;
    }
    
    public boolean setRecordVisible(String recordName, boolean visible) {
        boolean okay = true;
        
        JSONObject metadataRecords = getMetadataRecords();
        if (metadataRecords.has(recordName)) {
            JSONObject metadataRecord = metadataRecords.getJSONObject(recordName);
            metadataRecord.put(RECORD_VISIBLE_KEY,visible);
            metadataDirty = true;
        } else {
            logger.log(Level.WARNING,
                    "Record '{0}' does not exist in project {1}",
                    new Object[]{recordName,getProjectName()});
            okay = false;
        }
        
        return okay;
    }
    
    public double getIdealGPS_RateHz() {
        return metadata.getDouble(PROJECT_IDEAL_GPS_RATE_KEY);
    }
    
    public void setIdealGPS_RateHz(double rate) {
        metadata.put(PROJECT_IDEAL_GPS_RATE_KEY, rate);
        metadataDirty = true;
    }
    
    public double getIdealIMU_RateHz() {
        return metadata.getDouble(PROJECT_IDEAL_IMU_RATE_KEY);
    }
    
    public void setIdealIMU_RateHz(double rate) {
        metadata.put(PROJECT_IDEAL_IMU_RATE_KEY, rate);
        metadataDirty = true;
    }
    
    public double getIdealCAN_RateHz() {
        return metadata.getDouble(PROJECT_IDEAL_CAN_RATE_KEY);
    }
    
    public void setIdealCAN_RateHz(double rate) {
        metadata.put(PROJECT_IDEAL_CAN_RATE_KEY, rate);
        metadataDirty = true;
    }
    
    /**
     * @return
     * An array of all the Records in the project
     */
    public List<Record> getRecords() {
        ArrayList<Record> records = new ArrayList<>();
        for (Record record : recordsByName.values()) {
            records.add(record);
        }
        return records;
    }
    
    public Record importRecordFromZip(File zipFile) {
        Record outRecord = null;
        String zipNameNoExt = FilenameUtils.removeExtension(zipFile.getName());
        
        // Make sure input file is good before attempt to import it
        if (zipFile == null) {
            logger.log(Level.SEVERE,
                    "zipFile is null");
            return null;
        } else if ( ! zipFile.exists()) {
            logger.log(Level.WARNING,
                    "zipFile {0} doens't exist",
                    zipFile.toString());
            return null;
        } else if (recordsByName.containsKey(zipNameNoExt)) {
            logger.log(Level.WARNING,
                    "Skipping {0} because it's already imported to project.",
                    zipFile.toString());
            return null;
        }
        
        // Extract zipped Record to project path
        File recordsDir = getProjectRecordsDir(projectPath);
        if ( ! FileUtils.extractAll(zipFile,recordsDir)) {
            logger.log(Level.SEVERE,
                    "Failed to extract zipFile {0} to {1}",
                    new Object[]{zipFile,recordsDir});
            outRecord = null;
        } else {
            File recordPath = new File(recordsDir,zipNameNoExt);
            outRecord = new Record(recordPath);
            recordsByName.put(outRecord.getName(),outRecord);
            
            Color color = ColorPalette.getDefault(
                    recordsByName.size() - 1,
                    ColorPalette.WrapBehavior.FADE);
            
            // Create and add new record metadata
            JSONObject recordMetadata = new JSONObject();
            recordMetadata.put(RECORD_USER_DEFINED_NAME_KEY, outRecord.getName());
            recordMetadata.put(RECORD_COLOR_KEY,ColorUtils.toHexString(color));
            recordMetadata.put(RECORD_DATA_OFFSET_KEY,0);
            recordMetadata.put(RECORD_VISIBLE_KEY,true);
            recordMetadata.put(RECORD_REMOVED_KEY,false);
            getMetadataRecords().put(outRecord.getName(), recordMetadata);
            
            // Save updated project metadata after successful import
            saveMetadata(metadata, projectPath);
        }
        
        return outRecord;
    }
    
    public boolean removeRecordFromProject(String recordName, boolean deleteContents) {
        boolean okay = true;
        
        if ( ! recordsByName.containsKey(recordName)) {
            logger.log(Level.WARNING,
                    "Record {0} doesn't exist in project {1}",
                    new Object[]{recordName,getProjectName()});
            return false;
        }
        
        // Remove records from all internal data structures
        recordsByName.remove(recordName);
        if (deleteContents) {
            // Deleting will completely remove the record from the project
            getMetadataRecords().remove(recordName);
            
            // Perform record deletetion in seperate task thread
            File recordDir = getProjectRecordDir(projectPath, recordName);
            int taskId = TaskManager.startTask("Record removal", null, new TaskManager.Task() {
                @Override
                public void run() {
                    logger.log(Level.FINE,
                            "Removing {0}", new Object[]{recordDir});
                    try {
                        org.apache.commons.io.FileUtils.deleteDirectory(recordDir);
                    } catch (IOException ex) {
                        logger.log(Level.SEVERE,
                                "Caught IO exception while deleting record directory {0}. {1}",
                                new Object[]{recordDir, ex});
                    }
                }
            }, null);
            
            if (taskId < 0) {
                logger.log(Level.SEVERE,
                        "Failed to start task to removed Record {0}",
                        recordName);
                okay = false;
            }
        } else {
            // Leave record unzipped in the project, but just mark as removed
            JSONObject recordMetadata = getMetadataRecords().getJSONObject(recordName);
            recordMetadata.put(RECORD_REMOVED_KEY, true);
        }
        
        // Update project metadata after removal
        saveMetadata(metadata, projectPath);
        
        return okay;
    }
    
    public boolean isRecordRemoved(String recordName) {
        boolean removed = true;
        JSONObject metadataRecords = getMetadataRecords();
        if (metadataRecords.has(recordName)) {
            JSONObject recordMetadata = metadataRecords.getJSONObject(recordName);
            if (recordMetadata.has(RECORD_REMOVED_KEY)) {
                removed = recordMetadata.getBoolean(RECORD_REMOVED_KEY);
            }
        }
        return removed;
    }
    
    private JSONObject getMetadataRecords() {
        return metadata.getJSONObject(PROJECT_RECORDS_KEY);
    }
    
    private static boolean saveMetadata(JSONObject metadata, File projectPath) {
        boolean okay = true;
        
        File metadataFile = getProjectMetadataFile(projectPath);
        try {
            FileOutputStream fos = new FileOutputStream(metadataFile);
            fos.write(metadata.toString(4).getBytes());
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, null, ex);
            okay = false;
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            okay = false;
        }
        
        return okay;
    }
    
    private static final File getProjectMetadataFile(File projectPath) {
        return new File(projectPath, METADATA_FILENAME);
    }
    
    private static final File getProjectRecordsDir(File projectPath) {
        return new File(projectPath, "records");
    }
    
    private static final File getProjectRecordDir(File projectPath, String recordName) {
        return new File(getProjectRecordsDir(projectPath), recordName);
    }
    
    public static final boolean isValidProject(File projectPath) {
        boolean valid = projectPath.exists();
        valid = valid && getProjectMetadataFile(projectPath).exists();
        return valid;
    }
    
}
