/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.datalogger;

import com.solodata.utils.LineSegmentUtils;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.json.JSONObject;

/**
 *
 * @author daniel
 */
public class DetectionGate {
    public final Vector2D a;
    public final Vector2D b;
    
    /**
     * Constructs a gate composed of two points
     * 
     * @param a
     * @param b 
     */
    public DetectionGate(Vector2D a, Vector2D b) {
        this.a = a;
        this.b = b;
    }
    
    /**
     * 
     * @param c1
     * @param c2
     * @return 
     */
    public boolean detect(Vector2D c1, Vector2D c2)
    {
        return LineSegmentUtils.doIntersect(a,b,c1,c2);
    }
    
    public static JSONObject toJSON(DetectionGate gate) {
        JSONObject jo = new JSONObject();
        jo.put("a", DetectionGate.vectToJSON(gate.a));
        jo.put("b", DetectionGate.vectToJSON(gate.b));
        return jo;
    }
    
    public static DetectionGate fromJSON(JSONObject gate) {
        Vector2D a = vectToJSON(gate.getJSONObject("a"));
        Vector2D b = vectToJSON(gate.getJSONObject("b"));
        return new DetectionGate(a, b);
    }
    
    public static JSONObject vectToJSON(Vector2D v) {
        JSONObject jo = new JSONObject();
        jo.put("x", v.getX());
        jo.put("y", v.getY());
        return jo;
    }
    
    public static Vector2D vectToJSON(JSONObject v) {
        double x = v.getDouble("x");
        double y = v.getDouble("y");
        return new Vector2D(x,y);
    }
}
