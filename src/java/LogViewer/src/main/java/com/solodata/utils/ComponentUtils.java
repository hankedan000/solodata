/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.utils;

import java.awt.Component;
import java.awt.Frame;

/**
 *
 * @author daniel
 */
public class ComponentUtils {

    /**
     * @param component
     * @return the component's parent Frame. null if there isn't one found.
     */
    public static Frame getParentFrame(Component component) {
        Component currParent = component;
        Frame theFrame = null;
        while (currParent != null) {
            if (currParent instanceof Frame) {
                theFrame = (Frame) currParent;
                break;
            }
            currParent = currParent.getParent();
        }
        return theFrame;
    }
}
