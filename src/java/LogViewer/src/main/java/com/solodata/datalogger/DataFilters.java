/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.datalogger;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniel
 */
public class DataFilters {
    private static Logger logger = Logger.getLogger(DataFilters.class.getName());
    
    public static boolean smooth(List<Double> data, int w, ArrayList<Double> outData) {
        boolean okay = true;
        
        if (w < 1) {
            logger.log(Level.SEVERE,"w must be >= 1");
            return false;
        }
        
        outData.clear();
        outData.ensureCapacity(data.size());
        
        double sum = 0;
        double avgSamps[] = new double[w];
        for (int i=0; i<w; i++) {
            avgSamps[i] = 0;
        }
        
        for (int i=0; i<data.size(); i++) {
            sum -= avgSamps[0];// remove oldest averaged sample from sum
            sum += data.get(i);// add newest averaged sample to sum
            outData.add(i, sum / w);
            
            // Slide average samples down 1 and add the newest sample to window
            for (int j=1; j<w; j++) {
                avgSamps[j-1] = avgSamps[j];
            }
            avgSamps[w-1] = data.get(i);
        }
        
        return okay;
    }
}
