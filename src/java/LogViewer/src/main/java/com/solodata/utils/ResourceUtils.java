/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.utils;

import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author daniel
 */
public class ResourceUtils {
    private static final ResourceUtils inst = new ResourceUtils();
    private static ClassLoader cl;
    
    private ResourceUtils() {
        cl = this.getClass().getClassLoader();
    }
    
    public static ImageIcon loadImageIcon(String resPath) {
        URL url = inst.cl.getResource(resPath);
        return new ImageIcon(url);
    }
}
