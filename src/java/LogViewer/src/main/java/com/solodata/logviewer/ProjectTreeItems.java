/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.logviewer;

import com.solodata.datalogger.Record;

/**
 *
 * @author daniel
 */
public class ProjectTreeItems {
    public static class TreeProject {
        private final Project project;
        
        public TreeProject(Project project) {
            this.project = project;
        }

        public Project getProject() {
            return project;
        }
        
        @Override
        public String toString() {
            return project.getProjectName();
        }
    }
    
    public static class TreeRecord {
        private final Record record;
        private final Project project;
        
        public TreeRecord(Project parentProject, Record record) {
            this.record = record;
            this.project = parentProject;
        }
        
        public Record getRecord() {
            return record;
        }

        public Project getProject() {
            return project;
        }

        @Override
        public String toString() {
            return project.getUserDefinedRecordName(record.getName());
        }
    }
}
