/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.datalogger;

import com.solodata.tasks.TaskProgressNotifier;
import com.solodata.utils.VectorUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import solodata.CANSample;
import solodata.CANSample.CAN_Sample;
import solodata.IMUSample.IMU_Sample;
import solodata.NavSampleOuterClass.GPS_Info;
import solodata.NavSampleOuterClass.NavSample;
import solodata.cand.CandIndex;

public class AnalyzedRecord {
    private static final Logger logger = Logger.getLogger(AnalyzedRecord.class.getName());
    
    private final Record record;
    
    // Set to true once anaylze() was called
    private boolean analyzed = false;
    
    private enum ReferenceChannel {
        None, GPS, IMU, CAN
    };
    private ReferenceChannel refChan = ReferenceChannel.None;
    
    private class TimestampInfo {
        public double start_us;
        public double end_us;
        
        public TimestampInfo(double start_us, double end_us) {
            this.start_us = start_us;
            this.end_us = end_us;
        }
        
        public double duration_us() {
            return end_us - start_us;
        }
        
        public String toString() {
            return String.format(
                    "{start: %.0fus; end: %.0fus; dur: %.0fus}",
                    start_us, end_us, duration_us());
        }
    };
    
    // Time delta of first sample when compared to the 'refChan' first sample
    private TimestampInfo gpsTimeInfo = new TimestampInfo(0, 0);
    private TimestampInfo imuTimeInfo = new TimestampInfo(0, 0);
    private TimestampInfo canTimeInfo = new TimestampInfo(0, 0);
    
    // Time delta of first sample when compared to the 'refChan' first sample
    private double startSkewGPS_us = 0.0;
    private double startSkewIMU_us = 0.0;
    private double startSkewCAN_us = 0.0;
    
    private final Map<String,ArrayList<Double>> calculatedSignals = new HashMap();
    
    public static final int IMU_SET    = (1 << 0);
    public static final int IMU_INTERP = (1 << 1);
    public static final int GPS_SET    = (1 << 2);
    public static final int GPS_INTERP = (1 << 3);
    public static final int CAN_SET    = (1 << 4);
    public static final int CAN_INTERP = (1 << 5);
    private class SampleInfo {
        long utc_us;
        int flags;
        
        public SampleInfo(long utc_us, int flags) {
            this.utc_us = utc_us;
            this.flags = flags;
        }
    }
    private final ArrayList<SampleInfo> sampInfo = new ArrayList();
    private final Map<String,ArrayList<Double>> signalData = new HashMap<>();
    
    /**
     * Set to true if any of recorded data sets have significant time skew
     * relative to each other. This flag is set during the re-sampling of the
     * data sets. The flag is asserted true if any two data set start times are
     * larger than the SKEW_THRESHOLD.
     */
    private final double SKEW_THRESHOLD = 1.0;
    private boolean dataSkewDetected = false;
    
    /**
     * Prior to 5/10/2021, all Records had Nav samples where the GPS_Info.utc_us
     * field was actually stored in whole seconds rather than microseconds like
     * the field name suggests. This flag is set to true if the AnalyzedRecord
     * is older than the above.
     */
    private final boolean gpsInfoUtcIsInSeconds;
    
    private CandIndex.Index canIndex = null;
    
    public AnalyzedRecord(Record record) {
        this.record = record;
        
        // When the NavServer UTC bug was fixed
        Date utcFixedData = new GregorianCalendar(2021, 5, 10).getTime();
        gpsInfoUtcIsInSeconds = record.getDate().compareTo(utcFixedData) == -1;
    }
    
    public AnalyzedRecord analyze(SensorDataRates idealDataRates) {
        return analyze(idealDataRates, null);
    }
    
    private final int ANALYZE_PROGRESS_STEPS = 6;
    public AnalyzedRecord analyze(SensorDataRates idealDataRates, TaskProgressNotifier progNotifier) {
        if ( ! analyzed) {
            if (record.canIndexReader_.size() == 1) {
                canIndex = record.canIndexReader_.get_item(0);
            }
            
            // Resample data so all data is alignable by index
            resampleData(progNotifier);
            
            // Smooth accelerometer signals
            if (progNotifier != null) {
                progNotifier.updateProgress(5,ANALYZE_PROGRESS_STEPS);
                progNotifier.updateInformation("Smoothing accelerometer data");
            }
            computeSmoothedAccSignals();
            
            // Marked record as fully analyzed
            if (progNotifier != null) {
                progNotifier.updateProgress(ANALYZE_PROGRESS_STEPS,ANALYZE_PROGRESS_STEPS);
                progNotifier.updateInformation("Done");
            }
            analyzed = true;
        }
        return this;
    }
    
    public Record record() {
        return record;
    }
    
    public boolean isAnalyzed() {
        return analyzed;
    }
    
    public Set<String> getAvailableSignals() {
        Set<String> avail = new HashSet<>();
        avail.addAll(signalData.keySet());
        avail.addAll(calculatedSignals.keySet());
        return avail;
    }
    
    public int getIdealSampleCount() {
        return sampInfo.size();
    }
    
    public double getIdealSampleRate() {
        if (sampInfo.isEmpty()) {
            return 0.0;
        } else {
            double dt_us = Math.abs(sampInfo.get(sampInfo.size()-1).utc_us - sampInfo.get(0).utc_us);
            if (dt_us > 0) {
                return (sampInfo.size() - 1) / (dt_us / 1.0e6);
            } else {
                return 0.0;
            }
        }
    }
    
    public boolean hasSignal(String signalName) {
        return signalData.containsKey(signalName);
    }
    
    public String toCSV_String() {
        if ( ! isAnalyzed()) {
            return "";
        }
        
        List<String> columnTitles = new ArrayList();
        HashMap<String,List<Double>> allSignalData = new HashMap<>();
        StringBuilder csvBuilder = new StringBuilder();
        
        for (String signalName : getAvailableSignals()) {
            try
            {
                List<Double> data = getImmutableSignalData(signalName);
                allSignalData.put(signalName, data);
                columnTitles.add(signalName);
            }
            catch (UnsupportedOperationException e)
            {
                // not all signals are supported yet, so just dump the ones that are
                continue;
            }
        }
        
        // cort columns title alphabetically by signalName
        Collections.sort(columnTitles);
        for (String signalName : columnTitles) {
            csvBuilder.append(signalName).append(",");
        }
        csvBuilder.append("\n");
        
        for (int i=0; i<getIdealSampleCount(); i++) {
            for (String signalName : columnTitles) {
                try
                {
                    csvBuilder.append(allSignalData.get(signalName).get(i));
                }
                catch (IndexOutOfBoundsException e)
                {
                    // FIXME we should not be getting in here!!!
                }
                csvBuilder.append(",");
            }
            csvBuilder.append("\n");
        }
        
        return csvBuilder.toString();
    }
    
    public void toCSV_File(File csvFile) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(csvFile));
        writer.write(toCSV_String());
        writer.close();
    }
    
    public List<Double> getImmutableSignalData(String signalName) {
        List<Double> outData = null;
        
        if (signalData.containsKey(signalName)) {
            outData = Collections.unmodifiableList(signalData.get(signalName));
        } else if (signalName.compareTo("") == 0) {
            // return nothing for the "null" signal
            outData = Collections.unmodifiableList(new ArrayList<Double>());
        } else if (calculatedSignals.containsKey(signalName)) {
            outData = Collections.unmodifiableList(calculatedSignals.get(signalName));
        } else {
            throw new UnsupportedOperationException(
                    "Unsupported signalName " + signalName);
        }
        
        return outData;
    }
    
    public boolean isDataSkewed() {
        return dataSkewDetected;
    }
    
    public static class CrossDetectionInfo {
        public boolean crossDetected;
        public int sampleIdx;
        public long utc_us;
    }
    
    public CrossDetectionInfo findFirstCrossing(DetectionGate gate, int startIdx) {
        CrossDetectionInfo info = new CrossDetectionInfo();
        info.crossDetected = false;
        info.sampleIdx = startIdx;
        info.utc_us = 0;
        
        if ( ! signalData.containsKey("gps_lat") || ! signalData.containsKey("gps_lon")) {
            throw new RuntimeException("record does not contain GPS data");
        }
        
        ArrayList<Double> lonData = signalData.get("gps_lon");
        ArrayList<Double> latData = signalData.get("gps_lat");
        if (lonData.size() != latData.size()) {
            throw new RuntimeException("lat lon data must be equal size");
        } else if (lonData.size() < 2) {
            // not enough coordinate to detect a crossing
            return info;
        } else if (info.sampleIdx >= lonData.size()) {
            throw new IndexOutOfBoundsException("startIdx is out of bounds");
        }
        
        Vector2D prev = new Vector2D(lonData.get(info.sampleIdx),latData.get(info.sampleIdx));
        Vector2D curr = null;
        info.sampleIdx++;
        while (info.sampleIdx < lonData.size()) {
            curr = new Vector2D(lonData.get(info.sampleIdx),latData.get(info.sampleIdx));
            info.crossDetected = gate.detect(prev, curr);
            if (info.crossDetected) {
                info.utc_us = sampInfo.get(info.sampleIdx).utc_us;
                break;
            }
            
            prev = curr;
            info.sampleIdx++;
        }
        
        return info;
    }
    
    /**
     * Fixes GPS UTC timestamps that were before NavServer bug fix.
     * @param gpsUtcUs
     * @return
     * timestamp in microseconds
     */
    private double getCorrectedGPS_UtcUs(double gpsUtcUs) {
        if (gpsInfoUtcIsInSeconds) {
            return gpsUtcUs * 1.0e6;
        } else {
            return gpsUtcUs;
        }
    }
    
    private boolean resampleData(TaskProgressNotifier progNotifier) {
        boolean okay = true;
        
        // Approximate size of final resampled lists
        int mostRecordSamps = (int)Long.max(record.navSize(),record.canSize());
        
        if (progNotifier != null) {
            progNotifier.updateProgress(1,ANALYZE_PROGRESS_STEPS);
            progNotifier.updateInformation("reading records");
        }
        ArrayList<GPS_Info> gpsMeasSamps = new ArrayList<>(mostRecordSamps);
        ArrayList<IMU_Sample> imuMeasSamps = new ArrayList<>(mostRecordSamps);
        ArrayList<CAN_Sample> canMeasSamps = new ArrayList<>(mostRecordSamps);
        
        // Get all the measured nav samples (GPS/IMU)
        boolean useCPU_TimeForNav = false;
        for (int i=0; i<record.navSize(); i++) {
            NavSample ns = record.navReader_.get_item(i);
            if (ns.hasGps()) {
                boolean rebuild = false;
                double timeToUse = ns.getGps().getUtcUs();
                // fix GPS times for old NavServer bug (should even stop doing this...)
                if (gpsInfoUtcIsInSeconds) {
                    rebuild = true;
                    timeToUse = getCorrectedGPS_UtcUs(timeToUse);
                }
                // sometimes the processor time doesn't get set to GPS time.
                // hackily apply processor time to GPS sample so that CAN/IMU
                // data can be correlated.
                if (useCPU_TimeForNav || Math.abs(ns.getUtcUs() - timeToUse) > 1000000) {
                    if ( ! useCPU_TimeForNav) {
                        logger.log(Level.WARNING,
                                "GPS data is significantly time skewed. Replacing GPS times with processor times." +
                                " Is processor time synchronized to the GPS?");
                        useCPU_TimeForNav = true;
                    }
                    rebuild = true;
                    timeToUse = ns.getUtcUs();
                }
                
                if (rebuild) {
                    GPS_Info fixedSamp = GPS_Info.newBuilder()
                            .mergeFrom(ns.getGps())
                            .setUtcUs(timeToUse)
                            .build();
                    gpsMeasSamps.add(fixedSamp);
                } else {
                    gpsMeasSamps.add(ns.getGps());
                }
            }
            for (IMU_Sample imuSamp : ns.getImuSampList()) {
                imuMeasSamps.add(imuSamp);
            }
        }
        
        // Get all the measured CAN samples
        for (int i=0; i<record.canSize(); i++) {
            CAN_Sample cs = record.canReader_.get_item(i);
            canMeasSamps.add(cs);
        }
        
        // populate dataset start/end times
        if (gpsMeasSamps.size() > 2) {
            gpsTimeInfo.start_us = gpsMeasSamps.get(0).getUtcUs();
            gpsTimeInfo.end_us = gpsMeasSamps.get(gpsMeasSamps.size() - 1).getUtcUs();
        }
        if (imuMeasSamps.size() > 2) {
            imuTimeInfo.start_us = imuMeasSamps.get(0).getTimestamp();
            imuTimeInfo.end_us = imuMeasSamps.get(imuMeasSamps.size() - 1).getTimestamp();
        }
        if (canMeasSamps.size() > 2) {
            canTimeInfo.start_us = canMeasSamps.get(0).getTimestamp();
            canTimeInfo.end_us = canMeasSamps.get(canMeasSamps.size() - 1).getTimestamp();
        }
        logger.log(Level.FINE,
                "\ngpsTimeInfo = {0}\nimuTimeInfo = {1}\ncanTimeInfo = {2}",
                new Object[]{gpsTimeInfo,imuTimeInfo,canTimeInfo});
        
        // determine the reference cahnnel based on highest samples rate.
        // pick the one with most samples (highest sample rate?) and upsample
        // the rest to meet it.
        int resampLen = 0;
        TimestampInfo refTimeInfo = null;
        if (imuMeasSamps.size() > resampLen) {
            resampLen = imuMeasSamps.size();
            refChan = ReferenceChannel.IMU;
            refTimeInfo = imuTimeInfo;
        }
        if (gpsMeasSamps.size() > resampLen) {
            resampLen = gpsMeasSamps.size();
            refChan = ReferenceChannel.GPS;
            refTimeInfo = gpsTimeInfo;
        }
        if (canMeasSamps.size() > resampLen) {
            resampLen = canMeasSamps.size();
            refChan = ReferenceChannel.CAN;
            refTimeInfo = canTimeInfo;
        }
        startSkewGPS_us = gpsTimeInfo.start_us - refTimeInfo.start_us;
        startSkewIMU_us = imuTimeInfo.start_us - refTimeInfo.start_us;
        startSkewCAN_us = canTimeInfo.start_us - refTimeInfo.start_us;
        
        logger.log(Level.FINE,
                "gpsMeasSamps.size() = {0}; imuMeasSamps.size() = {1}; canMeasSamps.size() = {2}",
                new Object[]{gpsMeasSamps.size(),imuMeasSamps.size(),canMeasSamps.size()});
        
        logger.log(Level.FINE,
                "startSkewGPS_us = {0}; startSkewIMU_us = {1}; startSkewCAN_us = {2}",
                new Object[]{startSkewGPS_us,startSkewIMU_us,startSkewCAN_us});
        
        // -------------------------------------------------------------
        
        // init the samp_info with times from reference channel and default all flags
        sampInfo.ensureCapacity(resampLen);
        for (int i=0; i<resampLen; i++) {
            switch(refChan)
            {
                case IMU:
                    sampInfo.add(new SampleInfo(imuMeasSamps.get(i).getTimestamp(), 0));
                    break;
                case GPS:
                    sampInfo.add(new SampleInfo((long)gpsMeasSamps.get(i).getUtcUs(), 0));
                    break;
                case CAN:
                    sampInfo.add(new SampleInfo((long)canMeasSamps.get(i).getTimestamp(), 0));
                    break;
            }
        }
        
        if (progNotifier != null) {
            progNotifier.updateProgress(2,ANALYZE_PROGRESS_STEPS);
            progNotifier.updateInformation("resampling IMU data");
        }
        resampleIMU_Data(sampInfo, imuMeasSamps, refChan == ReferenceChannel.IMU, signalData);
        if (progNotifier != null) {
            progNotifier.updateProgress(3,ANALYZE_PROGRESS_STEPS);
            progNotifier.updateInformation("resampling GPS data");
        }
        resampleGPS_Data(sampInfo, gpsMeasSamps, refChan == ReferenceChannel.GPS, signalData);
        if (progNotifier != null) {
            progNotifier.updateProgress(4,ANALYZE_PROGRESS_STEPS);
            progNotifier.updateInformation("resampling CAN data");
        }
        resampleCAN_Data(sampInfo, canMeasSamps, canIndex, refChan == ReferenceChannel.CAN, signalData);
        
        return okay;
    }
    
    private static void resampleIMU_Data(ArrayList<SampleInfo> si, ArrayList<IMU_Sample> imuMeasData, boolean isRef, Map<String, ArrayList<Double>> outData) {
        if (imuMeasData.isEmpty()) {
            // nothing to resample
            return;
        }
        
        if (isRef) {
            ArrayList<Double> tstmp = new ArrayList<>(si.size());
            ArrayList<Double> accX = new ArrayList<>(si.size());
            ArrayList<Double> accY = new ArrayList<>(si.size());
            ArrayList<Double> accZ = new ArrayList<>(si.size());
            ArrayList<Double> gyroX = new ArrayList<>(si.size());
            ArrayList<Double> gyroY = new ArrayList<>(si.size());
            ArrayList<Double> gyroZ = new ArrayList<>(si.size());
            ArrayList<Double> magX = new ArrayList<>(si.size());
            ArrayList<Double> magY = new ArrayList<>(si.size());
            ArrayList<Double> magZ = new ArrayList<>(si.size());
            ArrayList<Double> quatX = new ArrayList<>(si.size());
            ArrayList<Double> quatY = new ArrayList<>(si.size());
            ArrayList<Double> quatZ = new ArrayList<>(si.size());
            ArrayList<Double> quatW = new ArrayList<>(si.size());
            
            if (imuMeasData.size() != si.size()) {
                throw new RuntimeException(
                        String.format("IMU is configured as reference and yet si.size() (%d) != imuMeasData.size() (%d)",
                                si.size(), imuMeasData.size()));
            }
            
            for (int ii=0; ii<si.size(); ii++) {
                IMU_Sample imu = imuMeasData.get(ii);
                tstmp.add((double)imu.getTimestamp());
                accX.add((double)imu.getAcc().getX());
                accY.add((double)imu.getAcc().getY());
                accZ.add((double)imu.getAcc().getZ());
                gyroX.add((double)imu.getGyro().getX());
                gyroY.add((double)imu.getGyro().getY());
                gyroZ.add((double)imu.getGyro().getZ());
                magX.add((double)imu.getMag().getX());
                magY.add((double)imu.getMag().getY());
                magZ.add((double)imu.getMag().getZ());
                quatX.add((double)imu.getQuat().getX());
                quatY.add((double)imu.getQuat().getY());
                quatZ.add((double)imu.getQuat().getZ());
                quatW.add((double)imu.getQuat().getW());
                
                si.get(ii).flags |= IMU_SET;
            }
            
            outData.put("imu_timestamp", tstmp);
            outData.put("imu_acc_x", accX);
            outData.put("imu_acc_y", accY);
            outData.put("imu_acc_z", accZ);
            outData.put("imu_gyro_x", gyroX);
            outData.put("imu_gyro_y", gyroY);
            outData.put("imu_gyro_z", gyroZ);
            outData.put("imu_mag_x", magX);
            outData.put("imu_mag_y", magY);
            outData.put("imu_mag_z", magZ);
            outData.put("imu_quat_x", quatX);
            outData.put("imu_quat_y", quatY);
            outData.put("imu_quat_z", quatZ);
            outData.put("imu_quat_w", quatW);
        } else {
            throw new UnsupportedOperationException(
                    "Only support IMU data as reference");
        }
    }
    
    private static void resampleGPS_Data(ArrayList<SampleInfo> si, ArrayList<GPS_Info> gpsMeasData, boolean isRef, Map<String, ArrayList<Double>> outData) {
        if (gpsMeasData.isEmpty()) {
            // nothing to resample
            return;
        }
        
        if (isRef) {
            throw new UnsupportedOperationException(
                    "GPS data as reference is not supported");
        } else {
            ArrayList<Double> tstmp = new ArrayList<>(si.size());
            ArrayList<Double> lat = new ArrayList<>(si.size());
            ArrayList<Double> lon = new ArrayList<>(si.size());
            ArrayList<Double> altitude = new ArrayList<>(si.size());
            ArrayList<Double> speed_mps = new ArrayList<>(si.size());
            ArrayList<Double> true_course = new ArrayList<>(si.size());
            
            // find initial point where GPS data aligns with ref channel.
            // fill sample till that point with exact GPS value. unfortunately
            // filling is the best we can do here since we can't interpolate
            // with a previously known values.
            int startGPS_Idx = 0;
            int startRefIdx = 0;
            GPS_Info gpsInfo = gpsMeasData.get(startGPS_Idx);
            long gpsUTC_us = (long)gpsInfo.getUtcUs();
            long refUTC_us = si.get(startRefIdx).utc_us;
            Level lvl = Level.FINER;
            logger.log(lvl,"gpsUTC_us = {0}; refUTC_us = {1}",new Object[]{gpsUTC_us,refUTC_us});
            if (gpsUTC_us < refUTC_us) {
                // GPS data lags the IMU data
                while (startGPS_Idx < gpsMeasData.size()) {
                    gpsInfo = gpsMeasData.get(startGPS_Idx);
                    gpsUTC_us = (long)gpsInfo.getUtcUs();
                    logger.log(lvl,"gpsMeasData[{0}].getUtcUs = {1}",new Object[]{startGPS_Idx,gpsUTC_us});
                    // find point where GPS sample first falls within time range of ref chan
                    if (gpsUTC_us >= refUTC_us) {
                        logger.log(lvl,"FOUND GPS SYNC POINT!");
                        break;
                    }
                    // keep waiting until GPS samples start to fall within
                    // ref chan's time range
                    startGPS_Idx++;
                }
            }
            // find point where GPS sample first falls within time range of ref chan
            if (gpsUTC_us >= refUTC_us) {
                // fill resampled data with duplicate GPS info until we've
                // reached the time when GPS and ref channel are aligned
                while (startRefIdx < si.size()) {
                    refUTC_us = si.get(startRefIdx).utc_us;
                    logger.log(lvl,"si[{0}].utc_us = {1}",new Object[]{startRefIdx,refUTC_us});
                    if (gpsUTC_us < si.get(startRefIdx).utc_us) {
                        logger.log(lvl,"FOUND REF SYNC POINT!");
                        break;
                    }
                    // TODO something tells me we could attempt to interpolate
                    // here if there was a previous GPS_Info sample to work with
                    tstmp.add(gpsInfo.getUtcUs());
                    lat.add(gpsInfo.getPosition().getLatDd());
                    lon.add(gpsInfo.getPosition().getLonDd());
                    speed_mps.add(gpsInfo.getSpeed());
                    true_course.add(gpsInfo.getTrueCourse());
                    altitude.add(gpsInfo.getAltitude());
                    si.get(startRefIdx).flags |= GPS_SET | GPS_INTERP;
                    startRefIdx++;
                }
            }
            
            logger.log(lvl,"startGPS_Idx = {0}; startRefIdx = {1}; ",new Object[]{startGPS_Idx, startRefIdx});
            
            final boolean LERP = true;
            int endRefIdx = startRefIdx;
            startRefIdx--;// keep on valid sample
            startGPS_Idx++;// get next GPS sample to place
            if (startGPS_Idx < gpsMeasData.size()) {
                gpsInfo = gpsMeasData.get(startGPS_Idx);
                gpsUTC_us = (long)gpsInfo.getUtcUs();
            }
            while (endRefIdx < si.size() && gpsInfo != null) {
                long refTime = si.get(endRefIdx).utc_us;
                logger.log(lvl,"gpsTime = {0}; si[{1}].utc_us = {2}; ",new Object[]{gpsUTC_us,endRefIdx,refTime});
                if (gpsUTC_us < refTime) {
                    // found our next insertion point for the GPS data
                    tstmp.add(gpsInfo.getUtcUs());
                    lat.add(gpsInfo.getPosition().getLatDd());
                    lon.add(gpsInfo.getPosition().getLonDd());
                    altitude.add(gpsInfo.getAltitude());
                    speed_mps.add(gpsInfo.getSpeed());
                    true_course.add(gpsInfo.getTrueCourse());
                    logger.log(lvl,"FOUND INSERTION POINT! startGPS_Idx: {0}; startRefIdx: {1}; endRefIdx: {2}; lat.size() = {3}",new Object[]{startGPS_Idx, startRefIdx, endRefIdx, lat.size()});
                    
                    if (LERP) {
                        VectorUtils.lerp(tstmp, startRefIdx, endRefIdx);
                        VectorUtils.lerp(lat, startRefIdx, endRefIdx);
                        VectorUtils.lerp(lon, startRefIdx, endRefIdx);
                        VectorUtils.lerp(altitude, startRefIdx, endRefIdx);
                        VectorUtils.lerp(speed_mps, startRefIdx, endRefIdx);
                        VectorUtils.lerp(true_course, startRefIdx, endRefIdx);
                    }
                    
                    startRefIdx = endRefIdx;
                    startGPS_Idx++;// get next GPS sample to place
                    if (startGPS_Idx < gpsMeasData.size()) {
                        gpsInfo = gpsMeasData.get(startGPS_Idx);
                        gpsUTC_us = (long)gpsInfo.getUtcUs();
                    }
                } else {
                    // haven't found insertion point yet. fill with zeros.
                    // values will get interpolated above
                    if (LERP) {
                        tstmp.add(0.0);
                        lat.add(0.0);
                        lon.add(0.0);
                        altitude.add(0.0);
                        speed_mps.add(0.0);
                        true_course.add(0.0);
                        si.get(endRefIdx).flags |= GPS_INTERP;
                    } else {
                        tstmp.add(gpsInfo.getUtcUs());
                        lat.add(gpsInfo.getPosition().getLatDd());
                        lon.add(gpsInfo.getPosition().getLonDd());
                        altitude.add(gpsInfo.getAltitude());
                        speed_mps.add(gpsInfo.getSpeed());
                        true_course.add(gpsInfo.getTrueCourse());
                    }
                }
                
                si.get(endRefIdx).flags |= GPS_SET;
                endRefIdx++;
            }
            
            logger.log(lvl,"lat.size() = {0}; si.size() = {1}; ",new Object[]{lat.size(),si.size()});
            
            outData.put("gps_timestamp", tstmp);
            outData.put("gps_lat", lat);
            outData.put("gps_lon", lon);
            outData.put("gps_altitude", altitude);
            outData.put("gps_speed_mps", speed_mps);
            outData.put("gps_true_course", true_course);
        }
    }
    
    static class ResampledVector {
        public final ArrayList<Double> data;
        int prevSet = -1;
        int currSet = -1;
        
        public ResampledVector(int size) {
            data = new ArrayList(Arrays.asList(new Double[size]));
        }
        
        public void set(int idx, double value) {
            data.set(idx, value);
            if (prevSet != -1) {
                VectorUtils.lerp(data, prevSet, currSet);
            } else if (prevSet == -1 && idx > 0) {
                // back fill data with first valid sample
                for (int i=0; i<idx; i++) {
                    data.set(i, value);
                }
            }
            prevSet = currSet;
            currSet = idx;
        }
    }
    
    private static void resampleCAN_Data(ArrayList<SampleInfo> si, ArrayList<CAN_Sample> canMeasData, CandIndex.Index index, boolean isRef, Map<String, ArrayList<Double>> outData) {
        if (canMeasData.isEmpty()) {
            // nothing to resample
            return;
        }
        
        Map<String,ResampledVector> resData = new HashMap<>(canMeasData.get(0).getSignalsCount());
        ResampledVector tstmp = new ResampledVector(si.size());
        
        if (isRef) {
            throw new UnsupportedOperationException(
                    "CAN data as reference is not supported");
        } else {
            // fill sample till that point with exact GPS value. unfortunately
            // filling is the best we can do here since we can't interpolate
            // with a previously known values.
            int startCAN_Idx = 0;
            int startRefIdx = 0;
            CAN_Sample canSamp = canMeasData.get(startCAN_Idx);
            long canUTC_us = canSamp.getTimestamp();
            long refUTC_us = si.get(startRefIdx).utc_us;
            Level lvl = Level.FINER;
            logger.log(lvl,"canUTC_us = {0}; refUTC_us = {1}",new Object[]{canUTC_us,refUTC_us});
            if (canUTC_us < refUTC_us) {
                // CAN data lags the IMU data
                while (startCAN_Idx < canMeasData.size()) {
                    canSamp = canMeasData.get(startCAN_Idx);
                    canUTC_us = canSamp.getTimestamp();
                    logger.log(lvl,"canMeasData[{0}].getUtcUs = {1}",new Object[]{startCAN_Idx,canUTC_us});
                    // find point where GPS sample first falls within time range of ref chan
                    if (canUTC_us >= refUTC_us) {
                        logger.log(lvl,"FOUND CAN SYNC POINT!");
                        break;
                    }
                    // keep waiting until CAN samples start to fall within
                    // ref chan's time range
                    startCAN_Idx++;
                }
            }
            // find point where CAN sample first falls within time range of ref chan
            if (canUTC_us >= refUTC_us) {
                // fill resampled data with duplicate GPS info until we've
                // reached the time when GPS and ref channel are aligned
                while (startRefIdx < si.size()) {
                    refUTC_us = si.get(startRefIdx).utc_us;
                    logger.log(lvl,"si[{0}].utc_us = {1}",new Object[]{startRefIdx,refUTC_us});
                    if (canUTC_us < si.get(startRefIdx).utc_us) {
                        logger.log(lvl,"FOUND REF SYNC POINT!");
                        break;
                    }
                    tstmp.set(startRefIdx, (double)canSamp.getTimestamp());
                    for (int signalIdx=0; signalIdx< canSamp.getSignalsCount(); signalIdx++) {
                        CANSample.CAN_Signal signal = canSamp.getSignals(signalIdx);
                        ResampledVector rv = null;
                        String sigName = "can_";
                        if (index == null) {
                            sigName += signal.getName();
                        } else {
                            sigName += index.getGroupsMap().get(canSamp.getGroupName()).getSignals(signalIdx).getName();
                        }
                        if (resData.containsKey(sigName)) {
                            rv = resData.get(sigName);
                        } else {
                            rv = new ResampledVector(si.size());
                            resData.put(sigName, rv);
                        }
                        rv.set(startRefIdx, signal.getValue());
                    }
                    startRefIdx++;
                }
            }
            
            startRefIdx--;// keep on valid sample
            startCAN_Idx++;// get next GPS sample to place
            if (startCAN_Idx < canMeasData.size()) {
                canSamp = canMeasData.get(startCAN_Idx);
                canUTC_us = canSamp.getTimestamp();
            }
            while (startRefIdx < si.size() && canSamp != null) {
                long refTime = si.get(startRefIdx).utc_us;
                logger.log(lvl,"canUTC_us = {0}; si[{1}].utc_us = {2}; ",new Object[]{canUTC_us,startRefIdx,refTime});
                if (canUTC_us < refTime) {
                    // found our next insertion point for the GPS data
                    tstmp.set(startRefIdx, (double)canSamp.getTimestamp());
                    for (int signalIdx=0; signalIdx< canSamp.getSignalsCount(); signalIdx++) {
                        CANSample.CAN_Signal signal = canSamp.getSignals(signalIdx);
                        ResampledVector rv = null;
                        String sigName = "can_";
                        if (index == null) {
                            sigName += signal.getName();
                        } else {
                            sigName += index.getGroupsMap().get(canSamp.getGroupName()).getSignals(signalIdx).getName();
                        }
                        if (resData.containsKey(sigName)) {
                            rv = resData.get(sigName);
                        } else {
                            rv = new ResampledVector(si.size());
                            resData.put(sigName, rv);
                        }
                        rv.set(startRefIdx, signal.getValue());
                    }
                    logger.log(lvl,"FOUND INSERTION POINT! startCAN_Idx: {0}; startRefIdx: {1}",new Object[]{startCAN_Idx, startRefIdx});
                    
                    startCAN_Idx++;// get next CAN sample to place
                    if (startCAN_Idx < canMeasData.size()) {
                        canSamp = canMeasData.get(startCAN_Idx);
                        canUTC_us = canSamp.getTimestamp();
                    }
                }
                
                si.get(startRefIdx).flags |= CAN_SET;
                startRefIdx++;
            }
        }
        
        outData.put("can_timestamp", tstmp.data);
        for (Map.Entry<String,ResampledVector> entry : resData.entrySet()) {
            outData.put(entry.getKey(), entry.getValue().data);
        }
    }
    
    private boolean computeSmoothedAccSignals() {
        boolean okay = true;
        // Number of accelerometer samples to smooth over
        int SMOOTH_W = 10;
        
        if (hasSignal("imu_acc_x")) {
            List<Double> accData = getImmutableSignalData("imu_acc_x");
            ArrayList<Double> smoothData = new ArrayList();
            if (DataFilters.smooth(accData, SMOOTH_W, smoothData)) {
                calculatedSignals.put("calc_acc_x_smooth", smoothData);
            }
        }
        if (hasSignal("imu_acc_y")) {
            List<Double> accData = getImmutableSignalData("imu_acc_y");
            ArrayList<Double> smoothData = new ArrayList();
            if (DataFilters.smooth(accData, SMOOTH_W, smoothData)) {
                calculatedSignals.put("calc_acc_y_smooth", smoothData);
            }
        }
        if (hasSignal("imu_acc_z")) {
            List<Double> accData = getImmutableSignalData("imu_acc_z");
            ArrayList<Double> smoothData = new ArrayList();
            if (DataFilters.smooth(accData, SMOOTH_W, smoothData)) {
                calculatedSignals.put("calc_acc_z_smooth", smoothData);
            }
        }
        
        return okay;
    }
    
}
