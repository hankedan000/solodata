/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniel
 */
public class TaskManager {
    public static abstract class Task implements Runnable, TaskProgressNotifier {
        private String name;
        private int id;
        private int groupId = INVALID_TASK_GROUP_ID;
        private Object userObject;
        private boolean canceled = false;
        private boolean kill = false;
        
        public TaskProgressNotifier getProgressNotifier() {
            return this;
        }
        
        public void updateProgress(int progress, int total) {
            TaskManager.updateTaskProgress(id, progress, total);
        }
        
        public void updateInformation(String infoText) {
            TaskManager.updateTaskInformation(id, infoText);
        }
        
        private void setName(String taskName) {
            name = taskName;
        }
        
        public String getName() {
            return name;
        }
        
        private void setId(int taskId) {
            id = taskId;
        }
        
        protected int getId() {
            return id;
        }
        
        private void setGroupId(int taskGroupId) {
            groupId = taskGroupId;
        }
        
        protected int getGroupId() {
            return groupId;
        }
        
        protected boolean isGroupedTask() {
            return groupId != INVALID_TASK_GROUP_ID;
        }
        
        private void setUserObject(Object obj) {
            userObject = obj;
        }
        
        protected Object getUserObject() {
            return userObject;
        }
        
        private void cancel() {
            canceled = true;
        }
        
        private void kill() {
            kill = true;
        }
    }
    
    public interface TaskGroupListener {
        public void allTasksCompleted();
        public void taskCanceled(Task task);
    }
    
    public interface TaskStatusListener {
        public void taskCompleted(Task task);
        public void taskCanceled(Task task);
    }
    
    public interface TaskManagerListener {
        public void taskStarted(int taskId, String taskName);
        public void taskProgressUpdated(int taskId, int progress, int total);
        public void taskInformationUpdated(int taskId, String infoText);
        public void taskCompleted(int taskId);
        public void taskCanceled(int taskId);
    }
    
    private static final Logger logger = Logger.getLogger(TaskManager.class.getName());
    
    // map of task that have been created stored by id
    private static final Map<Integer,Task> tasksById = new ConcurrentHashMap<>();
    
    private static final Map<Integer,Thread> threadsByTaskId = new ConcurrentHashMap<>();
    
    private static final Map<Integer,TaskStatusListener> statusListenerByTaskId = new ConcurrentHashMap<>();
    
    // monotonic counter used to assign task ids
    private static int nextTaskId = 0;
    
    // the next available task group id to use
    private static int nextTaskGroupId = 0;
    
    private static final int INVALID_TASK_GROUP_ID = -1;
    
    private static class TaskGroupScorecard {
        // Set to true once all defined tasks in the group have been started
        private boolean groupIsFinal = false;
        
        private Set<Integer> allTaskIds = new HashSet();
        private Set<Integer> canceledTaskIds = new HashSet();
        private Set<Integer> completedTaskIds = new HashSet();
        
        public TaskGroupScorecard addTask(int taskId) {
            allTaskIds.add(taskId);
            return this;
        }
        
        public TaskGroupScorecard addCompletedTask(int taskId) {
            if (allTaskIds.contains(taskId)) {
                completedTaskIds.add(taskId);
            } else {
                logger.log(Level.SEVERE,
                        "Attempted to mark a completed task with id {0}, but"
                        + " that task was never added to the group scorecard.",
                        new Object[]{taskId});
            }
            return this;
        }
        
        public TaskGroupScorecard addCanceledTask(int taskId) {
            if (allTaskIds.contains(taskId)) {
                canceledTaskIds.add(taskId);
            } else {
                logger.log(Level.SEVERE,
                        "Attempted to mark a canceled task with id {0}, but"
                        + " that task was never added to the group scorecard.",
                        new Object[]{taskId});
            }
            return this;
        }
        
        public TaskGroupScorecard markGroupFinal() {
            groupIsFinal = true;
            return this;
        }
        
        public boolean isComplete() {
            if (groupIsFinal) {
                Set<Integer> finishedTasks = new HashSet();
                finishedTasks.addAll(completedTaskIds);
                finishedTasks.addAll(canceledTaskIds);
                return finishedTasks.containsAll(allTaskIds);
            }
            return false;
        }
    }
    
    private static final Map<Integer,TaskGroupScorecard> taskScorecardsByGroupId = new HashMap();
    private static final Map<Integer,TaskGroupListener> taskGroupListenersByGroupId = new HashMap();
    
    private static final Timer completionTimer = new Timer(true);
    
    private static TaskManagerListener managerLister = null;
    
    private static TaskManager instance = null;
    
    private TaskManager() {
        // Check for completed task every 1000ms
        completionTimer.schedule(new TaskCompletionTimer(), 1000, 1000);
    }
    
    public static void setTaskManagerListener(TaskManagerListener listener) {
        managerLister = listener;
    }
    
    public static final int startTask(String taskName, Object userObject, Task task) {
        return startTask(taskName, userObject, task, null);
    }
    
    public static final int startTask(String taskName, Object userObject, Task task, TaskStatusListener statusListener) {
        return startTask(
                taskName,
                userObject,
                task,
                false,// not a grouped task
                true,
                statusListener,
                null);
    }
    
    public static final int startGroupedTask(String taskName, Object userObject, Task task, boolean isLastTaskInGroup, TaskStatusListener statusListener, TaskGroupListener groupListener) {
        return startTask(
                taskName,
                userObject,
                task,
                true,
                isLastTaskInGroup,
                statusListener,
                groupListener);
    }
    
    private static final int startTask(String taskName, Object userObject,
            Task task, boolean isGroupedTask, boolean isLastTaskInGroup, TaskStatusListener statusListener, 
            TaskGroupListener taskGroupListener) {
        if (instance == null) {
            instance = new TaskManager();
        }
        
        task.setName(taskName);
        task.setId(nextTaskId++);
        task.setUserObject(userObject);
        if (isGroupedTask) {
            task.setGroupId(nextTaskGroupId);
        }
        tasksById.put(task.getId(), task);
        if (statusListener != null) {
            statusListenerByTaskId.put(task.getId(),statusListener);
        }
        if (managerLister != null) {
            managerLister.taskStarted(task.getId(),taskName);
        }
        
        if (isGroupedTask) {
            TaskGroupScorecard tgs = null;
            if (taskScorecardsByGroupId.containsKey(task.getGroupId())) {
                tgs = taskScorecardsByGroupId.get(task.getGroupId());
            } else {
                tgs = new TaskGroupScorecard();
                taskScorecardsByGroupId.put(task.getGroupId(), tgs);
            }
            tgs.addTask(task.getId());
            if (isLastTaskInGroup) {
                tgs.markGroupFinal();
                nextTaskGroupId++;
            }
            
            taskGroupListenersByGroupId.put(task.getGroupId(),taskGroupListener);
        }
        
        Thread taskThread = new Thread(task,taskName);
        threadsByTaskId.put(task.getId(), taskThread);
        taskThread.start();
        
        return task.getId();
    }
    
    public static void cancelTask(int taskId) {
        if ( ! tasksById.containsKey(taskId)) {
            logger.log(Level.WARNING,
                    "No task exists for taskId {0}",taskId);
            return;
        }
        
        Task task = tasksById.get(taskId);
        
        // Mark task as canceled so it doesn't get notified as complete
        task.cancel();
        
        // Interrupt thread to stop it
        Thread taskThread = threadsByTaskId.get(taskId);
        taskThread.interrupt();
        
        if (task.isGroupedTask()) {
            taskScorecardsByGroupId.get(task.getGroupId()).addCanceledTask(task.getId());
        }
        
        notifyTaskCanceled(task);
    }
    
    protected static void updateTaskProgress(int taskId, int progress, int total) {
        if (managerLister != null) {
            managerLister.taskProgressUpdated(taskId,progress,total);
        }
    }
        
    protected static void updateTaskInformation(int taskId, String infoText) {
        if (managerLister != null) {
            managerLister.taskInformationUpdated(taskId,infoText);
        }
    }

    private static void taskCompleted(Task task) {
        notifyTaskCompleted(task);
        
        Thread taskThread = threadsByTaskId.get(task.getId());
        try {
            taskThread.join();
        } catch (InterruptedException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }
    
    private static void removeTask(int taskId) {
        logger.log(Level.FINE,
                "removing taskId {0}",
                taskId);
        tasksById.remove(taskId);
        statusListenerByTaskId.remove(taskId);
        threadsByTaskId.remove(taskId);
    }
    
    private static void notifyTaskCompleted(Task task) {
        if (statusListenerByTaskId.containsKey(task.getId())) {
            TaskStatusListener listener = statusListenerByTaskId.get(task.getId());
            listener.taskCompleted(task);
        }
        if (managerLister != null) {
            managerLister.taskCompleted(task.getId());
        }
    }
    
    private static void notifyTaskCanceled(Task task) {
        if (statusListenerByTaskId.containsKey(task.getId())) {
            TaskStatusListener listener = statusListenerByTaskId.get(task.getId());
            listener.taskCanceled(task);
        }
        if (taskGroupListenersByGroupId.containsKey(task.getGroupId())) {
            TaskGroupListener listener = taskGroupListenersByGroupId.get(task.getGroupId());
            if (listener != null) {
                listener.taskCanceled(task);
            }
        }
        if (managerLister != null) {
            managerLister.taskCanceled(task.getId());
        }
    }
    
    private static void notifyTaskGroupCompleted(Task task) {
        if (taskGroupListenersByGroupId.containsKey(task.getGroupId())) {
            TaskGroupListener listener = taskGroupListenersByGroupId.get(task.getGroupId());
            if (listener != null) {
                listener.allTasksCompleted();
            }
        }
    }
    
    // task used to periodically check if task have gracefully completed
    private class TaskCompletionTimer extends TimerTask {
        @Override
        public void run() {
            ArrayList<Integer> taskIdsToRemove = new ArrayList<>();
            
            for (Map.Entry<Integer,Thread> entry : threadsByTaskId.entrySet()) {
                Task task = tasksById.get(entry.getKey());
                logger.log(Level.FINER,
                        "checking liveliness of {0}",
                        task.getName());
                
                // If task is not running, see if we should notify completion
                if ( ! entry.getValue().isAlive()) {
                    if (task.canceled) {
                        // task was purposefully canceled don't notify completion
                        logger.log(Level.FINER,
                                "Task {0} was canceled",
                                task.getName());
                    } else {
                        logger.log(Level.FINER,
                                "Task {0} completed gracefully",
                                task.getName());
                        taskCompleted(task);
                        
                        // Is task is grouped, handle scorecarding logic
                        if (task.isGroupedTask()) {
                            int groupId = task.getGroupId();
                            if (taskScorecardsByGroupId.containsKey(groupId)) {
                                TaskGroupScorecard tgs = taskScorecardsByGroupId.get(groupId);
                                tgs.addCompletedTask(task.id);
                                
                                // All tasks in group completed
                                if (tgs.isComplete()) {
                                    // call task group listener
                                    notifyTaskGroupCompleted(task);
                                    
                                    // Cleanup once scorecard completed
                                    taskScorecardsByGroupId.remove(groupId);
                                    taskGroupListenersByGroupId.remove(groupId);
                                }
                            }
                        }
                    }
                    
                    // Mark task for removal
                    taskIdsToRemove.add(task.getId());
                }
            }
            
            // Remove task that have finished running
            for (int taskId : taskIdsToRemove) {
                removeTask(taskId);
            }
        }
    }
}
