/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.tasks;

/**
 *
 * @author daniel
 */
public interface TaskProgressNotifier {
    public void updateProgress(int progress, int total);

    public void updateInformation(String infoText);
}
