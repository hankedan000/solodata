/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.logviewer.perspectives;

import com.hankedan.jlogchart.DotMarker;
import com.hankedan.jlogchart.Marker;
import com.hankedan.jlogchart.Series;
import com.hankedan.jlogchart.SeriesBoundMarker;
import com.hankedan.jlogchart.XY_Chart;
import com.solodata.datalogger.AnalyzedRecord;
import com.solodata.datalogger.AnalyzedRecord.CrossDetectionInfo;
import com.solodata.datalogger.DetectionGate;
import com.solodata.datalogger.Record;
import com.solodata.logviewer.Project;
import com.solodata.logviewer.components.TrackMap;
import com.solodata.tasks.TaskManager;
import com.solodata.logviewer.dialogs.EditDataRatesDialog;
import com.solodata.logviewer.misc.ColorCellEditor;
import com.solodata.logviewer.misc.ColorCellRender;
import com.solodata.ui.maps.RecordMapMarkers;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

/**
 *
 * @author daniel
 */
public class AnalyzePerspective extends javax.swing.JPanel implements TrackMap.TrackMapListener {
    private static final Logger logger = Logger.getLogger(AnalyzePerspective.class.getName());
    
    private final DefaultTableModel recordTableModel;
    private final ListSelectionModel recordSelectionModel;
    private final Map<String,RecordMapMarkers> markersByRecordName = new HashMap();
    // Prevent tasks for adding records to analyze map concurrently
    private final Lock recordAnalyzedLock = new ReentrantLock();
    private final Map<String,AnalyzedRecord> analyzedRecordsByName = new HashMap();
    private Project activeProject = null;
    
    // This are are only when a record is marked "followed" in record list.
    // When set, the mapView will follow the marker's head when scrubbing
    // through time using the timeSlider.
    private Record followedRecord = null;
    private RecordMapMarkers followedRecordMarkers = null;
    // Set to true while updating "followed" checkbox in recordTable
    // This is used to prevent stack overflow of tableChanged() calls
    private boolean updatingFollowedRecord = false;
    
    // A list of all the log charts that this perspective owns
    private final List<XY_Chart> xyCharts = new ArrayList();
    
    private List<SeriesBoundMarker> xyChartfollowMarkers = new ArrayList();
    
    private XY_Chart xyChart1 = null;
    
    /**
     * Creates new form AnalyzePerspective
     */
    public AnalyzePerspective() {
        initComponents();
        
        // -------------
        
        // Init the record table component
        recordTableModel = new RecordTableModel();
        recordTableModel.addTableModelListener(new RecordTableModelListener());
        recordTable.setModel(recordTableModel);
        recordTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        recordTable.setDefaultEditor(Color.class, new ColorCellEditor());
        recordTable.setDefaultRenderer(Color.class, new ColorCellRender(true));
        recordSelectionModel = recordTable.getSelectionModel();
        recordSelectionModel.addListSelectionListener(new RecordSelectionListener());
        
        // Allow tabel to be sorted on certain columns
        TableRowSorter<TableModel> sorter = new TableRowSorter<>(recordTable.getModel());
        recordTable.setRowSorter(sorter);
        List<RowSorter.SortKey> sortKeys = new ArrayList<>(25);
        sortKeys.add(new RowSorter.SortKey(RecordTableModel.RECORD_NAME_COL_IDX, SortOrder.ASCENDING));
        sorter.setSortKeys(sortKeys);
        
        // -------------
        
        timeSlider.addChangeListener(new TimeSlideChangeListener());
        trackMap.addTrackMapListener(this);
        
        // FIXME this is a hack
        // i didn't feel like moving a bunch of this code that's related to the
        // xyChart into the TrackMap
        xyChart1 = trackMap.getXY_Chart();
        
        xyCharts.add(xyChart1);
    }

    public void setProject(Project project) {
        multiLogChart.setProject(project);
        activeProject = project;
        
        // Clear all internal datastructures of old project
        while (recordTableModel.getRowCount() > 0) {
            recordTableModel.removeRow(0);
        }
        xyChartfollowMarkers.clear();
        analyzedRecordsByName.clear();
        for (XY_Chart xyc : xyCharts) {
            xyc.clear();
        }
        
        // Disable slider when project is not valid
        timeSlider.setEnabled(project != null);
        timeSlider.setMinimum(0);
        timeSlider.setValue(0);
        
        // If project is null, leave table cleared, but then leave early
        if (project == null) {
            return;
        }
        
        // --------------------------------------------------------
        
        // populate track map with known gates
        DetectionGate startGate = project.getStartGate();
        alignToStart.setEnabled(startGate != null);
        if (startGate != null) {
            trackMap.setStartGate(startGate);
        }
        DetectionGate finishGate = project.getFinishGate();
        alignToFinish.setEnabled(finishGate != null);
        if (finishGate != null) {
            trackMap.setFinishGate(finishGate);
        }
        
        // Populate table with new records
        List<Record> records = project.getRecords();
        for (int i=0; i<records.size(); i++) {
            Record record = records.get(i);
            boolean isLastRecord = i == (records.size() - 1);
            recordTableModel.addRow(makeRecordTableRow(project,record));
            
            String analyzeTaskName = String.format("Analyzing %s",record.getName());
            TaskManager.startGroupedTask(analyzeTaskName, null, new TaskManager.Task() {
                @Override
                public void run() {
                    // Analyze the record
                    AnalyzedRecord ar = new AnalyzedRecord(record);
                    ar.analyze(activeProject.getIdealSensorDataRates(),getProgressNotifier());
                    
                    List<Vector2D> coords = null;
                    String LAT_SIG = "gps_lat";
                    String LON_SIG = "gps_lon";
                    if (ar.hasSignal(LAT_SIG) && ar.hasSignal(LON_SIG)) {
                        List<Double> latData = ar.getImmutableSignalData(LAT_SIG);
                        List<Double> lonData = ar.getImmutableSignalData(LON_SIG);
                        
                        // AnalyzedRecord gaurantees data vectors are of same length
                        coords = new ArrayList(latData.size());
                        for (int i=0; i<latData.size(); i++) {
                            coords.add(new Vector2D(lonData.get(i),latData.get(i)));
                        }
                    }
                    
                    // --------------------CRITICAL BEGIN----------------------
                    recordAnalyzedLock.lock();
                    analyzedRecordsByName.put(record.getName(), ar);
                    if (coords != null) {
                        Series s = xyChart1.addSeries(record.getName(), coords);
                        s.setVisible(activeProject.getRecordVisible(record.getName()));
                        s.setColor(activeProject.getRecordColor(record.getName()));
                        // Add color dot marker to the XY series
                        Marker dot = new DotMarker(5).setFill(s.getColor());
                        SeriesBoundMarker sbm = new SeriesBoundMarker(s, dot); 
                        xyChart1.addSeriesBoundMarker(sbm);
                        xyChartfollowMarkers.add(sbm);
                        // fit the XY_Chart to the added data
                        xyChart1.fitViewToData(true);// only series that are visible
                    }
                    recordAnalyzedLock.unlock();
                    // ---------------------CRITICAL END-----------------------
                }
            }, isLastRecord, null, new TaskManager.TaskGroupListener() {
                @Override
                public void allTasksCompleted() {
                    // Add all the analyzed records to the log charts
                    for (AnalyzedRecord ar : analyzedRecordsByName.values()) {
                        String recordName = ar.record().getName();
                        Color c = activeProject.getRecordColor(recordName);
                        multiLogChart.addAnalyzedRecord(ar,c);
                        
                        // Update record's data offset from project metadata
                        int dataStartOffset = activeProject.getRecordDataOffset(recordName);
                        setRecordDataOffset(recordName, dataStartOffset);
                    }
                    
                    // FIXME temp code to test cross detection logic
                    DetectionGate startGate = project.getStartGate();
                    if (startGate != null) {
                        alignRecordsToGate(startGate);
                    }
                }

                @Override
                public void taskCanceled(TaskManager.Task task) {
                    throw new UnsupportedOperationException("Not supported yet.");
                }
            });
        }
    }
    
    /**
     * Forces a rescan for new records that may have been added to the project
     * since the active project was set via 'setProject'.
     */
    public void scanForNewRecords() {
        logger.log(Level.WARNING,"scanning for new project not implmented yet");
    }
    
    private HashMap<String, CrossDetectionInfo> detectCrossings(DetectionGate gate) {
        HashMap<String, CrossDetectionInfo> crossByRecord = new HashMap<>();
        
        for (String recordName : analyzedRecordsByName.keySet()) {
            AnalyzedRecord ar = analyzedRecordsByName.get(recordName);
            CrossDetectionInfo cdi = ar.findFirstCrossing(gate, 0);
            if (cdi.crossDetected) {
                System.out.println(String.format("%s: cross detected @ %d", recordName, cdi.sampleIdx));
            } else {
                System.out.println(String.format("%s: no crossing detected", recordName));
            }
            crossByRecord.put(recordName, cdi);
        }
        
        return crossByRecord;
    }
    
    private void alignRecordsToGate(DetectionGate gate) {
        HashMap<String, CrossDetectionInfo> crossByRecord = detectCrossings(gate);
        
        // apply offsets to align series data at gate crossing point
        int alignIdx = -1;
        for (String recordName : crossByRecord.keySet()) {
            CrossDetectionInfo cdi = crossByRecord.get(recordName);
            if (cdi.crossDetected) {
                if (alignIdx == -1) {
                    setRecordDataOffset(recordName, 0);
                    alignIdx = cdi.sampleIdx;
                } else {
                    setRecordDataOffset(recordName, alignIdx - cdi.sampleIdx);
                }
            } else {
                setRecordDataOffset(recordName, 0);
            }
        }
    }
    
    private void setRecordDataOffset(String recordName, int newOffset) {
        // Update project metadata first
        if (activeProject != null) {
//            activeProject.setRecordDataOffset(recordName, newOffset);
        }
        
        // Update all the series
        multiLogChart.setRecordDataOffset(recordName, newOffset);
        for (XY_Chart xyc : xyCharts) {
            Series series = xyc.getSeriesByName(recordName);
            if (series != null) {
                series.setOffset(newOffset);
            }
        }

        // Modify slider min/max to reflect offset change
        updateTimeSlider();
    }
    
    private void setRecordColor(String recordName, Color newColor) {
        // Update project metadata first
        if (activeProject != null) {
            activeProject.setRecordColor(recordName, newColor);
        }
        
        // Update all the series
        multiLogChart.setRecordColor(recordName, newColor);
        for (XY_Chart xyc : xyCharts) {
            Series series = xyc.getSeriesByName(recordName);
            if (series != null) {
                series.setColor(newColor);
            }
        }
    }
    
    private Object[] makeRecordTableRow(Project project, Record record) {
        String recordName = record.getName();
        return new Object[]{
            project.getUserDefinedRecordName(recordName),
            project.getRecordVisible(recordName),
            false,// Default to follow no records
            project.getRecordColor(recordName),
            project.getRecordDataOffset(recordName)
        };
    }
    
    /**
     * @return 
     * A reference to the record that is currently selected in the record table.
     * null is returned if nothing is selected.
     */
    private Record getSelectedRecord() {
        Record outRecord = null;
        if (activeProject != null &&
                recordSelectionModel.getSelectedItemsCount() > 0) {
            // Mode is restricted to SINGLE_SELECTION mode, so grab fist item
            int idx = recordSelectionModel.getSelectedIndices()[0];
            
            // Fetch record based on selected index
            List<Record> records = activeProject.getRecords();
            if (idx < records.size()) {
                outRecord = records.get(idx);
            } else {
                logger.log(Level.SEVERE,
                        "idx {0} is largers that project's record size {1}",
                        new Object[]{idx,records.size()});
                outRecord = null;
            }
        }
        return outRecord;
    }
    
    private void onRecordSelected(Record record) {
        // TODO make this a toolbar option or something
        // drawing bolded lines can be quite a performance hit. having unique
        // colors per record is good enough to distinguish for now.
        boolean BOLD_ON_SELECT = false;
        if (BOLD_ON_SELECT) {
            // Bold the selected Record's series across all JLogCharts
            multiLogChart.boldRecord(record.getName(), true);

            // Bold the selected Record's series across all XY_Charts
            for (XY_Chart xyc : xyCharts) {
                for (Series series : xyc.getAllSeries()) {
                    if (series.name.compareTo(record.getName()) == 0) {
                        series.setBolded(true);
                    } else {
                        series.setBolded(false);
                    }
                }
            }
        }
        
        // TODO make this happen on right click or something
        boolean EXPORT_CSV = false;
        if (EXPORT_CSV) {
//            NavSampleCSVWriter.write(record.getNavSamples(), "/tmp/nav_data.csv");
        }
    }
    
    /**
     * Method called when the user changes the visibility checkbox in the
     * record table.
     * @param record
     * @param visible 
     */
    private void updateRecordVisibility(Record record, boolean visible) {
        if (activeProject != null) {
            activeProject.setRecordVisible(record.getName(), visible);
        }
        multiLogChart.setRecordVisibility(record.getName(), visible);
        
        for (XY_Chart xyc : xyCharts) {
            xyc.setSeriesVisible(record.getName(), visible);
        }
    }
    
    private void updateTimeSlider() {
        int newMin = Integer.MAX_VALUE;
        int newMax = Integer.MIN_VALUE;
        boolean minMaxValid = false;
        for (Series s : xyChart1.getAllSeries()) {
            newMin = Integer.min(newMin, s.getOffset());
            newMax = Integer.max(newMax, s.getOffset() + s.size() - 1);
            minMaxValid = true;
        }
        
        int newValue = timeSlider.getValue();
        if (newValue < newMin) {
            newValue = newMin;
        } else if (newValue > newMax) {
            newValue = newMax;
        }
        
        timeSlider.setEnabled(minMaxValid);
        if (minMaxValid) {
            timeSlider.setMinimum(newMin);
            timeSlider.setMaximum(newMax);
            timeSlider.setValue(newValue);
        }
    }
    
    private void setFollowRecord(Record record) {
        followedRecord = record;
        followedRecordMarkers = markersByRecordName.get(record.getName());
    }
    
    private void clearFollowRecord() {
        followedRecord = null;
        followedRecordMarkers = null;
    }

    @Override
    public void onStartGateUpdated(DetectionGate gate) {
        activeProject.setStartGate(gate);
        alignToStart.setEnabled(gate != null);
    }

    @Override
    public void onFinishGateUpdated(DetectionGate gate) {
        activeProject.setFinishGate(gate);
        alignToFinish.setEnabled(gate != null);
    }
    
    // ------------------------------------------------------------------------
    
    protected class RecordSelectionListener implements ListSelectionListener {
        @Override
        public void valueChanged(ListSelectionEvent arg0) {
            Record record = getSelectedRecord();
            if (record != null) {
                onRecordSelected(record);
            }
        }
    }
    
    // ------------------------------------------------------------------------
    
    protected class RecordTableModel extends DefaultTableModel {
        public static final int RECORD_NAME_COL_IDX = 0;
        public static final int RECORD_VISIBLE_COL_IDX = 1;
        public static final int RECORD_FOLLOW_COL_IDX = 2;
        public static final int RECORD_COLOR_COL_IDX = 3;
        public static final int RECORD_OFFSET_COL_IDX = 4;

        public RecordTableModel() {
            addColumn("Record");
            addColumn("Visible");
            addColumn("Follow");
            addColumn("Color");
            addColumn("Offset");
        }
        
        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case RECORD_NAME_COL_IDX:
                    return String.class;
                case RECORD_VISIBLE_COL_IDX:
                    return Boolean.class;
                case RECORD_FOLLOW_COL_IDX:
                    return Boolean.class;
                case RECORD_COLOR_COL_IDX:
                    return Color.class;
                case RECORD_OFFSET_COL_IDX:
                    return Integer.class;
                default:
                    logger.log(Level.WARNING,
                            "RecordTable.getColumnClass() default return"
                            + " class of String for columnIndex {0}",
                            columnIndex);
                    return String.class;
            }
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            switch (column) {
                case RECORD_NAME_COL_IDX:
                    return false;
                case RECORD_VISIBLE_COL_IDX:
                case RECORD_FOLLOW_COL_IDX:
                case RECORD_COLOR_COL_IDX:
                case RECORD_OFFSET_COL_IDX:
                    return true;
                default:
                    logger.log(Level.WARNING,
                            "RecordTable.isCellEditable() default return"
                            + " false for columnIndex {0}",
                            column);
                    return false;
            }
        }
    }
    
    // ------------------------------------------------------------------------
    
    protected class RecordTableModelListener implements TableModelListener {
        @Override
        public void tableChanged(TableModelEvent evt) {
            int colIdx = evt.getColumn();
            if (activeProject == null) {
                return;
            } else if (colIdx < 0) {
                // Not sure why but this can be -1 while populating table
                return;
            }

            List<Record> records = activeProject.getRecords();
            for (int rr=evt.getFirstRow(); rr<=evt.getLastRow(); rr++) {
                if (rr < 0 || rr >= records.size()) {
                    /**
                     * Row doesn't correspond to records; ignore event. This
                     * occurs when we are initially populating the table.
                     */
                    continue;
                }

                Record record = records.get(rr);
                switch (colIdx) {
                    case RecordTableModel.RECORD_NAME_COL_IDX:
                        // do nothing (should never change)
                        break;
                    case RecordTableModel.RECORD_VISIBLE_COL_IDX:
                        boolean visible = (boolean)recordTableModel.getValueAt(rr,colIdx);
                        updateRecordVisibility(record, visible);
                        break;
                    case RecordTableModel.RECORD_FOLLOW_COL_IDX:
                        // Prevent recursive stack overflow
                        if (updatingFollowedRecord) {
                            // Ignore intermediate updates to table
                            return;
                        }
                        
                        // Configure analyze view to follow this record's head
                        boolean follow = (boolean)recordTableModel.getValueAt(rr,colIdx);
                        updatingFollowedRecord = true;
                        if (follow) {
                            setFollowRecord(record);
                            // Make sure only one record is selectable as "followed"
                            for (int row=0; row<recordTableModel.getRowCount(); row++) {
                                boolean checked = (row == rr);
                                recordTableModel.setValueAt(checked, row, RecordTableModel.RECORD_FOLLOW_COL_IDX);
                            }
                        } else {
                            clearFollowRecord();
                            // Clear all the follow checkboxes in the record table
                            for (int row=0; row<recordTableModel.getRowCount(); row++) {
                                recordTableModel.setValueAt(false, row, RecordTableModel.RECORD_FOLLOW_COL_IDX);
                            }
                        }
                        updatingFollowedRecord = false;
                        break;
                    case RecordTableModel.RECORD_COLOR_COL_IDX:
                        Color c = (Color)recordTableModel.getValueAt(rr,colIdx);
                        setRecordColor(record.getName(), c);
                        break;
                    case RecordTableModel.RECORD_OFFSET_COL_IDX:
                        // Update the Record's offset from table
                        int offset = (int)recordTableModel.getValueAt(rr,colIdx);
                        setRecordDataOffset(record.getName(), offset);
                        break;
                    default:
                        logger.log(Level.WARNING,
                                "RecordTable.tableChanged() unhandled "
                                + " event for colIdx {0}",
                                colIdx);
                        break;
                }
            }
        }
    }
    
    // ------------------------------------------------------------------------
    
    protected class TimeSlideChangeListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent evt) {
            int absIdx = timeSlider.getValue();
            for (SeriesBoundMarker sbm : xyChartfollowMarkers) {
                sbm.setAbsOffset(absIdx);
            }
            multiLogChart.setTimeMarkerOffset(absIdx);
            
            xyChart1.repaint();
        }
    }
    
    // ------------------------------------------------------------------------
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        leftRightSplit = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        recordTable = new javax.swing.JTable();
        analyzePanel = new javax.swing.JPanel();
        toolbar = new javax.swing.JToolBar();
        analyzeButton = new javax.swing.JButton();
        editRatesButton = new javax.swing.JButton();
        alignToStart = new javax.swing.JButton();
        alignToFinish = new javax.swing.JButton();
        plotView = new javax.swing.JPanel();
        trackMap = new com.solodata.logviewer.components.TrackMap();
        multiLogChart = new com.solodata.logviewer.components.MultiLogChart();
        timeSlider = new javax.swing.JSlider();

        setLayout(new java.awt.BorderLayout());

        leftRightSplit.setDividerLocation(200);
        leftRightSplit.setLastDividerLocation(200);

        jScrollPane2.setMinimumSize(new java.awt.Dimension(200, 24));
        jScrollPane2.setPreferredSize(new java.awt.Dimension(200, 348));

        recordTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        recordTable.setFillsViewportHeight(true);
        recordTable.setMinimumSize(new java.awt.Dimension(200, 64));
        recordTable.setPreferredSize(new java.awt.Dimension(200, 64));
        jScrollPane2.setViewportView(recordTable);

        leftRightSplit.setLeftComponent(jScrollPane2);

        analyzePanel.setLayout(new java.awt.BorderLayout());

        toolbar.setFloatable(false);
        toolbar.setRollover(true);

        analyzeButton.setText("Analyze");
        analyzeButton.setToolTipText("Force a reanalyze of the record data.");
        analyzeButton.setFocusable(false);
        analyzeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        analyzeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolbar.add(analyzeButton);

        editRatesButton.setText("Edit Rates");
        editRatesButton.setToolTipText("Change the ideal sensor data rates");
        editRatesButton.setFocusable(false);
        editRatesButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        editRatesButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        editRatesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editRatesButtonActionPerformed(evt);
            }
        });
        toolbar.add(editRatesButton);

        alignToStart.setText("Align To Start");
        alignToStart.setToolTipText("Align datasets based on when the vehicle crossed the starting gate.");
        buttonGroup1.add(alignToStart);
        alignToStart.setEnabled(false);
        alignToStart.setFocusable(false);
        alignToStart.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        alignToStart.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        alignToStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alignToStartActionPerformed(evt);
            }
        });
        toolbar.add(alignToStart);

        alignToFinish.setText("Align To Finish");
        alignToFinish.setToolTipText("Align datasets based on when the vehicle crossed the finishing gate.");
        buttonGroup1.add(alignToFinish);
        alignToFinish.setEnabled(false);
        alignToFinish.setFocusable(false);
        alignToFinish.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        alignToFinish.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        alignToFinish.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alignToFinishActionPerformed(evt);
            }
        });
        toolbar.add(alignToFinish);

        analyzePanel.add(toolbar, java.awt.BorderLayout.NORTH);

        plotView.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        plotView.add(trackMap, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        plotView.add(multiLogChart, gridBagConstraints);

        analyzePanel.add(plotView, java.awt.BorderLayout.CENTER);
        analyzePanel.add(timeSlider, java.awt.BorderLayout.SOUTH);

        leftRightSplit.setRightComponent(analyzePanel);

        add(leftRightSplit, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void editRatesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editRatesButtonActionPerformed
        JFrame parentFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
        EditDataRatesDialog d = new EditDataRatesDialog(parentFrame,true,activeProject);
        d.setLocationRelativeTo(parentFrame);
        d.setVisible(true);
    }//GEN-LAST:event_editRatesButtonActionPerformed

    private void alignToStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alignToStartActionPerformed
        DetectionGate gate = activeProject.getStartGate();
        if (gate != null) {
            alignRecordsToGate(gate);
        }
    }//GEN-LAST:event_alignToStartActionPerformed

    private void alignToFinishActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alignToFinishActionPerformed
        DetectionGate gate = activeProject.getFinishGate();
        if (gate != null) {
            alignRecordsToGate(gate);
        }
    }//GEN-LAST:event_alignToFinishActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton alignToFinish;
    private javax.swing.JButton alignToStart;
    private javax.swing.JButton analyzeButton;
    private javax.swing.JPanel analyzePanel;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton editRatesButton;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane leftRightSplit;
    private com.solodata.logviewer.components.MultiLogChart multiLogChart;
    private javax.swing.JPanel plotView;
    private javax.swing.JTable recordTable;
    private javax.swing.JSlider timeSlider;
    private javax.swing.JToolBar toolbar;
    private com.solodata.logviewer.components.TrackMap trackMap;
    // End of variables declaration//GEN-END:variables
}
