/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.utils;

import java.security.InvalidParameterException;
import java.util.List;

/**
 *
 * @author daniel
 */
public class VectorUtils {
    /**
     * Fills the vector entries between 'start' and 'end' with linear
     * interpolated values.
     * 
     * @param V
     * The vector to linear interpolate points in
     * @param start
     * The index to use as a starting value for the linear interpolation. 
     * (start + 1) will be the first interpolated entry;
     * @param end 
     * The index to use as a ending value for the linear interpolation. 
     * (end - 1) will be the last interpolated entry
     */
    public static void lerp(List<Double> V, int start, int end) {
        if (start > end) {
            throw new InvalidParameterException(
                    "'start' must be less than 'end'");
        } else if (start == end) {
            /**
             * start and end are the same. Nothing to interpolate. Not an error
             * but this will cause divide by zero down below if we don't bail
             * out early.
             */
            return;
        } else if (start + 1 == end) {
            /**
             * start and end are back to back. Nothing to interpolate. Not an
             * error but this will cause a near-infinite loop down below if we
             * don't bail out early.
             */
            return;
        }
        
        double startVal = V.get(start);
        double endVal = V.get(end);
        double slope = (endVal - startVal) / (end - start);
        int step = 1;
        for (int ii=start+1; ii<end; ii++) {
            V.set(ii,startVal + slope * step++);
        }
    }
}
