/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.logviewer;

import com.solodata.Settings;
import com.solodata.utils.UserDirUtils;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author daniel
 */
public class LogViewerSettings extends Settings {
    private static LogViewerSettings instance = null;
    
    public static final String RESTORE_POSITION_ON_START_KEY = "restorePositionOnStart";
    public static final String PREVIOUS_WINDOW_POS_X_KEY = "previousWindowPositionX";
    public static final String PREVIOUS_WINDOW_POS_Y_KEY = "previousWindowPositionY";
    public static final String PREVIOUS_WINDOW_MAXIMIZED_KEY = "previousWindowMaximized";
    public static final String ENABLE_DARK_THEME_KEY = "enableDarkTheme";
    public static final String IMPORTED_PROJECTS_KEY = "importedProjects";
    
    private LogViewerSettings() {
        super(Paths.get(UserDirUtils.getUserPath("LogViewer").toString(),"settings.json").toString());
    }
    
    private static final LogViewerSettings getInstance() {
        if (instance == null) {
            instance = new LogViewerSettings();
        }
        return instance;
    }

    @Override
    protected final void loadDefaults() {
        checkAndSetDefault(RESTORE_POSITION_ON_START_KEY,true);
        checkAndSetDefault(PREVIOUS_WINDOW_POS_X_KEY,0);
        checkAndSetDefault(PREVIOUS_WINDOW_POS_Y_KEY,0);
        checkAndSetDefault(PREVIOUS_WINDOW_MAXIMIZED_KEY,false);
        checkAndSetDefault(ENABLE_DARK_THEME_KEY,false);
        checkAndSetDefault(IMPORTED_PROJECTS_KEY, new JSONArray());
    }
    
    public static Settings getSettings() {
        return getInstance();
    }
    
    // -----------------
    
    public static boolean getPreviousWindowMaximized() {
        return (boolean)getInstance().getSetting(PREVIOUS_WINDOW_MAXIMIZED_KEY);
    }
    
    public static void setPreviousWindowMaximized(boolean maximized) {
        getInstance().modifySettingAndNotify(PREVIOUS_WINDOW_MAXIMIZED_KEY, maximized);
    }
    
    // -----------------
    
    public static boolean getRestorePositionOnStart() {
        return (boolean)getInstance().getSetting(RESTORE_POSITION_ON_START_KEY);
    }
    
    public static void setRestorePositionOnStart(boolean restore) {
        getInstance().modifySettingAndNotify(RESTORE_POSITION_ON_START_KEY, restore);
    }
    
    // -----------------
    
    public static int getPreviousWindowPositionX() {
        return (int)getInstance().getSetting(PREVIOUS_WINDOW_POS_X_KEY);
    }
    
    public static void setPreviousWindowPositionX(int x) {
        getInstance().modifySettingAndNotify(PREVIOUS_WINDOW_POS_X_KEY, x);
    }
    
    // -----------------
    
    public static int getPreviousWindowPositionY() {
        return (int)getInstance().getSetting(PREVIOUS_WINDOW_POS_Y_KEY);
    }
    
    public static void setPreviousWindowPositionY(int y) {
        getInstance().modifySettingAndNotify(PREVIOUS_WINDOW_POS_Y_KEY, y);
    }
    
    // -----------------
    
    public static boolean getDarkThemeEnabled() {
        return (boolean)getInstance().getSetting(ENABLE_DARK_THEME_KEY);
    }
    
    public static void setDarkThemeEnabled(boolean enabled) {
        getInstance().modifySettingAndNotify(ENABLE_DARK_THEME_KEY, enabled);
    }
    
    // -----------------
    
    public static List<String> getImportedProjects() {
        ArrayList<String> projects = new ArrayList<>();
        JSONArray jsArray = (JSONArray)getInstance().getSetting(IMPORTED_PROJECTS_KEY);
        for (Object obj : jsArray) {
            projects.add((String)obj);
        }
        return projects;
    }
    
    public static void setImportedProjects(List<String> projects) {
        JSONArray jsArray = new JSONArray();
        for (String project : projects) {
            jsArray.put(project);
        }
        getInstance().modifySettingAndNotify(IMPORTED_PROJECTS_KEY, jsArray);
    }
    
}
