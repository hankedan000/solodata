/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview.gui;

import com.solodata.ui.maps.MapPolyline;
import com.opencsv.CSVWriter;
import com.solodata.NavSampleCSVWriter;
import com.solodata.soloview.Record;
import com.solodata.soloview.Session;
import java.awt.BasicStroke;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import solodata.NavDefn;
import solodata.NavSampleOuterClass;

/**
 *
 * @author daniel
 */
public class SessionView extends javax.swing.JPanel implements 
    Record.RecordRestoreListener {
    private static final String LOG_TAG = SessionView.class.getName();
    private static final int RECORD_VISIBLE_COL_IDX = 0;
    private static final int RECORD_NAME_COL_IDX = 1;
    
    private class RecordMarkers {
        private final Record record;
        public final MapMarkerDot headMarker;
        public final MapPolyline route;
        private int sampIdx = 0;
        
        /**
         * A class used to store and maintain MapMarkers per record
         * 
         * @param r
         * the record the markers belong to
         * 
         * @param map
         * the map that the markers will be placed into
         */
        public RecordMarkers(Record r, JMapViewer map) {
            record = r;
            
            headMarker = new MapMarkerDot(new Coordinate(0,0));
            headMarker.setStroke(new BasicStroke(2));
            
            route = new MapPolyline(map);
            route.setColor(Theme.getDefaultMapLineColor());
            route.setStroke(new BasicStroke(
                    5,
                    BasicStroke.CAP_ROUND,
                    BasicStroke.JOIN_ROUND));
            
            setVisible(false);
            
            // add marker to map
            map.addMapMarker(route);
            map.addMapMarker(headMarker);
        }
        
        public void setVisible(boolean visible) {
            headMarker.setVisible(visible);
            route.setVisible(visible);
        }
        
        /**
         * Advances the tracer to the nearest time offset with a valid GPS
         * sample.
         * 
         * @param dt 
         * delta time offset relative to the beginning of the record
         */
        public void advToTime(double dt) {
            List<NavSampleOuterClass.NavSample> samps = record.getNavSamples();
            if (samps == null) {
                return;
            }
            if (samps.size() < 1) {
                return;
            }
            
            // make sure sampIdx is within range
            sampIdx = Integer.min(sampIdx, samps.size() - 1);
            sampIdx = Integer.max(sampIdx, 0);
            
            double startTime = samps.get(0).getUtcUs() / 1.0e6;
            double currOffset = samps.get(sampIdx).getUtcUs() / 1.0e6 - startTime;
            if (currOffset < dt) {
                for ( ; sampIdx < samps.size(); sampIdx++) {
                    NavSampleOuterClass.NavSample samp = samps.get(sampIdx);
                    currOffset = samp.getUtcUs() / 1.0e6 - startTime;
                    if (samp.hasGps() && currOffset >= dt) {
                        // found the next GPS sample. stop
                        headMarker.setLat(samp.getGps().getPosition().getLatDd());
                        headMarker.setLon(samp.getGps().getPosition().getLonDd());
                        break;
                    }
                }
            } else {
                for ( ; sampIdx > 0; sampIdx--) {
                    NavSampleOuterClass.NavSample samp = samps.get(sampIdx);
                    currOffset = samp.getUtcUs() / 1.0e6 - startTime;
                    if (samp.hasGps() && currOffset <= dt) {
                        // found the next GPS sample. stop
                        headMarker.setLat(samp.getGps().getPosition().getLatDd());
                        headMarker.setLon(samp.getGps().getPosition().getLonDd());
                        break;
                    }
                }
            }
        }
    }
    
    private final DefaultTableModel recordTableModel;
    private final ListSelectionModel recordSelectionModel;
    private Session session = null;
    private final List<Record> records = new ArrayList<>();
    // map of markers keyed by record.id
    private final Map<Integer,RecordMarkers> markersByID = new HashMap<>();
    
    /**
     * Creates new form SessionView
     */
    public SessionView() {
        initComponents();
        recordTableModel = new DefaultTableModel() {
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case RECORD_VISIBLE_COL_IDX:
                        return Boolean.class;
                    case RECORD_NAME_COL_IDX:
                        return String.class;
                    default:
                        Logger.getLogger(LOG_TAG).log(Level.WARNING,
                                "RecordTable.getColumnClass() default return"
                                + " class of String for columnIndex {0}",
                                columnIndex);
                        return String.class;
                }
            }
        };
        recordTableModel.addColumn("");
        recordTableModel.addColumn("Record");
        recordTable.setModel(recordTableModel);
        recordTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        recordSelectionModel = recordTable.getSelectionModel();
        recordSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent evt) {
                Record record = getSelectedRecord();
                if (record != null) {
                    onRecordSelected(record);
                }
            }
        });
        recordTableModel.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent evt) {
                int colIdx = evt.getColumn();
                // optional repaint map if value change requires it
                boolean repaintMap = false;
                
                for (int rr=evt.getFirstRow(); rr<=evt.getLastRow(); rr++) {
                    if (rr < 0 || rr >= records.size()) {
                        /**
                         * Row doesn't correspond to records; ignore event. This
                         * occurs when we are initially populating the table.
                         */
                        continue;
                    }
                
                    Record record = records.get(rr);
                    RecordMarkers markers = markersByID.get(record.id);
                    switch (colIdx) {
                        case RECORD_VISIBLE_COL_IDX:
                            // set marker visible based on new value
                            markers.setVisible((boolean)recordTableModel.getValueAt(rr,colIdx));
                            repaintMap = true;
                            break;
                        case RECORD_NAME_COL_IDX:
                            // do nothing (should never change)
                            break;
                        default:
                            Logger.getLogger(LOG_TAG).log(Level.WARNING,
                                    "RecordTable.tableChanged() unhandled "
                                    + " event for colIdx {0}",
                                    colIdx);
                            break;
                    }
                }
                
                if (repaintMap) {
                    map().repaint();
                }
            }
        });
        
        // each step of slider is 1ms
        timeOffsetSlider.setMinimum(0);
        timeOffsetSlider.setMaximum(1000);
        timeOffsetSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent arg0) {
                onTimeOffsetChanged(getTimeOffset());
            }
        });
        
        // TODO just test code
        setSession(Paths.get("./session0"));
    }
    
    /**
     * @return 
     * A reference to the record that is currently selected in the record table.
     * null is returned if nothing is selected.
     */
    public Record getSelectedRecord() {
        if (recordSelectionModel.getSelectedItemsCount() > 0) {
            int idx = recordSelectionModel.getSelectedIndices()[0];
            if (idx < records.size()) {
                return records.get(idx);
            }
        }
        return null;
    }
    
    /**
     * @return 
     * The current time sliders offset in seconds relative to the beginning
     * of the earliest record in the session
     */
    public double getTimeOffset() {
        return timeOffsetSlider.getValue() / 1000.0;
    }
    
    /**
     * Internal callback when a valid record is selected in the drop down
     * 
     * @param record
     * The record that is/was selected
     */
    private void onRecordSelected(Record record) {
        RecordMarkers markers = markersByID.get(record.id);
        mapView.setDisplayToFitPolyline(markers.route);
        
        // TODO make this happen on right click or something
        boolean EXPORT_CSV = false;
        if (EXPORT_CSV) {
            NavSampleCSVWriter.write(record.getNavSamples(), "/tmp/nav_data.csv");
        }
    }
    
    /**
     * Internal callback when the time slider's value changes
     * 
     * @param timeOffset 
     * The time offset in seconds
     */
    private void onTimeOffsetChanged(double timeOffset) {
        for (RecordMarkers tracer : markersByID.values()) {
            tracer.advToTime(timeOffset);
        }
        
        // force a redraw of the map markers
        map().repaint();
    }

    /**
     * @return 
     * The SessionView's JMapViewer
     */
    private JMapViewer map() {
        return mapView.map();
    }
    
    /**
     * Internal setter for the session
     * 
     * @param sessionPath 
     */
    private void setSession(Path sessionPath) {
        // TODO need a way of managing multiple sessions
        session = new Session(0);
        map().removeAllMapMarkers();
        records.clear();
        markersByID.clear();
        updateRecordListing();
    }
    
    /**
     * Internal method that clears and repopulates the record table. This
     * method is usually only called once when a new session is loaded into the
     * viewer.
     */
    private void updateRecordListing() {
        if (session == null) {
            return;
        }
        
        // clear all the old rows from the table
        int rows = recordTableModel.getRowCount();
        for (int rr=0; rr<rows; rr++) {
            recordTableModel.removeRow(rr);
        }
        
        // populate table with new records
        double maxTime = 0;// logest record is used to init the time slider
        for (Record record : session.getAllRecords()) {
            recordTableModel.addRow(new Object[]{true,"record_" + record.id});
            records.add(record);
            
            // create new tracer for the record
            RecordMarkers marker = new RecordMarkers(record,map());
            markersByID.put(record.id, marker);
            
            record.restoreNavSamples(this);
            double dur = record.getInfo().stopTime - record.getInfo().startTime;
            maxTime = Double.max(maxTime, dur);
        }
        
        // update time offset slider based on longest duration record
        timeOffsetSlider.setMinimum(0);
        timeOffsetSlider.setMaximum((int) (maxTime * 1000));
        timeOffsetSlider.setValue(0);
    }
    
    public static void main(String[] args) {
        Theme.applyGlobalTheme();
        
        JFrame frame = new JFrame("Session View Standalone");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(1200, 800);
        frame.add(new SessionView());
        frame.setVisible(true);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        topBar = new javax.swing.JPanel();
        sessionLabel = new javax.swing.JLabel();
        sessionSelector = new javax.swing.JComboBox<>();
        recordPane = new javax.swing.JScrollPane();
        recordTable = new javax.swing.JTable();
        centerPane = new javax.swing.JPanel();
        mapView = new com.solodata.ui.maps.BasicMapView();
        timeOffsetSlider = new javax.swing.JSlider();
        bottomBar = new javax.swing.JPanel();
        progressBar = new javax.swing.JProgressBar();
        progressLabel = new javax.swing.JLabel();

        setLayout(new java.awt.BorderLayout());

        topBar.setMinimumSize(new java.awt.Dimension(100, 10));
        topBar.setPreferredSize(new java.awt.Dimension(400, 30));
        topBar.setLayout(new javax.swing.BoxLayout(topBar, javax.swing.BoxLayout.LINE_AXIS));

        sessionLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        sessionLabel.setText("Session: ");
        topBar.add(sessionLabel);

        sessionSelector.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        topBar.add(sessionSelector);

        add(topBar, java.awt.BorderLayout.NORTH);

        recordPane.setPreferredSize(new java.awt.Dimension(200, 348));

        recordTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        recordTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        recordTable.setMaximumSize(new java.awt.Dimension(2147483647, 200));
        recordPane.setViewportView(recordTable);

        add(recordPane, java.awt.BorderLayout.WEST);

        centerPane.setLayout(new java.awt.BorderLayout());

        mapView.setControlPanelVisible(false);
        centerPane.add(mapView, java.awt.BorderLayout.CENTER);
        centerPane.add(timeOffsetSlider, java.awt.BorderLayout.PAGE_END);

        add(centerPane, java.awt.BorderLayout.CENTER);

        bottomBar.setLayout(new javax.swing.BoxLayout(bottomBar, javax.swing.BoxLayout.LINE_AXIS));

        progressBar.setPreferredSize(new java.awt.Dimension(150, 12));
        bottomBar.add(progressBar);

        progressLabel.setText("Progress");
        bottomBar.add(progressLabel);

        add(bottomBar, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bottomBar;
    private javax.swing.JPanel centerPane;
    private com.solodata.ui.maps.BasicMapView mapView;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel progressLabel;
    private javax.swing.JScrollPane recordPane;
    private javax.swing.JTable recordTable;
    private javax.swing.JLabel sessionLabel;
    private javax.swing.JComboBox<String> sessionSelector;
    private javax.swing.JSlider timeOffsetSlider;
    private javax.swing.JPanel topBar;
    // End of variables declaration//GEN-END:variables

    // ------------------------------------------------------------------------
    // RecordRestoreListener
    // ------------------------------------------------------------------------
    
    @Override
    public void onProgressChanged(int progress, int maxProgress) {
        // do nothing
    }

    @Override
    public void onNavRestoreComplete(Record record) {
        RecordMarkers markers = markersByID.get(record.id);
        List<NavSampleOuterClass.NavSample> samples = record.getNavSamples();
        if (samples != null) {
            for (NavSampleOuterClass.NavSample sample : samples) {
                if (sample.hasGps() && sample.getGps().hasPosition()) {
                    NavDefn.Coordinate pos = sample.getGps().getPosition();
                    Coordinate coord = new Coordinate(pos.getLatDd(), pos.getLonDd());
                    markers.route.addPoint(coord);
                }
            }
        }
        markers.advToTime(getTimeOffset());
        markers.setVisible(true);
    }

    @Override
    public void onFailure(Record record, String reason) {
        Logger.getLogger(LOG_TAG).log(Level.SEVERE,
                "Record {0} failed to load!\nreason: ''{1}''",
                new Object[]{record.id, reason});
    }
}
