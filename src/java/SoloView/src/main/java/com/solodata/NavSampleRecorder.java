/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata;

import solodata.NavSampleOuterClass;

/**
 *
 * @author daniel
 */
public abstract class NavSampleRecorder {
    abstract
    public void close();
    
    abstract
    public boolean record(NavSampleOuterClass.NavSample samp);
}
