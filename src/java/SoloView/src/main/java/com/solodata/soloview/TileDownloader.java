/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.openstreetmap.gui.jmapviewer.TileXY;
import org.openstreetmap.gui.jmapviewer.interfaces.ICoordinate;
import org.openstreetmap.gui.jmapviewer.tilesources.AbstractTileSource;

/**
 *
 * @author daniel
 */
public class TileDownloader {
    private static final Logger logger = Logger.getLogger(TileDownloader.class.getName());
    
    public interface ProgressListener {
        /**
         * Update method that's called each time the TileDownloader starts
         * downloading tiles from a new zoom index.
         * 
         * @param minZoom
         * The minimum zoom index the download is configured for
         * @param maxZoom
         * The maximum zoom index the download is configured for
         * @param currZoom
         * The current zoom index the progress update is for
         */
        public void zoomProgressUpdate(int minZoom, int maxZoom, int currZoom);
        
        /**
         * Update method that's called each time the TileDownloader starts
         * downloading a new tile index.
         * 
         * @param minTile
         * The minimum tile index the download is configured for
         * @param maxTile
         * The maximum tile index the download is configured for
         * @param currTile 
         * The current tile index the progress update is for
         */
        public void tileProgressUpdate(int minTile, int maxTile, int currTile);
    };
    
    private final AbstractTileSource tileSource;
    
    private final String root;
    
    private JSONObject metadata = null;
    
    private ExecutorService executor;
    
    private ProgressListener progressListener = null;
    
    private class ZoomRange {
        int min = 0;
        int max = 0;
    }
    
    private class TileDownloadWorker implements Runnable {
        private final Logger logger = Logger.getLogger(TileDownloadWorker.class.getName());
        private final String urlStr;
        private final int zoom;
        private final int tilex;
        private final int tiley;
        
        public TileDownloadWorker(String urlStr, int zoom, int tilex, int tiley) {
            this.urlStr = urlStr;
            this.zoom = zoom;
            this.tilex = tilex;
            this.tiley = tiley;
        }
        
        @Override
        public void run() {
            File zoomDir = Paths.get(root,"tiles",String.format("z%d",zoom)).toFile();
            if ( ! zoomDir.exists()) {
                zoomDir.mkdirs();
            }

            String ext = "jpeg";// TODO get this properly
            Path tilePath = Paths.get(zoomDir.getPath(), String.format("x%dy%d.%s",tilex,tiley,ext));

            try {
                URL url = new URL(urlStr);
                ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
                FileOutputStream fileOutputStream = new FileOutputStream(tilePath.toString());
                FileChannel fileChannel = fileOutputStream.getChannel();
                fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
                Thread.sleep(10, 0);
            } catch (MalformedURLException ex) {
                logger.log(Level.SEVERE, null, ex);
            } catch (FileNotFoundException ex) {
                logger.log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
    };
    
    public TileDownloader(AbstractTileSource ats, String downloadPath) {
        tileSource = ats;
        String sourceName = ats.toString();
        sourceName = sourceName.toLowerCase();
        sourceName = sourceName.replace(' ','_');
        root = Paths.get(downloadPath,sourceName).toString();
        
        File dir = new File(root);
        dir.mkdirs();
        
        metadata = downloadMetadata();
        
        // create a thread pool for downloading tiles from source
        executor = Executors.newFixedThreadPool(10);
    }
    
    public void setProgressListener(ProgressListener l) {
        progressListener = l;
    }
    
    public void removeProgressListener() {
        progressListener = null;
    }
    
    public JSONObject downloadMetadata() {
        boolean okay = true;
        
        int minZoom = tileSource.getMinZoom();
        int maxZoom = tileSource.getMaxZoom();
        
        JSONObject zoomTileLimits = new JSONObject();
        for (int zoom = minZoom; zoom <= maxZoom; zoom++)
        {
            JSONObject limits = new JSONObject();
            limits.put("xMin", tileSource.getTileXMin(zoom));
            limits.put("xMax", tileSource.getTileXMax(zoom));
            limits.put("yMin", tileSource.getTileYMin(zoom));
            limits.put("yMax", tileSource.getTileYMax(zoom));
            
            zoomTileLimits.put(Integer.toString(zoom), limits);
        }
                
        JSONObject mdObj = new JSONObject();
        mdObj.put("minZoom",minZoom);
        mdObj.put("maxZoom",maxZoom);
        mdObj.put("zoomTileLimits",zoomTileLimits);
        
        File file = Paths.get(root, "metadata.json").toFile();
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(mdObj.toString(4).getBytes());
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        
        return mdObj;
    }
    
    public void approximateRegionDownload(int startZoom, int stopZoom, ICoordinate upperLeft, ICoordinate lowerRight) {
        ZoomRange zRange = sanitizeZoomRange(startZoom, stopZoom);
        
        for (int z=zRange.min; z<=zRange.max; z++) {
            TileXY ul = tileSource.latLonToTileXY(upperLeft, z);
            TileXY lr = tileSource.latLonToTileXY(lowerRight, z);
            int width = lr.getXIndex() - ul.getXIndex() + 1;
            int height = lr.getYIndex() - ul.getYIndex() + 1;
            
            logger.info(String.format("zoom %d: %d tiles",z,width*height));
        }
    }
    
    public boolean downloadRegion(int startZoom, int stopZoom, ICoordinate upperLeft, ICoordinate lowerRight) {
        boolean okay = true;
        
        new Thread() {
            @Override
            public void run() {
                ZoomRange zRange = sanitizeZoomRange(startZoom, stopZoom);
                logger.info(String.format("start = %d; stop = %d",startZoom,stopZoom));
                logger.info(String.format("minZoom = %d; maxZoom = %d", tileSource.getMinZoom(), tileSource.getMaxZoom()));
                logger.info(String.format("zRange.min = %d; zRange.max = %d",zRange.min,zRange.max));

                for (int z=zRange.min; z<=zRange.max; z++) {
                    if (progressListener != null) {
                        progressListener.zoomProgressUpdate(zRange.min, zRange.max, z);
                    }
                    downloadRegion(z, upperLeft, lowerRight);
                }
            }
        }.start();
        
        return okay;
    }
    
    public boolean downloadRegion(int zoom, ICoordinate upperLeft, ICoordinate lowerRight) {
        boolean okay = true;
        TileXY ul = tileSource.latLonToTileXY(upperLeft, zoom);
        TileXY lr = tileSource.latLonToTileXY(lowerRight, zoom);
        
        int width = lr.getXIndex() - ul.getXIndex() + 1;
        int height = lr.getYIndex() - ul.getYIndex() + 1;
        int totalTiles = width * height;
        int currTileIdx = 0;
        logger.fine(String.format("zoom : %d",zoom));
        for (int x=ul.getXIndex(); x<=lr.getXIndex(); x++) {
            logger.log(Level.FINE,
                    "ul.x = {0}; lr.x = {1}; x = {2}",
                    new Object[]{ul.getXIndex(),lr.getXIndex(),x});
            for (int y=ul.getYIndex(); y<=lr.getYIndex(); y++) {
                // notify progress update
                if (progressListener != null) {
                    progressListener.tileProgressUpdate(0, totalTiles, currTileIdx);
                }
                
                try {
                    logger.info(String.format("%d/%d tiles",currTileIdx,width*height));
                    String urlStr = tileSource.getTileUrl(zoom, x, y);
                    okay = downloadTile(urlStr, zoom, x, y) && okay;
                } catch (IOException ex) {
                    Logger.getLogger(TileDownloader.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                currTileIdx++;
            }
        }
        
        return okay;
    }
    
    public boolean downloadTile(int zoom, int tilex, int tiley) {
        boolean okay = true;
        
        try {
            String urlStr = tileSource.getTileUrl(zoom, tilex, tiley);
            okay = okay && downloadTile(urlStr, zoom, tilex, tiley);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            okay = false;
        }
        
        return okay;
    }
    
    public boolean downloadTile(String urlStr, int zoom, int tilex, int tiley) {
        Runnable worker = new TileDownloadWorker(urlStr, zoom, tilex, tiley);
        executor.execute(worker);
        
        return true;
    }
    
    private ZoomRange sanitizeZoomRange(int start, int stop) {
        ZoomRange range = new ZoomRange();
        range.min = start;
        range.max = stop;
        if (start > stop) {
            range.min = stop;
            range.max = start;
        }
        
        range.min = Integer.max(range.min, tileSource.getMinZoom());
        range.max = Integer.min(range.max, tileSource.getMaxZoom());
        
        return range;
    }
}
