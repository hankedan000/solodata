/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import solodata.NavSampleOuterClass;

/**
 *
 * @author daniel
 */
public class JsonNavSampleRecorder extends NavSampleRecorder {
    private static String LOG_TAG = JsonNavSampleRecorder.class.getName();
    private BufferedWriter bw_;
    
    public JsonNavSampleRecorder(String filepath) {
        File file = new File(filepath);
        
        try {
            // create file if it doesn't exist
            if ( ! file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            
            FileWriter fw = new FileWriter(file);
            bw_ = new BufferedWriter(fw);
        } catch (IOException ex) {
            Logger.getLogger(LOG_TAG).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean okay() {
        return bw_ != null;
    }
    
    @Override
    public void close() {
        if (bw_ == null) {
            return;
        }
        
        try {
            bw_.close();
        } catch (IOException ex) {
            Logger.getLogger(LOG_TAG).log(Level.WARNING, null, ex);
        }
        
        bw_ = null;
    }
    
    @Override
    public boolean record(NavSampleOuterClass.NavSample samp) {
        boolean okay = true;
        
        if (bw_ == null) {
            Logger.getLogger(LOG_TAG).log(Level.WARNING,
                    "Log writer is not opened! Dropping log.");
            return false;
        }
        
        try {
            bw_.write(JsonFormat.printer().print(samp) + ",\n");
        } catch (InvalidProtocolBufferException ex) {
            Logger.getLogger(LOG_TAG).log(Level.WARNING, null, ex);
            okay = false;
        } catch (IOException ex) {
            Logger.getLogger(LOG_TAG).log(Level.WARNING, null, ex);
            okay = false;
        }
        
        return okay;
    }
}
