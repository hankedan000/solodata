/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview;

import com.solodata.NavSampleRecorder;
import com.solodata.JsonNavSampleRecorder;
import com.solodata.JsonNavSampleLoader;
import com.solodata.NavSampleLoader;
import com.solodata.NavSampleSubscriber;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import solodata.NavSampleOuterClass;
import solodata.NavSampleOuterClass.NavSample;

/**
 *
 * @author daniel
 */
public class Record implements
        NavSampleSubscriber.NavSubListener,
        NavSampleLoader.NavSampleLoaderListener {
    private static final String LOG_TAG = Record.class.getName();
    
    public static class InvalidRecordException extends RuntimeException {
        public InvalidRecordException(String msg) {
            super(msg);
        }
    }
    
    public static interface RecordRestoreListener {
        abstract
        public void onProgressChanged(int progress, int maxProgress);
        
        abstract
        public void onNavRestoreComplete(Record record);
        
        abstract
        public void onFailure(Record record, String reason);
    }
    
    public static class Info {
        // UTC epoch time when started recording
        public double startTime = 0;
        // UTC epoch time when stopped recording
        public double stopTime = 0;
        // set to true if nav data is stored as Json
        public boolean hasJsonNavData = false;
        // the path to the json nav data
        public String jsonNavDataPath = "";
        // list of string tags
        public List<String> tags = new ArrayList<>();
        
        public Info() {
        }
        
        public Info(Info other) {
            startTime = other.startTime;
            stopTime = other.stopTime;
            hasJsonNavData = other.hasJsonNavData;
            jsonNavDataPath = other.jsonNavDataPath;
            tags = new ArrayList<>(other.tags);
        }
        
        public JSONObject toJSONObject() {
            JSONObject obj = new JSONObject();
            obj.put("startTime", startTime);
            obj.put("stopTime", stopTime);
            obj.put("hasJsonNavData", hasJsonNavData);
            obj.put("jsonNavDataPath", jsonNavDataPath);
            obj.put("tags",tags);
            return obj;
        }
        
        public void fromJSONObject(JSONObject obj) {
            startTime = obj.getDouble("startTime");
            stopTime = obj.getDouble("stopTime");
            hasJsonNavData = obj.getBoolean("hasJsonNavData");
            jsonNavDataPath = obj.getString("jsonNavDataPath");
            JSONArray tagsArray = obj.getJSONArray("tags");
            for (Object tag : tagsArray) {
                if (tag instanceof String) {
                    tags.add((String)tag);
                }
            }
        }
    }
    
    // unique identifier with respect to a session
    public final int id;
    
    // path to record directory
    private Path recordPath = null;
    
    // set to true if record was successfully initialized
    private boolean initialized = false;
    
    /**
     * Set to true if record is "read only". This happens after recording is
     * finished, or if recording is restored from disk (ie. previously recorded)
     */
    private boolean readOnly = false;
    
    // set to true if actively recording
    private boolean recording = false;
    
    // list of navigation sample recorders
    private List<NavSampleRecorder> navSampleRecorders = new ArrayList<>();
    
    private final Info info = new Info();
    
    private RecordRestoreListener rrl = null;
    
    /**
     * recorded/restored list of NavSamples
     * 
     * Note: NavSample restoration is optional when restore a record from disk.
     * This member will remain null, unless the nav samples were specified to
     * be restored.
     */
    private List<NavSampleOuterClass.NavSample> navSamples = null;
    
    /**
     * Constructs a new record within a session directory
     * 
     * @param sessionPath
     * path to an existing session
     * 
     * @param id 
     * unique identifier for the record. if the record already exists then
     * record create will fail
     */
    public Record(Path sessionPath, int id) {
        this.id = id;
        recordPath = Paths.get(sessionPath.toString(), Integer.toString(id));
        readOnly = false;
        
        initRecordDir();// throws exception if failed
        
        // only get here if initRecordDir() doesn't raise
        initialized = true;
    }
    
    /**
     * Private constructor used to restore records from disk
     * @param id 
     */
    private Record(int id) {
        this.id = id;
    }
    
    /**
     * Restore a previously recorded record from file system
     * 
     * @param recordDir 
     * @param restoreSamples 
     * @param rrl 
     * @return  
     */
    public static Record restore(Path recordDir, boolean restoreSamples, RecordRestoreListener rrl) {
        String dirname = recordDir.getFileName().toString();
        int id = -1;
        try {
            id = Integer.parseInt(dirname);
        } catch (NumberFormatException nfe) {
            throw new InvalidRecordException(
                    "'" + recordDir.toString() + "' is not a valid record");
        }
        
        Record record = new Record(id);
        record.recordPath = recordDir;
        record.restoreInfo();
        record.readOnly = true;
        record.initialized = true;
        
        if (restoreSamples) {
            record.restoreNavSamples(rrl);
        }
        
        return record;
    }
    
    public List<NavSample> getNavSamples() {
        return navSamples;
    }
    
    public void addTag(String tag) {
        info.tags.add(tag);
        saveInfo();
    }
    
    public void removeTag(String tag) {
        info.tags.remove(tag);
        saveInfo();
    }
    
    public boolean attachJsonNavRecorder() {
        if (readOnly) {
            Logger.getLogger(LOG_TAG).log(Level.WARNING,
                    "Cannot attach nav recorder on a read only record.");
            return false;
        } else if (recording) {
            Logger.getLogger(LOG_TAG).log(Level.WARNING,
                    "Cannot attach nav recorder while recording.");
            return false;
        }
        
        String JSON_FILENAME = "nav_samples.json";
        JsonNavSampleRecorder recorder = new JsonNavSampleRecorder(
            getJsonNavDataPath(JSON_FILENAME).toString());
        navSampleRecorders.add(recorder);
        info.hasJsonNavData = true;
        info.jsonNavDataPath = JSON_FILENAME;
        
        return true;
    }
    
    public boolean start() {
        if ( ! initialized) {
            Logger.getLogger(LOG_TAG).log(Level.SEVERE,
                    "Cannot start recording because record was not initialized"
                    + " successfully.");
            return false;
        } else if (readOnly) {
            Logger.getLogger(LOG_TAG).log(Level.SEVERE,
                    "Cannot start recording on a read only record.");
            return false;
        }
        
        navSamples = new ArrayList<>();
        // FIXME set this based on samples
        info.startTime = System.currentTimeMillis() / 1000.0;
        NavSampleSubscriber.getInstance().addListener(this);
        recording = true;
        
        return true;
    }
    
    public boolean stop() {
        if ( ! initialized) {
            Logger.getLogger(LOG_TAG).log(Level.SEVERE,
                    "Cannot stop recording because record was not initialized"
                    + " successfully.");
            return false;
        } else if (readOnly) {
            Logger.getLogger(LOG_TAG).log(Level.SEVERE,
                    "Cannot stop recording on a read only record.");
            return false;
        } else if ( ! recording) {
            Logger.getLogger(LOG_TAG).log(Level.WARNING,
                    "stop() called, but recording was never started...");
            return false;
        }
        
        // FIXME set this based on samples
        info.stopTime = System.currentTimeMillis() / 1000.0;
        NavSampleSubscriber.getInstance().removeListener(this);
        saveInfo();
        recording = false;
        readOnly = true;
        
        // close all nav recorders
        for (NavSampleRecorder navRecorder : navSampleRecorders) {
            navRecorder.close();
        }
        
        return true;
    }
    
    public Info getInfo() {
        return new Info(info);
    }
    
    public Path getJsonNavDataPath() {
        if (info.hasJsonNavData) {
            return Paths.get(recordPath.toString(),info.jsonNavDataPath);
        }
        return null;
    }
    
    public void restoreNavSamples(RecordRestoreListener rrl) {
        this.rrl = rrl;
        if (info.hasJsonNavData) {
            Path jsonPath = Paths.get(
                    recordPath.toString(),
                    info.jsonNavDataPath);

            JsonNavSampleLoader loader = new JsonNavSampleLoader(jsonPath,this);
            loader.start();
            try {
                if (rrl == null) {
                    // no listener, so block until loader finishes
                    loader.join();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(LOG_TAG).log(Level.SEVERE, null, ex);
            }
        } else {
            Logger.getLogger(LOG_TAG).log(Level.WARNING,
                    "Restore for non-Json recorded nav samples is not"
                    + " supported yet.");
        }
    }
    
    private void saveInfo() {
        File file = getInfoPath().toFile();
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(info.toJSONObject().toString(4).getBytes());
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void restoreInfo() {
        File file = getInfoPath().toFile();
        try {
            FileInputStream fis = new FileInputStream(file);
            JSONTokener tokener = new JSONTokener(fis);
            JSONObject fileObj = new JSONObject(tokener);
            info.fromJSONObject(fileObj);
        } catch (FileNotFoundException ex) {
            throw new InvalidRecordException("info.json doesn't exist!");
        }
    }
    
    private Path getJsonNavDataPath(String filename) {
        return Paths.get(recordPath.toString(),filename);
    }
    
    private Path getInfoPath() {
        return Paths.get(recordPath.toString(),"info.json");
    }
    
    private void initRecordDir() {
        File recordDir = recordPath.toFile();
        if (recordDir.exists()) {
            throw new InvalidRecordException(
                    "Record directory already exists! " + recordPath.toString());
        } else {
            recordDir.mkdirs();
        }
    }

    // ------------------------------------------------------------------------
    // NavSubscriberListener
    // ------------------------------------------------------------------------
    
    @Override
    public void onNavSubConnectionChanged(boolean connected) {
        // do nothing
    }

    @Override
    public void onNavSubLinkValidChanged(boolean linkValid) {
        // do nothing
    }

    @Override
    public void onNavSubSampleReceived(NavSampleOuterClass.NavSample ns) {
        if ( ! recording) {
            return;
        }
        
        for (NavSampleRecorder navRecorder : navSampleRecorders) {
            navRecorder.record(ns);
            navSamples.add(ns);
        }
    }

    // ------------------------------------------------------------------------
    // NavSampleLoaderListener
    // ------------------------------------------------------------------------
    
    @Override
    public void onProgressChanged(int progress, int maxProgress) {
        if (rrl != null) {
            rrl.onProgressChanged(progress, maxProgress);
        }
    }

    @Override
    public void onLoadComplete(ArrayList<NavSample> samples) {
        navSamples = samples;
        if (rrl != null) {
            rrl.onNavRestoreComplete(this);
        }
    }

    @Override
    public void onLoadFailed(String reason) {
        navSamples = null;
        if (rrl != null) {
            rrl.onFailure(this,reason);
        }
    }
    
}
