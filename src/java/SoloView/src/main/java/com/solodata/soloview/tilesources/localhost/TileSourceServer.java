/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview.tilesources.localhost;

import com.solodata.soloview.Settings;
import com.sun.net.httpserver.HttpServer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.Time;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;

/**
 *
 * @author daniel
 */
public class TileSourceServer implements Settings.SettingListener, Runnable {
    private HttpServer server;
    private Thread mainThread = null;
    
    private int port;
    private int threads;
    
    // a timer used to restart the server when a setting has been modified
    private Timer reconfigTimer = null;
    
    public TileSourceServer() {
        this.port = Settings.getInstance().getLocalhostTileSourceSeverPort();
        this.threads = Settings.getInstance().getLocalhostTileSourceSeverThreads();
        Settings.getInstance().addSettingListener(this);
    }
    
    public TileSourceServer(int port, int numThreads) {
        this.port = port;
        this.threads = numThreads;
    }
    
    public void start() {
        if (mainThread != null && mainThread.isAlive()) {
            throw new IllegalStateException(
                    "Can not start server while it's already running!");
        }
        
        mainThread = new Thread(this);
        mainThread.start();
        Logger.getLogger(TileSourceServer.class.getName()).log(
                Level.INFO,
                "TileSource server started!");
    }
    
    public void stop() {
        Logger.getLogger(TileSourceServer.class.getName()).log(
                Level.INFO,
                "shutting down TileSource server...");
        // wait up to 1 seconds for exchanges to finish
        if (server != null) {
            server.stop(1);
        }
        
        try {
            mainThread.join(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(TileSourceServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void restart() {
        stop();
        start();
    }

    @Override
    public void onSettingChanged(String settingKey, Object oldValue, Object newValue) {
        boolean settingChanged = false;
        
        if (settingKey.compareTo(Settings.LOCALHOST_TILE_SOURCE_SERVER_PORT_KEY) == 0) {
            port = (int)newValue;
            settingChanged = true;
        } else if (settingKey.compareTo(Settings.LOCALHOST_TILE_SOURCE_SERVER_THREADS_KEY) == 0) {
            threads = (int)newValue;
            settingChanged = true;
        }
        
        if (settingChanged) {
            if (reconfigTimer != null) {
                /**
                 * Already had a timer running, stop it and start another. This
                 * can occur if settings are modified back-to-back prior to us
                 * being able to restart the server.
                 */
                reconfigTimer.stop();
            }
            
            reconfigTimer = new Timer(500, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                    Logger.getLogger(TileSourceServer.class.getName()).log(
                            Level.INFO,
                            String.format(
                                    "Reconfiguring TileSourceServer... port = %d; threads = %d",
                                    port,
                                    threads));
                    
                    // cancel this timer
                    reconfigTimer.stop();
                    reconfigTimer = null;
                    
                    // restart the server
                    restart();
                }
            });
            reconfigTimer.start();
        }
    }

    @Override
    public void run() {
        ThreadPoolExecutor pool = (ThreadPoolExecutor)Executors.newFixedThreadPool(threads);
        try {
            server = HttpServer.create(new InetSocketAddress("localhost", port), 0);
            server.createContext("/", new TileHandler());
            server.setExecutor(pool);
            server.start();
        } catch (IOException ex) {
            Logger.getLogger(TileSourceServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        TileSourceServer server = new TileSourceServer();
        server.start();
    }
}
