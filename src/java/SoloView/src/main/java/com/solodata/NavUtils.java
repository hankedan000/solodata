/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata;

/**
 *
 * @author daniel
 */
public class NavUtils {
    /**
     * Convert distance in meter to decimal degrees
     * @param valMeters
     * @return 
     */
    public static double metersToDecDeg(double valMeters) {
        return valMeters * (0.001 / 111.0);
    }
}
