/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniel
 */
public class Session {
    private static String LOG_TAG = Session.class.getName();
    public final int id;
    
    private boolean recording = false;
    private final Map<Integer,Record> recordsByID = new HashMap<>();
    private int nextRecordID = 0;
    private Record currRecord = null;
    
    public Session(int id) {
        this.id = id;
        // initialize session directory
        initSession(id);
    }
    
    public void start() {
        if (recording) {
            Logger.getLogger(LOG_TAG).log(Level.WARNING,
                    "start() requested while already recording!");
            return;
        }
        
        // create new record and set recording flag according to success
        currRecord = createRecord(id, nextRecordID());
        recording = currRecord != null;
        recording = recording && currRecord.attachJsonNavRecorder();
        recording = recording && currRecord.start();
    }
    
    public void stop() {
        if ( ! recording) {
            Logger.getLogger(LOG_TAG).log(Level.WARNING,
                    "stop() requested while not recording!");
            return;
        }
        
        recording = false;
        if (currRecord != null) {
            currRecord.stop();
        }
    }
    
    public boolean isRecording() {
        return recording;
    }
    
    public Collection<Record> getAllRecords() {
        return recordsByID.values();
    }
    
    private void initSession(int sessionID) {
        // init session dir
        Path sessionPath = getSessionPath(sessionID);
        File sessionDir = sessionPath.toFile();
        sessionDir.mkdirs();
        
        // create directory to store records in
        Path recordsPath = getRecordsPath(sessionID);
        File recordsDir = recordsPath.toFile();
        recordsDir.mkdirs();
        
        // discover all existing records in session
        int highestID = -1;
        for (File recordDir : recordsDir.listFiles()) {
            Logger.getLogger(LOG_TAG).log(Level.FINE,
                    "Attempting to restore '{0}'",recordDir.toString());
            
            try {
                Record restoredRecord = Record.restore(recordDir.toPath(),false,null);
                recordsByID.put(restoredRecord.id, restoredRecord);
                highestID = Integer.max(restoredRecord.id, highestID);
                Logger.getLogger(LOG_TAG).log(Level.FINE,
                        "Restored record '{0}'",recordDir.toString());
            } catch (Record.InvalidRecordException ire) {
                continue;
            }
        }
        nextRecordID = highestID + 1;
    }
    
    private int nextRecordID() {
        return nextRecordID;
    }
    
    private Record createRecord(int sessionID, int recordID) {
        Record newRecord = null;
        
        // create new record
        try {
            newRecord = new Record(getRecordsPath(sessionID),recordID);
        } catch (RuntimeException re) {
            Logger.getLogger(LOG_TAG).log(Level.SEVERE,
                    "Failed to create new record for session {0}, record {1}",
                    new Object[]{sessionID,recordID});
            newRecord = null;
        }
        
        if (newRecord != null) {
            recordsByID.put(recordID,newRecord);
            nextRecordID++;
        }
        
        return newRecord;
    }
    
    private Path getSessionPath(int sessionID) {
        return Paths.get(
            DirUtils.getDataPath().toString(),
            "session" + Integer.toString(sessionID));
    }
    
    private Path getRecordsPath(int sessionID) {
        return Paths.get(
                getSessionPath(sessionID).toString(),
                "records");
    }
    
}
