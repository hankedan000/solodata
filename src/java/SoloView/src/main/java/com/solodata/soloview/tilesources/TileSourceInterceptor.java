/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview.tilesources;

import com.solodata.soloview.DirUtils;
import com.solodata.soloview.TileDownloader;
import java.io.IOException;
import org.openstreetmap.gui.jmapviewer.tilesources.AbstractTileSource;

/**
 *
 * @author daniel
 */
public class TileSourceInterceptor extends ProxyTileSource {
    
    private final AbstractTileSource tileSource;
    
    private final TileDownloader downloader;
    
    public TileSourceInterceptor (AbstractTileSource ats) {
        super(ats);
        tileSource = ats;
        downloader = new TileDownloader(
                ats,
                DirUtils.getOfflineTilesPath().toString());
    }

    @Override
    public String toString() {
        return String.format("%s (interceptor)",tileSource.toString());
    }

    @Override
    public String getTileUrl(int zoom, int tilex, int tiley) throws IOException {
        String urlStr = super.getTileUrl(zoom, tilex, tiley);
        
        downloader.downloadTile(urlStr, zoom, tilex, tiley);
        
        return urlStr;
    }
}
