/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview.tilesources.localhost;

import com.solodata.soloview.DirUtils;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniel
 */
public class TileHandler implements HttpHandler {
    private final String root;
    
    public TileHandler() {
        Path offlineTilesDir = DirUtils.getOfflineTilesPath();
        offlineTilesDir.toFile().mkdirs();
        root = offlineTilesDir.toString();
    }

    @Override
    public void handle(HttpExchange t) throws IOException {
        URI uri = t.getRequestURI();
        Logger.getLogger(TileHandler.class.getName()).log(
                Level.FINE, "Looking for file : {0}", uri.getPath());
        String path = uri.getPath();
        File file = new File(root + path).getCanonicalFile();

        if (!file.isFile()) {
            // Object does not exist or is not a file: reject with 404 error.
            String response = "404 (Not Found)\n";
            t.sendResponseHeaders(404, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        } else {
            // Object exists and is a file: accept with response code 200.
            String mime = "text/html";
            if (path.substring(path.length() - 3).equals(".js")) {
                mime = "application/javascript";
            }
            if (path.substring(path.length() - 3).equals("css")) {
                mime = "text/css";
            }
            if (path.substring(path.length() - 4).equals("jpeg")) {
                mime = "image/jpeg";
            }

            Headers h = t.getResponseHeaders();
            h.set("Content-Type", mime);
            t.sendResponseHeaders(200, 0);

            OutputStream os = t.getResponseBody();
            FileInputStream fs = new FileInputStream(file);
            final byte[] buffer = new byte[0x10000];
            int count = 0;
            while ((count = fs.read(buffer)) >= 0) {
                os.write(buffer, 0, count);
            }
            fs.close();
            os.close();
        }
    }

}
