/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview.gui;

import com.solodata.ui.maps.RecordMapMarkers;
import com.solodata.datalogger.Client;
import com.solodata.datalogger.Record;
import com.solodata.utils.FileUtils;
import com.solodata.soloview.Settings;
import com.solodata.ui.maps.tiles.LocalhostTileSource;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.commons.net.ftp.FTPFile;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import solodata.DataLogger.DataLogger;
import solodata.NavDefn;
import solodata.NavSampleOuterClass;

/**
 *
 * @author daniel
 */
public class LogViewer extends javax.swing.JPanel implements Client.DataLoggerListener{
    private static final String LOG_TAG = LogViewer.class.getName();
    private static final Logger logger = Logger.getLogger(LOG_TAG);
    
    // the DataLoggerClient we're connected to
    private com.solodata.datalogger.Client client = null;
    
    private final DefaultListModel<String> recordListModel = new DefaultListModel<>();
    
    // used to manually resize some panes the first time GUI is opened
    private boolean isFirstResize = true;
    
    private final Map<String,com.solodata.datalogger.Record> recordsByName = new HashMap<>();
    
    // map of markers keyed by record file name
    private final Map<String,RecordMapMarkers> markersByName = new HashMap<>();

    /**
     * Creates new form LogViewer
     */
    public LogViewer() {
        initComponents();
        
        mapView.setControlPanelVisible(false);
        
        if (Settings.getInstance().getOfflineTileSourceOverride()) {
            String offlineTileSource = Settings.getInstance().getOfflineTileSource();
            mapView.setTileSource(new LocalhostTileSource(offlineTileSource));
        }
        
        recordList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        recordList.setModel(recordListModel);
        recordList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent evt) {
                if (evt.getValueIsAdjusting()) {
                    // wait for final adjustment before handling event
                    return;
                }
                
                String selectedRecord = recordList.getSelectedValue();
                if (selectedRecord != null) {
                    logger.log(Level.FINER,"Selected record {0}",selectedRecord);
                    onRecordSelected(selectedRecord);
                }
            }
        });
        
        // populate with empty record to init pane
        populatePropertiesPanel(null);
        
        addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent evt) {
                if (isFirstResize) {
                    topBottomSplitView.setDividerLocation(0.75);
                    leftRightSplitView.setDividerLocation(0.25);
                    isFirstResize = false;
                }
            }

            @Override
            public void componentMoved(ComponentEvent evt) {
            }

            @Override
            public void componentShown(ComponentEvent evt) {
            }

            @Override
            public void componentHidden(ComponentEvent evt) {
            }
        });
    }

    public void init(com.solodata.datalogger.Client client) {
        if (client == null) {
            return;
        } else if (this.client != null) {
            logger.warning("LogViewer already initialized with a client");
            return;
        }
        
        this.client = client;
        this.client.addListener(this);
        reloadRecordList();
    }
    
    public JMapViewer map() {
        return mapView.map();
    }
    
    private void reloadRecordList() {
        logger.log(Level.FINE,"Reloading record list...");
        recordListModel.clear();
        
        if (client != null) {
            FTPFile[] recordFiles = client.listRemoteFiles();
            for (FTPFile file : recordFiles) {
                String name = file.getName();
                if (name.equals(".") || name.equals("..")) {
                    continue;
                }
                addRecordToList(name);
            }
        }
    }
    
    private void addRecordToList(String filename) {
        logger.log(Level.FINE,"Adding {0}",new Object[]{filename});
        recordListModel.addElement(filename);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        toolbar = new javax.swing.JToolBar();
        refreshButton = new javax.swing.JButton();
        topBottomSplitView = new javax.swing.JSplitPane();
        leftRightSplitView = new javax.swing.JSplitPane();
        recordScrollPane = new javax.swing.JScrollPane();
        recordList = new javax.swing.JList<>();
        mapView = new com.solodata.ui.maps.BasicMapView();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        recordPropertiesPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        recordNameLabel = new javax.swing.JLabel();
        recordNameTextField = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        recordSizeLabel = new javax.swing.JLabel();
        recordSizeTextField = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        recordNavSizeLabel = new javax.swing.JLabel();
        recordNavSizeTextField = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        recordCAN_SizeLabel = new javax.swing.JLabel();
        recordCAN_SizeTextField = new javax.swing.JTextField();

        setLayout(new java.awt.BorderLayout());

        toolbar.setFloatable(false);
        toolbar.setRollover(true);

        refreshButton.setText("Refresh");
        refreshButton.setFocusable(false);
        refreshButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        refreshButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        refreshButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshButtonActionPerformed(evt);
            }
        });
        toolbar.add(refreshButton);

        add(toolbar, java.awt.BorderLayout.NORTH);

        topBottomSplitView.setDividerLocation(100);
        topBottomSplitView.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        topBottomSplitView.setResizeWeight(1.0);

        leftRightSplitView.setDividerLocation(150);

        recordScrollPane.setMinimumSize(new java.awt.Dimension(150, 20));

        recordList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        recordScrollPane.setViewportView(recordList);

        leftRightSplitView.setLeftComponent(recordScrollPane);
        leftRightSplitView.setRightComponent(mapView);

        topBottomSplitView.setTopComponent(leftRightSplitView);

        recordPropertiesPanel.setLayout(new java.awt.GridLayout(4, 0));

        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.LINE_AXIS));

        recordNameLabel.setText("Record Name:");
        jPanel1.add(recordNameLabel);

        recordNameTextField.setEditable(false);
        recordNameTextField.setText("jTextField1");
        jPanel1.add(recordNameTextField);

        recordPropertiesPanel.add(jPanel1);

        jPanel2.setLayout(new javax.swing.BoxLayout(jPanel2, javax.swing.BoxLayout.LINE_AXIS));

        recordSizeLabel.setText("Record Size:");
        jPanel2.add(recordSizeLabel);

        recordSizeTextField.setEditable(false);
        recordSizeTextField.setText("jTextField1");
        jPanel2.add(recordSizeTextField);

        recordPropertiesPanel.add(jPanel2);

        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.LINE_AXIS));

        recordNavSizeLabel.setText("Nav Sample Count:");
        jPanel3.add(recordNavSizeLabel);

        recordNavSizeTextField.setEditable(false);
        recordNavSizeTextField.setText("jTextField1");
        jPanel3.add(recordNavSizeTextField);

        recordPropertiesPanel.add(jPanel3);

        jPanel4.setLayout(new javax.swing.BoxLayout(jPanel4, javax.swing.BoxLayout.LINE_AXIS));

        recordCAN_SizeLabel.setText("CAN Sample Count:");
        jPanel4.add(recordCAN_SizeLabel);

        recordCAN_SizeTextField.setEditable(false);
        recordCAN_SizeTextField.setText("jTextField2");
        jPanel4.add(recordCAN_SizeTextField);

        recordPropertiesPanel.add(jPanel4);

        jTabbedPane1.addTab("Properties", recordPropertiesPanel);

        topBottomSplitView.setBottomComponent(jTabbedPane1);

        add(topBottomSplitView, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private Record loadRecord(String recordFile) {
        Record record = null;
        
        // see if we already have the record loaded
        if (recordsByName.containsKey(recordFile)) {
            record = recordsByName.get(recordFile);
        }
        
        if (record == null && client != null) {
            // download record from FTP server
            File downloadDir = new File(FileUtils.getTempDir(),"download");
            if (downloadDir == null) {
                logger.severe("downloadDir is null!");
            }
            if (recordFile == null) {
                logger.severe("recordFile is null!");
            }
            File downloadZip = new File(downloadDir,recordFile);
            if (client.retrieveFile(recordFile, downloadZip)) {
                record = Record.fromZip(downloadZip);

                // cache record
                recordsByName.put(recordFile, record);

                // clean up download once we've unzipped
                downloadZip.delete();
            } else {
                logger.log(Level.SEVERE,
                    "Failed to retrieve record file {0}.",recordFile);
            }
        }
        
        return record;
    }
    
    private void populatePropertiesPanel(Record record) {
        
        String recordName = "";
        int recordSize = 0;
        long recordNavSize = 0;
        long recordCAN_Size = 0;
        
        if (record != null) {
            recordName = record.getName();
            recordNavSize = record.navSize();
            recordCAN_Size = record.canSize();
        }
        
        recordNameTextField.setText(recordName);
        recordSizeTextField.setText(String.format("%d bytes", recordSize));
        recordNavSizeTextField.setText(String.format("%d",recordNavSize));
        recordCAN_SizeTextField.setText(String.format("%d",recordCAN_Size));
    }
    
    private void populateMapMarkers(Record record) {
        RecordMapMarkers markers = new RecordMapMarkers(record,map());
        List<NavSampleOuterClass.NavSample> samples = record.getNavSamples();
        if (samples != null) {
            for (NavSampleOuterClass.NavSample sample : samples) {
                if (sample.hasGps() && sample.getGps().hasPosition()) {
                    NavDefn.Coordinate pos = sample.getGps().getPosition();
                    Coordinate coord = new Coordinate(pos.getLatDd(), pos.getLonDd());
                    markers.route.addPoint(coord);
                }
            }
        }
        
        markers.setVisible(true);
        // TODO add time slider back
//        markers.advToTime(getTimeOffset());
        markers.advToTime(0);
        mapView.setDisplayToFitPolyline(markers.route);
        markersByName.put(record.getName(),markers);
    }
    
    private void onRecordSelected(String recordZipFile) {
        Record record = loadRecord(recordZipFile);
        
        if (record != null) {
            populatePropertiesPanel(record);
            
            String recordName = record.getName();
            if (markersByName.containsKey(recordName)) {
                RecordMapMarkers markers = markersByName.get(recordName);
                mapView.setDisplayToFitPolyline(markers.route);
            } else {
                populateMapMarkers(record);
            }
        }
    }
    
    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshButtonActionPerformed
        reloadRecordList();
    }//GEN-LAST:event_refreshButtonActionPerformed

    public static void main(String[] args) {
        Theme.applyGlobalTheme();
        
        JFrame frame = new JFrame("LogViewer Standalone");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(1200, 800);
        frame.setVisible(true);
        
        LogViewer viewer = new LogViewer();
        frame.add(viewer);
        
        Client dlc = new Client();
        if (dlc.connect("localhost")) {
            viewer.init(dlc);
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JSplitPane leftRightSplitView;
    private com.solodata.ui.maps.BasicMapView mapView;
    private javax.swing.JLabel recordCAN_SizeLabel;
    private javax.swing.JTextField recordCAN_SizeTextField;
    private javax.swing.JList<String> recordList;
    private javax.swing.JLabel recordNameLabel;
    private javax.swing.JTextField recordNameTextField;
    private javax.swing.JLabel recordNavSizeLabel;
    private javax.swing.JTextField recordNavSizeTextField;
    private javax.swing.JPanel recordPropertiesPanel;
    private javax.swing.JScrollPane recordScrollPane;
    private javax.swing.JLabel recordSizeLabel;
    private javax.swing.JTextField recordSizeTextField;
    private javax.swing.JButton refreshButton;
    private javax.swing.JToolBar toolbar;
    private javax.swing.JSplitPane topBottomSplitView;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onDataLoggerConnectionChanged(boolean connected) {
        reloadRecordList();
    }

    @Override
    public void onDataLoggerLinkValidChanged(boolean linkValid) {
        reloadRecordList();
    }

    @Override
    public void onDataLoggerStatus(DataLogger.Status_msg status) {
        for (String recordFile : status.getNewRecordsList())
        {
            addRecordToList(recordFile);
        }
    }

    @Override
    public void onRecordingStarted() {
        // do nothing
    }

    @Override
    public void onRecordingStopped() {
        // so nothing
    }

    @Override
    public void onRecordingPaused() {
        // do nothing
    }
}
