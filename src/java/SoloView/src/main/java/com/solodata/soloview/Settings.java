/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author daniel
 */
public class Settings extends com.solodata.Settings {
    private static Settings instance = null;
    
    private static final String SETTINGS_FILENAME = "settings.json";
    
    public static final String DARK_THEME_KEY = "darkTheme";
    public static final String TOUCH_SCREEN_MODE_KEY = "touchScreenMode";
    public static final String LOCALHOST_TILE_SOURCE_SERVER_PORT_KEY = "localhostTileSourceServerPort";
    public static final String LOCALHOST_TILE_SOURCE_SERVER_THREADS_KEY = "localhostTileSourceServerThreads";
    public static final String NAV_PUBLISHER_HOST_KEY = "navPublisherHost";
    public static final String NAV_PUBLISHER_PORT_KEY = "navPublisherPort";
    public static final String OFFLINE_TILESOURCE_OVERRIDE_KEY = "offlineTileSourceOverride";
    public static final String ONLINE_TILESOURCE_KEY = "onlineTileSource";
    public static final String OFFLINE_TILESOURCE_KEY = "offlineTileSource";
    public static final String MAXIMIZE_ON_START_KEY = "maximizeOnStart";
    
    private Settings() {
        super(getUserFilepath(SETTINGS_FILENAME).toString());
    }
    
    @Override
    protected final void loadDefaults() {
        checkAndSetDefault(DARK_THEME_KEY,true);
        checkAndSetDefault(TOUCH_SCREEN_MODE_KEY,false);
        checkAndSetDefault(LOCALHOST_TILE_SOURCE_SERVER_PORT_KEY,8787);
        checkAndSetDefault(LOCALHOST_TILE_SOURCE_SERVER_THREADS_KEY,5);
        checkAndSetDefault(NAV_PUBLISHER_HOST_KEY,"localhost");
        checkAndSetDefault(NAV_PUBLISHER_PORT_KEY,6060);
        checkAndSetDefault(OFFLINE_TILESOURCE_OVERRIDE_KEY,false);
        checkAndSetDefault(ONLINE_TILESOURCE_KEY,"public_transport");
        checkAndSetDefault(OFFLINE_TILESOURCE_KEY,"");
        checkAndSetDefault(MAXIMIZE_ON_START_KEY,false);
    }
    
    private static Path getUserFilepath(String filePath) {
        return Paths.get(
                System.getProperty("user.home"),
                ".soloview",
                filePath);
    }
    
    // -----------------
    
    public boolean getDarkTheme() {
        return (boolean)getSetting(DARK_THEME_KEY);
    }
    
    public void setDarkTheme(boolean darkTheme) {
        modifySettingAndNotify(DARK_THEME_KEY,darkTheme);
    }
    
    // -----------------
    
    public boolean getTouchScreenMode() {
        return (boolean)getSetting(TOUCH_SCREEN_MODE_KEY);
    }
    
    public void setTouchScreenMode(boolean touchScreenMode) {
        modifySettingAndNotify(TOUCH_SCREEN_MODE_KEY,touchScreenMode);
    }
    
    // -----------------
    
    public int getLocalhostTileSourceSeverPort() {
        return (int)getSetting(LOCALHOST_TILE_SOURCE_SERVER_PORT_KEY);
    }
    
    public void setLocalhostTileSourceSeverPort(int port) {
        modifySettingAndNotify(LOCALHOST_TILE_SOURCE_SERVER_PORT_KEY,port);
    }
    
    // -----------------
    
    public int getLocalhostTileSourceSeverThreads() {
        return (int)getSetting(LOCALHOST_TILE_SOURCE_SERVER_THREADS_KEY);
    }
    
    public void setLocalhostTileSourceSeverThreads(int threads) {
        modifySettingAndNotify(LOCALHOST_TILE_SOURCE_SERVER_THREADS_KEY,threads);
    }
    
    // -----------------
    
    public String getNavPublisherHost() {
        return (String)getSetting(NAV_PUBLISHER_HOST_KEY);
    }
    
    public void setNavPublisherHost(String host) {
        modifySettingAndNotify(NAV_PUBLISHER_HOST_KEY,host);
    }
    
    // -----------------
    
    public int getNavPublisherPort() {
        return (int)getSetting(NAV_PUBLISHER_PORT_KEY);
    }
    
    public void setNavPublisherPort(int port) {
        modifySettingAndNotify(NAV_PUBLISHER_PORT_KEY,port);
    }
    
    // -----------------
    
    public boolean getOfflineTileSourceOverride() {
        return (boolean)getSetting(OFFLINE_TILESOURCE_OVERRIDE_KEY);
    }
    
    public void setOfflineTileSourceOverride(boolean enabled) {
        modifySettingAndNotify(OFFLINE_TILESOURCE_OVERRIDE_KEY,enabled);
    }
    
    // -----------------
    
    public String getOnlineTileSource() {
        return (String)getSetting(ONLINE_TILESOURCE_KEY);
    }
    
    public void setOnlineTileSource(String source) {
        modifySettingAndNotify(ONLINE_TILESOURCE_KEY,source);
    }
    
    // -----------------
    
    public String getOfflineTileSource() {
        return (String)getSetting(OFFLINE_TILESOURCE_KEY);
    }
    
    public void setOfflineTileSource(String source) {
        modifySettingAndNotify(OFFLINE_TILESOURCE_KEY,source);
    }
    
    // -----------------
    
    public boolean getMaximizeOnStart() {
        return (boolean)getSetting(MAXIMIZE_ON_START_KEY);
    }
    
    public void setMaximizeOnStart(boolean maximizeOnStart) {
        modifySettingAndNotify(MAXIMIZE_ON_START_KEY,maximizeOnStart);
    }
    
    // -----------------
    
    public static Settings getInstance() {
        if (instance == null) {
            instance = new Settings();
        }
        
        return instance;
    }
    
}
