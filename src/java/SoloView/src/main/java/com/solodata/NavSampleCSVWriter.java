/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata;

import com.opencsv.CSVWriter;
import com.solodata.soloview.gui.SessionView;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import solodata.NavSampleOuterClass;

/**
 *
 * @author daniel
 */
public class NavSampleCSVWriter {
    private static final String LOG_TAG = NavSampleCSVWriter.class.getName();
    
    public static boolean write(List<NavSampleOuterClass.NavSample> samps, String pathname) {
        boolean okay = true;
        
        List<String[]> data = new ArrayList<>();
        data.add(new String[]{
            "Time",
            "AccX","AccY","AccZ",
            "MagX","MagY","MagZ",
            "GyroX","GyroY","GyroZ",
            "OrientationW","OrientationX","OrientationY","OrientationZ"});
        
        double firstTime = Double.NaN;
        for (NavSampleOuterClass.NavSample samp : samps) {
            for (int i=0; i<samp.getImuSampCount(); i++) {
                if (Double.isNaN(firstTime)) {
                    firstTime = samp.getImuSamp(i).getTimestamp()/1.0e6;
                }
                data.add(new String[]{
                    Double.toString(samp.getImuSamp(i).getTimestamp()/1.0e6 - firstTime),
                    Double.toString(samp.getImuSamp(i).getAcc().getX()),
                    Double.toString(samp.getImuSamp(i).getAcc().getY()),
                    Double.toString(samp.getImuSamp(i).getAcc().getZ()),
                    Double.toString(samp.getImuSamp(i).getMag().getX()),
                    Double.toString(samp.getImuSamp(i).getMag().getY()),
                    Double.toString(samp.getImuSamp(i).getMag().getZ()),
                    Double.toString(samp.getImuSamp(i).getGyro().getX()),
                    Double.toString(samp.getImuSamp(i).getGyro().getY()),
                    Double.toString(samp.getImuSamp(i).getGyro().getZ()),
                    Double.toString(samp.getImuSamp(i).getQuat().getW()),
                    Double.toString(samp.getImuSamp(i).getQuat().getX()),
                    Double.toString(samp.getImuSamp(i).getQuat().getY()),
                    Double.toString(samp.getImuSamp(i).getQuat().getZ())});
            }
        }

        try {
            FileWriter writer = new FileWriter(new File(pathname));
            CSVWriter csvWriter = new CSVWriter(writer,'\t','\0','\\',"\n");
            csvWriter.writeAll(data);
            csvWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(LOG_TAG).log(Level.SEVERE, null, ex);
            okay = false;
        }
        
        return okay;
    }
}
