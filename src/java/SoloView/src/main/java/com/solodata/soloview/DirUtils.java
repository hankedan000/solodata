/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author daniel
 */
public class DirUtils {
    public static final String SOLOVIEW_USER_DIR = ".soloview";
    
    public static final void initUserDirs() {
        getUserPath().toFile().mkdirs();
        getOfflineTilesPath().toFile().mkdirs();
    }
    
    public static final Path getUserPath() {
        return Paths.get(System.getProperty("user.home"),SOLOVIEW_USER_DIR);
    }
    
    public static final Path getOfflineTilesPath() {
        return Paths.get(getUserPath().toString(), "offline_tiles");
    }
    
    public static final Path getDataPath() {
        return Paths.get(getUserPath().toString(), "data");
    }
    
}
