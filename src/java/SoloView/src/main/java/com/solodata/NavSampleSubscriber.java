/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata;
import com.google.protobuf.InvalidProtocolBufferException;
import java.nio.channels.ClosedSelectorException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;
import org.zeromq.ZContext;
import solodata.NavSampleOuterClass;

/**
 *
 * @author daniel
 */
public class NavSampleSubscriber extends Thread {
    private static final String LOG_TAG = NavSampleSubscriber.class.getName();
    
    public interface NavSubListener {
        void onNavSubConnectionChanged(boolean connected);
        void onNavSubLinkValidChanged(boolean linkValid);
        void onNavSubSampleReceived(NavSampleOuterClass.NavSample ns);
    }
    
    private static NavSampleSubscriber instance = null;
    
    private final ZContext context;
    private final ZMQ.Socket socket;
    // a synchronized list of NavSub listeners
    private final List<NavSubListener> listeners;
    // set to false when the subscriber thread should shutdown and join
    private boolean stayAlive;
    // set to true if socket connected to host
    private boolean connected;
    // set to true once we receive samples from server
    private boolean linkValid;

    private NavSampleSubscriber()
    {
        context = new ZContext();
        socket = context.createSocket(SocketType.SUB);
        listeners = Collections.synchronizedList(new ArrayList<>());
        stayAlive = true;
        connected = false;
        linkValid = false;
    }
    
    public static NavSampleSubscriber getInstance() {
        if (instance == null) {
            instance = new NavSampleSubscriber();
        }
        
        return instance;
    }
    
    public boolean connect(String host, int port) {
        if (connected) {
            Logger.getLogger(LOG_TAG).log(Level.WARNING,
                    "connect() requested to host ''{0}'' and port ''{1}'''',"
                    + " but already connected",
                    new Object[]{host, port});
            return true;
        }
        
        connected = true;// assume true and modify going forward
        connected = connected && socket.connect(String.format("tcp://%s:%d",host,port));
        connected = connected && socket.subscribe("");
        notifyConnectionChanged(connected);
        return connected;
    }
    
    public boolean disconnect() {
        if (connected) {
            socket.close();
            connected = false;
            notifyConnectionChanged(connected);
        }
        return true;
    }
    
    public void addListener(NavSubListener listener) {
        listeners.add(listener);
    }
    
    public void removeListener(NavSubListener listener) {
        listeners.remove(listener);
    }

    public void requestShutdown() {
        Logger.getLogger(LOG_TAG).log(Level.FINE,
                "Shutdown requested...");
        disconnect();
        stayAlive = false;
    }
    
    private void notifyConnectionChanged(boolean connected) {
        for (NavSubListener listener : listeners)
        {
            // notify listener that we got a sample!
            listener.onNavSubConnectionChanged(connected);
        }
    }
    
    @Override
    public void run() {
        NavSampleOuterClass.NavSample ns;
        
        while (stayAlive) {
            try {
                byte[] data = socket.recv();
            
                ns = NavSampleOuterClass.NavSample.parseFrom(data);
                Logger.getLogger(LOG_TAG).log(Level.FINER, ns.toString());
                
                boolean linkValidChanged = false;
                if ( ! linkValid) {
                    // flag link as valid once we get the first sample
                    linkValid = true;
                    linkValidChanged = true;
                }
                
                for (NavSubListener listener : listeners)
                {
                    if (linkValidChanged) {
                        // notify listeners if link valid has changed states
                        listener.onNavSubLinkValidChanged(linkValid);
                    }
                    
                    // notify listener that we got a sample!
                    listener.onNavSubSampleReceived(ns);
                }
            } catch (InvalidProtocolBufferException ex) {
                Logger.getLogger(LOG_TAG).log(Level.WARNING,null, ex);
            } catch (ClosedSelectorException ex) {
                if (stayAlive) {
                    // it's expected to see weird exception when shutting down
                    Logger.getLogger(LOG_TAG).log(Level.WARNING,null, ex);
                }
            }
        }
    }
    
}
