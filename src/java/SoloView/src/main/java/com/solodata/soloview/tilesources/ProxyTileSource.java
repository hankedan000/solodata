/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview.tilesources;

import java.awt.Point;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.openstreetmap.gui.jmapviewer.Tile;
import org.openstreetmap.gui.jmapviewer.TileRange;
import org.openstreetmap.gui.jmapviewer.TileXY;
import org.openstreetmap.gui.jmapviewer.interfaces.ICoordinate;
import org.openstreetmap.gui.jmapviewer.interfaces.IProjected;
import org.openstreetmap.gui.jmapviewer.tilesources.AbstractTileSource;

/**
 *
 * @author daniel
 */
public class ProxyTileSource extends AbstractTileSource {
    private AbstractTileSource ts = null;
    
    public ProxyTileSource(AbstractTileSource ats) {
        ts = ats;
    }
    
    @Override
    public int getMaxZoom() {
        return ts.getMaxZoom();
    }

    @Override
    public int getMinZoom() {
        return ts.getMinZoom();
    }

    @Override
    public String getName() {
        return ts.getName();
    }

    @Override
    public String getId() {
        return ts.getId();
    }

    @Override
    public String getTileUrl(int zoom, int tilex, int tiley) throws IOException {
        return ts.getTileUrl(zoom, tilex, tiley);
    }

    @Override
    public String getTileId(int zoom, int tilex, int tiley) {
        return ts.getTileId(zoom, tilex, tiley);
    }

    @Override
    public int getTileSize() {
        return ts.getTileSize();
    }

    @Override
    public int getDefaultTileSize() {
        return ts.getDefaultTileSize();
    }

    @Override
    public double getDistance(double lat1, double lon1, double lat2, double lon2) {
        return ts.getDistance(lat1, lon1, lat2, lon2);
    }

    @Override
    public Point latLonToXY(double lat, double lon, int zoom) {
        return ts.latLonToXY(lat, lon, zoom);
    }

    @Override
    public Point latLonToXY(ICoordinate point, int zoom) {
        return ts.latLonToXY(point, zoom);
    }

    @Override
    public ICoordinate xyToLatLon(Point point, int zoom) {
        return ts.xyToLatLon(point, zoom);
    }

    @Override
    public ICoordinate xyToLatLon(int x, int y, int zoom) {
        return ts.xyToLatLon(x, y, zoom);
    }

    @Override
    public TileXY latLonToTileXY(double lat, double lon, int zoom) {
        return ts.latLonToTileXY(lat, lon, zoom);
    }

    @Override
    public TileXY latLonToTileXY(ICoordinate point, int zoom) {
        return ts.latLonToTileXY(point, zoom);
    }

    @Override
    public ICoordinate tileXYToLatLon(TileXY xy, int zoom) {
        return ts.tileXYToLatLon(xy, zoom);
    }

    @Override
    public ICoordinate tileXYToLatLon(Tile tile) {
        return ts.tileXYToLatLon(tile);
    }

    @Override
    public ICoordinate tileXYToLatLon(int x, int y, int zoom) {
        return ts.tileXYToLatLon(x, y, zoom);
    }

    @Override
    public int getTileXMax(int zoom) {
        return ts.getTileXMax(zoom);
    }

    @Override
    public int getTileXMin(int zoom) {
        return ts.getTileXMin(zoom);
    }

    @Override
    public int getTileYMax(int zoom) {
        return ts.getTileYMax(zoom);
    }

    @Override
    public int getTileYMin(int zoom) {
        return ts.getTileYMin(zoom);
    }

    @Override
    public Map<String, String> getMetadata(Map<String, List<String>> headers) {
        return ts.getMetadata(headers);
    }

    @Override
    public IProjected tileXYtoProjected(int x, int y, int zoom) {
        return ts.tileXYtoProjected(x, y, zoom);
    }

    @Override
    public TileXY projectedToTileXY(IProjected p, int zoom) {
        return ts.projectedToTileXY(p, zoom);
    }

    @Override
    public boolean isInside(Tile inner, Tile outer) {
        return ts.isInside(inner, outer);
    }

    @Override
    public TileRange getCoveringTileRange(Tile tile, int newZoom) {
        return ts.getCoveringTileRange(tile, newZoom);
    }

    @Override
    public String getServerCRS() {
        return ts.getServerCRS();
    }
    
}
