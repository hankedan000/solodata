/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata;

import com.google.protobuf.util.JsonFormat;
import com.solodata.soloview.Record;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import solodata.NavSampleOuterClass;
import solodata.NavSampleOuterClass.NavSample;

/**
 *
 * @author daniel
 */
public class JsonNavSampleLoader extends NavSampleLoader {
    private static final String LOG_TAG = JsonNavSampleLoader.class.getName();
    
    public JsonNavSampleLoader(Path datapath) {
        super(datapath, null);
    }
    
    public JsonNavSampleLoader(Path datapath, NavSampleLoaderListener listener) {
        super(datapath,listener);
    }
    
    @Override
    public void run() {
        super.run(); //To change body of generated methods, choose Tools | Templates.
        
        if (datapath == null) {
            if (listener != null) {
                listener.onLoadFailed("Json datapath is null");
            }
            return;
        }
        
        String reason = "";
        
        try {
            String dataStr = "[" + Files.readString(datapath) + "]";
            JSONTokener tokener = new JSONTokener(dataStr);
            JSONArray sampsArray = new JSONArray(tokener);
            NavSampleOuterClass.NavSample.Builder builder = NavSampleOuterClass.NavSample.newBuilder();
            int byteTotal = 0;
            synchronized (this) {
                // allocate memory for all samples up front
                samples.ensureCapacity(sampsArray.length());
                for (int i=0; i<sampsArray.length(); i++)
                {
                    builder.clear();
                    JSONObject sampObj = sampsArray.getJSONObject(i);
                    JsonFormat.parser().merge(sampObj.toString(), builder);
                    NavSample ns = builder.build();
                    byteTotal += ns.getSerializedSize();
                    samples.add(ns);
                    if (listener != null) {
                        listener.onProgressChanged(i,sampsArray.length());
                    }
                }
            }
            Logger.getLogger(LOG_TAG).log(Level.INFO,
                    "Restored " + byteTotal + " byte(s) from " + datapath.toString());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LOG_TAG).log(Level.SEVERE, null, ex);
            reason = "Caught FileNotFoundException.\n" + ex.getMessage();
        } catch (JSONException ex) {
            Logger.getLogger(LOG_TAG).log(Level.SEVERE, null, ex);
            reason = "Caught JSONException.\n" + ex.getMessage();
        } catch (IOException ex) {
            Logger.getLogger(Record.class.getName()).log(Level.SEVERE, null, ex);
            reason = "Caught IOException.\n" + ex.getMessage();
        }
        
        if (reason != "") {
            if (listener != null) {
                listener.onLoadFailed(reason);
            }
        } else {
            if (listener != null) {
                listener.onLoadComplete(samples);
            }
        }
    }
    
}
