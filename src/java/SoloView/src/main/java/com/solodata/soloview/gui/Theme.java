/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview.gui;

import com.solodata.soloview.Settings;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.UIManager;

/**
 *
 * @author daniel
 */
public class Theme {
    private static final Color START_BUTTON_COLOR = new Color(0,204,102);
    private static final Color STOP_BUTTON_COLOR = new Color(204,0,0);
    private static final Color DEFAULT_MAP_LINE_COLOR = new Color(0x2ec4ff);
    
    // must restart application to apply theme change
    private static final boolean isDark = Settings.getInstance().getDarkTheme();
    
    public static void applyGlobalTheme() {
        if (isDark) {
            UIManager.put( "control", new Color( 128, 128, 128) );
            UIManager.put( "info", new Color(128,128,128) );
            UIManager.put( "nimbusBase", new Color( 18, 30, 49) );
            UIManager.put( "nimbusAlertYellow", new Color( 248, 187, 0) );
            UIManager.put( "nimbusDisabledText", new Color( 128, 128, 128) );
            UIManager.put( "nimbusFocus", new Color(115,164,209) );
            UIManager.put( "nimbusGreen", new Color(176,179,50) );
            UIManager.put( "nimbusInfoBlue", new Color( 66, 139, 221) );
            UIManager.put( "nimbusLightBackground", new Color( 18, 30, 49) );
            UIManager.put( "nimbusOrange", new Color(191,98,4) );
            UIManager.put( "nimbusRed", new Color(169,46,34) );
            UIManager.put( "nimbusSelectedText", new Color( 255, 255, 255) );
            UIManager.put( "nimbusSelectionBackground", new Color( 104, 93, 156) );
            UIManager.put( "text", new Color( 230, 230, 230) );
        }
        
        try {
          for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                javax.swing.UIManager.setLookAndFeel(info.getClassName());
                break;
            }
          }
        } catch (ClassNotFoundException e) {
          e.printStackTrace();
        } catch (InstantiationException e) {
          e.printStackTrace();
        } catch (IllegalAccessException e) {
          e.printStackTrace();
        } catch (javax.swing.UnsupportedLookAndFeelException e) {
          e.printStackTrace();
        } catch (Exception e) {
          e.printStackTrace();
        }
    }
    
    public static void applyButtonTheme(JButton button) {
        if (isDark) {
        } else {
            // leave untouched
        }
    }
    
    public static void applyStartStopTheme(JButton button, boolean applyStop) {
        applyButtonTheme(button);
        
        button.setForeground(Color.BLACK);
        if (applyStop) {
            button.setBackground(STOP_BUTTON_COLOR);
        } else {
            button.setBackground(START_BUTTON_COLOR);
        }
    }
    
    public static Color getDefaultMapLineColor() {
        return DEFAULT_MAP_LINE_COLOR;
    }
}
