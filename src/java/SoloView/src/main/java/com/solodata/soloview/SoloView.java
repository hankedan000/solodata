/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.soloview;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.solodata.datalogger.Client;
import com.solodata.NavSampleSubscriber;
import com.solodata.soloview.gui.Theme;
import com.solodata.ui.maps.tiles.LocalhostTileSource;
import com.solodata.soloview.tilesources.localhost.TileSourceServer;
import java.awt.Color;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import org.apache.commons.net.ftp.FTPFile;
import solodata.DataLogger.DataLogger;
import solodata.NavSampleOuterClass;

/**
 *
 * @author daniel
 */
public class SoloView extends javax.swing.JFrame implements
        WindowListener,
        Settings.SettingListener,
        NavSampleSubscriber.NavSubListener,
        Client.DataLoggerListener{
    private static final String LOG_TAG = SoloView.class.getName();
    private static final Logger logger = Logger.getLogger(LOG_TAG);

    private final TileSourceServer tileSourceServer;
    
    private final Client dlc;
    private final DefaultListModel<String> recordListingModel = new DefaultListModel<>();
   
    /**
     * Creates new form SoloView
     */
    public SoloView() {
        initComponents();
        addWindowListener(this);
        initDataLoggerPanel();
        recordListing.setModel(recordListingModel);
        timeLabel.setVisible(false);// hide until we have GPS time
        
        // run a local server for offline tile maps
        tileSourceServer = new TileSourceServer();
        tileSourceServer.start();
        
        // configure the NavSampleSubscriber
        initNavSubscriber();
        
        // sync UI elements with Settings from file
        maximizeOnStartupCheckbox.setState(Settings.getInstance().getMaximizeOnStart());
        
        if (Settings.getInstance().getOfflineTileSourceOverride()) {
            String offlineTileSource = Settings.getInstance().getOfflineTileSource();
            liveMap.map().setTileSource(new LocalhostTileSource(offlineTileSource));
        }
        
        dlc = new Client();
        logViewer.init(dlc);
        initDataLoggerClient();
        
        // init recording infrustructure
        updateStartStopButton();
    }
    
    /**
     * This method will restart the SoloView application.
     * Warning: This method only works if SoloView was launched from a jar
     */
    public void restartApplication()
    {
        // save settings before closing application
        Settings.getInstance().save();
        
        final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
        final File currentJar;
        try {
            currentJar = new File(SoloView.class.getProtectionDomain().getCodeSource().getLocation().toURI());

            /* is it a jar file? */
            if ( ! currentJar.getName().endsWith(".jar"))
            {
                logger.log(Level.SEVERE,
                        "currentJar is not a jar! currentJar = {0}",
                        currentJar);
                return;
            }
            
            /* Build command: java -jar application.jar */
            final ArrayList<String> command = new ArrayList<>();
            command.add(javaBin);
            command.add("-jar");
            command.add(currentJar.getPath());

            final ProcessBuilder builder = new ProcessBuilder(command);
            builder.start();
            System.exit(0);
        } catch (URISyntaxException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }
    
    private final void initNavSubscriber() {
        NavSampleSubscriber navSub = NavSampleSubscriber.getInstance();
        navSub.addListener(liveMap);
        navSub.addListener(this);
        if (navSub.connect(
                Settings.getInstance().getNavPublisherHost(),
                Settings.getInstance().getNavPublisherPort()))
        {
            navSub.start();
        }
    }
    
    private final void initDataLoggerClient() {
        dlc.addListener(this);
        if (dlc.connect(Settings.getInstance().getNavPublisherHost())) {
            dlc.start();
        }
    }
    
    private final void initDataLoggerPanel() {
        navSampleCountField.setText("0");
        canSampleCountField.setText("0");
        updateZippingStatus(false);
    }
    
    private final void updateZippingStatus(boolean zippingInProgress) {
        if (zippingInProgress) {
            zippingStatusLabel.setText("ZIPPING IN PROGRESS... DO NOT POWER OFF!!!");
            zippingStatusLabel.setOpaque(true);
            zippingStatusLabel.setBackground(Color.ORANGE);
            zippingStatusLabel.setForeground(Color.BLACK);
        } else {
            zippingStatusLabel.setText("NO ACTIVE ZIPPING TASKS");
            zippingStatusLabel.setOpaque(false);
            zippingStatusLabel.setBackground(Color.GRAY);
            zippingStatusLabel.setForeground(Color.WHITE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        tabs = new javax.swing.JTabbedPane();
        liveMap = new com.solodata.soloview.gui.LiveMapView();
        offlineRegionSelector1 = new com.solodata.soloview.gui.OfflineMapManager();
        dataLoggingPanel = new javax.swing.JPanel();
        recordButtons = new javax.swing.JPanel();
        startStopButton = new javax.swing.JButton();
        newSessionButton = new javax.swing.JButton();
        recordListingScrollPane = new javax.swing.JScrollPane();
        recordListing = new javax.swing.JList<>();
        statusPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        navSampleCountField = new javax.swing.JTextField();
        recordNavCheckbox = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        canSampleCountField = new javax.swing.JTextField();
        recordCanCheckbox = new javax.swing.JCheckBox();
        zippingStatusLabel = new javax.swing.JLabel();
        logViewer = new com.solodata.soloview.gui.LogViewer();
        jToolBar1 = new javax.swing.JToolBar();
        timeLabel = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        restartMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        maximizeOnStartupCheckbox = new javax.swing.JCheckBoxMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SoloView");

        tabs.addTab("Live Map", liveMap);
        tabs.addTab("Offline Map Manager", offlineRegionSelector1);

        dataLoggingPanel.setLayout(new java.awt.BorderLayout());

        recordButtons.setLayout(new java.awt.GridLayout(1, 0));

        startStopButton.setText("Start");
        startStopButton.setEnabled(false);
        startStopButton.setPreferredSize(new java.awt.Dimension(59, 70));
        startStopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startStopButtonActionPerformed(evt);
            }
        });
        recordButtons.add(startStopButton);

        newSessionButton.setText("New Session");
        newSessionButton.setEnabled(false);
        newSessionButton.setMinimumSize(new java.awt.Dimension(105, 100));
        recordButtons.add(newSessionButton);

        dataLoggingPanel.add(recordButtons, java.awt.BorderLayout.SOUTH);

        recordListing.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        recordListingScrollPane.setViewportView(recordListing);

        dataLoggingPanel.add(recordListingScrollPane, java.awt.BorderLayout.WEST);

        statusPanel.setLayout(new java.awt.GridBagLayout());

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setLabelFor(navSampleCountField);
        jLabel1.setText("Nav Sample Count:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        statusPanel.add(jLabel1, gridBagConstraints);

        navSampleCountField.setEditable(false);
        navSampleCountField.setText("jTextField1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 60;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        statusPanel.add(navSampleCountField, gridBagConstraints);

        recordNavCheckbox.setSelected(true);
        recordNavCheckbox.setText("Record");
        recordNavCheckbox.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        statusPanel.add(recordNavCheckbox, gridBagConstraints);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setLabelFor(canSampleCountField);
        jLabel2.setText("CAN Sample Count:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(9, 5, 0, 0);
        statusPanel.add(jLabel2, gridBagConstraints);

        canSampleCountField.setEditable(false);
        canSampleCountField.setText("jTextField1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 60;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 214, 230);
        statusPanel.add(canSampleCountField, gridBagConstraints);

        recordCanCheckbox.setSelected(true);
        recordCanCheckbox.setText("Record");
        recordCanCheckbox.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        statusPanel.add(recordCanCheckbox, gridBagConstraints);

        zippingStatusLabel.setBackground(new java.awt.Color(255, 102, 51));
        zippingStatusLabel.setForeground(new java.awt.Color(0, 0, 0));
        zippingStatusLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        zippingStatusLabel.setText("jLabel3");
        zippingStatusLabel.setOpaque(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        statusPanel.add(zippingStatusLabel, gridBagConstraints);

        dataLoggingPanel.add(statusPanel, java.awt.BorderLayout.CENTER);

        tabs.addTab("Data Logging", dataLoggingPanel);
        tabs.addTab("Log Viewer", logViewer);

        getContentPane().add(tabs, java.awt.BorderLayout.CENTER);
        tabs.getAccessibleContext().setAccessibleName("SoloView UI");

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        timeLabel.setText("<GPS TIME>");
        jToolBar1.add(timeLabel);

        getContentPane().add(jToolBar1, java.awt.BorderLayout.SOUTH);

        fileMenu.setText("File");

        restartMenuItem.setText("Restart");
        restartMenuItem.setToolTipText("Restarts Soloview");
        restartMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                restartMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(restartMenuItem);

        menuBar.add(fileMenu);

        editMenu.setText("Edit");

        maximizeOnStartupCheckbox.setSelected(true);
        maximizeOnStartupCheckbox.setText("Maximize on startup");
        maximizeOnStartupCheckbox.setToolTipText("If checked, SoloView will maximize its UI when it starts.");
        maximizeOnStartupCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maximizeOnStartupCheckboxActionPerformed(evt);
            }
        });
        editMenu.add(maximizeOnStartupCheckbox);

        menuBar.add(editMenu);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void startStopButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startStopButtonActionPerformed
        // start or stop the recording
        if (dlc.isRecording()) {
            dlc.stopLog();
        } else {
            boolean recordNav = recordNavCheckbox.isEnabled() &&
                    recordNavCheckbox.isSelected();
            boolean recordCan = recordCanCheckbox.isEnabled() &&
                    recordCanCheckbox.isSelected();
            dlc.startLog(recordNav,recordCan);
        }
    }//GEN-LAST:event_startStopButtonActionPerformed

    private void maximizeOnStartupCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maximizeOnStartupCheckboxActionPerformed
        Settings.getInstance().setMaximizeOnStart(maximizeOnStartupCheckbox.getState());
    }//GEN-LAST:event_maximizeOnStartupCheckboxActionPerformed

    private void restartMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_restartMenuItemActionPerformed
        restartApplication();
    }//GEN-LAST:event_restartMenuItemActionPerformed

    private void updateStartStopButton() {
        boolean applyStop = dlc.isRecording();
        Theme.applyStartStopTheme(startStopButton, applyStop);
        if (applyStop) {
            startStopButton.setText("Stop");
        } else {
            startStopButton.setText("Start");
        }
    }
    
    private void updateRecordListing() {
        recordListingModel.clear();
        FTPFile[] files = dlc.listRemoteFiles();
        for (FTPFile file : files) {
            String name = file.getName();
            if (name.equals(".") || name.equals("..")) {
                    continue;
            }
            recordListingModel.addElement(name);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        if (Settings.getInstance().getDarkTheme()) {
            FlatDarkLaf.install();
        } else {
            FlatLightLaf.install();
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                SoloView app = new SoloView();
                app.setVisible(true);
                
                // maximize UI on startup if enabled
                if (Settings.getInstance().getMaximizeOnStart()) {
                    app.setExtendedState(app.getExtendedState() | JFrame.MAXIMIZED_BOTH);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField canSampleCountField;
    private javax.swing.JPanel dataLoggingPanel;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JToolBar jToolBar1;
    private com.solodata.soloview.gui.LiveMapView liveMap;
    private com.solodata.soloview.gui.LogViewer logViewer;
    private javax.swing.JCheckBoxMenuItem maximizeOnStartupCheckbox;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JTextField navSampleCountField;
    private javax.swing.JButton newSessionButton;
    private com.solodata.soloview.gui.OfflineMapManager offlineRegionSelector1;
    private javax.swing.JPanel recordButtons;
    private javax.swing.JCheckBox recordCanCheckbox;
    private javax.swing.JList<String> recordListing;
    private javax.swing.JScrollPane recordListingScrollPane;
    private javax.swing.JCheckBox recordNavCheckbox;
    private javax.swing.JMenuItem restartMenuItem;
    private javax.swing.JButton startStopButton;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JTabbedPane tabs;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JLabel zippingStatusLabel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void windowOpened(WindowEvent arg0) {
        Settings.getInstance().restore();
    }

    @Override
    public void windowClosing(WindowEvent arg0) {
        Settings.getInstance().save();
        NavSampleSubscriber.getInstance().requestShutdown();
        dlc.requestShutdown();
        tileSourceServer.stop();
    }

    @Override
    public void windowClosed(WindowEvent arg0) {
    }

    @Override
    public void windowIconified(WindowEvent arg0) {
    }

    @Override
    public void windowDeiconified(WindowEvent arg0) {
    }

    @Override
    public void windowActivated(WindowEvent arg0) {
    }

    @Override
    public void windowDeactivated(WindowEvent arg0) {
    }

    @Override
    public void onSettingChanged(String settingKey, Object oldValue, Object newValue) {
        if (settingKey.compareTo(Settings.NAV_PUBLISHER_HOST_KEY) == 0 ||
            settingKey.compareTo(Settings.NAV_PUBLISHER_PORT_KEY) == 0)
        {
            // trigger a reconfiguration of the subscriber
            initNavSubscriber();
            
            // reconnect data logger to new nav host
            if (dlc.connected()) {
                dlc.requestShutdown();
            }
            if (dlc.connect(Settings.getInstance().getNavPublisherHost())) {
                dlc.start();
            }
        }
    }

    @Override
    public void onNavSubSampleReceived(NavSampleOuterClass.NavSample ns) {
        // update timestamp every GPS sample
        if (ns.hasGps()) {
            long utc_ms = (long)(ns.getUtcUs() / 1.0e3);
            Date date = new Date(utc_ms);
            SimpleDateFormat format = new SimpleDateFormat("MMMM d, yyyy hh:mm:ss");
            timeLabel.setText(format.format(date));
            if ( ! timeLabel.isVisible()) {
                timeLabel.setVisible(true);
            }
        }
    }

    @Override
    public void onNavSubConnectionChanged(boolean connected) {
    }

    @Override
    public void onNavSubLinkValidChanged(boolean linkValid) {
        if ( ! linkValid) {
            logger.log(Level.WARNING,
                    "navSub link lost!");
            timeLabel.setVisible(false);
        }
    }

    @Override
    public void onDataLoggerConnectionChanged(boolean connected) {
        /**
         * connected just means the socket is valid. doesn't mean we're getting
         * valid status from the DataLogger yet.
         */
        if ( ! connected) {
            startStopButton.setEnabled(false);
        }
        updateStartStopButton();
        updateRecordListing();
    }

    @Override
    public void onDataLoggerLinkValidChanged(boolean linkValid) {
        startStopButton.setEnabled(linkValid);
    }

    @Override
    public void onDataLoggerStatus(DataLogger.Status_msg status) {
        logger.log(Level.FINE,
                "Got DataLogger status: {0}",status.toString());
        
        if (status.getNewRecordsCount() > 0) {
            updateRecordListing();
        }
        
        navSampleCountField.setText(String.format("%d", status.getTotalNavSamplesSeen()));
        canSampleCountField.setText(String.format("%d", status.getTotalCanSamplesSeen()));
        recordNavCheckbox.setEnabled(status.getNavDataValid());
        recordCanCheckbox.setEnabled(status.getCanDataValid());
        updateZippingStatus(status.getZippingInProgress());
    }

    @Override
    public void onRecordingStarted() {
        updateStartStopButton();
    }

    @Override
    public void onRecordingStopped() {
        updateStartStopButton();
    }

    @Override
    public void onRecordingPaused() {
        logger.info("Recording paused!");
    }
}
