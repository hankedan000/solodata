/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata;

import java.nio.file.Path;
import java.util.ArrayList;
import solodata.NavSampleOuterClass;

/**
 *
 * @author daniel
 */
public class NavSampleLoader extends Thread {
    public static interface NavSampleLoaderListener {
        public void onProgressChanged(int progress, int maxProgress);
        
        public void onLoadComplete(ArrayList<NavSampleOuterClass.NavSample> samples);
        
        public void onLoadFailed(String reason);
    }
    
    protected final NavSampleLoaderListener listener;
    protected final Path datapath;
    public final ArrayList<NavSampleOuterClass.NavSample> samples;
    
    public NavSampleLoader(Path datapath) {
        this(datapath, null);
    }
    
    public NavSampleLoader(Path datapath, NavSampleLoaderListener listener) {
        this.datapath = datapath;
        this.listener = listener;
        samples = new ArrayList<>();
    }
    
}
