/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.DataLogger;

import com.solodata.datalogger.Record;
import java.io.File;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author daniel
 */
public class RecordTest {
    private final File TEST_RECORD_ZIP;
    
    public RecordTest() {
        // assumes path is relative to Netbeans project root
        TEST_RECORD_ZIP = new File("../../../datasets/tests/DataLogger/test_record.zip");
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
    /**
     * Test of fromZip method, of class Record.
     */
    @org.junit.jupiter.api.Test
    public void testFromZip() {
        System.out.println("fromZip");
        File zipFile = null;
        Record result = Record.fromZip(zipFile);
        assertEquals(null, result);
        
        zipFile = new File("non_existant_record.zip");
        result = Record.fromZip(zipFile);
        assertTrue(result != null);
        
        result = Record.fromZip(TEST_RECORD_ZIP);
        assertTrue(result != null);
    }

    /**
     * Test of hasNav method, of class Record.
     */
    @org.junit.jupiter.api.Test
    public void testHasNav() {
        System.out.println("hasNav");
        File zipFile = new File("non_existant_record.zip");
        Record result = Record.fromZip(zipFile);
        assertEquals(false, result.hasNav());
        
        result = Record.fromZip(TEST_RECORD_ZIP);
        assertEquals(true, result.hasNav());
    }

    /**
     * Test of hasCan method, of class Record.
     */
    @org.junit.jupiter.api.Test
    public void testHasCan() {
        System.out.println("hasCan");
        File zipFile = new File("non_existant_record.zip");
        Record result = Record.fromZip(zipFile);
        assertEquals(false, result.hasCan());
        
        result = Record.fromZip(TEST_RECORD_ZIP);
        assertEquals(true, result.hasCan());
    }

}
