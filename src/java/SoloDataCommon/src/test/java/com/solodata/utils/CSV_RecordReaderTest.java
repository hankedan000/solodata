/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.utils;

import java.util.List;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author daniel
 */
public class CSV_RecordReaderTest {
    final String TEST_CSV_TEXT_TITLE_ONLY = "hello,world,col2,col3";
    final String TEST_CSV_TEXT_DATA_ONLY = "1,10,100,1000\n2,20,200,2000";
    final String TEST_CSV_TEXT_FULL = "hello,world,col2,col3\n1,10,100,1000\n2,20,200,2000";
    
    public CSV_RecordReaderTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of columnCount method, of class CSV_RecordReader.
     */
    @Test
    public void testColumnCount() {
        System.out.println("columnCount");
        CSV_RecordReader csv = new CSV_RecordReader(TEST_CSV_TEXT_TITLE_ONLY, true);
        assertEquals(4, csv.columnCount());
        
        csv = new CSV_RecordReader(TEST_CSV_TEXT_DATA_ONLY, false);
        assertEquals(4, csv.columnCount());
        
        csv = new CSV_RecordReader(TEST_CSV_TEXT_FULL, true);
        assertEquals(4, csv.columnCount());
    }

    /**
     * Test of getColumnTitle method, of class CSV_RecordReader.
     */
    @Test
    public void testGetColumnTitle() {
        System.out.println("getColumnTitle");
        CSV_RecordReader csv = new CSV_RecordReader(TEST_CSV_TEXT_TITLE_ONLY, true);
        assertEquals("hello",csv.getColumnTitle(0));
        assertEquals("world",csv.getColumnTitle(1));
        assertEquals("col2",csv.getColumnTitle(2));
        assertEquals("col3",csv.getColumnTitle(3));
        
        csv = new CSV_RecordReader(TEST_CSV_TEXT_FULL, true);
        assertEquals("hello",csv.getColumnTitle(0));
        assertEquals("world",csv.getColumnTitle(1));
        assertEquals("col2",csv.getColumnTitle(2));
        assertEquals("col3",csv.getColumnTitle(3));
    }

    /**
     * Test of getImmutableColumnData method, of class CSV_RecordReader.
     */
    @Test
    public void testGetImmutableColumnData() {
        System.out.println("getImmutableColumnData");
        CSV_RecordReader csv = new CSV_RecordReader(TEST_CSV_TEXT_DATA_ONLY, false);
        List<Double> col0 = csv.getImmutableColumnData(0);
        assertEquals(2,col0.size());
        assertEquals(1.0,col0.get(0),0.1);
        assertEquals(2.0,col0.get(1),0.1);
        List<Double> col1 = csv.getImmutableColumnData(1);
        assertEquals(2,col1.size());
        assertEquals(10.0,col1.get(0),0.1);
        assertEquals(20.0,col1.get(1),0.1);
        List<Double> col2 = csv.getImmutableColumnData(2);
        assertEquals(2,col2.size());
        assertEquals(100.0,col2.get(0),0.1);
        assertEquals(200.0,col2.get(1),0.1);
        List<Double> col3 = csv.getImmutableColumnData(3);
        assertEquals(2,col3.size());
        assertEquals(1000.0,col3.get(0),0.1);
        assertEquals(2000.0,col3.get(1),0.1);
        
        csv = new CSV_RecordReader(TEST_CSV_TEXT_FULL, true);
        col0 = csv.getImmutableColumnData(0);
        assertEquals(2,col0.size());
        assertEquals(1.0,col0.get(0),0.1);
        assertEquals(2.0,col0.get(1),0.1);
        col1 = csv.getImmutableColumnData(1);
        assertEquals(2,col1.size());
        assertEquals(10.0,col1.get(0),0.1);
        assertEquals(20.0,col1.get(1),0.1);
        col2 = csv.getImmutableColumnData(2);
        assertEquals(2,col2.size());
        assertEquals(100.0,col2.get(0),0.1);
        assertEquals(200.0,col2.get(1),0.1);
        col3 = csv.getImmutableColumnData(3);
        assertEquals(2,col3.size());
        assertEquals(1000.0,col3.get(0),0.1);
        assertEquals(2000.0,col3.get(1),0.1);
    }

    /**
     * Test of hasTitles method, of class CSV_RecordReader.
     */
    @Test
    public void testHasTitles() {
        System.out.println("hasTitles");
        CSV_RecordReader csv = new CSV_RecordReader(TEST_CSV_TEXT_TITLE_ONLY, true);
        assertEquals(true, csv.hasTitles());
        
        csv = new CSV_RecordReader(TEST_CSV_TEXT_DATA_ONLY, false);
        assertEquals(false, csv.hasTitles());
        
        csv = new CSV_RecordReader(TEST_CSV_TEXT_FULL, true);
        assertEquals(true, csv.hasTitles());
    }
    
}
