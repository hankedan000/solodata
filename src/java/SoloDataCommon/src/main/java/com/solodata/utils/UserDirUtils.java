/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.utils;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author daniel
 */
public class UserDirUtils {
    public static final String SOLODATA_USER_DIR = ".solodata";
    
    public static final void initUserDirs() {
        getUserPath().toFile().mkdirs();
    }
    
    public static final Path getUserPath() {
        Path path = Paths.get(System.getProperty("user.home"),SOLODATA_USER_DIR);
        // Make directory if it doesn't exist already
        path.toFile().mkdirs();
        return path;
    }
    
    public static final Path getUserPath(String subDir) {
        Path path = getUserPath().resolve(subDir);
        // Make directory if it doesn't exist already
        path.toFile().mkdirs();
        return path;
    }
    
}
