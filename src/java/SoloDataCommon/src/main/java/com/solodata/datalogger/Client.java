/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.datalogger;

import com.solodata.datalogger.Record;
import com.google.protobuf.InvalidProtocolBufferException;
import com.solodata.utils.FileUtils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.ClosedSelectorException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTP;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQException;
import solodata.DataLogger.DataLogger;

/**
 *
 * @author daniel
 */
public class Client extends Thread {
    private static final String LOG_TAG = Client.class.getName();
    private static final Logger logger = Logger.getLogger(LOG_TAG);
    
    public interface DataLoggerListener {
        void onDataLoggerConnectionChanged(boolean connected);
        void onDataLoggerLinkValidChanged(boolean linkValid);
        void onDataLoggerStatus(DataLogger.Status_msg status);
        void onRecordingStarted();
        void onRecordingStopped();
        void onRecordingPaused();
    }
    
    private final ZContext context;
    private final ZMQ.Socket statusSocket;
    private final ZMQ.Socket controlSocket;
    private final FTPClient ftpClient;
    // a synchronized list of DataLogger listeners
    private final List<DataLoggerListener> listeners;
    // set to false when the subscriber thread should shutdown and join
    private boolean stayAlive;
    // set to true if statusSocket connected to host
    private boolean connected;
    // set to true once we receive samples from server
    private boolean linkValid;
    // current record state (assumed valid if linkValid is true)
    private DataLogger.RecordState_E currRecordState;
    // set to true while actively recording
    private boolean recordBusy;
    
    public Client()
    {
        context = new ZContext();
        statusSocket = context.createSocket(SocketType.SUB);
        controlSocket = context.createSocket(SocketType.REQ);
        ftpClient = new FTPClient();
        listeners = Collections.synchronizedList(new ArrayList<>());
        stayAlive = true;
        connected = false;
        linkValid = false;
        recordBusy = false;
        currRecordState = DataLogger.RecordState_E.STOPPED;
    }
    
    public boolean connect(String host) {
        if (connected) {
            logger.log(Level.WARNING,
                    "connect() requested to host ''{0}'',"
                    + " but already connected",
                    new Object[]{host});
            return true;
        }
        
        connected = true;// assume true and modify going forward
        connected = connected && statusSocket.connect(String.format("tcp://%s:%d",host,5050));
        connected = connected && statusSocket.subscribe("");
        connected = connected && controlSocket.connect(String.format("tcp://%s:%d",host,5051));
        
        if (connected) {
            final int FTP_PORT = 2121;
            try {
                ftpClient.connect(host, FTP_PORT);
                
                // After connection attempt, you should check the reply code to
                // verify success.
                int reply = ftpClient.getReplyCode();
                if ( ! FTPReply.isPositiveCompletion(reply)) {
                    ftpClient.disconnect();
                    logger.log(Level.SEVERE,
                            "FTP server refused connection.");
                    connected = false;
                } else {
                    // after connecting to the server set the local passive mode
                    ftpClient.enterLocalPassiveMode();
                }
                
                if (connected && ! ftpClient.login("logger", "password")) {
                    logger.log(Level.SEVERE,
                            "Failed to login to FTPServer");
                    connected = false;
                }
                
                /**
                 * Setup file type and transfer mode
                 * 
                 * We want BINARY_FILE_TYPE because the FTP transfer
                 * defaults to ASCII_FILE_TYPE and this can introduce
                 * corruption in the DataLogger zip files.
                 */
                if (connected) {
                    ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                    ftpClient.setFileTransferMode(FTP.BLOCK_TRANSFER_MODE);
                }
            } catch (IOException ex) {
                logger.log(Level.SEVERE,
                        "Caught IOException. Failed to connect to DataLogger"
                        + " FTPServer @ {0}:{1}. {2}",
                        new Object[]{host,FTP_PORT,ex});
                connected = false;
            }
        }
        
        notifyConnectionChanged(connected);
        return connected;
    }
    
    public boolean connected() {
        return connected;
    }
    
    private void disconnect() {
        if (connected) {
            statusSocket.close();
            controlSocket.close();
            try {
                ftpClient.logout();
                ftpClient.disconnect();
            } catch (IOException ex) {
                logger.log(Level.SEVERE,
                        "Caught IOException while closing FTPClient. {0}",
                        ex);
            }
            connected = false;
            notifyConnectionChanged(connected);
        }
    }
    
    public boolean isRecording() {
        return recordBusy;
    }
    
    public void addListener(DataLoggerListener listener) {
        listeners.add(listener);
    }
    
    public void removeListener(DataLoggerListener listener) {
        listeners.remove(listener);
    }

    public void requestShutdown() {
        logger.log(Level.FINE,
                "Shutdown requested...");
        if (recordBusy) {
            // stop any active logs
            stopLog();
        }
        stayAlive = false;
        disconnect();
        interrupt();
    }
    
    public boolean startLog(boolean recordNav, boolean recordCan) {
        boolean okay = true;
        
        if (connected) {
            DataLogger.ControlRequest_msg req = DataLogger.ControlRequest_msg.newBuilder()
                    .setRecordNav(recordNav)
                    .setRecordCan(recordCan)
                    .setRecordControl(DataLogger.RecordControl_E.START)
                    .build();
            
            // send the request
            byte[] reqData = req.toByteArray();
            okay = okay && controlSocket.send(reqData);
            
            // wait for reply
            byte[] repData = controlSocket.recv();
            try {
                DataLogger.ControlReply_msg reply = DataLogger.ControlReply_msg.parseFrom(repData);
                
                // check that recording was started successfully
                if (reply.getRecordState() == DataLogger.RecordState_E.RECORDING) {
                    recordBusy = true;
                    notifyRecordingStarted();
                } else {
                    logger.log(Level.SEVERE,
                            "Failed to start recording because current record state is {0}.",
                            reply.getRecordState().toString());
                    okay = false;
                }
            } catch (InvalidProtocolBufferException ex) {
                logger.log(Level.SEVERE,
                        "Received invalid reply message from DataLogger. {0}",
                        ex.toString());
                okay = false;
            }
        }
        else
        {
            logger.log(Level.SEVERE,
                    "Called start() when DataLoggerClient is not connected to"
                    + " DataLogger.");
            okay = false;
        }
        
        return okay;
    }
    
    public boolean stopLog() {
        boolean okay = true;
        
        if (connected) {
            DataLogger.ControlRequest_msg req = DataLogger.ControlRequest_msg.newBuilder()
                    .setRecordControl(DataLogger.RecordControl_E.STOP)
                    .build();
            
            // send the request
            byte[] reqData = req.toByteArray();
            okay = okay && controlSocket.send(reqData);
            
            // wait for reply
            byte[] repData = controlSocket.recv();
            try {
                DataLogger.ControlReply_msg reply = DataLogger.ControlReply_msg.parseFrom(repData);
                
                // check that recording was started successfully
                if (reply.getRecordState() == DataLogger.RecordState_E.STOPPED) {
                    recordBusy = false;
                    notifyRecordingStopped();
                } else {
                    logger.log(Level.SEVERE,
                            "Failed to stop recording because current record state is {0}.",
                            reply.getRecordState().toString());
                    okay = false;
                }
            } catch (InvalidProtocolBufferException ex) {
                logger.log(Level.SEVERE,
                        "Received invalid reply message from DataLogger. {0}",
                        ex.toString());
                okay = false;
            }
        }
        else
        {
            logger.log(Level.SEVERE,
                    "Called stop() when DataLoggerClient is not connected to"
                    + " DataLogger.");
            okay = false;
        }
        
        return okay;
    }
    
    public boolean pauseLog() {
        boolean okay = true;
        
        if (connected) {
            DataLogger.ControlRequest_msg req = DataLogger.ControlRequest_msg.newBuilder()
                    .setRecordControl(DataLogger.RecordControl_E.PAUSE)
                    .build();
            
            // send the request
            byte[] reqData = req.toByteArray();
            okay = okay && controlSocket.send(reqData);
            
            // wait for reply
            byte[] repData = controlSocket.recv();
            try {
                DataLogger.ControlReply_msg reply = DataLogger.ControlReply_msg.parseFrom(repData);
                
                // check that recording was started successfully
                if (reply.getRecordState() == DataLogger.RecordState_E.PAUSED) {
                    notifyRecordingPaused();
                } else {
                    logger.log(Level.SEVERE,
                            "Failed to pause recording because current record state is {0}.",
                            reply.getRecordState().toString());
                    okay = false;
                }
            } catch (InvalidProtocolBufferException ex) {
                logger.log(Level.SEVERE,
                        "Received invalid reply message from DataLogger. {0}",
                        ex.toString());
                okay = false;
            }
        }
        else
        {
            logger.log(Level.SEVERE,
                    "Called pause() when DataLoggerClient is not connected to"
                    + " DataLogger.");
            okay = false;
        }
        
        return okay;
    }
    
    public FTPFile[] listRemoteFiles() {
        FTPFile[] records = new FTPFile[0];
        if (connected) {
            try {
                records = ftpClient.listFiles();
            } catch (IOException ex) {
                logger.log(Level.SEVERE,
                        "Caught IOException while getting record files from"
                        + " FTPServer. {0}",
                        ex.toString());
            }
        }
        return records;
    }
    
    public boolean retrieveFile(String recordFilepath, File destFile) {
        boolean okay = true;
        
        try {
            InputStream ris = ftpClient.retrieveFileStream(recordFilepath);
            
            if (ris == null) {
                logger.log(Level.SEVERE,
                        "Retrieved InputStream is null for {0}",
                        new Object[]{recordFilepath});
                okay = false;
            } else {
                if ( ! FileUtils.streamToFile(ris, destFile)) {
                    logger.log(Level.SEVERE,
                            "Failed to download file {0} from FTP server.",
                            new Object[]{recordFilepath});
                    okay = false;
                }
            }
            
            /**
             * must call complete command so that subsequent calls don't fail
             * unexpectedly.
             */
            if ( ! ftpClient.completePendingCommand()) {
                logger.log(Level.SEVERE,
                        "Failed to complete command");
                okay = false;
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE,
                    "Caught IOException while retrieving FileStream for {0}. {1}",
                    new Object[]{recordFilepath,ex.toString()});
            okay = false;
        }
        
        return okay;
    }
    
    private void notifyConnectionChanged(boolean connected) {
        for (DataLoggerListener listener : listeners)
        {
            // notify listener that we got a sample!
            listener.onDataLoggerConnectionChanged(connected);
        }
    }
    
    private void notifyRecordingStarted() {
        for (DataLoggerListener listener : listeners)
        {
            listener.onRecordingStarted();
        }
    }
    
    private void notifyRecordingStopped() {
        for (DataLoggerListener listener : listeners)
        {
            listener.onRecordingStopped();
        }
    }
    
    private void notifyRecordingPaused() {
        for (DataLoggerListener listener : listeners)
        {
            listener.onRecordingPaused();
        }
    }
    
    @Override
    public void run() {
        DataLogger.Status_msg status;
        
        while (stayAlive) {
            try {
                byte[] data = statusSocket.recv();
            
                status = DataLogger.Status_msg.parseFrom(data);
                logger.log(Level.FINER, status.toString());
                
                boolean linkValidChanged = false;
                if ( ! linkValid) {
                    // flag link as valid once we get the first sample
                    linkValid = true;
                    linkValidChanged = true;
                }
                
                // update current record state
                currRecordState = status.getRecordState();
                
                for (DataLoggerListener listener : listeners)
                {
                    if (linkValidChanged) {
                        // notify listeners if link valid has changed states
                        listener.onDataLoggerLinkValidChanged(linkValid);
                    }
                    
                    // notify listener that we got a sample!
                    listener.onDataLoggerStatus(status);
                }
            } catch (InvalidProtocolBufferException ex) {
                logger.log(Level.WARNING,null, ex);
            } catch (ClosedSelectorException ex) {
                if (stayAlive) {
                    // it's expected to see weird exception when shutting down
                    logger.log(Level.WARNING,null, ex);
                }
            } catch (ZMQException ex) {
                if (stayAlive) {
                    // it's expected to see weird exception when shutting down
                    logger.log(Level.WARNING,null, ex);
                }
            }
        }
    }
    
    public static void main(String args[]) {
        Client dlc = new Client();
        dlc.addListener(new DataLoggerListener() {
            @Override
            public void onDataLoggerConnectionChanged(boolean connected) {
                if (connected) {
                    System.out.println("Connected to DataLogger!");
                } else {
                    System.out.println("Disconnected from DataLogger!");
                }
            }

            @Override
            public void onDataLoggerLinkValidChanged(boolean linkValid) {
            }

            @Override
            public void onDataLoggerStatus(DataLogger.Status_msg status) {
                logger.log(Level.FINER, status.toString());
                
                for (String newRecord : status.getNewRecordsList())
                {
                    File downloadDir = new File(FileUtils.getTempDir(),"download");
                    File downloadZip = new File(downloadDir,newRecord);

                    if (dlc.retrieveFile(newRecord, downloadZip)) {
                        logger.info("Download was successful!");
                        Record record = Record.fromZip(downloadZip);
                        logger.log(Level.INFO,"hasNav = {0}",record.hasNav());
                        logger.log(Level.INFO,"hasCan = {0}",record.hasCan());

                        // clean up download once we're done with it
                        downloadZip.delete();
                    } else {
                        logger.log(Level.SEVERE,
                                "Failed to retrieve record file {0}.",newRecord);
                    }
                }
            }

            @Override
            public void onRecordingStarted() {
                logger.info("Recording started!");
            }

            @Override
            public void onRecordingStopped() {
                logger.log(Level.INFO,
                        "Recording stopped!");
            }

            @Override
            public void onRecordingPaused() {
                logger.info("Recording paused!");
            }
        });
        
        dlc.connect("localhost");
        
        dlc.start();
        
        try {
            for (int i=0; i<10; i++) {
                logger.info("Starting a recording.");
                boolean started = dlc.startLog(true, true);
                if (started) {
                    TimeUnit.SECONDS.sleep(2);
                    logger.info("Stopping the recording.");
                    dlc.stopLog();
                }
                TimeUnit.SECONDS.sleep(2);
            }
            
            dlc.requestShutdown();
            dlc.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
