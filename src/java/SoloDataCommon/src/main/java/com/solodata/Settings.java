/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author daniel
 */
public class Settings {
    public interface SettingListener {
        void onSettingChanged(String settingKey, Object oldValue, Object newValue);
    }
    
    protected final Logger logger = Logger.getLogger(Settings.class.getName());
    
    protected final File settingsFile;
    
    private final JSONObject obj = new JSONObject();
    
    private final List<SettingListener> listeners = new ArrayList<>();
    
    private final Thread fileWatcherThread;
    
    // Lock used to prevent JSON parse errors when internally saving settings
    // file and file watch wakes up to parse the updates.
    private final ReentrantLock fileAccessLock = new ReentrantLock();
    
    public Settings(String path) {
        // Make parent directory structure if it doens't exist
        settingsFile = new File(path);
        settingsFile.getParentFile().mkdirs();
        
        restore();
        
        // load defaults after restore to prevent unecessary setting changes
        loadDefaults();
        
        // watch for external modifications to settings file
        fileWatcherThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WatchService watchService = FileSystems.getDefault().newWatchService();
                    Path path = settingsFile.getParentFile().toPath();
                    path.register(
                            watchService,
                            StandardWatchEventKinds.ENTRY_CREATE,
                            StandardWatchEventKinds.ENTRY_MODIFY);
                    
                    boolean poll = true;
                    while (poll) {
                        WatchKey key = watchService.take();
                        for (WatchEvent event : key.pollEvents()) {
                            logger.log(Level.INFO,
                                    "Event kind : " + event.kind() + " - File : " + event.context());
                            
                            if (((Path)event.context()).toString().compareTo(settingsFile.getName()) == 0) {
                                // setting file modified externally; restore
                                restore();
                            }
                        }
                        poll = key.reset();
                    }
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }
        });
        fileWatcherThread.start();
    }
    
    protected void loadDefaults() {
        // base class doesn't have any default settings
    }
    
    protected final void checkAndSetDefault(String key, Object defaultVal) {
        if ( ! obj.has(key)) {
            obj.put(key, defaultVal);
        }
    }
    
    protected final Object getSetting(String key) {
        return obj.get(key);
    }
    
    public final void restore() {
        if ( ! settingsFile.exists()) {
            return;
        }
        
        // Wait for file lock to open
        fileAccessLock.lock();
        boolean unlocked = false;

        try {
            FileInputStream fis = new FileInputStream(settingsFile);
            JSONTokener tokener = new JSONTokener(fis);
            JSONObject fileObj = new JSONObject(tokener);
            
            // Release file lock once JSON has been parsed succesfully (ie. no
            // exceptions were thrown). We don't want to wait until the finally
            // block to unlock because notifying listeners could take a while,
            // depending on what they're doing.
            fileAccessLock.unlock();
            unlocked = true;
            
            Set<String> keys = fileObj.keySet();
            for (String key : keys) {
                Object val = fileObj.get(key);
                logger.log(Level.FINER,
                        String.format("Restored %s:%s from file",key,val.toString()));
                
                modifySettingAndNotify(key, val);
            }
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, null, ex);
        } finally {
            if ( ! unlocked) {
                // Release file lock in case JSON parsing failed
                fileAccessLock.unlock();
            }
        }
    }
    
    // method is called automatically when file watcher thread terminates
    public final void save() {
        fileAccessLock.lock();
        
        try {
            FileOutputStream fos = new FileOutputStream(settingsFile);
            fos.write(obj.toString(4).getBytes());
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        } finally {
            fileAccessLock.unlock();
        }
    }
    
    public final void addSettingListener(SettingListener listener) {
        listeners.add(listener);
    }
    
    public final void removeSettingListener(SettingListener listener) {
        listeners.remove(listener);
    }
    
    protected final boolean valueChanged(Object oldValue, Object newValue) {
        if (oldValue.getClass() != newValue.getClass()) {
            throw new RuntimeException(
                    String.format(
                            "Setting types don't match! oldValue is %s newValue is %s",
                            oldValue.getClass(),
                            newValue.getClass()));
        }
        
        boolean changed = false;
        if (oldValue instanceof Boolean) {
            changed = (boolean)oldValue != (boolean)newValue;
        } else if (oldValue instanceof Integer) {
            changed = (int)oldValue != (int)newValue;
        } else if (oldValue instanceof String) {
            changed = ((String)oldValue).compareTo((String)newValue) != 0;
        } else if (oldValue instanceof JSONArray) {
            // Assume JSONArrays always change
            changed = true;
        } else {
            throw new UnsupportedOperationException(
                    String.format(
                            "Can't compare objects of type %s (yet)",
                            oldValue.getClass()));
        }
        
        return changed;
    }
    
    protected final void modifySettingAndNotify(String key, Object newValue) {
        if (obj.has(key)) {
            Object oldValue = obj.get(key);
            obj.put(key, newValue);
            
            if (valueChanged(oldValue, newValue)) {
                // notify listeners of setting change
                for (SettingListener listener : listeners) {
                    listener.onSettingChanged(key, oldValue, newValue);
                }
            }
        } else {
            obj.put(key, newValue);
        }
    }
    
}
