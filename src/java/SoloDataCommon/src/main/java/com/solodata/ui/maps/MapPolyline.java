/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.ui.maps;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.List;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.Layer;
import org.openstreetmap.gui.jmapviewer.MapObjectImpl;
import org.openstreetmap.gui.jmapviewer.Style;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

/**
 *
 * @author daniel
 */
public class MapPolyline extends MapObjectImpl implements MapMarker {
    
    private final ArrayList<Coordinate> points = new ArrayList<>();
    private STYLE markerStyle;
    private JMapViewer map = null;
    private Stroke stroke = new BasicStroke(1);
    
    /**
     * Constructs a new {@code MapMarkerCircle}.
     * @param map
     */
    public MapPolyline(JMapViewer map) {
        this(null, null, map);
    }

    /**
     * Constructs a new {@code MapMarkerCircle}.
     * @param name Name of the map marker
     * @param map
     */
    public MapPolyline(String name, JMapViewer map) {
        this(null, name, map);
    }

    /**
     * Constructs a new {@code MapMarkerCircle}.
     * @param layer Layer of the map marker
     * @param map
     */
    public MapPolyline(Layer layer, JMapViewer map) {
        this(layer, null, map);
    }

    /**
     * Constructs a new {@code MapMarkerCircle}.
     * @param layer Layer of the map marker
     * @param name Name of the map marker
     * @param map
     */
    public MapPolyline(Layer layer, String name, JMapViewer map) {
        this(layer, name, STYLE.VARIABLE, getDefaultStyle(), map);
    }

    /**
     * Constructs a new {@code MapMarkerCircle}.
     * @param layer Layer of the map marker
     * @param name Name of the map marker
     * @param markerStyle Marker style (fixed or variable)
     * @param style Graphical style
     * @param map
     */
    public MapPolyline(Layer layer, String name, STYLE markerStyle, Style style, JMapViewer map) {
        super(layer, name, style);
        this.markerStyle = markerStyle;
        this.map = map;
    }
    
    public List<Coordinate> getCoordinates() {
        return points;
    }
    
    public void addPoint(Coordinate coord) {
        points.add(coord);
    }
    
    @Override
    public void paint(Graphics g, Point position, int radius) {
        if (points.size() < 2) {
            return;
        }
        Graphics2D g2 = (Graphics2D)g;
        g2.setStroke(stroke);
        
        Point p1 = null;
        g.setColor(getColor());
        for (int i=0; i<points.size(); i++) {
            Coordinate c2 = points.get(i);
            Point p2 = map.getMapPosition(c2,false);
            if (p1 != null) {
                g.drawLine(p1.x,p1.y,p2.x,p2.y);
            }
            
            p1 = p2;
        }
    }
    
    public static Style getDefaultStyle() {
        return new Style(Color.RED, new Color(200, 200, 200, 200), null, getDefaultFont());
    }

    @Override
    public Coordinate getCoordinate() {
        return new Coordinate(0, 0);
    }

    @Override
    public double getLat() {
        return getCoordinate().getLat();
    }

    @Override
    public double getLon() {
        return getCoordinate().getLon();
    }

    @Override
    public final double getRadius() {
        /**
         * always return 1.0 degree radius. we'll use this as a conversion
         * factor from degree to pixels in the paint method.
         */
        return 1.0;
    }

    @Override
    public STYLE getMarkerStyle() {
        return markerStyle;
    }

    @Override
    public void setLat(double arg0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLon(double arg0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void setStroke(Stroke stroke) {
        this.stroke = stroke;
    }

}
