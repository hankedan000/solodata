/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.datalogger;

/**
 *
 * @author daniel
 */
public class SensorDataRates {
    public double gpsRateHz;
    public double imuRateHz;
    public double canRateHz;

    public SensorDataRates() {
        this(0,0,0);
    }
    
    public SensorDataRates(double gpsRateHz, double imuRateHz, double canRateHz) {
        this.gpsRateHz = gpsRateHz;
        this.imuRateHz = imuRateHz;
        this.canRateHz = canRateHz;
    }
    
    public double getHighestRate() {
        return Double.max(Double.max(gpsRateHz,imuRateHz),canRateHz);
    }
    
    public double getSlowestRate() {
        return Double.min(Double.min(gpsRateHz,imuRateHz),canRateHz);
    }
}
