/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.ui.maps;

import com.solodata.datalogger.Record;
import java.awt.BasicStroke;
import java.awt.Color;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import solodata.NavSampleOuterClass;

/**
 *
 * @author daniel
 */
public class RecordMapMarkers {
    public static final Color DEFAULT_MAP_LINE_COLOR = new Color(0x2ec4ff);
    
    private static final Logger logger = Logger.getLogger(RecordMapMarkers.class.getName());
    
    private final Record record;
    public final MapMarkerDot headMarker;
    public final MapPolyline route;
    
    // Sample index where head marker is currently pointed
    private int sampIdx = 0;
    
    // Current time offset where header is set to
    //
    // Note: This value is not aligned to a specific sample's timestamp. It's
    // the last time offset that was passed into advToTime()
    private double currTimeOffset = 0;

    /**
     * A class used to store and maintain MapMarkers per record
     * 
     * @param r
     * the record the markers belong to
     * 
     * @param map
     * the map that the markers will be placed into
     */
    public RecordMapMarkers(Record r, JMapViewer map) {
        record = r;

        headMarker = new MapMarkerDot(new Coordinate(0,0));
        headMarker.setStroke(new BasicStroke(2));

        route = new MapPolyline(map);
        route.setColor(DEFAULT_MAP_LINE_COLOR);
        route.setStroke(new BasicStroke(
                5,
                BasicStroke.CAP_ROUND,
                BasicStroke.JOIN_ROUND));

        setVisible(false);

        // add marker to map
        map.addMapMarker(route);
        map.addMapMarker(headMarker);
    }

    public void setVisible(boolean visible) {
        headMarker.setVisible(visible);
        route.setVisible(visible);
    }

    /**
     * Advances the tracer to the nearest time offset with a valid GPS
     * sample.
     * 
     * @param dt 
     * delta time offset relative to the beginning of the record
     */
    public void advToTime(double dt) {
        logger.log(Level.FINER,"dt = {0}",dt);
        List<NavSampleOuterClass.NavSample> samps = record.getNavSamples();
        if (samps == null) {
            return;
        }
        if (samps.size() < 1) {
            return;
        }

        // make sure sampIdx is within range
        sampIdx = Integer.min(sampIdx, samps.size() - 1);
        sampIdx = Integer.max(sampIdx, 0);

        logger.log(Level.FINER,"sampIdx BEFORE = {0}",sampIdx);
        
        double startTime = record.getStartTimeSeconds();
        if (currTimeOffset < dt) {
            // Advancing forward in time
            for ( ; sampIdx < samps.size(); sampIdx++) {
                NavSampleOuterClass.NavSample samp = samps.get(sampIdx);
                double currOffset = samp.getUtcUs() / 1.0e6 - startTime;
                if (samp.hasGps() && currOffset >= dt) {
                    // found the next GPS sample. stop
                    headMarker.setLat(samp.getGps().getPosition().getLatDd());
                    headMarker.setLon(samp.getGps().getPosition().getLonDd());
                    break;
                }
            }
        } else {
            // Advancing backwards in time
            for ( ; sampIdx > 0; sampIdx--) {
                NavSampleOuterClass.NavSample samp = samps.get(sampIdx);
                double currOffset = samp.getUtcUs() / 1.0e6 - startTime;
                if (samp.hasGps() && currOffset <= dt) {
                    // found the next GPS sample. stop
                    headMarker.setLat(samp.getGps().getPosition().getLatDd());
                    headMarker.setLon(samp.getGps().getPosition().getLonDd());
                    break;
                }
            }
        }
        
        logger.log(Level.FINER,"sampIdx AFTER = {0}",sampIdx);
        
        // Update time offset with new delta
        currTimeOffset = dt;
    }
}
