/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.inputstream.ZipInputStream;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.util.UnzipUtil;

/**
 * Example demonstrating the use of InputStreams to extract files from the
 * ZipFile
 */
public class FileUtils {
    private static final String LOG_TAG = FileUtils.class.getName();
    private static final Logger logger = Logger.getLogger(LOG_TAG);
    
    private static final int BUFF_SIZE = 4096;

    public static File getTempDir() {
        String osTmp = System.getProperty("java.io.tmpdir");
        File solodataTmp = new File(osTmp,"solodata");
        if ( ! solodataTmp.exists()) {
            if ( ! solodataTmp.mkdirs()) {
                throw new RuntimeException(String.format(
                        "Failed to create temporary directory '%s'",
                        solodataTmp.toString()));
            }
        }
        return solodataTmp;
    }
    
    /**
     * Copies contents of the the InputStream to a file.
     * 
     * @param is
     * The InputStream to begin copying from.
     * 
     * @param outFile
     * A writable file to copy contents into.
     * 
     * @return 
     * True if all contents were copied successfully, false otherwise.
     */
    public static boolean streamToFile(InputStream is, File outFile) {
        boolean okay = true;
        
        // see if parent dirs exists, if not make them
        File parentDir = outFile.getParentFile();
        if ( ! parentDir.exists() && ! parentDir.mkdirs()) {
            logger.log(Level.SEVERE,
                    "Failed to create parent directories for ''{0}''",
                    outFile);
            okay = false;
        }
        
        FileOutputStream fos = null;
        if (okay) {
            try {
                fos = new FileOutputStream(outFile);
            } catch (FileNotFoundException ex) {
                logger.log(Level.SEVERE,null,ex);
                okay = false;
            }
        }
        
        if (fos != null) {
            try {
                is.transferTo(fos);
            } catch (IOException ex) {
                logger.log(Level.SEVERE,null,ex);
                okay = false;
            } finally {
                try {
                    fos.close();
                } catch (IOException ex) {
                    logger.log(Level.SEVERE,null,ex);
                    okay = false;
                }
            }
        }
        
        return okay;
    }
    
    public static boolean extractAll(File zipPath, File destPath) {
        boolean okay = true;
        ZipInputStream is = null;
        OutputStream os = null;
        
        try {
            // Initiate the ZipFile
            ZipFile zipFile = new ZipFile(zipPath);
            // If zip file is password protected then set the password
            if (zipFile.isEncrypted()) {
                String password = "password";
                zipFile.setPassword(password.toCharArray());
            }
            // Get a list of FileHeader. FileHeader is the header information
            // for all the files in the ZipFile
            List fileHeaderList = zipFile.getFileHeaders();
            // Loop through all the fileHeaders
            for (int i = 0; i < fileHeaderList.size(); i++) {
                FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
                if (fileHeader != null) {
                    // Build the output file
                    String outFilePath = destPath.toString()
                        + System.getProperty("file.separator")
                        + fileHeader.getFileName();
                    File outFile = new File(outFilePath);
                    // Checks if the file is a directory
                    if (fileHeader.isDirectory()) {
                        // This functionality is up to your requirements
                        // For now I create the directory
                        outFile.mkdirs();
                        continue;
                    }
                    // Check if the directories(including parent directories)
                    // in the output file path exists
                    File parentDir = outFile.getParentFile();
                    if (!parentDir.exists()) {
                        parentDir.mkdirs();
                    }
                    // Get the InputStream from the ZipFile
                    is = zipFile.getInputStream(fileHeader);
                    // Initialize the output stream
                    os = new FileOutputStream(outFile);
                    int readLen = -1;
                    byte[] buff = new byte[BUFF_SIZE];
                    // Loop until End of File and write the contents to the
                    // output stream
                    while ((readLen = is.read(buff)) != -1) {
                        os.write(buff, 0, readLen);
                    }
                    // Please have a look into this method for some important
                    // comments
                    closeFileHandlers(is, os);
                    // To restore File attributes (ex: last modified file time,
                    // read only flag, etc) of the extracted file, a utility
                    // class can be used as shown below
                    UnzipUtil.applyFileAttributes(fileHeader, outFile);
                    logger.log(Level.FINER,
                            "Done extracting: {0}", fileHeader.getFileName());
                } else {
                    logger.log(Level.SEVERE,
                            "fileheader is null. Shouldn't be here");
                    okay = false;
                }
            }
        } catch (ZipException e) {
            logger.log(Level.SEVERE,
                    "Caught ZipException: {0}",new Object[]{e});
            okay = false;
        } catch (FileNotFoundException e) {
            logger.log(Level.SEVERE,
                    "Caught FileNotFoundException: {0}",e);
            okay = false;
        } catch (IOException e) {
            logger.log(Level.SEVERE,
                    "Caught IOException: {0}",e);
            okay = false;
        } catch (Exception e) {
            logger.log(Level.SEVERE,
                    "Caught Exception: {0}",e);
            okay = false;
        } finally {
            try {
                closeFileHandlers(is, os);
            } catch (IOException e) {
                logger.log(Level.SEVERE,
                        "Caught IOException while closing file handlers: {0}",e);
                okay = false;
            }
        }
        
        return okay;
    }

    private static void closeFileHandlers(ZipInputStream is, OutputStream os)
            throws IOException {
        // Close output stream
        if (os != null) {
            os.close();
            os = null;
        }
        // Closing inputstream also checks for CRC of the the just extracted
        // file. If CRC check has to be skipped (for ex: to cancel the unzip
        // operation, etc) use method is.close(boolean skipCRCCheck) and set the
        // flag, skipCRCCheck to false
        // NOTE: It is recommended to close outputStream first because Zip4j
        // throws an exception if CRC check fails
        if (is != null) {
            is.close();
            is = null;
        }
    }
}
