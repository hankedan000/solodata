/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.ui.maps;

import java.awt.Graphics;
import java.awt.Point;
import javax.vecmath.Vector2f;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;

/**
 *
 * @author daniel
 */
public class Vector2MapMarker extends MapMarkerDot {
    private final Vector2f vector;
    private float pxPerUnit;
    
    public Vector2MapMarker(Coordinate coord) {
        this(coord,0,0);
    }
    
    public Vector2MapMarker(Coordinate coord, float x, float y) {
        this(coord,new Vector2f(x,y));
    }
    
    public Vector2MapMarker(Coordinate coord, Vector2f v) {
        super(coord);
        this.vector = v;
        this.pxPerUnit = 1;
    }
    
    public void setDrawScale(float pxPerUnit) {
        this.pxPerUnit = pxPerUnit;
    }
    
    public void setX(float x) {
        vector.x = x;
    }
    
    public void setY(float y) {
        vector.y = y;
    }
    
    public float getX() {
        return vector.x;
    }
    
    public float setY() {
        return vector.y;
    }
    
    public Vector2f getVector() {
        return new Vector2f(vector);
    }

    @Override
    public void paint(Graphics g, Point position, int radius) {
        super.paint(g, position, radius); //To change body of generated methods, choose Tools | Templates.
    }
    
}
