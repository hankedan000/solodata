/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.datalogger;

import com.solodata.utils.FileUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FilenameUtils;
import protorecord.Reader;
import solodata.CANSample;
import solodata.cand.CandIndex;
import solodata.NavSampleOuterClass;

/**
 *
 * @author daniel
 */
public class Record {
    private static final String LOG_TAG = Record.class.getName();
    private static final Logger logger = Logger.getLogger(LOG_TAG);
    
    private String recordName_ = "**INVALID_RECORD**";
    protected Reader<NavSampleOuterClass.NavSample> navReader_ = null;
    protected Reader<CANSample.CAN_Sample> canReader_ = null;
    protected Reader<CandIndex.Index> canIndexReader_ = null;
    private List<NavSampleOuterClass.NavSample> navSamples = null;
    
    // Start time as determined by earliest Nav or CAN sample timestamp
    private final double startTimeUs;
    // Stop time as determined by latest Nav or CAN sample timestamp
    private final double stopTimeUs;
    
    /**
     * Constructs a new DataLoggerRecord given the recording directory
     * 
     * @param recordPath 
     * The root directory of the DataLogger recording
     */
    public Record(File recordPath) {
        // build paths to protorecord directories inside the record
        File navSamplesPath = new File(recordPath,"nav_samples");
        File canSamplesPath = new File(recordPath,"can_samples");
        File canIndexPath = new File(recordPath,"can_index");
        recordName_ = recordPath.getName();
        navReader_ = new Reader<>(navSamplesPath,NavSampleOuterClass.NavSample.parser());
        canReader_ = new Reader<>(canSamplesPath,CANSample.CAN_Sample.parser());
        canIndexReader_ = new Reader<>(canIndexPath,CandIndex.Index.parser());
        
        // Compute Record start/stop time based on Nav/CAN sample timestamps
        double earliestTime = 0;
        double latestTime = 0;
        boolean timesValid = false;
        if (hasNav()) {
            earliestTime = navReader_.get_item(0).getUtcUs();
            latestTime = navReader_.get_item(navReader_.size() - 1).getUtcUs();
            timesValid = true;
        }
        if (hasCan()) {
            earliestTime = Double.min(earliestTime, canReader_.get_item(0).getTimestamp());
            latestTime = Double.max(latestTime, canReader_.get_item(canReader_.size() - 1).getTimestamp());
            timesValid = true;
        }
        
        logger.log(Level.FINE,
                "navReader_.get_start_time = {0}; canReader_.get_start_time = {1}",
                new Object[]{navReader_.get_start_time(),canReader_.get_start_time()});
        // Assign final timestamps based on validity
        if (timesValid) {
            startTimeUs = earliestTime;
            stopTimeUs = latestTime;
        } else {
            startTimeUs = 0.0;
            stopTimeUs = 0.0;
        }
        logger.log(Level.FINE,
                "record {0}: startTimeUs = {1}; stopTimeUs = {2}",
                new Object[]{getName(),startTimeUs,stopTimeUs});
    } 
    
    /**
     * Constructs a Record by unzipping the archived record to a temporary
     * path and opening it from there.
     * 
     * @param zipFile
     * The path to the zipped recording
     * 
     * @return 
     * The constructed Record. A null object is returned if an error occurred.
     */
    public static Record fromZip(File zipFile) {
        File tempDir = FileUtils.getTempDir();
        File extractTemp = new File(tempDir,"extracted_records");
        Record dlr = null;
        
        if (zipFile != null && FileUtils.extractAll(zipFile,extractTemp)) {
            String zipNameNoExt = FilenameUtils.removeExtension(zipFile.getName());
            File recordPath = new File(extractTemp,zipNameNoExt);
            dlr = new Record(recordPath);
        }
        
        return dlr;
    }
    
    public String getName() {
        return recordName_;
    }
    
    public boolean hasNav() {
        return navReader_ != null && navReader_.size() > 0;
    }
    
    public boolean hasCan() {
        return canReader_ != null && canReader_.size() > 0;
    }
    
    public long navSize() {
        return (hasNav() ? navReader_.size(): 0);
    }
    
    public long canSize() {
        return (hasCan()? canReader_.size() : 0);
    }
    
    public double getStartTimeSeconds() {
        return startTimeUs / 1.0e6;
    }
    
    public double getStopTimeSeconds() {
        return stopTimeUs / 1.0e6;
    }
    
    public double getDurationSeconds() {
        return getStopTimeSeconds() - getStartTimeSeconds();
    }
    
    public Date getDate() {
        long startTime_ms = (long)getStartTimeSeconds() * 1000;
        return new Date(startTime_ms);
    }
    
    public List<NavSampleOuterClass.NavSample> getNavSamples() {
        if (navSamples == null) {
            navSamples = new ArrayList<>((int)navSize());
            int i = 0;
            while (navReader_.has_next()) {
                navSamples.add(i++, navReader_.take_next());
            }
        }
        return navSamples;
    }
}
