/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.ui.maps.tiles;

import java.io.IOException;
import org.openstreetmap.gui.jmapviewer.tilesources.TMSTileSource;
import org.openstreetmap.gui.jmapviewer.tilesources.TileSourceInfo;

/**
 *
 * @author daniel
 */
public class LocalhostTileSource extends TMSTileSource {
    public static final int DEFAULT_TILE_SOURCE_PORT = 8787;
    
    private final String name;
    
    private int serverPort;
    
    public LocalhostTileSource(String sourceName) {
        this(sourceName,DEFAULT_TILE_SOURCE_PORT);
    }
    
    public LocalhostTileSource(String sourceName, int port) {
        super(new TileSourceInfo("Localhost", null, null));
        minZoom = 1;
        maxZoom = 20;
        name = sourceName;
        serverPort = port;
    }

    @Override
    public String getTileUrl(int zoom, int tilex, int tiley) throws IOException {
        return String.format(
                "http://localhost:%d/%s/tiles/z%d/x%dy%d.jpeg",
                serverPort,name,zoom,tilex,tiley);
    }
}
