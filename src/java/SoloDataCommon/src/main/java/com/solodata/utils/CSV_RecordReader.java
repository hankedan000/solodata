/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniel
 */
public class CSV_RecordReader {
    private final Logger logger = Logger.getLogger(CSV_RecordReader.class.getName());
    private final ArrayList<String> columnTitles = new ArrayList<>();
    private final ArrayList<ArrayList<Double>> columnData = new ArrayList<>();
    
    public CSV_RecordReader(File file, boolean firstRowAreTitles) throws FileNotFoundException {
        processBuffer(new BufferedReader(new FileReader(file)), firstRowAreTitles);
    }
    
    public CSV_RecordReader(String text, boolean firstRowAreTitles) {
        processBuffer(new BufferedReader(new StringReader(text)), firstRowAreTitles);
    }
    
    public boolean hasTitles() {
        return columnTitles.size() > 0;
    }
    
    public int columnCount() {
        return Math.max(columnTitles.size(), columnData.size());
    }
    
    public String getColumnTitle(int col) {
        return columnTitles.get(col);
    }
    
    public List<Double> getImmutableColumnData(int col) {
        return Collections.unmodifiableList(columnData.get(col));
    }
    
    private void processBuffer(BufferedReader reader, boolean firstRowAreTitles) {
        try {
            String line = "";
            int row = 0;// row in CSV
            while ((line = reader.readLine()) != null) {
                boolean isTitleRow = firstRowAreTitles && row == 0;
                processLine(line, isTitleRow);
                row++;
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE,
                    "Caught IOException while processing CSV. {1}",
                    new Object[]{ex});
        }
    }
    
    private void processLine(String line, boolean isTitleRow) {
        String cols[] = line.split(",");
        
        if (isTitleRow) {
            if ( ! columnTitles.isEmpty()) {
                throw new RuntimeException("Can not process more than one title line");
            }
            
            for (String col : cols) {
                columnTitles.add(col);
            }
        } else {
            // initialize data vector for the first time
            if (columnData.isEmpty()) {
                columnData.ensureCapacity(cols.length);
                for (int cc=0; cc<cols.length; cc++) {
                    columnData.add(new ArrayList<Double>(1024));
                }
            }

            // fill data vectors with values from CSV
            for (int cc=0; cc<cols.length; cc++) {
                columnData.get(cc).add(Double.parseDouble(cols[cc]));
            }
        }
    }
}
