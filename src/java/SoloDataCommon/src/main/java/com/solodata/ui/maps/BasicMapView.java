/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.ui.maps;

import com.solodata.ui.maps.tiles.LocalhostTileSource;
import com.solodata.ui.TouchScreenMapController;
import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.DefaultMapController;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MemoryTileCache;
import org.openstreetmap.gui.jmapviewer.OsmTileLoader;
import org.openstreetmap.gui.jmapviewer.events.JMVCommandEvent;
import org.openstreetmap.gui.jmapviewer.interfaces.JMapViewerEventListener;
import org.openstreetmap.gui.jmapviewer.interfaces.TileLoader;
import org.openstreetmap.gui.jmapviewer.interfaces.TileSource;
import org.openstreetmap.gui.jmapviewer.tilesources.BingAerialTileSource;
import org.openstreetmap.gui.jmapviewer.tilesources.OsmTileSource;

/**
 *
 * @author daniel
 */
public class BasicMapView extends javax.swing.JPanel implements
        JMapViewerEventListener {
    private static final String LOG_TAG = BasicMapView.class.getName();
    
    public static class TileSourceNames {
        public final static String OSM_CARTOGRAPHER = "osm_carto";
        public final static String OSM_PUBLIC_TRANSPORT = "public_transport";
        public final static String BING_SATELITE = "bing_satelite";
    }
    
    private static final long serialVersionUID = 1L;

    private MemoryTileCache tileCache;
    private JMapViewer map;
    private TouchScreenMapController touchController = null;
    private DefaultMapController defaultController = null;

    private JLabel zoomLabel;
    private JLabel zoomValue;

    private JLabel mperpLabelName;
    private JLabel mperpLabelValue;
    
    private JComboBox<String> tileSourceSelector;
    private JPanel ctrlPanel;
    
    private TileSource currTileSource = null;

    /**
     * Creates new form LiveMapView
     */
    public BasicMapView() {
        initComponents();
        setSize(400, 400);
        
        // configure touch screen mode based on application Settings
        setTouchScreenMode(false);
        
        // Listen to the map viewer for user operations so components will
        // receive events and update
        map().addJMVListener(this);
        
        setControlPanelVisible(true);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Init Components Code">                          
    private void initComponents() {
        setLayout(new BorderLayout());
        
        tileCache = new MemoryTileCache();
        map = new JMapViewer(tileCache);
        
        ctrlPanel = new JPanel(new BorderLayout());
        JPanel panelTop = new JPanel();
        JPanel panelBottom = new JPanel();
        JPanel helpPanel = new JPanel();

        mperpLabelName = new JLabel("Meters/Pixels: ");
        mperpLabelValue = new JLabel(String.format("%s", map().getMeterPerPixel()));

        zoomLabel = new JLabel("Zoom: ");
        zoomValue = new JLabel(String.format("%s", map().getZoom()));

        add(ctrlPanel, BorderLayout.NORTH);
        add(helpPanel, BorderLayout.SOUTH);
        ctrlPanel.add(panelTop, BorderLayout.NORTH);
        ctrlPanel.add(panelBottom, BorderLayout.SOUTH);
        JLabel helpLabel = new JLabel("Use right mouse button to move,\n "
                + "left double click or mouse wheel to zoom.");
        helpPanel.add(helpLabel);
        JButton button = new JButton("setDisplayToFitMapMarkers");
        button.addActionListener(e -> map().setDisplayToFitMapMarkers());
        tileSourceSelector = new JComboBox<>(new String[] {
                TileSourceNames.OSM_CARTOGRAPHER,
                TileSourceNames.OSM_PUBLIC_TRANSPORT,
                TileSourceNames.BING_SATELITE
        });
        tileSourceSelector.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                setTileSourceFromName((String)e.getItem(), false);
            }
        });
        // select public transport by default
        tileSourceSelector.setSelectedIndex(1);
        
        JComboBox<TileLoader> tileLoaderSelector;
        tileLoaderSelector = new JComboBox<>(new TileLoader[] {new OsmTileLoader(map())});
        tileLoaderSelector.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                map().setTileLoader((TileLoader) e.getItem());
            }
        });
        map().setTileLoader((TileLoader) tileLoaderSelector.getSelectedItem());
        panelTop.add(tileSourceSelector);
        panelTop.add(tileLoaderSelector);
        final JCheckBox showMapMarker = new JCheckBox("Map markers visible");
        showMapMarker.setSelected(map().getMapMarkersVisible());
        showMapMarker.addActionListener(e -> map().setMapMarkerVisible(showMapMarker.isSelected()));
        panelBottom.add(showMapMarker);
        ///
        final JCheckBox showToolTip = new JCheckBox("ToolTip visible");
        showToolTip.addActionListener(e -> map().setToolTipText(null));
        panelBottom.add(showToolTip);
        ///
        final JCheckBox showTileGrid = new JCheckBox("Tile grid visible");
        showTileGrid.setSelected(map().isTileGridVisible());
        showTileGrid.addActionListener(e -> map().setTileGridVisible(showTileGrid.isSelected()));
        panelBottom.add(showTileGrid);
        final JCheckBox showZoomControls = new JCheckBox("Show zoom controls");
        showZoomControls.setSelected(map().getZoomControlsVisible());
        showZoomControls.addActionListener(e -> map().setZoomControlsVisible(showZoomControls.isSelected()));
        panelBottom.add(showZoomControls);
        final JCheckBox scrollWrapEnabled = new JCheckBox("Scrollwrap enabled");
        scrollWrapEnabled.addActionListener(e -> map().setScrollWrapEnabled(scrollWrapEnabled.isSelected()));
        panelBottom.add(scrollWrapEnabled);
        panelBottom.add(button);

        panelTop.add(zoomLabel);
        panelTop.add(zoomValue);
        panelTop.add(mperpLabelName);
        panelTop.add(mperpLabelValue);

        add(map, BorderLayout.CENTER);
    }// </editor-fold>    

    public JMapViewer map() {
        return map;
    }
    
    public void setTileSource(TileSource tileSource) {
        currTileSource = tileSource;
        map().setTileSource(tileSource);
    }
    
    public TileSource getTileSource() {
        return currTileSource;
    }
    
    public TileSource selectedTileSource(boolean offline) {
        return getTileSourceFromName((String)tileSourceSelector.getSelectedItem(),offline);
    }
    
    public boolean setOfflineTileSource(String sourceName)
    {
        return setTileSourceFromName(sourceName, true);
    }
    
    public static TileSource getTileSourceFromName(String name, boolean offline) {
        TileSource source = null;
        if (offline) {
            source = new LocalhostTileSource(name);
        } else {
            switch (name) {
                case TileSourceNames.OSM_PUBLIC_TRANSPORT:
                    source = new OsmTileSource.TransportMap();
                    break;
                case TileSourceNames.OSM_CARTOGRAPHER:
                    source = new OsmTileSource.Mapnik();
                    break;
                case TileSourceNames.BING_SATELITE:
                    source = new BingAerialTileSource();
                    break;
                default:
                    Logger.getLogger(LOG_TAG).log(Level.SEVERE,
                            "Unsupported tile source name ''{0}''",
                            name);
                    source = null;
                    break;
            }
        }
        return source;
    }
    
    public boolean setTileSourceFromName(String name, boolean offline) {
        TileSource newSource = getTileSourceFromName(name, offline);
        
        if (newSource != null) {
            setTileSource(newSource);
        }
        
        return newSource != null;
    }
    
    public final void setControlPanelVisible(boolean visible) {
        ctrlPanel.setVisible(visible);
    }
    
    public final void setTouchScreenMode(boolean touchScreenMode) {
        if (touchController != null) {
            map.removeMouseListener(touchController);
            map.removeMouseMotionListener(touchController);
            // map.removeMouseWheelListener(touchController);// doesn't implement this
            touchController = null;
        }
        if (defaultController != null) {
            map.removeMouseListener(defaultController);
            map.removeMouseMotionListener(defaultController);
            map.removeMouseWheelListener(defaultController);
            defaultController = null;
        }
        
        if (touchScreenMode) {
            touchController = new TouchScreenMapController(map);
        } else {
            defaultController = new DefaultMapController(map);
        }
    }
    
    public void setDisplayToFitPolyline(MapPolyline polyline) {
        int nbElemToCheck = polyline.getCoordinates().size();
        if (nbElemToCheck == 0)
            return;
        if ( ! polyline.isVisible())
            return;

        int xMin = Integer.MAX_VALUE;
        int yMin = Integer.MAX_VALUE;
        int xMax = Integer.MIN_VALUE;
        int yMax = Integer.MIN_VALUE;
        int mapZoomMax = getTileSource().getMaxZoom();

        List<Coordinate> coords = polyline.getCoordinates();
        for (Coordinate coord : coords) {
            Point p = getTileSource().latLonToXY(coord, mapZoomMax);
            xMax = Math.max(xMax, p.x);
            yMax = Math.max(yMax, p.y);
            xMin = Math.min(xMin, p.x);
            yMin = Math.min(yMin, p.y);
        }

        int height = Math.max(0, getHeight());
        int width = Math.max(0, getWidth());
        int newZoom = mapZoomMax;
        int x = xMax - xMin;
        int y = yMax - yMin;
        while (x > width || y > height) {
            newZoom--;
            x >>= 1;
            y >>= 1;
        }
        x = xMin + (xMax - xMin) / 2;
        y = yMin + (yMax - yMin) / 2;
        int z = 1 << (mapZoomMax - newZoom);
        x /= z;
        y /= z;
        map().setDisplayPosition(x, y, newZoom);
    }

    private void updateZoomParameters() {
        if (mperpLabelValue != null)
            mperpLabelValue.setText(String.format("%s", map().getMeterPerPixel()));
        if (zoomValue != null)
            zoomValue.setText(String.format("%s", map().getZoom()));
    }
    
    @Override
    public void processCommand(JMVCommandEvent command) {
        if (command.getCommand().equals(JMVCommandEvent.COMMAND.ZOOM) ||
                command.getCommand().equals(JMVCommandEvent.COMMAND.MOVE)) {
            updateZoomParameters();
        }
    }

}
