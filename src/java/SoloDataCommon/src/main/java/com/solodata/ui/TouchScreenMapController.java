/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata.ui;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openstreetmap.gui.jmapviewer.JMapController;
import org.openstreetmap.gui.jmapviewer.JMapViewer;

/**
 * Default map controller which implements map moving by pressing the right
 * mouse button and zooming by double click or by mouse wheel.
 *
 * @author Jan Peter Stotz
 *
 */
public class TouchScreenMapController extends JMapController implements
        MouseListener,
        MouseMotionListener {
    
    enum States {
        IDLE,
        SINGLE_CLICK_DOWN,
        SINGLE_CLICK_UP,
        DOUBLE_CLICK_DOWN,
        DOUBLE_CLICK_UP,
        PANNING,
        ZOOMING
    }

    private Point lastDragPoint;
    private Point latestPoint;
    private Point initZoomPoint;
    private int initZoomLevel;
    
    // number of vertical pixels per zoom level step when double tap zooming
    private final int PIXEL_DRAG_PER_ZOOM = 20;

    private boolean doubleClickZoomEnabled = true;
    
    private States currState = States.IDLE;
    
    private final Timer timer = new Timer(true);
    
    private TimerTask timeoutTask = null;
    
    /**
     * Constructs a new {@code DefaultMapController}.
     * @param map map panel
     */
    public TouchScreenMapController(JMapViewer map) {
        super(map);
        entered(currState);
    }
    
    private void entered(States state) {
        switch (state) {
            case IDLE:
                break;
            case SINGLE_CLICK_DOWN:
                break;
            case SINGLE_CLICK_UP:
                scheduleTimeout(500);// 500ms
                break;
            case DOUBLE_CLICK_DOWN:
                break;
            case DOUBLE_CLICK_UP:
                if (doubleClickZoomEnabled) {
                    map.zoomIn(latestPoint);
                }
                scheduleTimeout(5);// 5ms
                break;
            case PANNING:
                lastDragPoint = latestPoint;
                break;
            case ZOOMING:
                initZoomPoint = latestPoint;
                initZoomLevel = map.getZoom();
                lastDragPoint = latestPoint;
                break;
        }
    }
    
    private void exited(States state) {
        switch (state) {
            case IDLE:
                break;
            case SINGLE_CLICK_DOWN:
                break;
            case SINGLE_CLICK_UP:
                cancelTimeout();
                break;
            case DOUBLE_CLICK_DOWN:
                break;
            case DOUBLE_CLICK_UP:
                cancelTimeout();
                break;
            case PANNING:
                break;
            case ZOOMING:
                break;
        }
    }
    
    private void transition(States nextState) {
        Logger.getLogger(TouchScreenMapController.class.getName()).log(
                Level.FINE,
                "STATE TRANSITION: {0} -> {1}",
                new Object[]{currState.toString(), nextState.toString()});
        exited(currState);
        entered(nextState);
        currState = nextState;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        latestPoint = e.getPoint();
        switch (currState) {
            case IDLE:
                break;
            case SINGLE_CLICK_DOWN:
                transition(States.PANNING);
                break;
            case SINGLE_CLICK_UP:
                break;
            case DOUBLE_CLICK_DOWN:
                transition(States.ZOOMING);
                break;
            case DOUBLE_CLICK_UP:
                break;
            case PANNING:
            {
                int diffx = lastDragPoint.x - latestPoint.x;
                int diffy = lastDragPoint.y - latestPoint.y;
                map.moveMap(diffx, diffy);
                break;
            }
            case ZOOMING:
            {
                int diffy = latestPoint.y - initZoomPoint.y;
                int zoomDelta = diffy / PIXEL_DRAG_PER_ZOOM;
                map.setZoom(initZoomLevel + zoomDelta, initZoomPoint);
                break;
            }
        }
        lastDragPoint = latestPoint;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        latestPoint = e.getPoint();
        switch (currState) {
            case IDLE:
                transition(States.SINGLE_CLICK_DOWN);
                break;
            case SINGLE_CLICK_DOWN:
                break;
            case SINGLE_CLICK_UP:
                transition(States.DOUBLE_CLICK_DOWN);
                break;
            case DOUBLE_CLICK_DOWN:
                break;
            case DOUBLE_CLICK_UP:
                break;
            case PANNING:
                break;
            case ZOOMING:
                break;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        latestPoint = e.getPoint();
        switch (currState) {
            case IDLE:
                break;
            case SINGLE_CLICK_DOWN:
                transition(States.SINGLE_CLICK_UP);
                break;
            case SINGLE_CLICK_UP:
                break;
            case DOUBLE_CLICK_DOWN:
                transition(States.DOUBLE_CLICK_UP);
                break;
            case DOUBLE_CLICK_UP:
                break;
            case PANNING:
                transition(States.IDLE);
                break;
            case ZOOMING:
                transition(States.IDLE);
                break;
        }
    }
    
    private void scheduleTimeout(long delay) {
        cancelTimeout();
        
        timeoutTask = new TimerTask() {
            @Override
            public void run() {
                transition(States.IDLE);
            }
        };
        
        timer.schedule(timeoutTask, delay);
    }
    
    private void cancelTimeout() {
        if (timeoutTask != null) {
            timeoutTask.cancel();
            timeoutTask = null;
        }
    }

    public boolean isDoubleClickZoomEnabled() {
        return doubleClickZoomEnabled;
    }

    public void setDoubleClickZoomEnabled(boolean doubleClickZoomEnabled) {
        this.doubleClickZoomEnabled = doubleClickZoomEnabled;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // do nothing
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // do nothing
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        // do nothing
    }

}
