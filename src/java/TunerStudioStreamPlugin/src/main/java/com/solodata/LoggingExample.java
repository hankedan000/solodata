/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solodata;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author daniel
 */
public class LoggingExample {
    private static final Logger log = Logger.getLogger(LoggingExample.class.getName());
    
    public static void main(String args[]) {
        PropertyConfigurator.configure("/home/daniel/git/solodata/src/java/TunerStudioStreamPlugin/src/main/java/com/solodata/log4j.properties");
        log.info("Helloworld!");
    }
}
