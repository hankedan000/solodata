package com.example.solodata

import com.google.android.gms.maps.model.LatLng
import org.json.JSONObject
import kotlin.math.PI
import kotlin.math.sqrt

fun radians(deg: Float): Float {
    return (deg * PI / 180.0f).toFloat()
}

fun deg2meters(d: Float): Float {
    // earth radius in meters
    val EARTH_RAD_M = 6371000.0f
    return EARTH_RAD_M * radians(d)
}

fun meters2miles(m: Float): Float {
    return (m * 2 / 3218.69).toFloat()
}

fun sec2hrs(s: Float): Float {
    return s / 60f / 60f
}

fun knots2mph(knots: Float): Float {
    return (knots * 1.15078).toFloat()
}

class NavSample (obj: JSONObject) {
    var utc_sec: Int = 0
    var utc_usec: Int = 0
    var coord: LatLng
    var velInferred = false
    var velValid = false
    var vel_mph = 0f

    init {
        val utc = obj.getDouble("utc")
        utc_sec = Math.floor(utc).toInt()
        utc_usec = ((utc - utc_sec) * 1.0e6).toInt()
        val lat = obj.getDouble("lat")
        val lon = obj.getDouble("lon")
        coord = LatLng(lat,lon)
        velInferred = false
        velValid = true
        try {
            val vel_knots = obj.getDouble("speed_knots")
            vel_mph = knots2mph(vel_knots.toFloat())
        } catch (ex: RuntimeException) {
            vel_mph = 0.0f
            velValid = false
        }
    }

    fun isValid(): Boolean {
        var valid = true

        valid = valid && -90 < coord.latitude && coord.latitude < 90
        valid = valid && -180 < coord.longitude && coord.longitude < 180

        return valid
    }

    fun isVelocityValid(): Boolean {
        return velValid
    }

    fun getTime(): Double {
        var time = 0.0
        time += utc_sec.toDouble()
        time += utc_usec / 1.0e6
        return time
    }

    fun inferVelocity(next: NavSample): Boolean {
        var okay = true

        if (velValid) {
            println("Warning! Inferring velocity, but velocity was already valid.")
        }

        val dt = (next.getTime() - getTime()).toFloat()
        if (dt > 0) {
            var lat = next.coord.latitude
            var lon = next.coord.longitude
            lat -= coord.latitude
            lon -= coord.longitude

            val deg = sqrt(lat*lat+lon*lon)
            val dist_meters = deg2meters(deg.toFloat())
            val dist_miles = meters2miles(dist_meters)

            velInferred = true
            velValid = true
            vel_mph = dist_miles / sec2hrs(dt)
            //println("vel = " + vel_mph);
            //println("\t dt = " + dt);
            //println("\t dist_miles = " + dist_miles);
        } else {
            println("dt is $dt. Can't compute velocity from negative time delta")
            okay = false
        }

        return okay
    }

    fun getVelocity(): Float {
        return vel_mph
    }

    override fun toString(): String {
        return toJSON().toString()
    }

    fun toJSON(): JSONObject {
        val obj = JSONObject()

        obj.put("utc_sec", utc_sec)
        obj.put("utc_usec", utc_usec)
        obj.put("lat", coord.latitude)
        obj.put("lon", coord.longitude)

        return obj
    }
}