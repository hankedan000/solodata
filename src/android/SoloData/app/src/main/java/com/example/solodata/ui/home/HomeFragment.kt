package com.example.solodata.ui.home

import android.animation.ArgbEvaluator
import android.content.res.Resources
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.solodata.NavDataset
import com.example.solodata.NavSample
import com.example.solodata.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.PolylineOptions
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream


class HomeFragment : Fragment(), OnMapReadyCallback {
    private var TAG: String = "HomeFragment"
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var mapView : MapView
    private lateinit var gmap : GoogleMap

    fun readDataset(
    ): NavDataset {
        val ds = NavDataset()

        try {
            val inputStream: InputStream = resources.openRawResource(R.raw.river_road)
            val jsonStr = inputStream.bufferedReader().use { it.readText() }
            val jsonArr = JSONArray(jsonStr)
            for (i in 0 until jsonArr.length())
            {
                ds.add(NavSample(jsonArr.getJSONObject(i)))
            }
            Log.i(TAG,"Loaded ${ds.samplesClean.size} sample(s)")
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return ds
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        mapView = root.findViewById(R.id.mapView)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)

        return root
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onPause() {
        mapView.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        mapView.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    fun map(
        value: Float,
        istart: Float,
        istop: Float,
        ostart: Float,
        ostop: Float
    ): Float {
        return ostart + (ostop - ostart) * ((value - istart) / (istop - istart))
    }

    fun drawGradientPolyline(googleMap: GoogleMap, dataset: NavDataset) {
        val samps = dataset.samplesClean
        if (samps.size > 1)
        {
            for (i in 1 until samps.size)
            {
                val samp = samps[i]
                val polylineOptions = PolylineOptions()
                polylineOptions.add(samps[i-1].coord)
                polylineOptions.add(samp.coord)
                val factor = map(samp.getVelocity(),0f,100f,0f,1f)
                val color = ArgbEvaluator().evaluate(factor,Color.RED,Color.GREEN) as Int
                polylineOptions.color(color)
                googleMap.addPolyline(polylineOptions)
            }
        }

        // TODO consider doing an update based on LatLngBounds
        gmap.setMinZoomPreference(12.0f)
        gmap.moveCamera(CameraUpdateFactory.newLatLng(dataset.mean()))
    }

    override fun onMapReady(googleMap: GoogleMap) {
        gmap = googleMap

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success = googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    context, R.raw.gmap_style
                )
            )

            if (!success) {
                Log.e(TAG, "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e(TAG, "Can't find gmap_style. Error: ", e)
        }


        val ds = readDataset()
        drawGradientPolyline(gmap,ds)
        Log.i(TAG,"Loaded GoogleMap successfully!")
    }
}