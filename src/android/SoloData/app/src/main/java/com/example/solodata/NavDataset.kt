package com.example.solodata

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import org.json.JSONArray


class NavDataset {
    var samplesAll: MutableList<NavSample> = ArrayList()
    var samplesClean: MutableList<NavSample> = ArrayList()
    // FIXME initializing this to (0,0) is incorrect when using includes() to accumulate bound box
    private var boundBox = LatLngBounds(LatLng(0.0,0.0), LatLng(0.0,0.0))

    private var sumLat: Double = 0.0
    private var sumLon: Double = 0.0

    fun loadJSONArray(samps: JSONArray) {
        for (i in 0 until samps.length()) {
            val obj = samps.getJSONObject(i)
            add(NavSample(obj))
        }
    }

    fun add(sample: NavSample) {
        samplesAll.add(sample)

        if (sample.isValid()) {
            samplesClean.add(sample)

            // accumulate bounding box around clean samples
            boundBox = boundBox.including(sample.coord)

            // keep running sum
            sumLat += sample.coord.latitude
            sumLon += sample.coord.longitude
        }
    }

    fun startTime(): Double {
        var time = 0.0

        if (samplesClean.size > 0) {
            time = samplesClean[0].getTime()
        }

        return time
    }

    fun endTime(): Double {
        var time = 0.0

        val size = samplesClean.size
        if (size > 0) {
            time = samplesClean[size - 1].getTime()
        }

        return time
    }

    fun computeVelocity() {
        if (samplesClean.size > 1) {
            for (i in 1 until samplesClean.size) {
                samplesClean[i - 1].inferVelocity(samplesClean[i])
            }
        }
    }

    fun mean(): LatLng {
        return LatLng(sumLat/samplesClean.size,sumLon/samplesClean.size)
    }

    override fun toString(): String {
        var str = ""
        str += "Total samples:       " + samplesAll.size + "\n"
        str += "Total clean samples: " + samplesClean.size + "\n"
        str += "min coord:           " + boundBox.northeast + "\n"
        str += "max coord:           " + boundBox.southwest + "\n"
        str += "mean coord:          " + mean() + "\n"
        return str
    }
}