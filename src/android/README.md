# API keys
Place you API keys in your user's global gradle.properties. On linux this can
be found in `/home/<user>/.gradle/gradle.properties`

The contents of the gradle.properties should contain the following. Substituting
the templated arguments for your own personal API keys. API can be acquired via
the [Google API Dashboard](https://console.developers.google.com/apis/dashboard).

```
GOOGLE_MAPS_API_KEY="<your-google-maps-android-api-key>"
```
