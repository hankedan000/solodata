# Intertia & Navigation Unit (INU)
Measures acceleration, roll/pitch/ya, as well as GPS position over CAN.

# Thermal Acquisition Unit (TAU)
Measures thermocouple sensors using MAX6675 and relays the data to Megasquirt
via CAN.
