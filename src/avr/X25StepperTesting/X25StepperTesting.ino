//----------------------------------------------------------------------
// https://github.com/clearwater/SwitecX25
// 
// This is an example of using the SwitchX25 library.
// It zero's the motor, sets the position to mid-range
// and waits for serial input to indicate new motor positions.
// 
// Open the serial monitor and try entering values 
// between 0 and 944.
// 
// Note that the maximum speed of the motor will be determined
// by how frequently you call update().  If you put a big slow
// serial.println() call in the loop below, the motor will move
// very slowly!
//----------------------------------------------------------------------

#include <SwitecX25.h>

// standard X25.168 range 315 degrees at 1/3 degree steps
#define STEPS (315*3)

// For motors connected to digital pins 7,6,5,4
SwitecX25 motor1(STEPS,7,6,5,4);

// values used to drive a miata RPM gauge
// x_values -> RPM
// y_values -> steps (1/3 degrees)
int16_t rpmGaugeXbins[] = {0  ,1000,2000,3000,4000,5000,6000,7000,8000};
int16_t rpmGaugeYbins[] = {0  ,68  ,176 ,275 ,371 ,471 ,583 ,680 ,796};

void setup(void)
{
  // run the motor against the stops
  motor1.zero();

  Serial.begin(9600);
  Serial.println("Enter an RPM from 0 to 8000.");
}

int16_t
lerpS16(
  const int16_t *xBins,
  const int16_t *yBins,
  const size_t &nBins,
  const int16_t &value)
{
  if (nBins <= 1)
  {
    return value;
  }

  int16_t loX_Val, hiX_Val;
  loX_Val = *xBins;

  // handle case where value is below the x-axis (-) limit
  if (value <= loX_Val)
  {
    return *yBins;
  }

  size_t offset = 1;
  for (size_t i=1; i<nBins; i++)
  {
    hiX_Val = *(xBins+offset);

    if (loX_Val <= value && value < hiX_Val)
    {// found bins to interpolate between
      int16_t loY_Val = *(yBins+offset-1);
      int16_t hiY_Val = *(yBins+offset);
      return map(value,loX_Val,hiX_Val,loY_Val,hiY_Val);
    }

    // increment to look at next two x-axis bins
    loX_Val = hiX_Val;
    offset += 1;
  }

  /**
   * never found any values to interpolate between, so value must be higher than
   * the x-axis (+) limit value. return y-axis (+) limit value as result.
   */
  return *(yBins+(nBins-1));
}

void setRPM(uint16_t rpm)
{
  uint16_t nextPos = rpm;
  nextPos = lerpS16(rpmGaugeXbins,rpmGaugeYbins,9,rpm);
  motor1.setPosition(nextPos);
}

void loop(void)
{
  static int nextRpm = 0;
  // the motor only moves when you call update
  motor1.update();

  if (Serial.available()) {
    char c = Serial.read();
    if (c==10 || c==13) {
      setRPM(nextRpm);
      nextRpm = 0;
    } else if (c>='0' && c<='9') {
      nextRpm = 10*nextRpm + (c-'0');
    }
  }
}
