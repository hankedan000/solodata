#ifndef STOPWATCH_H_
#define STOPWATCH_H_

#include <Arduino.h>

#define STOPWATCH_START(sw_ptr) { (sw_ptr)->start = micros(); }
#define STOPWATCH_STOP(sw_ptr) { (sw_ptr)->span = micros() - (sw_ptr)->start; }

struct Stopwatch
{
	volatile uint32_t start;
	volatile uint16_t span;
};

// provide two global stopwatches for use. user can add more.
extern Stopwatch gStopwatch0;
extern Stopwatch gStopwatch1;

#endif