/*
 *  SwitecX25 Arduino Library
 *  Guy Carpenter, Clearwater Software - 2012
 *
 *  Licensed under the BSD2 license, see license.txt for details.
 *
 *  All text above must be included in any redistribution.
 */

#ifndef SwitecX25_h
#define SwitecX25_h
#include <Arduino.h>

class SwitecX25
{
 public:
   static const unsigned char pinCount = 3;
   static const unsigned char stateCount = 6;
   unsigned char pins[pinCount];
   unsigned char currentState;    // 6 steps 
   unsigned int currentStep;      // step we are currently at
   unsigned int targetStep;       // target we are moving to
   unsigned int steps;            // total steps available
   unsigned long time0;           // time when we entered this state
   unsigned int microDelay;       // microsecs until next state
   unsigned short (*accelTable)[2]; // accel table can be modified.
   unsigned int maxVel;           // fastest vel allowed
   unsigned int vel;              // steps travelled under acceleration
   signed char dir;                      // direction -1,0,1  
   boolean stopped;               // true if stopped
   boolean zeroing;               // true if doing a non-blocking zeroing
   
   // Constructs a SwitecX25 that used direct port minipulation for IO,
   // rather using the Arduino's digitalWrite() interface
   //
   // Note: 'M1', 'M2&M3', and 'M4' must be on 3 adjacent pins!!!
   //
   // @param[in] ddr
   // the AVR ddr register for motor IO (ex. &DDRB, or &DDRD)
   //
   // @param[in] port
   // the AVR port register for motor IO (ex. &PORTB, or &PORTD)
   //
   // @param[in] portOffset
   // the AVR port's bit offset of the 'M1' pin
   SwitecX25(unsigned int steps, volatile uint8_t *ddr, volatile uint8_t *port, uint8_t portOffset);
   SwitecX25(unsigned int steps, unsigned char pin1, unsigned char pin2, unsigned char pin3);
  
   void stepUp();
   void stepDown();
   void zero();
   void zeroNoBlock();
   void update();
   void updateBlocking();
   void setPosition(unsigned int pos);
  
 private:
   void advance();
   void writeIO();


   volatile uint8_t *ddr_;
   volatile uint8_t *port_;
   uint8_t portOffset_;
   bool usePortManip_;

};


#endif

