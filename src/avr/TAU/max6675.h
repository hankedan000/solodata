// this library is public domain. enjoy!
// www.ladyada.net/learn/sensors/thermocouple

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

struct MAX6675_Sample_t
{
  uint8_t              : 1;
  uint8_t   devID      : 1;// always zero
  uint8_t   openThermo : 1;
  uint8_t   adcValue   : 12;
  uint8_t   dummySign  : 1;// always zero
}__attribute__((packed));

class MAX6675 {
 public:
  MAX6675(int8_t SCLK, int8_t CS, int8_t MISO);

  void sample();

  bool getADC(uint16_t *adc) const;

  bool error() const;

  bool openThermo() const;

  bool commsError() const;

  bool readCelsius(double &c);
  
  bool readFahrenheit(double &f);
  
  static double adcToCelsius(const uint16_t &adcVal);

  static double adcToFahrenheit(const uint16_t &adcVal);

 private:
  int8_t sclk, miso, cs;
  // set to true if the MAX6675 detects an open circuit on the sensor inputs
  bool openThermo_;
  // set to true if the communication to this MAX6675 is bad
  bool commsError_;
  uint16_t adc_;

  uint8_t spiread(void) const;
};
