#include <Arduino.h>

// Sample Arduino MAX6675 Arduino Sketch

#include "EndianUtils.h"
#include "logging_impl_lite.h"
#include "max6675.h"
#include "MovingAverage.h"
#include "MegaCAN_ExtDevice.h"
#include "MegaCAN_RT_BroadcastHelper.h"
#include "tables.h"

#include <TaskScheduler.h>
// #define _TASK_TIMECRITICAL      // Enable monitoring scheduling overruns
// #define _TASK_SLEEP_ON_IDLE_RUN // Enable 1 ms SLEEP_IDLE powerdowns between tasks if no callback methods were invoked during the pass
// #define _TASK_STATUS_REQUEST    // Compile with support for StatusRequest functionality - triggering tasks on status change events in addition to time only
// #define _TASK_WDT_IDS           // Compile with support for wdt control points and task ids
// #define _TASK_LTS_POINTER       // Compile with support for local task storage pointer
// #define _TASK_PRIORITY          // Support for layered scheduling priority
// #define _TASK_MICRO_RES         // Support for microsecond resolution
// #define _TASK_STD_FUNCTION      // Support for std::function (ESP8266 and ESP32 ONLY)
// #define _TASK_DEBUG             // Make all methods and variables public for debug purposes
// #define _TASK_INLINE            // Make all methods "inline" - needed to support some multi-tab, multi-file implementations
// #define _TASK_TIMEOUT           // Support for overall task timeout
// #define _TASK_OO_CALLBACKS      // Support for dynamic callback method binding

// Required for MegaCAN library
DECL_MEGA_CAN_REV("Thermal Acquisition Unit");
DECL_MEGA_CAN_SIG("MegaTAU-1.1.0      ");
//                 123456789.123456789

// uses counting pattern ADC samples instead of real samples from MAX6675
#undef USE_DEBUG_ADC_SAMPLES

// MAX6675 related variables
#define MAX_SENSORS 4
#define MAX_SAMPLE_PERIOD_S 0.25f

int ktcCSs[MAX_SENSORS] = {4,5,6,7};
int ktcSO  = 8;
int ktcCLK = 9;

volatile MAX6675 *ktc[MAX_SENSORS];
MovingAverage<double> avg;
/**
 * The MAX6675 thermocouple conditioners can only be
 * sampled at a certain rate. This round robin ID is
 * used to cycle between sensors to maximize that
 * rate. This ID will count from 0 to MAX_SENSORS-1.
 * A timer interrupt is used to increment the round
 * robin ID at a given interval to guarantee that the
 * maximum sampling rate is abided by.
 */
volatile uint8_t rndRobId = 0;

// CAN related variables
#define CAN_CS  10
#define CAN_INT 3// SDU = 2; TAU = 3
#define CAN_ID  2
#define CAN_MSG_BUFFER_SIZE 40

#define RT_BCAST_OFFSET PAGE2_FIELD_OFFSET(rtBcast)

MegaCAN::CAN_Msg can_buff[CAN_MSG_BUFFER_SIZE];
MegaCAN::ExtDevice megaCan(CAN_CS,CAN_ID,CAN_INT,can_buff,CAN_MSG_BUFFER_SIZE,TABLES,NUM_TABLES);

// Scheduler
Scheduler ts;

void
can_isr();

void
send_rt_bcast_group(
  uint16_t baseId,
  uint8_t group)
{
  uint16_t id = baseId + group;
  DEBUG("sending rt group %d; id %d",group,id);
  
  switch (group)
  {
    case 0:// 00: ADC0,ADC1,ADC2,ADC3
      megaCan.send11bitFrame(id,8,((uint8_t*)(&outPC) + OUTPC_FIELD_OFFSET(adc0)));
      break;
    default:
      WARN("bad rt group %d",group);
      break;
  }
}

void
setup()
{
	Serial.begin(115200);
	setupLogging();

	for (unsigned int i=0; i<MAX_SENSORS; i++)
	{
		INFO(
				"Configuring MAX6675[%d] on CS %d",
				i,
				ktcCSs[i]);

		ktc[i] = new MAX6675(ktcCLK,ktcCSs[i],ktcSO);
	}

	cli();

	// MCP2515 configuration
	megaCan.init();
  pinMode(CAN_INT, INPUT_PULLUP);// Configuring pin for CAN interrupt input
  attachInterrupt(digitalPinToInterrupt(CAN_INT), can_isr, LOW);

  // setup real-time broadcast class
  MegaCAN::RT_Bcast.setup(&ts, RT_BCAST_OFFSET, send_rt_bcast_group);

	// MAX6675 interrupt configuration
	TCCR1A = 0x00;// normal port operation (no I/O)
	TCCR1B = 0x0d;// CTC mode, io_clk/1024
	TCNT1 = 0x00;
	OCR1A = static_cast<uint16_t>(
			MAX_SAMPLE_PERIOD_S / MAX_SENSORS * F_CPU / 1024U);
	TIMSK1 = 0x02;// enabled interrupt for OCR1A

	INFO("setup complete!");

	// enabled interrupts
	sei();
}

#ifdef USE_DEBUG_ADC_SAMPLES
uint16_t yep = 0;
uint16_t nope = 0;
#endif

uint32_t loop_start_time_us;
void
loop()
{
  loop_start_time_us = micros();
#ifdef USE_DEBUG_ADC_SAMPLES
	nope++;
#endif

	megaCan.handle();
  ts.execute();
  MegaCAN::RT_Bcast.execute();
  
  // update CAN status registers
  outPC.canStatus = megaCan.getCAN_Status();
  outPC.status0.bits.needsBurn = megaCan.needsBurn();
  outPC.status0.bits.flashDataLost = megaCan.flashDataLost();
  outPC.status0.bits.currFlashTable = megaCan.currFlashTable();

  // update thermo couple status registers
  outPC.max6675Status.bits.openSensor0     = ktc[0]->openThermo();
  outPC.max6675Status.bits.commsErrSensor0 = ktc[0]->commsError();
  outPC.max6675Status.bits.openSensor1     = ktc[1]->openThermo();
  outPC.max6675Status.bits.commsErrSensor1 = ktc[1]->commsError();
  outPC.max6675Status.bits.openSensor2     = ktc[2]->openThermo();
  outPC.max6675Status.bits.commsErrSensor2 = ktc[2]->commsError();
  outPC.max6675Status.bits.openSensor3     = ktc[3]->openThermo();
  outPC.max6675Status.bits.commsErrSensor3 = ktc[3]->commsError();
  
  // update statistics registers
  EndianUtils::setBE(outPC.loopTimeUs, (uint16_t)(micros() - loop_start_time_us));
}

// external interrupt service routine (INT1 for CAN interface)
void can_isr()
{
#ifdef USE_DEBUG_ADC_SAMPLES
  EndianUtils::setBE((&outPC.adc0)[0], yep++);
  EndianUtils::setBE((&outPC.adc0)[1], nope);
  nope = 0;
#endif

  megaCan.interrupt();
}

ISR (TIMER1_COMPA_vect)
{
	ktc[rndRobId]->sample();
	uint16_t adc;
	ktc[rndRobId]->getADC(&adc);

#ifdef USE_DEBUG_ADC_SAMPLES
	if (rndRobId >= 2)
	{
    EndianUtils::setBE((&outPC.adc0)[rndRobId], adc);
	}
#else
  EndianUtils::setBE((&outPC.adc0)[rndRobId], adc);
#endif

	rndRobId++;
	if (rndRobId >= MAX_SENSORS)
	{
		rndRobId = 0;
	}
}
