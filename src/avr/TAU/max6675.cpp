// this library is public domain. enjoy!
// www.ladyada.net/learn/sensors/thermocouple

#ifdef __AVR
  #include <avr/pgmspace.h>
#elif defined(ESP8266)
  #include <pgmspace.h>
#endif
#include <util/delay.h>
#include <stdlib.h>
#include "max6675.h"

MAX6675::MAX6675(int8_t SCLK, int8_t CS, int8_t MISO) {
  sclk = SCLK;
  cs = CS;
  miso = MISO;

  openThermo_ = true;
  commsError_ = false;
  adc_ = 0;

  //define pin modes
  pinMode(cs, OUTPUT);
  pinMode(sclk, OUTPUT); 
  pinMode(miso, INPUT);

  // pull ups
  digitalWrite(miso, HIGH);
  digitalWrite(cs, HIGH);
}

void MAX6675::sample() {
  commsError_ = false;
  digitalWrite(cs, LOW);

  adc_ = spiread();
  adc_ <<= 8;
  adc_ |= spiread();

  digitalWrite(cs, HIGH);

  MAX6675_Sample_t *samp = reinterpret_cast<MAX6675_Sample_t*>(&adc_);
  openThermo_ = samp->openThermo;
  commsError_ |= samp->devID != 0;// device ID should always be zero
  commsError_ |= samp->dummySign != 0;// sign bit should always be zero

  // chop off the status bits and just store ADC value
  adc_ >>= 3;
  adc_ &= 0xFFF;
}

bool MAX6675::getADC(uint16_t *adc) const {
  *adc = adc_;
  return !error();
}

bool MAX6675::error() const
{
	return openThermo_ || commsError_;
}

bool MAX6675::openThermo() const
{
  return openThermo_;
}

bool MAX6675::commsError() const
{
	return commsError_;
}

bool MAX6675::readCelsius(double &c) {
  sample();
  c = adcToCelsius(adc_);
  
  return !openThermo_;
}

bool MAX6675::readFahrenheit(double &f) {
  sample();
  f = adcToFahrenheit(adc_);
  
  return !openThermo_;
}

static double MAX6675::adcToCelsius(const uint16_t &adcVal)
{
  return adcVal * 0.25;
}

static double MAX6675::adcToFahrenheit(const uint16_t &adcVal)
{
  double c = adcToCelsius(adcVal);
  return c * 9.0/5.0 + 32;
}

byte MAX6675::spiread(void) const {
  int i;
  byte d = 0;

  for (i=7; i>=0; i--)
  {
    digitalWrite(sclk, LOW);
    if (digitalRead(miso)) {
      //set the bit to 0 no matter what
      d |= (1 << i);
    }

    digitalWrite(sclk, HIGH);
  }

  return d;
}
