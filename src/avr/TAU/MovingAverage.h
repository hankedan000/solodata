#ifndef MOVING_AVERAGE_H_
#define MOVING_AVERAGE_H_

#include <stdint.h>

#define WINDOW_SIZE 16

template <typename T>
class MovingAverage
{
  public:
    MovingAverage()
    {
      flush();
    }
    
    /**
     * Clears out all internal state and begins averaging
     * computation again from the next added sample
     */
    void
    flush()
    {
      newest_ = 0;
      sum_ = 0;
      currWindowSize_ = 0;
    }
    
    /**
     * Adds a new sample to the filter and recomputes the average
     *
     * @param[in] s
     * The sample to add
     */
    void
    add(
      const T &s)
    {
      if (full())
      {
        // subtract oldest sample from sum prior to overwriting
        sum_ -= samps_[newest_];
      }
      samps_[newest_++] = s;
      
      sum_ += s;
      if (currWindowSize_ < WINDOW_SIZE)
      {
        currWindowSize_++;
      }
      
      if (newest_ >= WINDOW_SIZE)
      {
        newest_ = 0;
      }
    }
      
    /**
     * @return
     * The average value over all windowed samples in the filter
     */
    T
    average() const
    {
      double avg = sum_;
      return avg / currWindowSize_;
    }
    
  private:
    /**
     * @return
     * True if all samples in the window are valid, false otherwise
     */
    bool
    full() const
    {
      return currWindowSize_ == WINDOW_SIZE;
    }
    
  private:
    T samps_[WINDOW_SIZE];
    uint8_t newest_;
    T sum_;
    uint8_t currWindowSize_;
    
};
#endif
