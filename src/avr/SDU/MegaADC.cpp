#include "MegaADC.h"

MegaADC::MegaADC(
		uint8_t cs,
		uint8_t myId,
		uint8_t intPin,
    MegaCAN::CAN_Msg *buff,
    uint8_t buffSize)
 : MegaCAN::Device(cs,myId,intPin,buff,buffSize)
{
	for (unsigned int aa=0; aa<MAX_CAN_ADC; aa++)
	{
		setADC(aa,0);
	}
}

void
MegaADC::setADC(
		uint8_t adcIdx,
		const uint16_t &adcVal)
{
	if (adcIdx < MAX_CAN_ADC)
	{
		adcData_bs_[adcIdx] = bswap16(adcVal);
	}
}

uint16_t
MegaADC::getADC(
		const uint8_t &adcIdx) const
{
	return bswap16(adcData_bs_[adcIdx]);
}

bool
MegaADC::readFromTable(
		const uint8_t table,
		const uint16_t offset,
		const uint8_t len,
		const uint8_t *&resData)
{
	resData = reinterpret_cast<uint8_t*>(adcData_bs_);
	return true;
}
