#ifndef MEGA_ADC_H_
#define MEGA_ADC_H_

#include "MegaCAN_Device.h"

#define MAX_CAN_ADC 4

class MegaADC : public MegaCAN::Device
{
public:
	MegaADC(
			uint8_t cs,
			uint8_t myId,
			uint8_t intPin,
			MegaCAN::CAN_Msg *buff,
			uint8_t buffSize);

	void
	setADC(
			uint8_t adcIdx,
			const uint16_t &adcVal);

	uint16_t
	getADC(
			const uint8_t &adcIdx) const;

protected:
	/**
	 * Overridable method for subclass to implement. This method is called by
	 * the base class when a corresponding read request was made to this CAN
	 * device.
	 *
	 * @param[in] table
	 * The table index to read from
	 *
	 * @param[in] offset
	 * The byte offset to begin reading from (relative to the table's start)
	 *
	 * @param[in] len
	 * The number of bytes to read
	 *
	 * @param[out] resData
	 * A pointer to the corresponding memory to read from
	 *
	 * @return
	 * True if the read is valid, false if not.
	 */
	virtual bool
	readFromTable(
			const uint8_t table,
			const uint16_t offset,
			const uint8_t len,
			const uint8_t *&resData) override;

private:
	uint16_t adcData_bs_[MAX_CAN_ADC];

};

#endif
