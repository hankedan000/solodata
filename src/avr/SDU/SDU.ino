#include <Arduino.h>
#include <logging_impl_lite.h>

#include "MegaADC.h"

// Required for MegaCAN library
DECL_MEGA_CAN_REV("Suspension Displacement Unit");
DECL_MEGA_CAN_SIG("SDU-1.0.0          ");

//#define LOG_SENSORS
int sensePINs[MAX_CAN_ADC] = {A0,A1,A2,A3};

// CAN related variables
#define CAN_CS  10
#define CAN_INT 2
#define CAN_ID  4
#define CAN_MSG_BUFFER_SIZE 40

MegaCAN::CAN_Msg can_buff[CAN_MSG_BUFFER_SIZE];
MegaADC megaCan(CAN_CS,CAN_ID,CAN_INT,can_buff,CAN_MSG_BUFFER_SIZE);

void
canISR();

void
setup()
{
	Serial.begin(115200);
	setupLogging();

	for (unsigned int i=0; i<MAX_CAN_ADC; i++)
	{
		INFO(
		  "Configuring SENSOR[%d] on pin %d",
			i,
			sensePINs[i]);
    pinMode(sensePINs[i],INPUT);
	}

  // disable interrupts
	cli();

	// MCP2515 configuration
	megaCan.init();
	pinMode(CAN_INT, INPUT);// Configuring pin for CAN interrupt input
	digitalWrite(CAN_INT, HIGH);// pull up on CAN interrupt pin
  attachInterrupt(digitalPinToInterrupt(CAN_INT),canISR,FALLING);

	INFO("setup complete!");

	// enabled interrupts
	sei();
}

void
loop()
{
  for (unsigned int i=0; i<MAX_CAN_ADC; i++)
  {
    uint16_t val = analogRead(sensePINs[i]);
    megaCan.setADC(i,val);
    
#ifdef LOG_SENSORS
    Serial.print("Sensor");
    Serial.print(i);
    Serial.print(": ");
    Serial.print(val);
    Serial.print(i < MAX_CAN_ADC - 1 ? "; " : "\n");
#endif
  }

	megaCan.handle();
}

// interrupt service routine for CAN (INT0)
void
canISR()
{
	megaCan.interrupt();
}
