#define NUM_TEETH 36

#define CRANK_PIN   2  // digital pin to sim toothed encoder on
#define RPM_POT_PIN A5 // potentiometer to control RPM

#define MIN_RPM 500
#define MAX_RPM 8000

volatile uint32_t clks = 0;
volatile uint8_t toothState = 0;
volatile uint8_t tooth = 0;

uint8_t halfToothClks = 0;

void setup()
{
  pinMode(CRANK_PIN,OUTPUT);
  
  DDRC &= 0x1f;// A0-4 inputs
  DDRB &= 0x3f;// D8-13 inputs
  PORTC |= 0x1f;// A0-4 pull ups
  PORTB |= 0x3f;// D8-13 pull ups
  
  setRPM(1000);
  
  // configure timer 2
  TCCR2B = 0x01;// TMR2_clk -> 16MHz / 1
  TCCR2A = 0x02;// Clear Timer on Compare (CTC) mode
  TIMSK2 = 0x02;// interrupt on CMP2A
  OCR2A  = 160;// compare match every 10us
  TCNT2  = 0;
  
  sei();
}

void setRPM(uint32_t rpm)
{
  halfToothClks = (60UL*1000UL*100UL)/(rpm*NUM_TEETH*2);
}

void loop()
{
    uint16_t a = analogRead(RPM_POT_PIN);
    uint16_t rpm = map(a,1023,0,MIN_RPM,MAX_RPM);
    setRPM(rpm);
}

ISR(TIMER2_COMPA_vect){
  clks++;
  if (clks >= halfToothClks)
  {
    clks = 0;
    
    if (toothState)
    {
      tooth++;
    }
    
    if (tooth == NUM_TEETH)
    {
      tooth = 0;
    }
    
    if (tooth == 0)
    {
      digitalWrite(CRANK_PIN,0);
    }
    else
    {
      digitalWrite(CRANK_PIN,toothState);
    }
    
    toothState ^= 1;
  }
}
