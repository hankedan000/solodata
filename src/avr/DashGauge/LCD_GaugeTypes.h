#ifndef LCD_GAUGE_TYPES_H_
#define LCD_GAUGE_TYPES_H_

#include <avr/pgmspace.h>

// typedef used to identify a gauge variable
typedef uint8_t var_id_t;
#define VAR_ID_INVALID (var_id_t)(-1)
#define MAX_VARS ((1 << (sizeof(var_id_t)*8)) - 1)
#define MAX_USABLE_VARS (MAX_VARS - 1)// need one for VAR_ID_INVALID

enum PageType
{
  PageType_null = 0,
  PageType_hsplit = 1,
  // TODO vsplit not implemented
  PageType_vsplit = 2,
  PageType_monitor = 3,
};

const static char gauge_type_string_null[] PROGMEM = "OFF";
const static char gauge_type_string_hsplit[] PROGMEM = "H-Split";
const static char gauge_type_string_vsplit[] PROGMEM = "V-Split";
const static char gauge_type_string_monitor[] PROGMEM = "Monitor";
static const char *const GaugeTypeString[] = {
  gauge_type_string_null,   // 0
  gauge_type_string_hsplit, // 1
  gauge_type_string_vsplit, // 2
  gauge_type_string_monitor,// 3
};

#define MAX_H_SPLIT_GAUGES 3
struct H_SplitDefn_t
{
  uint8_t n_vars;
  var_id_t vars[MAX_H_SPLIT_GAUGES];
};

/**
 * Monitor page will track a variable and display statistics over time.
 * Stats displayed include min, max, and average value.
 */
struct MonitorDefn_t
{
  // the variable to monitor
  var_id_t var_id;
};

struct WarnConditionDefn_t
{
  uint16_t blink_rate_ms;
  const char **str;
};

#define MAX_PAGE_DATA_BYTES 5
struct PageDefn_t
{
  uint8_t type;
  union PageData_t
  {
    H_SplitDefn_t hsplit;
    MonitorDefn_t mon;
    uint8_t raw[MAX_PAGE_DATA_BYTES];
  } data;
};
static_assert(sizeof(PageDefn_t::data) == MAX_PAGE_DATA_BYTES);

#endif
