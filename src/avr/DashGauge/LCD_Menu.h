#ifndef LCD_MENU_H_
#define LCD_MENU_H_

#include <stddef.h>// offsetof()

#include "LCD_GaugeTypes.h"
#include "menu.h"

#define NUM_MENUS 6

#define MENU_ID_MONITOR      0
#define MENU_ID_HSPLIT       1
#define MENU_ID_NULL_GAUGE   2
#define MENU_ID_SETTINGS     3
#define MENU_ID_MONITOR_EDIT 4
#define MENU_ID_HSPLIT_EDIT  5

extern Menu lcd_menus[];

#define MIN_MENU_HIDE_TIME 3
#define MAX_MENU_HIDE_TIME 90
struct MenuHideTimeProperty : public PropertyEditor
{
  virtual
  void
  modify(
    const int8_t steps) override;
  
  virtual
  void
  load() override;
  
  virtual
  void
  store() override;
  
  virtual
  const char *
  to_string() override;
};

struct GaugeTypeProperty : public PropertyEditor
{
  virtual
  void
  modify(
    const int8_t steps) override;
  
  virtual
  void
  load() override;
  
  virtual
  void
  store() override;
  
  virtual
  const char *
  to_string() override;
};

struct HSplitNumVarsProperty : public PropertyEditor
{
  virtual
  void
  modify(
    const int8_t steps) override;
  
  virtual
  void
  load() override;
  
  virtual
  void
  store() override;
  
  virtual
  const char *
  to_string() override;

  const size_t n_var_offset = offsetof(PageDefn_t,data) + offsetof(H_SplitDefn_t,n_vars);
};

struct DashVarProperty : public PropertyEditor
{
  DashVarProperty(
    const PageType &page_type,
    const uint8_t &var_idx)
   : var_offset(0)
  {
    var_offset += offsetof(PageDefn_t,data);
    if (page_type == PageType_hsplit)
    {
      var_offset += offsetof(H_SplitDefn_t,vars) + var_idx;
    }
    else if (page_type == PageType_monitor)
    {
      var_offset += offsetof(MonitorDefn_t,var_id);
    }
  }
  
  virtual
  void
  modify(
    const int8_t steps) override;
  
  virtual
  void
  load() override;
  
  virtual
  void
  store() override;
  
  virtual
  const char *
  to_string() override;

  /**
   * offset into the PageDefn_t where the var property is that this class
   * will be modifying.
   */
  size_t var_offset;
};

#endif
