#ifndef DASH_CAN_H_
#define DASH_CAN_H_

#include "MegaCAN_Device.h"
#include "MS_Msgs.h"
#include "tables.h"

// the largest flash table size allowed to be stored in flash
#define NUM_DIRTY_FLASH_WORDS (MAX_FLASH_TABLE_SIZE / 8)// 8bits per bytes

static_assert((MAX_FLASH_TABLE_SIZE % 8) == 0,
    "Max flash table size must be a multiple of 8 bytes");

// structure of data that is broadcasted from the Megasquirt ECU
struct EngineData_T
{
  MSG00_t msg00;
  MSG02_t msg02;
  MSG03_t msg03;

  // FIXME doesn't come from ECU
  uint16_t speed_mph;
};
extern struct EngineData_T eng_data;

class DashCAN : public MegaCAN::Device
{
public:
  DashCAN(
      uint8_t cs,
      uint8_t myId,
      uint8_t intPin,
      MegaCAN::CAN_Msg *buff,
      uint8_t buffSize,
      const TableDescriptor_t *tables,
      uint8_t numTables);

  bool
  needsBurn() const
  {
    return needsBurn_;
  }

  bool
  flashDataLost() const
  {
    return flashDataLost_;
  }

  uint8_t
  currFlashTable() const
  {
    return currFlashTable_;
  }

protected:
  /**
   * override this base method so that we can override default options
   */
  virtual void
  getOptions(
    struct MegaCAN::Options *opts) override;

  /**
   * override this base method so that we can set filtes for broadcast
   * frame reception (11bit protocol) as well as extended message destined
   * for the dashboard's ID.
   */
  virtual void
  applyCanFilters(
    MCP_CAN *can) override;

  /**
   * Overridable method for subclass to implement. This method is called by
   * the base class when a corresponding read request was made to this CAN
   * device.
   *
   * @param[in] table
   * The table index to read from
   *
   * @param[in] offset
   * The byte offset to begin reading from (relative to the table's start)
   *
   * @param[in] len
   * The number of bytes to read
   *
   * @param[out] resData
   * A pointer to the corresponding memory to read from
   *
   * @return
   * True if the read is valid, false if not.
   */
  virtual bool
  readFromTable(
      const uint8_t table,
      const uint16_t offset,
      const uint8_t len,
      const uint8_t *&resData) override;

  /**
   * Called when a standard 11bit megasquirt broadcast frame is received.
   * 
   * @param[in] id
   * The 11bit CAN identifier
   * 
   * @param[in] length
   * The number of data bytes in the CAN frame
   * 
   * @param[in] data
   * A pointer to the data segment of the CAN frame
   */
  virtual void
  handleStandard(
      const uint32_t id,
      const uint8_t length,
      uint8_t *data) override;

  virtual bool
  writeToTable(
    const uint8_t table,
    const uint16_t offset,
    const uint8_t len,
    const uint8_t *data) override;

  // called by base class when we should burn the current flash table
  virtual bool
  burnTable(
      const uint8_t table) override;
  
  virtual uint16_t
  tableBlockingFactor() override
  {
    return 32;
  }
  
  virtual uint16_t
  writeBlockingFactor() override
  {
    return 32;
  }

private:
  bool
  loadFlashTable(
    const uint8_t table);

private:
  /**
   * An array of table descriptors used to read/write data to RAM
   * or flash.
   */
  const TableDescriptor_t *tables_;
  uint8_t numTables_;
  
  /**
   * The current table of flash that's loaded into RAM
   */
  uint8_t currFlashTable_;
  
  /**
   * flag set true when a flash table has been loaded into RAM and
   * modified, but not yet burned. This flag is cleared once the
   * flash table has been burned via a MSG_BURN request.
   */
  bool needsBurn_;

  /**
   * A bitmask of which bytes in the active flash table have been
   * modified in RAM, but not yet burned. This table is used to
   * only burn the words that were modified in effort to reduce
   * the total number of write cycles we put on the EEPROM.
   */
  uint8_t dirtyFlashBits_[NUM_DIRTY_FLASH_WORDS];

  /**
   * set to true when a flash table was modified and then swapped out
   * for another table prior to receiving a MSG_BURN request
   */
  bool flashDataLost_;

};

#endif
