#ifndef TABLES_H_
#define TABLES_H_

#include <EEPROM.h>

#include <FlashUtils.h>
#include "LCD_GaugeTypes.h"
#include "stdint.h"

#define MAX_FLASH_TABLE_SIZE 128

#define PAGE0_SIZE 128
#define PAGE1_SIZE 128
#define PAGE2_SIZE 128
#define PAGE3_SIZE 128
#define PAGE1_FLASH_OFFSET 0  // EEPROM byte offset where page is stored
#define PAGE3_FLASH_OFFSET 128// EEPROM byte offset where page is stored

#define TACH_MAPPING_CURVE_N_BINS 6
#define SPEEDO_MAPPING_CURVE_N_BINS 6
#define CLT_MAPPING_CURVE_N_BINS 6
#define ADC_MAPPING_CURVE_N_BINS 6

// forward declare
struct Page1_T;

#define PAGE1_FIELD_OFFSET(FIELD_NAME) (PAGE1_FLASH_OFFSET + offsetof(Page1_T, FIELD_NAME))
#define PAGE3_FIELD_OFFSET(FIELD_NAME) (PAGE3_FLASH_OFFSET + offsetof(Page3_T, FIELD_NAME))

// command values that can handled by the 'canCommand' register
#define CAN_CMD_DO_NOTHING               0x00
#define CAN_CMD_RESET_ALL_ERROR_COUNTERS 0x01
#define CAN_CMD_SIM_REQ_DROP             0x02

// convert a flash table field offset to an absolute offset in EEPROM
// template function must be specialized for all required 'TABLE_T'
template <class TABLE_T>
size_t
flashTableAbsOffset(
  const size_t &field_offset,
  const TABLE_T *__unused_arg = nullptr);

#define FLASH_TABLE_READ(_table_type,_table_field) \
  (FlashUtils::flashRead_BE<typeof(_table_type::_table_field)>(flashTableAbsOffset<_table_type>(offsetof(_table_type,_table_field))))

#define FLASH_TABLE_WRITE(_table_type,_table_field,_value) \
  (FlashUtils::flashWrite_BE<typeof(_table_type::_table_field)>(flashTableAbsOffset<_table_type>(offsetof(_table_type,_table_field)),_value))

enum TableType_E
{
  eRam, eFlash, eNull
};

struct TableDescriptor_t
{
  void *tableData;
  size_t tableSize;
  TableType_E tableType;
  uint16_t flashOffset;
};

//====================================================================
//============================ Out PC ================================
//====================================================================
// Storage Type: RAM
// Variables reported to Tuner Studio for stats/logging
//====================================================================
struct OutPC_T
{
  // ----- offset 0
  uint8_t canStatus;
  uint8_t canLogicErrorCount;
  uint8_t canSW_RxOverflowCount;
  uint8_t canHW_Rx0_OverflowCount;
  uint8_t canHW_Rx1_OverflowCount;
  uint16_t loopTimeUs;
  union Status0_T
  {
    struct Status0Bits_T
    {
      uint8_t needsBurn      : 1;
      uint8_t flashDataLost  : 1;
      uint8_t reserved       : 2;
      uint8_t currFlashTable : 4;
    } bits;
    uint8_t value;
  } status0;
  // write values to command CAN operations
  // se CAN_CMD_* defineds for possible values
  uint8_t canCommand;
  uint8_t reserved0[7];
  // ----- offset 16
  uint16_t motorControlTimeUs;
  uint16_t handleStdTimeUs;
  uint16_t gStopwatch0;
  uint16_t gStopwatch1;
  uint8_t reserved1[104];
};
static_assert(sizeof(OutPC_T) == PAGE0_SIZE);

struct ADC_MappingControl_T
{
  union
  {
    struct ADC_MappingControlBits_T
    {
      // set to 1 to enable mapping of this ADC channel
      uint8_t enabled  : 1;
      uint8_t reserve0 : 3;
      // the mapping curve to use (0->A, 1->B, 2->C, 3->D)
      uint8_t curve    : 2;
      uint8_t reserve1 : 2;
    } bits;
    uint8_t value;
  };
};

//====================================================================
//====================== Motor Control Table =========================
//====================================================================
// Storage Type: Flash
// Variables that control the gauge motor
//====================================================================
struct Page1_T
{
  uint16_t tachMappingCurve_xBins[TACH_MAPPING_CURVE_N_BINS];
  int16_t  tachMappingCurve_yBins[TACH_MAPPING_CURVE_N_BINS];
  uint16_t speedoMappingCurve_xBins[SPEEDO_MAPPING_CURVE_N_BINS];
  int16_t  speedoMappingCurve_yBins[SPEEDO_MAPPING_CURVE_N_BINS];
  uint16_t cltMappingCurve_xBins[CLT_MAPPING_CURVE_N_BINS];
  int16_t  cltMappingCurve_yBins[CLT_MAPPING_CURVE_N_BINS];
  uint16_t adcMappingCurveD_xBins[ADC_MAPPING_CURVE_N_BINS];
  int16_t  adcMappingCurveD_yBins[ADC_MAPPING_CURVE_N_BINS];
  ADC_MappingControl_T adc0MappingCtrl;
  ADC_MappingControl_T adc1MappingCtrl;
  ADC_MappingControl_T adc2MappingCtrl;
  ADC_MappingControl_T adc3MappingCtrl;
  ADC_MappingControl_T adc4MappingCtrl;
  ADC_MappingControl_T adc5MappingCtrl;
  uint8_t reserved[26];
};
static_assert(sizeof(Page1_T) == PAGE1_SIZE);
static_assert(sizeof(Page1_T) <= MAX_FLASH_TABLE_SIZE);

//====================================================================
//======================== Test/Debug Table ==========================
//====================================================================
// Storage Type: RAM
// Variables used for test/debug. Most of these are written to from
// debug/test dialogs within Tuner Studio.
//====================================================================
#define TEST_MODE_DISABLE        0
#define TEST_MODE_STEP_OVERRIDE  1
#define TEST_MODE_VALUE_OVERRIDE 2

struct Page2_T
{
  // Variables for testing gauges
  union {
    struct {
      // see TEST_MODE_* defines
      uint8_t mode     : 2;
      uint8_t reserve0 : 6;
    } bits;
    uint8_t value;
  } gaugeTestCtrl;
  uint8_t reserved0[3];
  uint16_t testValueTach;
  uint16_t testValueSpeedo;
  uint16_t testValueCoolantTemp;
  uint16_t testValueOilPressure;
  uint16_t testValueFuelLevel;
  uint8_t reserved[114];
};
static_assert(sizeof(Page2_T) == PAGE2_SIZE);
static_assert(sizeof(Page2_T) <= MAX_FLASH_TABLE_SIZE);

//====================================================================
//========================= Settings Table ===========================
//====================================================================
// Storage Type: Flash
// Store dash settings. Can be set from TunerStuio or LCD UI.
//====================================================================
#define NUM_GAUGE_PAGE_DEFNS 10
struct Page3_T
{
  // Duration in seconds before the menu will be automatically hidden
  // when no UI input is detected.
  uint8_t menuHideTime_s;
  uint8_t reserved0[19];
  PageDefn_t pageDefns[NUM_GAUGE_PAGE_DEFNS];// offset 20
  uint8_t reserved1[48];// offset 80
};
static_assert(sizeof(Page3_T) == PAGE3_SIZE);
static_assert(sizeof(Page3_T) <= MAX_FLASH_TABLE_SIZE);

// a place to temporarily store modified pages of flash
extern uint8_t temp_page[MAX_FLASH_TABLE_SIZE];
extern OutPC_T out_pc;
extern Page2_T page2;

#define NUM_TABLES 4
static const TableDescriptor_t TABLES[NUM_TABLES] = {
  {&out_pc  , sizeof(OutPC_T), TableType_E::eRam  , -1                }, // table 0
  {temp_page, sizeof(Page1_T), TableType_E::eFlash, PAGE1_FLASH_OFFSET}, // table 1
  {&page2   , sizeof(Page2_T), TableType_E::eRam  , -1                }, // table 2
  {temp_page, sizeof(Page3_T), TableType_E::eFlash, PAGE3_FLASH_OFFSET}  // table 3
};

void
initTables();

#endif
