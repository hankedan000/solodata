#ifndef LCD_GAUGE_H_
#define LCD_GAUGE_H_

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include "DashVars.h"
#include "LCD_Menu.h"
#include "LCD_GaugeTypes.h"
#include "tables.h"

#define LINES_PER_MENU 5

#include <avr/pgmspace.h>
const static char unit_string_null[] PROGMEM = "";
const static char unit_string_s[] PROGMEM = "s";
const static char unit_string_ms[] PROGMEM = "ms";
const static char unit_string_us[] PROGMEM = "us";
const static char unit_string_perc[] PROGMEM = "%";
const static char unit_string_volt[] PROGMEM = "V";
const static char unit_string_degF[] PROGMEM = "degF";
const static char unit_string_degC[] PROGMEM = "degC";
const static char unit_string_mph[] PROGMEM = "mph";
const static char unit_string_rpm[] PROGMEM = "rpm";
const static char unit_string_kpa[] PROGMEM = "kpa";
const static char unit_string_psi[] PROGMEM = "psi";
static const char *const UnitStrings[] = {
  unit_string_null,// 0
  unit_string_s,   // 1
  unit_string_ms,  // 2
  unit_string_us,  // 3
  unit_string_perc,// 4
  unit_string_volt,// 5
  unit_string_degF,// 6
  unit_string_degC,// 7
  unit_string_mph, // 8
  unit_string_rpm, // 9
  unit_string_kpa, // 10
  unit_string_psi, // 11
};

// Structure of data used by the 'monitor' gauge pages
struct MonitorPageData
{
  var_id_t var;
  uint16_t min;
  uint16_t max;
};

struct PageSpecificData
{
  PageType type;
  union PageSpecificData_U
  {
    MonitorPageData mon;
    H_SplitDefn_t hsplit;
  } data;
};

class LCD_Display {
public:
  LCD_Display(
    uint16_t width,
    uint16_t height,
    int8_t reset_pin);

  void
  init();

  /**
   * Configures a gauge page to monitor the specified DashVar.
   * 
   * @param[in] page_idx
   * The gauge page to set to be a 'monitor'
   * 
   * @param[in] var
   * The DashVar to monitor
   * 
   * @return
   * 0 on success
   * -1 on error (page_idx invalid, var invalid, or var is already monitored)
   */
  int8_t
  setPageMonitor(
    const uint8_t &page_idx,
    const var_id_t &var);

  /**
   * Configures a gauge page to be an H-Split page.
   * 
   * @param[in] page_idx
   * The gauge page to set to be a 'monitor'
   * 
   * @param[in] hsplit
   * The H-Split page defintion to assign to the page
   * 
   * @return
   * 0 on success
   * -1 on error
   */
  int8_t
  setPageHSplit(
    const uint8_t &page_idx,
    const H_SplitDefn_t &hsplit);

  /**
   * Resets a monitor page so that the DashVar's min/max values will be
   * monitor from a fresh start again.
   * 
   * @param[in] page_idx
   * The monitor page to reset
   * 
   * @return
   * 0 on success
   * -1 if page_idx doesn't reference a 'monitor' gauge
   */
  int8_t
  resetMonitorData(
    const uint8_t &page_idx);

  /**
   * Getter for a gauge page's MonitorPageData structure.
   * 
   * @param[in] page_idx
   * The gauge page to request for. This page should be configured
   * as a 'PageType_monitor' already.
   * 
   * @param[out] mon_data
   * A returned pointer to the page's MonitorPageData structure
   * 
   * @return
   * 0 on success
   * -1 if the page index is out of range
   * -2 if the gauge is not configured as a monitor 
   */
  int8_t
  getMonitorData(
    const uint8_t &page_idx,
    MonitorPageData *&mon_data);

  void
  monitor();

  void
  draw();

  void
  togglePageBuffers();

  void
  nextPage();

  void
  prevPage();

  void
  menuVisible(
    bool visible)
  {
    first_frame_of_page_ = true;
    menu_visible_ = visible;
  }

  bool
  menuVisible()
  {
    return menu_visible_;
  }

  uint8_t
  activePage()
  {
    return active_page_;
  }
  
  /**
   * increments to the next item in the list, or leave back button
   * 
   * @return
   * 1 if successful increment
   * 0 if no more items to increment to
   */
  int8_t
  nextMenuItem();
  
  /**
   * decrements to the prev item in the list, or back button if allowed
   * 
   * @return
   * 1 if successful decrement
   * 0 if no more items to decrement to
   */
  int8_t
  prevMenuItem();

  /**
   * Perform menu 'click' action
   * 
   * @return
   * 1 if action was valid and performed
   * 0 if no action performed
   * -1 on error
   */
  int8_t
  menuClick();

private:
  /**
   * If the PageSpecificData has been allocated for a specific use (ie.
   * monitoring variables), then this function will detach that association,
   * allowing 'pd' to be used for another purpose.
   * 
   * @param[in] pd
   * The page data to release
   * 
   * @return
   * 0 on success
   * -1 on error
   */
  int8_t
  releasePageData(
    PageSpecificData *pd);

  /**
   * internal method used to reset a monitor page's data. This operation must
   * be atomic because DashVars can be updated in interrupt context when data
   * arrives off the CAN bus.
   * 
   * @param[in] mon_data
   * valid pointer to monitor data to reset
   */
  void
  internalResetMonitorData(
    MonitorPageData *mon_data);

  /**
   * updates the root menu based on the current gauge page type.
   * 
   * @return
   * 0 on success
   * -1 on failure
   */
  int8_t
  updateRootMenu();

  void
  drawPageIndicator();
  
  void
  drawError(
    const __FlashStringHelper *err_str);
  
  void
  drawError(
    const char *err_str);

  void
  drawH_SplitPage(
    const H_SplitDefn_t *hsplit);

  void
  drawMonitorPage(
    const MonitorPageData *mon_data);

  void
  drawWarnPage(
    const WarnConditionDefn_t *warn);

  void
  drawGauge(
    uint16_t xOffset,
    const VarDefn_t *var);

  void
  drawMenu();

private:
  Adafruit_SSD1306 display_;

  // the currently diplay page index
  uint8_t active_page_;

  // data used to store info for displaying the gauge pages
  PageSpecificData page_data_[NUM_GAUGE_PAGE_DEFNS];

  // true on the fist frame after a page toggle. set false after the draw
  bool first_frame_of_page_;

  // millis() of the last warning frame we drew (used to blink screen)
  unsigned long last_warn_frame_blink_time_;
  bool screen_inverted_;
  
  bool menu_visible_;
  MenuDriver lcd_menu_driver_;
  
};

#endif
