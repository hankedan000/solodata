#ifndef DASH_MOTORS_H_
#define DASH_MOTORS_H_

#include <stdint.h>

void
gaugeSetup();

/**
 * Re-homes all gauges (non-blocking)
 */
void
homeAllGauges();

// Total duration to control all X25 motors
extern volatile uint16_t motor_control_time_us;

#endif
