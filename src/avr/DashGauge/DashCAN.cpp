#include "DashCAN.h"
#include <EEPROM.h>
#include "EndianUtils.h"
#include "tables.h"

struct EngineData_T eng_data;

DashCAN::DashCAN(
    uint8_t cs,
    uint8_t myId,
    uint8_t intPin,
    MegaCAN::CAN_Msg *buff,
    uint8_t buffSize,
    const TableDescriptor_t *tables,
    uint8_t numTables)
 : MegaCAN::Device(cs,myId,intPin,buff,buffSize)
 , tables_(tables)
 , numTables_(numTables)
 , currFlashTable_(numTables)
 , needsBurn_(false)
 , flashDataLost_(false)
{
  memset(dirtyFlashBits_,0,NUM_DIRTY_FLASH_WORDS);// reset all dirty bits
}

void
DashCAN::getOptions(
  struct MegaCAN::Options *opts)
{
  opts->handleStandardMsgsImmediately = true;
}

void
DashCAN::applyCanFilters(
  MCP_CAN *can)
{
  // filter MegaSquirt extended frames destined for this endpoint into RXB0
  uint32_t mask = 0x0;
  uint32_t filt = 0x0;
  MS_HDR_t *maskHdr = reinterpret_cast<MS_HDR_t*>(&mask);
  MS_HDR_t *filtHdr = reinterpret_cast<MS_HDR_t*>(&filt);
  maskHdr->toId = 0xf;// only check the 4bit toId in the megasquirt header
  filtHdr->toId = canId() & 0xf;// make sure the message is for me!
  can->init_Mask(0,1,mask);
  can->init_Filt(0,1,filt);
  can->init_Filt(1,1,filt);

  // filter Megasquirt broadacst frames into RXB1
  can->init_Mask(1,0,0x00000000);
  can->init_Filt(2,0,0x00000000);
  can->init_Filt(3,0,0x00000000);
  can->init_Filt(4,0,0x00000000);
  can->init_Filt(5,0,0x00000000);
}

bool
DashCAN::readFromTable(
    const uint8_t table,
    const uint16_t offset,
    const uint8_t len,
    const uint8_t *&resData)
{
  DEBUG(
    "readFromTable - table = %d; offset = %d; len = %d",
    table,
    offset,
    len);

  if (table > numTables_)
  {
    ERROR("%d > numTables_", table);
    return false;
  }

  const TableDescriptor_t &td = tables_[table];
  if (td.tableData == nullptr)
  {
    ERROR("null table %d", table);
    return false;
  }
  else if ((offset + len) > td.tableSize)
  {
    ERROR("outofbound - table %d; offset; %d; len; %d", table, offset, len);
    return false;
  }

  // see if we need to load in a new table of flash
  if (td.tableType == TableType_E::eFlash && currFlashTable_ != table)
  {
    if ( ! loadFlashTable(table))
    {
      return false;
    }
  }

  // return pointer to data within table
  // Note: when tables are loaded in from flash tableData will point to RAM
  resData = (uint8_t*)(td.tableData) + offset;
    
  return true;
}

void
DashCAN::handleStandard(
    const uint32_t id,
    const uint8_t length,
    uint8_t *data)
{
  uint32_t startTime = micros();
  DEBUG("handleStandard");

  switch (id)
  {
    case MSG00_ID:
    {
      memcpy(&eng_data.msg00,data,length);
      
      DEBUG(
          "time = %d; pw1 = %d; pw2 = %d; rpm = %d",
          MSG00_t::get_seconds(&eng_data.msg00),
          MSG00_t::get_pw1(&eng_data.msg00),
          MSG00_t::get_pw2(&eng_data.msg00),
          MSG00_t::get_rpm(&eng_data.msg00));
      break;
    }
    case MSG02_ID:
    {
      memcpy(&eng_data.msg02,data,length);

      DEBUG(
          "baro = %d; map = %d; clt_F = %d",
          MSG02_t::get_baro(&eng_data.msg02),
          MSG02_t::get_map(&eng_data.msg02),
          MSG02_t::get_clt(&eng_data.msg02));
      break;
    }
    case MSG03_ID:
    {
      memcpy(&eng_data.msg03,data,length);

      DEBUG(
          "tps = %d; batt = %d",
          MSG03_t::get_tps(&eng_data.msg03),
          MSG03_t::get_batt(&eng_data.msg03));
      break;
    }
//    case MSG10_ID:
//    {
//      MSG10_t* msg = reinterpret_cast<MSG10_t*>(data);
//
//      DEBUG(
//          "status2 = 0x%d0.2x",
//          msg->status2);
//      break;
//    }
    default:
    {
      // silently ignore
      break;
    }
  }// switch

  EndianUtils::setBE(out_pc.handleStdTimeUs, (uint16_t)(micros() - startTime));
}

bool
DashCAN::writeToTable(
  const uint8_t table,
  const uint16_t offset,
  const uint8_t len,
  const uint8_t *data)
{
  DEBUG(
    "writeToTable - table = %d; offset = %d; len = %d",
    table,
    offset,
    len);

  if (table > numTables_)
  {
    ERROR("%d > numTables_", table);
    return false;
  }

  const TableDescriptor_t &td = tables_[table];
  if (td.tableData == nullptr)
  {
    ERROR("null table %d", table);
    return false;
  }

  if ((offset + len) > td.tableSize)
  {
    ERROR("outofbound - table %d; offset; %d; len; %d", table, offset, len);
    return false;
  }

  // handle writes to the 'canCommand' register
  if (table == 0 && offset == offsetof(OutPC_T,canCommand))
  {
    if (len == 1)
    {
      switch (data[0])
      {
        case CAN_CMD_DO_NOTHING:
          break;
        case CAN_CMD_RESET_ALL_ERROR_COUNTERS:
          resetErrorCounters();
          break;
        case CAN_CMD_SIM_REQ_DROP:
          simReqDrop(1);
          break;
        default:
          ERROR("invalid canCommand 0x%x",data[0]);
          return false;
          break;
      }
    }
    else
    {
      ERROR("canCommand invalid len");
      return false;
    }
    return true;
  }

  // see if we need to load in a new table of flash
  if (td.tableType == TableType_E::eFlash && currFlashTable_ != table)
  {
    if ( ! loadFlashTable(table))
    {
      return false;
    }
  }

  // write data to temporary RAM location where flash was loaded
  needsBurn_ = len > 0;
  uint16_t flashOffset = offset;
  for (uint8_t i=0; i<len; i++)
  {
    dirtyFlashBits_[flashOffset/8] |= 1<<(flashOffset%8);// update dirty bits
    ((uint8_t*)(td.tableData))[flashOffset] = data[i];
    flashOffset++;
  }
    
  return true;
}

bool
DashCAN::burnTable(
    const uint8_t table)
{
  if (table >= numTables_)
  {
    ERROR("invalid table %d", table);
    return false;
  }
  else if (tables_[table].tableType == TableType_E::eRam)
  {
    // no need to burn RAM
    return true;
  }
  else if (table != currFlashTable_)
  {
    ERROR("table to burn is %d but current is %d", table, currFlashTable_);
    return false;
  }
  else if ( ! needsBurn_)
  {
    // be nice and don't use unecessary write cycles on flash
    WARN("flash was never modified. ignoring burn request");
    return true;
  }
  
  const TableDescriptor_t &td = tables_[table];
  
  // write modified table contents from RAM to flash
  for (unsigned int i=0; i<td.tableSize; i++)
  {
    if (dirtyFlashBits_[i/8] & (1<<(i%8)))
    {
      EEPROM.write(td.flashOffset + i, ((uint8_t*)(td.tableData))[i]);
    }
  }
  needsBurn_ = false;
  memset(dirtyFlashBits_,0,NUM_DIRTY_FLASH_WORDS);// reset all dirty bits

  return true;
}

bool
DashCAN::loadFlashTable(
  const uint8_t table)
{
  if (needsBurn_)
  {
    // TODO could be nice and write to flash, but hoping TunerStudio
    // is smart enough to not put us in here. We'll see...
    WARN("unburned flash contents being lost");
    flashDataLost_ = true;
  }

  // load flash table contents into RAM
  const TableDescriptor_t &td = tables_[table];
  for (unsigned int i=0; i<td.tableSize; i++)
  {
    ((uint8_t*)(td.tableData))[i] = EEPROM.read(td.flashOffset + i);
  }
  currFlashTable_ = table;
  needsBurn_ = false;
  memset(dirtyFlashBits_,0,NUM_DIRTY_FLASH_WORDS);// reset all dirty bits
  DEBUG("loaded %dbytes from flash table %d", td.tableSize, table);
  
  return true;
}
