#ifndef MENU_H_
#define MENU_H_

#include <avr/pgmspace.h>
#include <stdint.h>
#include <WString.h>
#include <logging.h>

#define ALLOW_BACK 1
#define NO_BACK    0

#define MAX_NUM_MENUS 255
// maximum number of menus that can be on the back stack
#define MAX_BACK_STACK_DEPTH 10

typedef uint8_t MenuId;

enum ItemActionType
{
  eGoToMenu,
  eCallback,
  ePropertyEdit
};

// base class that allows editing a 'property' from the menu
struct PropertyEditor
{
  /**
   * Called from MenuDriver when requesting an edit for the property
   * 
   * @param[in] steps
   * direction to modify the property, can be positive or negative.
   * it's up to the child class to interpret what step really means
   */
  virtual
  void
  modify(
    const int8_t steps)
  {
  }

  /**
   * Called by MenuDriver when button is pressed on item to begin editing
   * the property. Child class can use this callback to load the modified
   * property from persistent storage.
   */
  virtual
  void
  load()
  {
  }

  /**
   * Called by MenuDriver when button is pressed after editing the property.
   * Child class can use this callback to store/apply the modified property.
   */
  virtual
  void
  store()
  {
  }

  /**
   * Called by MenuDriver when edit is canceled. Editing should stop, but no
   * storage operation will be requested.
   */
  virtual
  void
  cancel()
  {
  }

  /**
   * Called by MenuDriver when it wants to get a string representation of
   * the property. The returned string will be used to display the property's
   * value within the menu.
   */
  virtual
  const char *
  to_string()
  {
    return '\0';
  }

  // Set by MenuDriver
  // 1 if currently editing property, 0 otherwise
  uint8_t editing;
};

union ItemActionUnion
{
  MenuId goto_id;
  void (*callback)();
  PropertyEditor *p_edit;
};

struct MenuItemAction
{
  ItemActionType type;
  ItemActionUnion data;
};

struct MenuItem
{
  // text to display in the menu
  const char *text;

  MenuItemAction action;
};


enum MenuType
{
  eMT_Numbered,
  eMT_List
};

struct Menu
{
  const MenuId id;
  const char *title;
  
  MenuType type;
  uint8_t n_items;
  const MenuItem *items;

  // ALLOW_BACK if 'back' button is supported on this menu
  // NO_BACK otherwise
  uint8_t allow_back;

  // called when the menu is displayed
  void (*on_enter)();
};

//--------------------------------------------------------------------------------------
// Helper Macros
//--------------------------------------------------------------------------------------

// returns the length of static array of MenuItem's
#define N_ITEMS_IN_MENU(_menu_item_list) (sizeof(_menu_item_list)/sizeof(MenuItem))

// helpers for creating menu items
#define MAKE_MENU_ITEM_CALLBACK(_text,_callback_ptr) \
  {.text = _text, .action = {.type = ItemActionType::eCallback, .data = {.callback = _callback_ptr}}}
#define MAKE_MENU_ITEM_GOTO(_text,_goto_id) \
  {.text = _text, .action = {.type = ItemActionType::eGoToMenu, .data = {.goto_id = _goto_id}}}
#define MAKE_MENU_ITEM_PROP_EDIT(_text,_p_edit) \
  {.text = _text, .action = {.type = ItemActionType::ePropertyEdit, .data = {.p_edit = _p_edit}}}

#define MAKE_MENU(_id,_title,_type,_n_items,_items,_allow_back,_on_enter) \
  {.id = _id, .title = _title, .type = _type, .n_items = _n_items, .items = _items, .allow_back = _allow_back, .on_enter = _on_enter}
#define MAKE_LIST_MENU(_id,_title,_n_items,_items,_allow_back,_on_enter) \
  MAKE_MENU(_id,_title,MenuType::eMT_List,_n_items,_items,_allow_back,_on_enter)
#define MAKE_NUMBERED_MENU(_id,_title,_n_items,_items,_allow_back,_on_enter) \
  MAKE_MENU(_id,_title,MenuType::eMT_Numbered,_n_items,_items,_allow_back,_on_enter)

class MenuDriver
{
  public:
    MenuDriver(
      Menu *menus,
      uint8_t n_menus,
      uint8_t n_rows_displayed)
     : menus_(menus)
     , n_menus_(n_menus)
     , n_rows_disp_(n_rows_displayed)
     , curr_menu_(0)
     , curr_item_(0)
     , disp_top_item_(0)
     , back_highlighted_(0)
     , editing_prop_(0)
    {
    }

    /**
     * Sets the current menu to the provide menu id
     * 
     * @param[in] id
     * The menu identifier to change to
     * 
     * @param[in] allow_back
     * set to ALLOW_BACK if you want to push the current page onto the back stack.
     * otherwise, the current state of the back stack will be cleared.
     * 
     * @return
     * 0 on success
     * -1 on error
     */
    int8_t
    set_menu(
      const MenuId id,
      const uint8_t allow_back = NO_BACK)
    {
      int8_t ret = 0;
      if (id < n_menus_)
      {
        // in the future we could do an on_exit() callback here if needed

        if (allow_back == ALLOW_BACK)
        {
          if (back_stack_idx_ >= MAX_BACK_STACK_DEPTH)
          {
            ERROR("backstack full!");
            ret = -1;
          }
          else
          {
            back_stack_[back_stack_idx_++] = curr_menu_;
          }
        }
        else
        {
          // not allowing back operation. don't want discontinuity, so reset stack.
          back_stack_idx_ = 0;
        }

        change_menu(id);
      }
      else
      {
        ERROR("menu id out of range! id = %d; n_menus_ = %d", id, n_menus_);
        ret = -1;
      }
      return ret;
    }
    
    /**
     * Go back to previous menu if one is available
     * 
     * @return
     * 0 on success
     * -1 if back stack is empty
     */
    int8_t
    back()
    {
      if (back_stack_idx_ > 0)
      {
        back_stack_idx_--;
        change_menu(back_stack_[back_stack_idx_]);
        return 0;
      }
      else
      {
        return -1;
      }
    }

    uint8_t
    top_disp_item()
    {
      return disp_top_item_;
    }

    uint8_t
    bot_disp_item()
    {
      uint8_t bot = disp_top_item_ + n_rows_disp_ - 1;
      if (bot >= menus_[curr_menu_].n_items)
      {
        bot = menus_[curr_menu_].n_items - 1;
      }
      return bot;
    }

    /**
     * @return
     * 1 if back button is highlighted currently
     * 0 otherwise
     */
    uint8_t
    back_highlighted() const
    {
      return back_highlighted_;
    }
    
    /**
     * @return
     * 1 if editing a property currently
     * 0 otherwise
     */
    uint8_t
    editing() const
    {
      return editing_prop_;
    }

    uint8_t
    /**
     * @return
     * ALLOW_BACK if menu support back button
     * NO_BACK otherwise
     */
    allow_back() const
    {
      return curr_menu()->allow_back;
    }

    uint8_t
    curr_item()
    {
      return curr_item_;
    }

    const Menu *
    curr_menu()
    {
      return &menus_[curr_menu_];
    }

    /**
     * increments to the next item in the list
     * 
     * @param[in] steps
     * Number of menu rows to step by
     * 
     * @return
     * 1 if successful increment
     * 0 if no more items to increment to
     */
    int8_t
    next_item(
      uint8_t steps = 1)
    {
      const Menu *menu = curr_menu();
      // redirect cursor movement to property editor's modify() function
      if (editing_prop_)
      {
        if (menu->items[curr_item_].action.data.p_edit != nullptr)
        {
          menu->items[curr_item_].action.data.p_edit->modify(steps);
        }
        return 0;
      }

      // handle case where cursor is on the 'back' button rather than a menu item
      if (back_highlighted_ && steps > 0)
      {
        back_highlighted_ = false;
        // getting out of back button takes a step
        steps--;
      }
      
      curr_item_ += steps;
      // handle case where steps pushes us past the end
      if (curr_item_ >= menu->n_items)
      {
        curr_item_ = menu->n_items - 1;
      }

      // see if we need to scroll the window
      uint8_t curr_rel_top = curr_item_ - top_disp_item();
      if (curr_rel_top >= n_rows_disp_)
      {
        disp_top_item_ = curr_item_ - n_rows_disp_ + 1;
      }
      
      return (curr_item_ < (menu->n_items - 1)) ? 1 : 0;
    }

    /**
     * decrements to the prev item in the list
     * 
     * @param[in] steps
     * Number of menu rows to step by
     * 
     * @return
     * 1 if successful decrement
     * 0 if no more items to decrement to
     */
    int8_t
    prev_item(
      const uint8_t steps = 1)
    {
      const Menu *menu = curr_menu();
      // redirect cursor movement to property editor's modify() function
      if (editing_prop_)
      {
        if (menu->items[curr_item_].action.data.p_edit != nullptr)
        {
          menu->items[curr_item_].action.data.p_edit->modify(-1 * steps);
        }
        return 0;
      }
      
      // handle case where cursor is on the 'back' button rather than a menu item
      if (allow_back() == ALLOW_BACK && curr_item_ == 0)
      {
        if (back_highlighted_)
        {
          return 0;
        }
        back_highlighted_ = true;
        return 0;
      }

      // handle normal menu scrolling case
      if (curr_item_ < steps)
      {
        curr_item_ = 0;
      }
      else
      {
        curr_item_ -= steps;
      }
      
      // see if we need to scroll the window
      if (curr_item_ < disp_top_item_)
      {
        disp_top_item_ = curr_item_;
      }
      
      return (curr_item_ == 0) ? 0 : 1;
    }

    /**
     * Activates the current menu item or perform back if highlighted
     * 
     * @return
     * 2 if action was valid and menu 'hide' is requested
     * 1 if action was valid and performed
     * 0 if no action performed
     * -1 on error
     */
    int8_t
    menu_click()
    {
      int8_t ret = 0;

      // handle back button operations
      if (back_highlighted_)
      {
        ret = back();
        if (ret == -1)
        {
          // no more items in back stack, so request menu 'hide'
          return 2;
        }
        else
        {
          // back operation was successful
          return 1;
        }
      }

      // ---------------------------

      // handle normal menu item operations
      const Menu *menu = &menus_[curr_menu_];
      const MenuItem *item = &menu->items[curr_item_];
      switch (item->action.type)
      {
        case ItemActionType::eGoToMenu:
          ret = 1;
          if (set_menu(item->action.data.goto_id,ALLOW_BACK))
          {
            // set menu failed
            ret = -1;
          }
          break;
        case ItemActionType::eCallback:
          if (item->action.data.callback != nullptr)
          {
            (*item->action.data.callback)();
            ret = 1;
          }
          break;
        case ItemActionType::ePropertyEdit:
          // toggle edit flag
          editing_prop_ = ! editing_prop_;
          
          // notify PropertyEditor of load/store event
          if (editing_prop_)
          {
            ret = start_pedit(item->action.data.p_edit);
          }
          else
          {
            ret = stop_pedit(item->action.data.p_edit,0);// 0 -> store
          }
          break;
        default:
          ERROR("invalid action type %d", item->action.type);
          ret = -1;
          break;
      }
      return ret;
    }

  private:
    /**
     * Internal handler for changing the current menu
     * 
     * @param[in] id
     * The menu identifier to change to.
     * WARNING: it's on the caller to make sure the id is valid
     */
    void
    change_menu(
      const MenuId id)
    {
      curr_menu_ = id;
      curr_item_ = 0;
      disp_top_item_ = 0;
      back_highlighted_ = 0;

      // cancel property editing if changing menus before storage request
      if (editing_prop_)
      {
        const Menu *menu = &menus_[curr_menu_];
        const MenuItem *item = &menu->items[curr_item_];
        stop_pedit(item->action.data.p_edit,1);// 1 -> cancel
        editing_prop_ = 0;
      }
      
      // perform on_enter() callback if defined
      if (menus_[curr_menu_].on_enter != nullptr)
      {
        (*menus_[curr_menu_].on_enter)();
      }
    }

    int8_t
    start_pedit(
      PropertyEditor *pedit)
    {
      if (pedit == nullptr)
      {
        ERROR("pedit is null!");
        return -1;
      }
      
      // notify PropertyEditor of load event
      pedit->load();
      pedit->editing = 1;
      
      return 0;
    }

    int8_t
    stop_pedit(
      PropertyEditor *pedit,
      uint8_t cancel)
    {
      if (pedit == nullptr)
      {
        ERROR("pedit is null!");
        return -1;
      }
      
      // notify PropertyEditor of store/canel event
      if (cancel)
      {
        pedit->cancel();
      }
      else
      {
        pedit->store();
      }
      pedit->editing = 0;

      return 0;
    }

  private:
    Menu *menus_;
    uint8_t n_menus_;
    uint8_t n_rows_disp_;

    // the current menu id to display
    MenuId curr_menu_;
    // the cursors location in the current menu
    uint8_t curr_item_;

    // the menu item that is at the top of the display.
    // this is used for scrolling through lists
    uint8_t disp_top_item_;

    // 1 if back button should be highlight (rather than current menu item)
    uint8_t back_highlighted_;

    // 1 if currently editing a property, 0 if not
    uint8_t editing_prop_;

    // variables to manage the back stack
    MenuId back_stack_[MAX_BACK_STACK_DEPTH];
    // points to the next available index in the stack (to push the current page to).
    // to get the previous page to go back to, look at (back_stack_idx_-1)
    uint8_t back_stack_idx_;
    
};


#endif
