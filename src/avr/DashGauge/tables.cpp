#include "tables.h"

// a place to temporarily store modified pages of flash
uint8_t temp_page[MAX_FLASH_TABLE_SIZE];
OutPC_T out_pc;
Page2_T page2;

void
initTables()
{
  // init test tables
  page2.gaugeTestCtrl.bits.mode = TEST_MODE_DISABLE;
  page2.testValueTach = 0;
  page2.testValueSpeedo = 0;
  page2.testValueCoolantTemp = 0;
  page2.testValueOilPressure = 0;
  page2.testValueFuelLevel = 0;
}

template <>
size_t
flashTableAbsOffset(
  const size_t &field_offset,
  const Page1_T *__unused_arg)
{
  return PAGE1_FLASH_OFFSET + field_offset;
}

template <>
size_t
flashTableAbsOffset(
  const size_t &field_offset,
  const Page3_T *__unused_arg)
{
  return PAGE3_FLASH_OFFSET + field_offset;
}
