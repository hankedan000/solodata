#include "LCD_Menu.h"

#include "DashGaugeGlobals.h"
#include "DashMotors.h"
#include <stdio.h>
#include "tables.h"

#define max(a,b) (a>b ? a : b)
#define min(a,b) (a<b ? a : b)

/**
 * macros that returns the flash offset in bytes to the designated 
 * gauge page's PageDefn_t structure.
 */
// 
#define PAGE_DEFN_OFFSET(_page_idx) (PAGE3_FIELD_OFFSET(pageDefns) + sizeof(PageDefn_t) * _page_idx)
#define ACTIVE_PAGE_DEFN_OFFSET() PAGE_DEFN_OFFSET(display.activePage())

#define MAX_PEDIT_STR_BUFF 100
char pedit_str_buff[MAX_PEDIT_STR_BUFF];

/**
 * variables used to stage PropertyEditor values that have been loaded
 * from flash. These variables are shared amongst PropertyEdit classes
 * and typically used with the to_string() methods.
 */
uint8_t pedit_temp8;
uint16_t pedit_temp16;

/** 
 * variables used to stage PropertyEditor values that have been loaded
 * from flash. Unlike the above pedit_temp* variables, these variables
 * should be exclusively owned by the PropertyEditor while it's actively
 * editing a property. The convention is to load these variables from
 * flash in PropertyEditor::load() and then subsequently store the
 * modified result in PropertyEditor::store().
 */
uint8_t pedit_excl8;
uint16_t pedit_excl16;

GaugeTypeProperty gauge_type_pedit;
MenuHideTimeProperty menu_hide_time_pedit;

//-----------------------------------------------------------------
// MONITOR GAUGE EDIT MENU
//-----------------------------------------------------------------
DashVarProperty dv_monitor_pedit(PageType_monitor,0);
MenuItem monitor_gauge_edit_menu_items[] = {
  MAKE_MENU_ITEM_PROP_EDIT("Var: ",&dv_monitor_pedit)
};

//-----------------------------------------------------------------
// MONITOR GAUGE MENU
//-----------------------------------------------------------------
void monitor_gauge_reset();// callback prototype
MenuItem monitor_gauge_menu_items[] = {
  MAKE_MENU_ITEM_CALLBACK("Reset monitor",&monitor_gauge_reset),
  MAKE_MENU_ITEM_GOTO("Edit Gauge",MENU_ID_MONITOR_EDIT),
  MAKE_MENU_ITEM_PROP_EDIT("",&gauge_type_pedit),
  MAKE_MENU_ITEM_GOTO("Settings",MENU_ID_SETTINGS)
};

void
monitor_gauge_reset()
{
  display.resetMonitorData(display.activePage());
  display.menuVisible(false);
}

//-----------------------------------------------------------------
// HSPLIT GAUGE EDIT MENU
//-----------------------------------------------------------------
HSplitNumVarsProperty num_var_hsplit_pedit;
DashVarProperty dv_hsplit_pedit0(PageType_hsplit,0);
DashVarProperty dv_hsplit_pedit1(PageType_hsplit,1);
DashVarProperty dv_hsplit_pedit2(PageType_hsplit,2);
MenuItem hsplit_gauge_edit_menu_items[] = {
  MAKE_MENU_ITEM_PROP_EDIT("Num Gauges: ",&num_var_hsplit_pedit),
  MAKE_MENU_ITEM_PROP_EDIT("Var 1: ",&dv_hsplit_pedit0),
  MAKE_MENU_ITEM_PROP_EDIT("Var 2: ",&dv_hsplit_pedit1),
  MAKE_MENU_ITEM_PROP_EDIT("Var 3: ",&dv_hsplit_pedit2)
};

//-----------------------------------------------------------------
// HSPLIT GAUGE MENU
//-----------------------------------------------------------------
MenuItem hsplit_gauge_menu_items[] = {
  MAKE_MENU_ITEM_GOTO("Edit Gauge",MENU_ID_HSPLIT_EDIT),
  MAKE_MENU_ITEM_PROP_EDIT("",&gauge_type_pedit),
  MAKE_MENU_ITEM_GOTO("Settings",MENU_ID_SETTINGS)
};

//-----------------------------------------------------------------
// NULL GAUGE MENU
//-----------------------------------------------------------------
MenuItem null_gauge_menu_items[] = {
  MAKE_MENU_ITEM_PROP_EDIT("",&gauge_type_pedit),
  MAKE_MENU_ITEM_GOTO("Settings",MENU_ID_SETTINGS)
};

//-----------------------------------------------------------------
// SETTINGS MENU
//-----------------------------------------------------------------
void settings_home_gauges();// callback prototype
MenuItem settings_menu_items[] = {
  MAKE_MENU_ITEM_PROP_EDIT("",&menu_hide_time_pedit),
  MAKE_MENU_ITEM_CALLBACK("Home gauges",&settings_home_gauges),
  MAKE_MENU_ITEM_CALLBACK("Restore defaults",nullptr)
};

// callback function for when the "home gauges" setting menu item is selected
void
settings_home_gauges()
{
  homeAllGauges();
}

//-----------------------------------------------------------------
// PROPERTY EDITORS
//-----------------------------------------------------------------

void
MenuHideTimeProperty::modify(
  const int8_t steps)
{
  pedit_excl8 += (steps > 0 ? 1 : -1);
  // make sure value stays within valid range
  pedit_excl8 = max(pedit_excl8,MIN_MENU_HIDE_TIME);
  pedit_excl8 = min(pedit_excl8,MAX_MENU_HIDE_TIME);
}

void
MenuHideTimeProperty::load()
{
  pedit_excl8 = FLASH_TABLE_READ(Page3_T,menuHideTime_s);
}

void
MenuHideTimeProperty::store()
{
  FLASH_TABLE_WRITE(Page3_T,menuHideTime_s,pedit_excl8);
}

const char *
MenuHideTimeProperty::to_string()
{
  snprintf(
    pedit_str_buff,MAX_PEDIT_STR_BUFF,
    "Menu hide time: %us",
    (editing ? pedit_excl8 : FLASH_TABLE_READ(Page3_T,menuHideTime_s)));
  return pedit_str_buff;
};

// ------------------------------------------

void
GaugeTypeProperty::modify(
  const int8_t steps)
{
  /**
   * cycle throught options like this
   * 
   * +--> null <-> hsplit <-> monitor <--+
   * |                                   |
   * +-----------------------------------+
   */
  switch (pedit_excl8)
  {
    case PageType_hsplit:
      pedit_excl8 = (steps > 0 ? PageType_monitor : PageType_null);
      break;
    case PageType_monitor:
      pedit_excl8 = (steps > 0 ? PageType_null    : PageType_hsplit);
      break;
    case PageType_null:
      pedit_excl8 = (steps > 0 ? PageType_hsplit  : PageType_monitor);
      break;
    default:
      pedit_excl8 = PageType_null;
      break;
  }
}

void
GaugeTypeProperty::load()
{
  // load current gauge page's type from flash
  size_t offset = ACTIVE_PAGE_DEFN_OFFSET();
  offset += offsetof(PageDefn_t,type);
  pedit_excl8 = EEPROM.read(offset);
}

void
GaugeTypeProperty::store()
{
  // store current gauge page's type from flash
  uint8_t page_idx = display.activePage();
  size_t offset = PAGE_DEFN_OFFSET(page_idx);
  offset += offsetof(PageDefn_t,type);
  EEPROM.write(offset,pedit_excl8);

  display.menuVisible(false);
  updatePageDefnFromFlash(page_idx);
}

const char *
GaugeTypeProperty::to_string()
{
  uint8_t gauge_type = pedit_excl8;
  if ( ! editing)
  {
    size_t offset = ACTIVE_PAGE_DEFN_OFFSET();
    offset += offsetof(PageDefn_t,type);
    gauge_type = EEPROM.read(offset);
  }
  snprintf(
    pedit_str_buff,MAX_PEDIT_STR_BUFF,
    "Gauge Type: %S",
    (const __FlashStringHelper*)GaugeTypeString[gauge_type]);
  return pedit_str_buff;
};

// ------------------------------------------

void
HSplitNumVarsProperty::modify(
  const int8_t steps)
{
  if (steps > 0 && pedit_excl8 < MAX_H_SPLIT_GAUGES)
  {
    pedit_excl8++;
  }
  else if (steps < 0 && pedit_excl8 > 0)
  {
    pedit_excl8--;
  }
}

void
HSplitNumVarsProperty::load()
{
  // load dash var from flash
  size_t defn_offset = ACTIVE_PAGE_DEFN_OFFSET();
  pedit_excl8 = EEPROM.read(defn_offset + n_var_offset);
}

void
HSplitNumVarsProperty::store()
{
  // store dash var to flash
  uint8_t page_idx = display.activePage();
  size_t defn_offset = PAGE_DEFN_OFFSET(page_idx);
  EEPROM.write(defn_offset + n_var_offset,pedit_excl8);
  
  updatePageDefnFromFlash(page_idx);
}

const char *
HSplitNumVarsProperty::to_string()
{
  uint8_t n_vars = pedit_excl8;
  if ( ! editing)
  {
    size_t defn_offset = ACTIVE_PAGE_DEFN_OFFSET();
    n_vars = EEPROM.read(defn_offset + n_var_offset);
  }
  snprintf(
    pedit_str_buff,MAX_PEDIT_STR_BUFF,
    "%u",
    n_vars);
  return pedit_str_buff;
};

// ------------------------------------------

void
DashVarProperty::modify(
  const int8_t steps)
{
  if (steps > 0)
  {
    if (pedit_excl8 == (NUM_VARS - 1))
    {
      pedit_excl8 = 0;
    }
    else
    {
      pedit_excl8++;
    }
  }
  else
  {
    if (pedit_excl8 == 0)
    {
      pedit_excl8 = NUM_VARS - 1;
    }
    else
    {
      pedit_excl8--;
    }
  }
}

void
DashVarProperty::load()
{
  // load dash var from flash
  size_t defn_offset = ACTIVE_PAGE_DEFN_OFFSET();
  pedit_excl8 = EEPROM.read(defn_offset + var_offset);
}

void
DashVarProperty::store()
{
  // store dash var to flash
  uint8_t page_idx = display.activePage();
  size_t defn_offset = PAGE_DEFN_OFFSET(page_idx);
  EEPROM.write(defn_offset + var_offset,pedit_excl8);
  
  updatePageDefnFromFlash(page_idx);
}

const char *
DashVarProperty::to_string()
{
  var_id_t var_id = pedit_excl8;
  if ( ! editing)
  {
    size_t defn_offset = ACTIVE_PAGE_DEFN_OFFSET();
    var_id = EEPROM.read(defn_offset + var_offset);
  }
  snprintf(
    pedit_str_buff,MAX_PEDIT_STR_BUFF,
    "%S",
    (const __FlashStringHelper*)dash_vars[var_id]->title);
  return pedit_str_buff;
};

//-----------------------------------------------------------------
// ALL MENUS
//-----------------------------------------------------------------

Menu lcd_menus[] = {
//menu_id                              title             n_items                                         items                          'back' allowed    on_enter callback
  MAKE_LIST_MENU(MENU_ID_MONITOR,      "Menu",           N_ITEMS_IN_MENU(monitor_gauge_menu_items),      monitor_gauge_menu_items,      ALLOW_BACK,       nullptr),
  MAKE_LIST_MENU(MENU_ID_HSPLIT,       "Menu",           N_ITEMS_IN_MENU(hsplit_gauge_menu_items),       hsplit_gauge_menu_items,       ALLOW_BACK,       nullptr),
  MAKE_LIST_MENU(MENU_ID_NULL_GAUGE,   "Menu",           N_ITEMS_IN_MENU(null_gauge_menu_items),         null_gauge_menu_items,         ALLOW_BACK,       nullptr),
  MAKE_LIST_MENU(MENU_ID_SETTINGS,     "Settings",       N_ITEMS_IN_MENU(settings_menu_items),           settings_menu_items,           ALLOW_BACK,       nullptr),
  MAKE_LIST_MENU(MENU_ID_MONITOR_EDIT, "Monitor Gauge",  N_ITEMS_IN_MENU(monitor_gauge_edit_menu_items), monitor_gauge_edit_menu_items, ALLOW_BACK,       nullptr),
  MAKE_LIST_MENU(MENU_ID_HSPLIT_EDIT,  "H-Split Gauge",  N_ITEMS_IN_MENU(hsplit_gauge_edit_menu_items),  hsplit_gauge_edit_menu_items,  ALLOW_BACK,       nullptr)
};

static_assert(sizeof(lcd_menus)/sizeof(Menu) == NUM_MENUS);
