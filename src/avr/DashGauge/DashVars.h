#ifndef DASH_VARS_H_
#define DASH_VARS_H_

#include <avr/pgmspace.h>
#include "LCD_GaugeTypes.h"

// monitor index used to indicate that a DashVar is not monitored
#define NO_MONITOR 0xff

uint16_t
get_uint16(
  const void *v);

enum Unit
{
  none = 0,
  s = 1,
  ms = 2,
  us = 3,
  percent = 4,
  volts = 5,
  degF = 6,
  degC = 7,
  mph = 8,
  rpm = 9,
  kpa = 10,
  psi = 11
};

struct VarDefn_t
{
  // the variable's title
  const char * const title PROGMEM;

  // getter function for the dash variable's value
  uint16_t (*getter)(const void *);
  // arguments that should be passed to the getter
  const void *argv;
  
  // the variable's unit type
  const Unit unit;
  
  /**
   * The index of the monitor gauge that's attached to this variable.
   * If this variable is not being monitored, then this index is set
   * to NO_MONITOR; otherwise, set this to the gauge page index that
   * is doing the monitoring.
   * Valid range: [0 to (NUM_GAUGE_PAGE_DEFNS - 1)] or NO_MONITOR
   */
  uint8_t mon_page_idx;
};

#define VAR_ID_SPEED   0
#define VAR_ID_BATTERY 1
#define VAR_ID_RPM     2
#define VAR_ID_MAP     3
#define VAR_ID_MAT     4
#define VAR_ID_CLT     5

static const char VAR_TITLE_SPEED[] PROGMEM = "Speed";
static const char VAR_TITLE_BATTERY[] PROGMEM = "Battery";
static const char VAR_TITLE_RPM[] PROGMEM = "RPM";
static const char VAR_TITLE_MAP[] PROGMEM = "MAP";
static const char VAR_TITLE_MAT[] PROGMEM = "MAT";
static const char VAR_TITLE_CLT[] PROGMEM = "CLT";

extern VarDefn_t var_speed;
extern VarDefn_t var_battery;
extern VarDefn_t var_rpm;
extern VarDefn_t var_map;
extern VarDefn_t var_mat;
extern VarDefn_t var_clt;
static VarDefn_t *dash_vars[] = {
  &var_speed    ,// 0
  &var_battery  ,// 1
  &var_rpm      ,// 2
  &var_map      ,// 3
  &var_mat      ,// 4
  &var_clt      ,// 5
};

#define NUM_VARS (sizeof(dash_vars) / sizeof(VarDefn_t *))
static_assert(NUM_VARS <= MAX_USABLE_VARS);

#endif
