#ifndef DASH_GAUGE_GLOBALS_H_
#define DASH_GAUGE_GLOBALS_H_

#include "LCD_Gauge.h"

extern LCD_Display display;

int8_t
updatePageDefnFromFlash(
  uint8_t page_idx);

#endif
