#include "LCD_Gauge.h"

#include <logging.h>
#include <util/atomic.h>

#define YELLOW_AREA_HEIGHT 17

//------------------------------------------------------------------
// Public Methods
//------------------------------------------------------------------

LCD_Display::LCD_Display(
  uint16_t width,
  uint16_t height,
  int8_t reset_pin)
 : display_(width,height,&Wire,reset_pin)
 , active_page_(0)
 , menu_visible_(false)
 , lcd_menu_driver_(lcd_menus,NUM_MENUS,LINES_PER_MENU)
{
  for (uint8_t i=0; i<NUM_GAUGE_PAGE_DEFNS; i++)
  {
    page_data_[i].type = PageType_null;
  }
}

void
LCD_Display::init()
{
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display_.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    ERROR("SSD1306 allocation failed");
    for(;;); // Don't proceed, loop forever
  }

  // Show initial display buffer contents on the screen --
  // the library initializes this with an Adafruit splash screen.
  display_.display();

  // Clear the buffer
  display_.clearDisplay();
}

int8_t
LCD_Display::setPageMonitor(
  const uint8_t &page_idx,
  const var_id_t &var)
{
  if (page_idx >= NUM_GAUGE_PAGE_DEFNS)
  {
    ERROR("page_idx %d oob",page_idx);
    return -1;
  }
  else if (var >= NUM_VARS)
  {
    ERROR("var %d oob",var);
    return -1;
  }

  int8_t ret = 0;
  PageSpecificData *pd = &page_data_[page_idx];

  // release page data from previous allocation
  if (releasePageData(pd) == 0)
  {
    // allocate page data for monitor now
    pd->type = PageType::PageType_monitor;
    pd->data.mon.var = var;
    internalResetMonitorData(&pd->data.mon);

    dash_vars[var]->mon_page_idx = page_idx;
  }
  else
  {
    ERROR("failed to release page %d",page_idx);
    ret = -1;
  }
  
  updateRootMenu();

  return ret;
}

int8_t
LCD_Display::setPageHSplit(
  const uint8_t &page_idx,
  const H_SplitDefn_t &hsplit)
{
  if (page_idx >= NUM_GAUGE_PAGE_DEFNS)
  {
    ERROR("page_idx %d oob",page_idx);
    return -1;
  }

  int8_t ret = 0;
  PageSpecificData *pd = &page_data_[page_idx];
  // release page data from previous allocation
  if (releasePageData(pd) == 0)
  {
    // allocate page data for H-Split now
    pd->type = PageType::PageType_hsplit;
    pd->data.hsplit = hsplit;
  }
  else
  {
    ERROR("failed to release page %d",page_idx);
    ret = -1;
  }
  
  updateRootMenu();

  return ret;
}

int8_t
LCD_Display::resetMonitorData(
  const uint8_t &page_idx)
{
  MonitorPageData *mon_data;
  if (getMonitorData(page_idx,mon_data) == 0)
  {
    internalResetMonitorData(mon_data);
    return 0;
  }
  return -1;
}

int8_t
LCD_Display::getMonitorData(
  const uint8_t &page_idx,
  MonitorPageData *&mon_data)
{
  int8_t ret = 0;

  if (page_idx < NUM_GAUGE_PAGE_DEFNS)
  {
    if (page_data_[page_idx].type == PageType::PageType_monitor)
    {
      mon_data = &page_data_[page_idx].data.mon;
    }
    else
    {
      ERROR("page is not a monitor");
      ret = -2;
    }
  }
  else
  {
    ERROR("page_idx %d oob",page_idx);
    ret = -1;
  }

  return ret;
}

void
LCD_Display::monitor()
{
  for (uint8_t i=0; i<NUM_GAUGE_PAGE_DEFNS; i++)
  {
    if (page_data_[i].type == PageType_monitor)
    {
      MonitorPageData *mpd = &page_data_[i].data.mon;
      VarDefn_t *var = dash_vars[mpd->var];
      uint16_t val = (*var->getter)(var->argv);
      if (val < mpd->min)
      {
        mpd->min = val;
      }
      if (val > mpd->max)
      {
        mpd->max = val;
      }
    }
  }
}

void
LCD_Display::draw()
{
  if (menu_visible_)
  {
    drawMenu();
  }
  else
  {
    PageSpecificData *pd = &page_data_[active_page_];
    switch (pd->type)
    {
      case PageType_hsplit:
        drawH_SplitPage(&pd->data.hsplit);
        break;
      case PageType_monitor:
        drawMonitorPage(&pd->data.mon);
        break;
      case PageType_null:
        drawError(F("NULL PAGE"));
        break;
      default:
        drawError(F("invalid page type"));
        break;
    }
    
    drawPageIndicator();
  
    display_.display();
  
    first_frame_of_page_ = false;
  }
}

void
LCD_Display::nextPage()
{
  active_page_++;
  if (active_page_ >= NUM_GAUGE_PAGE_DEFNS)
  {
    active_page_ = 0;
  }
  
  updateRootMenu();
  first_frame_of_page_ = true;
  screen_inverted_ = false;
  first_frame_of_page_ = true;
  screen_inverted_ = false;
}

void
LCD_Display::prevPage()
{
  if (active_page_ == 0)
  {
    active_page_ = NUM_GAUGE_PAGE_DEFNS - 1;
  }
  else
  {
    active_page_--;
  }

  updateRootMenu();
  first_frame_of_page_ = true;
  screen_inverted_ = false;
  first_frame_of_page_ = true;
  screen_inverted_ = false;
}

int8_t
LCD_Display::nextMenuItem()
{
  return lcd_menu_driver_.next_item();
}

int8_t
LCD_Display::prevMenuItem()
{
  return lcd_menu_driver_.prev_item();
}

int8_t
LCD_Display::menuClick()
{
  int8_t ret = lcd_menu_driver_.menu_click();
  // check if menu 'hide' was requested when backstack is empty
  if (ret == 2)
  {
      menuVisible(false);
      ret = 1;
  }
  return ret;
}

//------------------------------------------------------------------
// Private Methods
//------------------------------------------------------------------

int8_t
LCD_Display::releasePageData(
  PageSpecificData *pd)
{
  if (pd == nullptr)
  {
    ERROR("pd is null");
    return -1;
  }
  int8_t ret = 0;

  switch (pd->type)
  {
    case PageType::PageType_null:
    case PageType::PageType_hsplit:
    case PageType::PageType_vsplit:
      // nothing to release
      break;
    case PageType::PageType_monitor:
      // mark variable so it's no longer being monitored
      if (pd->data.mon.var < NUM_VARS)
      {
        dash_vars[pd->data.mon.var]->mon_page_idx = NO_MONITOR;
      }
      else
      {
        ERROR("invalid var %d",pd->data.mon.var);
        ret = -1;
      }
      break;
    default:
      ERROR("unsupported page type %d",pd->type);
      ret = -1;
      break;
  }

  if (ret == 0)
  {
    pd->type = PageType::PageType_null;
  }

  return ret;
}

void
LCD_Display::internalResetMonitorData(
  MonitorPageData *mon_data)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    mon_data->min = 0xFFFF;
    mon_data->max = 0x0000;
  }
}

int8_t
LCD_Display::updateRootMenu()
{
  int8_t ret = 0;
  
  switch (page_data_[active_page_].type)
  {
    case PageType_monitor:
      lcd_menu_driver_.set_menu(MENU_ID_MONITOR,NO_BACK);
      break;
    case PageType_hsplit:
      lcd_menu_driver_.set_menu(MENU_ID_HSPLIT,NO_BACK);
      break;
    case PageType_null:
    default:
      lcd_menu_driver_.set_menu(MENU_ID_NULL_GAUGE);
      break;
  }

  return ret;
}

void
LCD_Display::drawPageIndicator()
{
  const uint8_t ind_width = display_.width() / NUM_GAUGE_PAGE_DEFNS;
  uint8_t ind_offset = ind_width * active_page_;
  display_.drawLine(ind_offset,display_.height()-1,ind_offset+ind_width,display_.height()-1,SSD1306_WHITE);
}
  
void
LCD_Display::drawError(
  const __FlashStringHelper *err_str)
{
  display_.clearDisplay();

  display_.setTextColor(SSD1306_WHITE);
  display_.setTextSize(1);
  display_.setCursor(0,0);
  display_.print(err_str);
}

void
LCD_Display::drawError(
    const char *err_str)
{
  display_.clearDisplay();

  display_.setTextColor(SSD1306_WHITE);
  display_.setTextSize(1);
  display_.setCursor(0,0);
  display_.print(err_str);
}

void
LCD_Display::drawH_SplitPage(
  const H_SplitDefn_t *hsplit)
{
  display_.clearDisplay();
  
  const uint16_t xStep = display_.width() / hsplit->n_vars;
  uint16_t xOffset = 0;
  for (uint8_t vv=0; vv<hsplit->n_vars; vv++)
  {
    if (hsplit->vars[vv] < NUM_VARS)
    {
      drawGauge(xOffset,dash_vars[hsplit->vars[vv]]);
    }
    xOffset += xStep;
  }
}

void
LCD_Display::drawMonitorPage(
  const MonitorPageData *mon_data)
{
  display_.clearDisplay();
  
  display_.setTextColor(SSD1306_WHITE);

  // draw the "live" variable gauge on the left side of screen
  const VarDefn_t *var = dash_vars[mon_data->var];
  drawGauge(0,var);
  
  display_.setTextSize(1);
  display_.setCursor(60,20);
  display_.print(F("min: "));
  display_.print(mon_data->min);
  display_.setCursor(60,30);
  display_.print(F("max: "));
  display_.print(mon_data->max);
}

void
LCD_Display::drawWarnPage(
  const WarnConditionDefn_t *warn)
{
  unsigned long now_ms = millis();
  
  if (first_frame_of_page_)
  {
    display_.clearDisplay();
    
    display_.setTextColor(SSD1306_WHITE);
    display_.setTextSize(2);
  
    display_.setCursor(0,YELLOW_AREA_HEIGHT);
    display_.print(*warn->str);

    last_warn_frame_blink_time_ = now_ms;
  }
  else
  {
    if (warn->blink_rate_ms != 0)
    {
      if ((now_ms - last_warn_frame_blink_time_) >= warn->blink_rate_ms)
      {
        screen_inverted_ = ! screen_inverted_;
        display_.invertDisplay(screen_inverted_);
        last_warn_frame_blink_time_ = now_ms;
      }
    }
  }
  
  display_.display();
}

void
LCD_Display::drawGauge(
  uint16_t xOffset,
  const VarDefn_t *var)
{
  display_.setTextColor(SSD1306_WHITE);

  if (var->title != NULL)
  {
    display_.setTextSize(1);
    display_.setCursor(xOffset, 0);
    display_.print((const __FlashStringHelper*)var->title);
  }

  display_.setTextSize(3); // Draw 2X-scale text
  display_.setCursor(xOffset, 16);
  display_.print(var->getter(var->argv));
  
  if (var->unit != Unit::none)
  {
    display_.setTextSize(2);
    display_.setCursor(xOffset, 38);
    display_.print((const __FlashStringHelper*)UnitStrings[var->unit]);
  }
}

#define MENU_TITLE_TEXT_SIZE 1
#define MENU_ITEM_TEXT_SIZE 1

void
LCD_Display::drawMenu()
{
  const Menu *menu = lcd_menu_driver_.curr_menu();
  uint16_t yoffset = 0;
  
  display_.clearDisplay();
  screen_inverted_ = false;
  display_.invertDisplay(screen_inverted_);
  display_.setTextColor(SSD1306_WHITE);
  
  // -------------------------

  // draw menu bar
  display_.setTextSize(MENU_TITLE_TEXT_SIZE);
  display_.setCursor(0,yoffset);
  if (lcd_menu_driver_.allow_back() == ALLOW_BACK)
  {
    display_.print(lcd_menu_driver_.back_highlighted() ? '>' : ' ');
    display_.print(F("Back"));
  }
  display_.setCursor(50,yoffset);
  display_.print(menu->title);

  // -------------------------

  // draw menu items
  display_.setTextSize(MENU_ITEM_TEXT_SIZE);
  yoffset = YELLOW_AREA_HEIGHT;
  const uint16_t ystep = (display_.height() - YELLOW_AREA_HEIGHT) / LINES_PER_MENU;

  const uint8_t curr_item = lcd_menu_driver_.curr_item();
  for (unsigned int i=lcd_menu_driver_.top_disp_item(); i<=lcd_menu_driver_.bot_disp_item(); i++)
  {
    display_.setCursor(0,yoffset);
    char selector = 'X';// invalid selector
    if (lcd_menu_driver_.back_highlighted())
    {
      // cursor is on the 'back' button, so all items won't have selector
      selector = ' ';
    }
    else if (i == curr_item)
    {
      // item is where the menu cursor currently is
      selector = (lcd_menu_driver_.editing() ? '*' : '>');
    }
    else
    {
      selector = ' ';
    }
    display_.print(selector);
    if (menu->type == MenuType::eMT_Numbered)
    {
      display_.print(i+1);
      display_.print(' ');
    }
    display_.print(menu->items[i].text);

    if (menu->items[i].action.type == ItemActionType::ePropertyEdit)
    {
      if (menu->items[i].action.data.p_edit != nullptr)
      {
        display_.print(menu->items[i].action.data.p_edit->to_string());
      }
    }
    
    yoffset += ystep;
  }
  
  display_.display();
}
