#include <logging_impl_lite.h>

#include <stopwatch.h>
#include <SPI.h>
#include <Wire.h>
#include <avr/wdt.h>
#include <Encoder.h>
#include <PinChangeInterrupt.h>

#include "ButtonDriver.h"
#include "DashCAN.h"
#include "DashGaugeGlobals.h"
#include "DashVars.h"
#include "EndianUtils.h"
#include "LCD_Gauge.h"
#include "tables.h"
#include "DashMotors.h"

// CAN constants
#define CAN_CS  10
#define CAN_INT 3
#define CAN_ID  4
#define CAN_MSG_BUFFER_SIZE 40

// Required for MegaCAN library
DECL_MEGA_CAN_REV("MegaDash");
DECL_MEGA_CAN_SIG("MegaDash-0.1.0     ");

//-----------------------------------------------------------
// function prototypes
//-----------------------------------------------------------

/**
 * Called when the encoder wheel moves one 'notch'
 * 
 * @param[in] clockwise
 * true if the wheel's position incremented one 'notch' clockwise
 * false if the wheel's position incremented one 'nothch' counter clockwise
 */
void
enc_scroll_evt(
  bool clockwise);

void
enc_on_press();

void
enc_on_release();

void
enc_on_hold();

void
check_for_ui_hide();

//-----------------------------------------------------------
// ISR prototypes
//-----------------------------------------------------------

void
enc_sw_isr();

void
can_isr();

uint8_t * heapptr, * stackptr;
void check_mem() {
  //uint8_t * heapptr, * stackptr;  // I declared these globally
  stackptr = (uint8_t *)malloc(4);  // use stackptr temporarily
  heapptr = stackptr;                  // save value of heap pointer
  free(stackptr);                        // free up the memory again (sets stackptr to 0)
  stackptr =  (uint8_t *)(SP);       // save value of stack pointer
}

//-----------------------------------------------------------
// global variable
//-----------------------------------------------------------

MegaCAN::CAN_Msg can_buff[CAN_MSG_BUFFER_SIZE];

// Declaration for a Megasquirt CAN interface
DashCAN dcan(CAN_CS,CAN_ID,CAN_INT,can_buff,CAN_MSG_BUFFER_SIZE,TABLES,NUM_TABLES);

// LCD constants
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
LCD_Display display(SCREEN_WIDTH,SCREEN_HEIGHT,OLED_RESET);

// Encoder used for UI control
//   Best Performance: both pins have interrupt capability
//   Good Performance: only the first pin has interrupt capability
//   Low Performance:  neither pin has interrupt capability
#define ENC_SW_PIN  11
#define ENC_DT_PIN  18
#define ENC_CLK_PIN 19
#define ENC_HOLD_DUR_MS 1000
Encoder enc(ENC_DT_PIN,ENC_CLK_PIN);
ButtonDriver enc_sw_drv(
  ENC_SW_PIN,ACTIVE_LOW,
  enc_on_press,enc_on_release,
  ENC_HOLD_DUR_MS,enc_on_hold);
// the number of encoder steps values that get measure per 'notch' of the knob
#define ENC_STEPS_PER_NOTCH 4
#define ENC_STEP_TO_NOTCH(_steps) (_steps / ENC_STEPS_PER_NOTCH)

// time (in seconds) when the last UI input event was detected
volatile uint8_t time_last_ui_input_s = 0;

volatile uint8_t active_page = 0;

// -----------------------------------------------------------
// setup
// -----------------------------------------------------------

void setup() {
  initTables();
  
  wdt_disable();
  setupLogging();

  // LCD setup
  display.init();
  loadAllPageDefnsFromFlash();
  
  // setup the SwitecX25 gauges
  gaugeSetup();

  // enable watchdog with a 2s timeout
  // FIXME stock arduino bootloader doesn't work with WDT reset.
  // it will get stuck in reset forward once the WDT is engaged.
  // might want to try alternative bootloaders to fix this...
//  wdt_enable(WDTO_2S);

  // MCP2515 configuration
  dcan.init();
  pinMode(CAN_INT, INPUT_PULLUP);// Configuring pin for CAN interrupt input
  attachInterrupt(digitalPinToInterrupt(CAN_INT), can_isr, LOW);

  // Setup encoder switch
  pinMode(ENC_SW_PIN, INPUT);
  attachPCINT(digitalPinToPCINT(ENC_SW_PIN), enc_sw_isr, CHANGE);

  INFO("setup complete!");

  // only show RAM usage when necessary
  if (false)
  {
    check_mem();
    size_t ram_left = (size_t)stackptr - (size_t)heapptr;
    INFO("stackptr: %p", stackptr);
    INFO("heapptr: %p", heapptr);
    INFO("ram_left: ~%dbytes", ram_left);
  }
  
}

// -----------------------------------------------------------
// main loop
// -----------------------------------------------------------

uint32_t loop_start_time_us;
long oldPosition = 0;
void loop() {
  loop_start_time_us = micros();
  wdt_reset();
  dcan.handle();// handle pending MegaCAN frames
  enc_sw_drv.check_for_hold();

  // handle encoder scrolling
  long newPosition = ENC_STEP_TO_NOTCH(enc.read());
  if (newPosition != oldPosition)
  {
    // determine if clockwise rotation
    bool cw = newPosition > oldPosition;
    enc_scroll_evt(cw);
    oldPosition = newPosition;
  }

  // must check after UI input detection logic is complete for
  // this loop iteration; otherwise we might hide menu too soon.
  check_for_ui_hide();

  eng_data.speed_mph += 1;
  if (eng_data.speed_mph > 200)
  {
    eng_data.speed_mph = 0;
  }

  display.monitor();
  display.draw();
  
  // update CAN status registers
  out_pc.canStatus = dcan.getCAN_Status();
  out_pc.canLogicErrorCount = dcan.getLogicErrorCount();
  out_pc.canSW_RxOverflowCount = dcan.getSW_RxOverflowCount();
  out_pc.canHW_Rx0_OverflowCount = dcan.getHW_Rx0_OverflowCount();
  out_pc.canHW_Rx1_OverflowCount = dcan.getHW_Rx1_OverflowCount();
  out_pc.status0.bits.needsBurn = dcan.needsBurn();
  out_pc.status0.bits.flashDataLost = dcan.flashDataLost();
  out_pc.status0.bits.currFlashTable = dcan.currFlashTable();
  
  // update statistics registers
  EndianUtils::setBE(out_pc.loopTimeUs, (uint16_t)(micros() - loop_start_time_us));
  EndianUtils::setBE(out_pc.motorControlTimeUs, (uint16_t&)motor_control_time_us);
  EndianUtils::setBE(out_pc.gStopwatch0, (uint16_t&)gStopwatch0.span);
  EndianUtils::setBE(out_pc.gStopwatch1, (uint16_t&)gStopwatch1.span);
}

// -----------------------------------------------------------
// public functions
// -----------------------------------------------------------

void
loadAllPageDefnsFromFlash()
{
  for (uint8_t page_idx=0; page_idx<NUM_GAUGE_PAGE_DEFNS; page_idx++)
  {
    updatePageDefnFromFlash(page_idx);
  }
}

int8_t
updatePageDefnFromFlash(
  uint8_t page_idx)
{
  int8_t ret = 0;
  PageDefn_t page;
  size_t from_offset = PAGE3_FIELD_OFFSET(pageDefns) + sizeof(PageDefn_t) * page_idx;
  for (uint8_t i=0; i<sizeof(PageDefn_t); i++)
  {
    ((uint8_t*)&page)[i] = EEPROM.read(from_offset+i);
  }
  switch (page.type)
  {
    case PageType::PageType_hsplit:
      ret = display.setPageHSplit(page_idx,page.data.hsplit);
      break;
    case PageType::PageType_monitor:
      ret = display.setPageMonitor(page_idx,page.data.mon.var_id);
      break;
    case PageType::PageType_null:
      WARN("unsupported page setting to 'null'");
      break;
    default:
      // there can be invalid pages defined in flash if things aren't setup
      // yet. the display driver will default all definutions to 'null' pages
      // internally so it's okay to do nothing here until all the pages are
      // setup by the user.
      break;
  }
  
  return ret;
}

void
enc_scroll_evt(
  bool clockwise)
{
  if (display.menuVisible())
  {
    time_last_ui_input_s = millis() / 1000;
    
    // handle menu scrolling
    if (clockwise)
    {
      display.nextMenuItem();
    }
    else
    {
      display.prevMenuItem();
    }
  }
  else
  {
    if (clockwise)
    {
      display.nextPage();
    }
    else
    {
      display.prevPage();
    }
  }
}

void
enc_on_press()
{
  DEBUG("PRESS");
}

void
enc_on_release()
{
  DEBUG("RELEASE");

  if (display.menuVisible())
  {
    // perform designated menu 'click' operation
    if (display.menuClick() < 0)
    {
      ERROR("menu click failed");
    }
  }
  else
  {
    // show the menu on button press
    display.menuVisible(true);
  }
}

void
enc_on_hold()
{
  DEBUG("HELD");

  // hide the menu on button hold
  if (display.menuVisible())
  {
    display.menuVisible(false);
  }
}

void
check_for_ui_hide()
{
  // hide the menu if no UI input is detected for a period of time
  if (display.menuVisible())
  {
    if ((uint8_t)(millis() / 1000 - time_last_ui_input_s) >= FLASH_TABLE_READ(Page3_T,menuHideTime_s))
    {
      display.menuVisible(false);
    }
  }
}

// -----------------------------------------------------------
// Interrupt routines
// -----------------------------------------------------------

// encoder pushbutton ISR
void enc_sw_isr()
{
  enc_sw_drv.on_pin_change();
  time_last_ui_input_s = millis() / 1000;
}

// external interrupt service routine (INT1 for CAN interface)
void can_isr()
{
  dcan.interrupt();
}
