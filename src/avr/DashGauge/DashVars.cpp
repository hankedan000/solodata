#include "DashVars.h"
#include "DashCAN.h"

uint16_t
get_uint16(
  const void *v)
{
  return *((const uint16_t *)v);
}

VarDefn_t var_speed      = {VAR_TITLE_SPEED,     get_uint16,             &eng_data.speed_mph,  Unit::mph   , NO_MONITOR};
VarDefn_t var_battery    = {VAR_TITLE_BATTERY,   MSG03_t::get_batt,      &eng_data.msg03,      Unit::volts , NO_MONITOR};
VarDefn_t var_rpm        = {VAR_TITLE_RPM,       MSG00_t::get_rpm,       &eng_data.msg00,      Unit::rpm   , NO_MONITOR};
VarDefn_t var_map        = {VAR_TITLE_MAP,       MSG02_t::get_map,       &eng_data.msg02,      Unit::kpa   , NO_MONITOR};
VarDefn_t var_mat        = {VAR_TITLE_MAT,       MSG02_t::get_mat,       &eng_data.msg02,      Unit::degF  , NO_MONITOR};
VarDefn_t var_clt        = {VAR_TITLE_CLT,       MSG02_t::get_clt,       &eng_data.msg02,      Unit::degF  , NO_MONITOR};
