#ifndef BUTTON_DRIVER_H_
#define BUTTON_DRIVER_H_

#include <Arduino.h>

#define ACTIVE_HIGH 1
#define ACTIVE_LOW 0

#define NO_HOLD 0

#include <util/atomic.h>
#define BD_ATOMIC_START ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
#define BD_ATOMIC_END }

enum ButtonState
{
  eStateUnknown,
  eStatePressed,
  eStateReleased,
  eStateHeld
};

struct ButtonDriver
{
  ButtonDriver(
    uint8_t pin,
    uint8_t mode,
    void (*on_press)() = nullptr,
    void (*on_release)() = nullptr,
    uint16_t hold_dur_ms = NO_HOLD,
    void (*on_hold)() = nullptr)
   : pin(pin)
   , mode(mode)
   , prev_state(ButtonState::eStateUnknown)
   , press_time_ms(0)
   , on_press(on_press)
   , on_release(on_release)
   , hold_dur_ms(hold_dur_ms)
   , on_hold(on_hold)
  {
  }

  void
  on_pin_change()
  {
    uint8_t active = (digitalRead(pin) == mode);

    switch (prev_state)
    {
      case ButtonState::eStateUnknown:
      case ButtonState::eStateReleased:
        if (active)
        {
          press_time_ms = millis();
          if (on_press != nullptr)
          {
            (*on_press)();
          }
        }
        break;
      case ButtonState::eStatePressed:
        if ( ! active)
        {
          if (on_release != nullptr)
          {
            (*on_release)();
          }
        }
        break;
      case ButtonState::eStateHeld:
        // do nothing
        break;
    }

    prev_state = (active ? ButtonState::eStatePressed : ButtonState::eStateReleased);
  }

  void
  check_for_hold()
  {
    if (hold_dur_ms == NO_HOLD)
    {
      return;
    }

    BD_ATOMIC_START
    if (prev_state == ButtonState::eStatePressed)
    {
      uint8_t active = (digitalRead(pin) == mode);

      if (active)
      {
        if ((uint16_t)(millis() - press_time_ms) >= hold_dur_ms)
        {
          if (on_hold != nullptr)
          {
            (*on_hold)();
          }
          prev_state = ButtonState::eStateHeld;
        }
      }
    }
    BD_ATOMIC_END
  }

  const uint8_t pin;
  const uint8_t mode;
  volatile ButtonState prev_state;
  
  // millis() time when button was pressed
  volatile unsigned int press_time_ms;
  
  const void (*on_press)();
  const void (*on_release)();

  const uint16_t hold_dur_ms;
  const void (*on_hold)();
};

#endif
