#include "DashMotors.h"

#include "DashCAN.h"
#include "DashVars.h"
#include "EndianUtils.h"
#include "FlashUtils.h"
#include <SwitecX25.h>
#include "tables.h"

// standard X25.168 range 315 degrees at 1/3 degree steps
#define STEPS (315*3)

// we setup a timer to periodically update the stepper gauges
#define TMR2_CLK_DIV 1024 // IO_clk / 1024 (WARN! not used to setup registers must change those separately!!!)
#define TMR2_CLK_FREQ (F_CPU/TMR2_CLK_DIV)
#define TMR2_CLK_PERIOD_NS (1000000000ULL/TMR2_CLK_FREQ)

// tach is on digital pins 34,33,32 (M1,M2&M3,M4)
SwitecX25 tachMotor(STEPS,&DDRC,&PORTC,3);
// speedometer is on digital pins 49,48,47 (M1,M2&M3,M4)
SwitecX25 speedoMotor(STEPS,&DDRL,&PORTL,0);
// coolant temp is on digital pins 37,36,35 (M1,M2&M3,M4)
SwitecX25 cltMotor(STEPS,&DDRC,&PORTC,0);

// duration in microseconds to control all motors
volatile uint16_t motor_control_time_us;

void
gaugeSetup()
{
  // run the motor against the stops
  homeAllGauges();

  TCCR2A = 0x02;// no pin output, clear counter on compare
  TCCR2B = 0x07;// 1/1024 prescaler
  OCR2A = 1000000/TMR2_CLK_PERIOD_NS;// ~1ms update rate
  TCNT2 = 0;// reset counter
  TIMSK2 = 0x2;// enable OC2A interrupt
}

void
homeAllGauges()
{
  tachMotor.zeroNoBlock();
  speedoMotor.zeroNoBlock();
  cltMotor.zeroNoBlock();
}

uint16_t
overrideGetter(
  const void *ovrPtr)
{
  return EndianUtils::getBE(*((const uint16_t *)ovrPtr));
}

void
updateSingleGauge(
  SwitecX25 *motor,
  uint16_t normalValueGetter(const void *),
  const void *normalValueArg,
  uint16_t overrideValueGetter(const void *),
  const void *overrideValueArg,
  const size_t &xBinsFlashOffset,
  const size_t &yBinsFlashOffset,
  const size_t &nFlashBins)
{
  uint16_t nextPos;
  // update tach position
  if (page2.gaugeTestCtrl.bits.mode == TEST_MODE_STEP_OVERRIDE)
  {
    // source step value from overrid data
    nextPos = overrideValueGetter(overrideValueArg);
  }
  else
  {
    // source next position from overridenormal value
    nextPos = FlashUtils::lerpS16(
      xBinsFlashOffset,
      yBinsFlashOffset,
      nFlashBins,
      (page2.gaugeTestCtrl.bits.mode == TEST_MODE_VALUE_OVERRIDE ?
       overrideValueGetter(overrideValueArg) :
       normalValueGetter(normalValueArg)));
  }
  motor->setPosition(nextPos);
  motor->update();
}

uint16_t
motorControlTime()
{
  return motor_control_time_us;
}

uint32_t motor_isr_start_time_us;
ISR(TIMER2_COMPA_vect)
{
  motor_isr_start_time_us = micros();
  
  // update tach
  updateSingleGauge(
    &tachMotor,
    MSG00_t::get_rpm,
    &eng_data.msg00,
    overrideGetter,
    &page2.testValueTach,
    PAGE1_FIELD_OFFSET(tachMappingCurve_xBins),
    PAGE1_FIELD_OFFSET(tachMappingCurve_yBins),
    TACH_MAPPING_CURVE_N_BINS);
  
  // update speedometer
  updateSingleGauge(
    &speedoMotor,
    get_uint16,
    &eng_data.speed_mph,
    overrideGetter,
    &page2.testValueSpeedo,
    PAGE1_FIELD_OFFSET(speedoMappingCurve_xBins),
    PAGE1_FIELD_OFFSET(speedoMappingCurve_yBins),
    SPEEDO_MAPPING_CURVE_N_BINS);
  
  // update coolant temp
  updateSingleGauge(
    &cltMotor,
    MSG02_t::get_clt,
    &eng_data.msg02,
    overrideGetter,
    &page2.testValueCoolantTemp,
    PAGE1_FIELD_OFFSET(cltMappingCurve_xBins),
    PAGE1_FIELD_OFFSET(cltMappingCurve_yBins),
    CLT_MAPPING_CURVE_N_BINS);

  motor_control_time_us = micros() - motor_isr_start_time_us;
}
