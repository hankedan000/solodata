#ifndef LOGGING_H_
#define LOGGING_H_

#include <stdio.h>
#include <avr/pgmspace.h>

#define DEBUG_LEVEL 10
#define INFO_LEVEL  20
#define WARN_LEVEL  30
#define ERROR_LEVEL 40

#define LOG_THRESHOLD INFO_LEVEL
#define USE_FLASH_STRINGS

#ifdef USE_FLASH_STRINGS
   #define PRINT_LOG(level_str,log_msg,...)\
      printf_P(PSTR(level_str " | " log_msg "\n"),##__VA_ARGS__)
#else
   #define PRINT_LOG(level_str,log_msg,...)\
      printf(level_str " - " log_msg "\n",##__VA_ARGS__)
#endif

// the "##" will remove comma when there are no variable arguments provided
#if LOG_THRESHOLD <= DEBUG_LEVEL
   #define DEBUG(log_msg,...) PRINT_LOG("DEBUG",log_msg,##__VA_ARGS__)
#else
   #define DEBUG(log_msg,...) {}
#endif

#if LOG_THRESHOLD <= INFO_LEVEL
   #define INFO(log_msg,...) PRINT_LOG("INFO ",log_msg,##__VA_ARGS__)
#else
   #define INFO(log_msg,...) {}
#endif

#if LOG_THRESHOLD <= WARN_LEVEL
   #define WARN(log_msg,...) PRINT_LOG("WARN ",log_msg,##__VA_ARGS__)
#else
   #define WARN(log_msg,...) {}
#endif

#if LOG_THRESHOLD <= ERROR_LEVEL
   #define ERROR(log_msg,...) PRINT_LOG("ERROR",log_msg,##__VA_ARGS__)
#else
   #define ERROR(log_msg,...) {}
#endif

#endif
