/*
 * demo.c
 *
 *  Created on: Jan 13, 2019
 *      Author: daniel
 */
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <avr/delay.h>
#include "../libcanard/avr_driver/avr-can-lib/can.h"
#include "../uart/uart.h"
#include <stdio.h>

#include "nmea_man.h"
#include "../softuart/softuart.h"
#include "../softuart/logging.h"

#define UART_BAUD 115200

const uint8_t can_filter[] PROGMEM =
{
   // Group 0
   MCP2515_FILTER(0),            // Filter 0
   MCP2515_FILTER(0),            // Filter 1

   // Group 1
   MCP2515_FILTER_EXTENDED(0),      // Filter 2
   MCP2515_FILTER_EXTENDED(0),      // Filter 3
   MCP2515_FILTER_EXTENDED(0),      // Filter 4
   MCP2515_FILTER_EXTENDED(0),      // Filter 5

   MCP2515_FILTER(0),            // Mask 0 (for group 0)
   MCP2515_FILTER_EXTENDED(0),      // Mask 1 (for group 1)
};
// You can receive 11 bit identifiers with either group 0 or 1.

// -----------------------------------------------------------------------------
// Main loop for receiving and sending messages.

static FILE uart_stream = FDEV_SETUP_STREAM(uart0_putc, NULL, _FDEV_SETUP_WRITE);

bool setup()
{
   bool okay = true;

   /**
    * I'm using software UART for a debug console
    * TX -> PORTD7 (Arduino digital pin 7)
    * RX -> PORTD6 (Arduino digital pin 6)
    */
   softuart_turn_rx_on();
   softuart_init();
   sei();

   uart0_init(UART_BAUD_SELECT(UART_BAUD,F_CPU));

   stdout = &uart_stream; //Required for printf init

   for(int i=0;i<5;i++) printf_P(PSTR("\n\r"));
   printf_P(
         PSTR("======= INS MODULE =======\n"
              "Compile date: %s\n"
              "Compile time: %s\n"
              "==========================\n"),
         __DATE__,
         __TIME__);
   INFO("Initializing INS...");

   nm_init(softuart_getchar,softuart_putchar,softuart_kbhit);

   // Initialize MCP2515
   if (can_init(BITRATE_125_KBPS))
   {
      // Load filters and masks
      can_static_filter(can_filter);
      INFO("CAN OK!");
   }
   else
   {
      ERROR("can_init failed!");
      okay = false;
   }

   return okay;
}

void
sendNavSample(
      const struct NavSample *samp);

int main(void)
{
   setup();

   uint32_t cnt = 0;
   while (1)
   {
      nm_exec();
      if (cnt++ > 10000)
      {
         sendNavSample(getLatestNav());
//         INFO("tov = %ld",getLatestNav()->utc.tv_sec);
//         INFO("lat = %ld",(long)(getLatestNav()->lat*1000000));
//         INFO("lon = %ld",(long)(getLatestNav()->lon*1000000));
         cnt = 0;
      }
   }

   // Create a test message
   can_t msg;

   msg.id = 0x123456;
   msg.flags.rtr = 0;
   msg.flags.extended = 1;

   msg.length = 4;
   msg.data[0] = 0xde;
   msg.data[1] = 0xad;
   msg.data[2] = 0xbe;
   msg.data[3] = 0xef;

   while (1)
   {
      DEBUG("Looping!");

      // Send the message
      uint8_t stat = can_send_message(&msg);
      msg.data[0]++;
      INFO("stat = 0x%x",stat);

      _delay_ms(1000);

      // Check if a new messag was received
      if (can_check_message())
      {
         can_t msg;

         // Try to read the message
         if (can_get_message(&msg))
         {
            // If we received a message resend it with a different id
            msg.id += 10;

            // Send the new message
            can_send_message(&msg);
         }
      }
   }

   return 0;
}

void
sendNavSample(
      const struct NavSample *samp)
{
   size_t sampSize = sizeof(struct NavSample);
   const uint8_t *ptr = (const uint8_t *)samp;

   uart0_putc('$');
   while (sampSize>0)
   {
      uart0_putc(*ptr);
      ptr++;
      sampSize--;
   }
   uart0_putc(';');
   uart0_putc('\n');
}
