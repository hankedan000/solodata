#ifndef NMEA_MAN_H_
#define NMEA_MAN_H_

#include "../minmea/minmea.h"
#include "../softuart/logging.h"
#include "ublox_msg.h"

#include <avr/pgmspace.h>
#include <stdarg.h>

struct minmea_sentence_rmc rmc;
struct minmea_sentence_zda zda;

// pointer to a get char method
char(*nm_getc)(void);
void(*nm_putc)(const char);
uint8_t(*nm_numc)(void);

// TODO max command size?!?!
char nm_cmd_buff[80];

struct nm_sen_buff
{
   #define MAX_SENTENCE_BYTES 83
   char a[MAX_SENTENCE_BYTES];
   uint8_t offset;
   bool inSentence;
} buff;

struct NavSample
{
   struct timespec utc;
   float lat;// decimal degrees
   float lon;// decimal degrees
//   uint16_t heading;
//   uint16_t speed;
//   uint16_t altitude;
//   uint16_t ax;
//   uint16_t ay;
//   uint16_t az;
//   uint16_t mx;
//   uint16_t my;
//   uint16_t mz;
};
#define NAV_HISTORY_DEPTH 10
int8_t validIdx = -1;
uint8_t currIdx = 0;
struct NavSample navSamps[NAV_HISTORY_DEPTH];

const struct NavSample *
getLatestNav()
{
   if (validIdx < 0)
   {
      return navSamps;
   }
   else
   {
      return navSamps + validIdx;
   }
}

void
__nm_storeSentence(
      const char *sen)
{
   bool valid = true;
   enum minmea_sentence_id id = minmea_sentence_id(sen,true);

   switch (id)
   {
   case MINMEA_SENTENCE_RMC:
   {
      if (minmea_parse_rmc(&rmc,sen))
      {
         navSamps[currIdx].lat = minmea_tocoord(&rmc.latitude);
         navSamps[currIdx].lon = minmea_tocoord(&rmc.longitude);
         struct timespec *ts = &navSamps[currIdx].utc;
         if (minmea_gettime(ts,&rmc.date,&rmc.time) != 0)
         {
            ERROR("Failed to gettime!");
            valid = false;
         }
      }
      else
      {
         ERROR("Failed to parse RMC string!");
         valid = false;
      }
      break;
   }
   case MINMEA_SENTENCE_ZDA:
   {
      INFO("It's an ZDA sentence!");
      if (minmea_parse_zda(&zda,sen))
      {
         INFO("year = %d\nmonth = %d\nday = %d",zda.date.year,zda.date.month,zda.date.day);
         struct timespec ts;
         if (minmea_gettime(&ts,&zda.date,&zda.time) != 0)
         {
            ERROR("Failed to gettime!");
            valid = false;
         }
      }
      else
      {
         ERROR("Failed to parse ZDA string!");
         valid = false;
      }
      break;
   }
   default:
      ERROR("Unsupported NMEA sentence\n'%s'",sen);
      valid = false;
      break;
   }

   if (valid)
   {
      validIdx = currIdx;
      currIdx++;
      if (currIdx >= NAV_HISTORY_DEPTH)
      {
         currIdx = 0;
      }
   }
}

void
__nm_putS(
      const char* s)
{
   if (!nm_putc)
   {
      ERROR("putc null");
      return;
   }

   while (*s != '\0')
   {
      nm_putc(*s);
      s++;
   }
}

void
__nm_pubCmd(
      const char *cmd)
{
   if (!nm_putc)
   {
      ERROR("putc null!");
      return;
   }
   DEBUG("cmd = '%s'",cmd);

   // begin command transmission
   nm_putc('$');
   uint8_t ck = 0;
   while (*cmd != '\0')
   {
      nm_putc(*cmd);
      ck ^= *cmd;
      cmd++;
   }

   // generate and send checksum
   char ckStr[3];
   sprintf_P(ckStr,PSTR("%02X"),ck);
   DEBUG("ck = %02X",ck);
   nm_putc('*');
   nm_putc(ckStr[0]);
   nm_putc(ckStr[1]);
   nm_putc('\r');
   nm_putc('\n');
}

/**
 * Sends a command to a ublox7 compatible device
 *
 * @param[in] cmd
 * null terminated string representing the character sequence between $ and *
 *
 * Example:
 * To send the command "$PUBX,40,GLL,1,0,0,0,0,0*5D", cmd would consist of the
 * null terminated string "PUBX,40,GLL,1,0,0,0,0,0"
 */
void
__nm_sendCmd(
      const char *cmdFmt,...)
{
   va_list args;
   va_start(args, cmdFmt);
   // finalize the command format
   vsprintf(nm_cmd_buff,cmdFmt,args);

   // send it!
   __nm_pubCmd(nm_cmd_buff);
}

void
__nm_sendCmd_P(
      const char *cmdFmt_P,...)
{
   va_list args;
   va_start(args,cmdFmt_P);
   // finalize the command format
   vsprintf_P(nm_cmd_buff,cmdFmt_P,args);

   // send it!
   __nm_pubCmd(nm_cmd_buff);
}

void
__nm_pubUbloxCmd(
      const struct UbloxClassAndID ubCID,
      uint16_t len,
      const uint8_t *payload)
{
   if (!nm_putc)
   {
      ERROR("putc null!");
      return;
   }
   DEBUG("\n"
         "msgClass = %d; \n"
         "msgId = %d; \n"
         "len = %d",
         ubCID.c,
         ubCID.i,
         len);

   uint8_t *len8 = (uint8_t*)&len;
   uint8_t ck_a = 0,ck_b = 0;

   // begin command transmission
   nm_putc(0xb5);
   nm_putc(0x62);

   // send msg class
   nm_putc(ubCID.c);
   ck_a += ubCID.c; ck_b += ck_a;

   // send msg ID
   nm_putc(ubCID.i);
   ck_a += ubCID.i; ck_b += ck_a;

   // send payload length (little endian)
   nm_putc(*(len8));
   ck_a += *(len8); ck_b += ck_a;
   nm_putc(*(len8+1));
   ck_a += *(len8+1); ck_b += ck_a;

   while (len > 0)
   {
      nm_putc(*payload);
      ck_a += *payload; ck_b += ck_a;
      payload++;
      len--;
   }

   // send checksum
   nm_putc(ck_a);
   nm_putc(ck_b);
}

void
setSentenceEnable(
      const char *senType,
      bool enabled)
{
   __nm_sendCmd_P(
         PSTR("PUBX,40,%s,0,%c,0,0,0,0"),
         senType,
         (enabled ? '1' : '0'));
}

void
nm_init(
      char(*getc)(void),
      void(*putc)(const char),
      uint8_t(*numc)(void))
{
   nm_getc = getc;
   nm_putc = putc;
   nm_numc = numc;

   buff.offset = 0;
   buff.inSentence = false;

   // default to 1Hz data rate
   struct Ublox_CFG_RATE cfgRate;
   cfgRate.measRate = 1000;// 1000ms measurement period
   cfgRate.navRate = 1;// can't change, must be 1
   cfgRate.timeRef = 0;// UTC time reference
   __nm_pubUbloxCmd(UBLOX_CFG_RATE,sizeof(cfgRate),(uint8_t*)&cfgRate);

   setSentenceEnable("GSV",false);
   setSentenceEnable("GLL",false);
   setSentenceEnable("VTG",false);
   setSentenceEnable("GGA",false);
   setSentenceEnable("GSA",false);
}

void
nm_exec()
{
   if (!nm_getc)
   {
      ERROR("getc null");
      return;
   }

   if (!nm_numc)
   {
      ERROR("numc null");
      return;
   }

   while (nm_numc() > 0)
   {
      buff.a[buff.offset] = nm_getc();

      if (buff.a[buff.offset] == '$')
      {
         buff.inSentence = true;
      }
      else if (buff.a[buff.offset] == '\n')
      {
         buff.a[buff.offset+1] = '\0';
         __nm_storeSentence(buff.a);
         buff.inSentence = false;
         buff.offset = 0;
      }

      if (buff.inSentence)
      {
         // minus 1 is so we can null terminate
         if (buff.offset < (MAX_SENTENCE_BYTES-1))
         {
            buff.offset++;
         }
         else
         {
            ERROR("Overflowed sentence buffer!");
         }
      }
   }
}

#endif
