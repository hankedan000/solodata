#ifndef UBLOX_MSG_H_
#define UBLOX_MSG_H_

#include <stdint.h>

struct UbloxClassAndID
{
   uint8_t c,i;
};

#define UBLOX_CFG_RATE (struct UbloxClassAndID){.c = 0x06, .i = 0x08}
struct Ublox_CFG_RATE
{
   /*
   2 U2 -
   4 U2 - timeRef - Alignment to re
   */
   /**
    * Measurement Rate:
    * GPS measurements are taken every measRate milliseconds
    * Units: ms
    */
   uint16_t measRate;

   /**
    * Navigation Rate:
    * Number of measurement cycles. This parameter cannot be changed, and
      must be set to 1.
    * Units: cyles
    */
   uint16_t navRate;

   /**
    * Alignment to reference time
    * 0 = UTC time
    * 1 = GPS time
    */
   uint16_t timeRef;
};

#endif
