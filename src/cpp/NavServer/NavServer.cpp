#include <fstream>
#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string.h>
#include <unistd.h>
#include <vector>
#include <ZMQ_Boilerplate.h>
#include <Reactor.h>

#include "gps_utils.h"
#include "imud_config.h"
#include "navserver_config.h"

// include protobuf header files
#include "IMU_Sample.pb.h"
#include "NavSample.pb.h"

// include ZeroMQ headers
#include <zmq.h>

// include headers for working with gpsd
#include <gps.h>
#include <nlohmann/json.hpp>

// for LTTng tracing
#define TRACEPOINT_DEFINE
#include "NavServerTracepoints.h"

using json = nlohmann::json;

#define GPSD_COMPAT_PROTO_MAJOR_VER 3
#define GPSD_COMPAT_PROTO_MINOR_VER 12

#define NS_IN_SEC       1000000000LL     /* nanoseconds in a second */
#define MS_IN_NS        1000000LL        /* milliseconds in a nano second */
#define US_IN_SEC       1000000LL        /* microseconds in a second */
#define MS_IN_SEC       1000LL           /* milliseconds in a second */

/* stole this from gpsd timespec.h
 * return the difference between timespecs in nanoseconds
 * int may be too small, 32 bit long is too small, floats are too imprecise,
 * doubles are not quite precise enough 
 * MUST be at least int64_t to maintain precision on 32 bit code */
#define timespec_diff_ns(x, y) \
	(int64_t)((((x).tv_sec-(y).tv_sec)*NS_IN_SEC)+(x).tv_nsec-(y).tv_nsec)

SET_LOGGING_CFG_NAME("solodata_logging.cfg");
ZMQ_Boilerplate boilerplate("NavServer");
Reactor reactor;

// global NavServer configuration
solodata::navserver_config_t ns_config;

bool stay_alive = true;
bool saw_first_sample = false;
// buffers used to serial/deserialize data to/from sockets
std::vector<uint8_t> navsamp_buffer;

// gpsd connection management
bool gps_opened = false;
struct gps_data_t gpsdata;
// current measurement rate of the GPS
// Note: can change at runtime
double curr_gps_rate_hz = 0.0;
/**
 * Last received value of 'gps_data.fix.mode' from the gpsd service
 * 
 * MODE_NOT_SEEN   0       // mode update not seen yet
 * MODE_NO_FIX     1       // none
 * MODE_2D         2       // good for latitude/longitude
 * MODE_3D         3       // good for altitude/climb too
 */
unsigned int prev_fix_mode = MODE_NOT_SEEN;
// the current GPS device we're listening to
struct devconfig_t gps_dev;

// zmq sockets
void *imu_sub = nullptr;
void *nav_pub = nullptr;

void
resize_navsamp_buffer(
	size_t required_bytes)
{
	size_t curr_size = navsamp_buffer.size();
	size_t next_size = required_bytes * 2;
	LOG4CXX_WARN(boilerplate.getLogger(),
		"navsamp_buffer resized"
		" from " << curr_size << " bytes"
		" to "   << next_size << " bytes.");
	navsamp_buffer.resize(next_size);
}

std::string
fixModeToString(
	unsigned int fixMode)
{
	switch (fixMode)
	{
		case MODE_NOT_SEEN:
			return "NOT_SEEN";
		case MODE_NO_FIX:
			return "NO_FIX";
		case MODE_2D:
			return "2D";
		case MODE_3D:
			return "3D";
		default:
			return std::string("**INVALID(") + std::to_string(fixMode) + ")**";
	}
}

solodata::GPS_Info_FixMode_E
gpsdFixModeToProto(
	unsigned int fixMode)
{
	switch (fixMode)
	{
		case MODE_NOT_SEEN:
			return solodata::GPS_Info_FixMode_E_FM_NOT_SEEN;
		case MODE_NO_FIX:
			return solodata::GPS_Info_FixMode_E_FM_NO_FIX;
		case MODE_2D:
			return solodata::GPS_Info_FixMode_E_FM_2D;
		case MODE_3D:
			return solodata::GPS_Info_FixMode_E_FM_3D;
		default:
			return solodata::GPS_Info_FixMode_E_FM_INVALID;
	}
}

/**
 * A wrapper around libgps gps_read() function that will catch and
 * log errors.
 * 
 * @param[inout] gpsdata
 * the gps_data_t to make the libgps calls with
 * 
 * @param[in] log_gpsdata
 * optionally log the gps_data_t structure at INFO level after read.
 * 
 * @param[inout] raw_data
 * optionally read the raw gps data into this buffer. if null, then
 * gps buffer copy is skipped.
 * 
 * @param[in] buff_size
 * size in bytes of the provided raw_data buffer. if 0, then gps
 * buffer copy is skipped.
 * 
 * @return
 * true if read was successful, false if read failed, or if the
 * raw_data buffer size was insufficiently sized to copy the raw
 * GPS data.
 */
bool
wrapped_gps_read(
	struct gps_data_t *gpsdata,
	bool log_gpsdata = false,
	char *raw_data = nullptr,
	size_t buff_size = 0)
{
	bool okay = true;
	int ret;
#if GPSD_API_MAJOR_VERSION >= 7
	ret = gps_read(gpsdata,raw_data,buff_size);
#else
	ret = gps_read(gpsdata);
#endif

	if (ret < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to read GPS data!"
			" received errno " << errno << " '" << gps_errstr(errno) << "'");
		okay = false;
	}
	else if (raw_data != nullptr && buff_size > 0)
	{
#if GPSD_API_MAJOR_VERSION >= 7
		// libgps does copy for us
#else
		strncpy(raw_data,gps_data(gpsdata),buff_size);
#endif
		
		if (ret > buff_size)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				__func__ << " - "
				"raw_data buffer is insufficiently sized. " << ret << " byte(s)"
				" available to read, but raw_data is only " << buff_size << " bytes(s)");
			okay = false;
		}
		else
		{
			LOG4CXX_DEBUG(boilerplate.getLogger(),
				__func__ << " - "
				"read " << ret << " byte(s). raw_data = " << raw_data);
		}
	}

	if (okay)
	{
		log4cxx::LevelPtr level = (
			log_gpsdata ?
			log4cxx::Level::getInfo() :
			log4cxx::Level::getDebug());
		LOG4CXX_LOG(boilerplate.getLogger(),level,
			__func__ << " - "
			"gpsdata = " << gps_utils::gpsdata_to_string(*gpsdata));
	}

	return okay;
}

/**
 * A wrapper around libgps gps_waiting() and gps_read() functions
 * that will catch and log errors.
 * 
 * @param[inout] gpsdata
 * the gps_data_t to make the libgps calls with
 * 
 * @param[in] timeout_us
 * the wait time out duration in microseconds
 * 
 * @param[in] log_gpsdata
 * optionally log the gps_data_t structure at INFO level after read.
 * 
 * @param[inout] raw_data
 * optionally read the raw gps data into this buffer. if null, then
 * gps buffer copy is skipped.
 * 
 * @param[in] buff_size
 * size in bytes of the provided raw_data buffer. if 0, then gps
 * buffer copy is skipped.
 * 
 * @return
 * true if read was successful, false if wait timed out or other
 * failure occurred.
 */
bool
gps_wait_and_read(
	struct gps_data_t *gpsdata,
	unsigned int timeout_us = 10000,
	bool log_gpsdata = false,
	char *raw_data = nullptr,
	size_t buff_size = 0,
	bool noisy_timeout = true)
{
	bool okay = true;

	if (okay && ! gps_waiting(gpsdata,timeout_us))
	{
		LOG4CXX_LOG(boilerplate.getLogger(),
			(noisy_timeout ? log4cxx::Level::getError() : log4cxx::Level::getDebug()),
			__func__ << " - "
			"timed out after " << timeout_us << "us while waiting for"
			" GPS response."
			" received error '" << gps_errstr(errno) << "'");
		okay = false;
	}
	
	okay = okay && wrapped_gps_read(gpsdata,log_gpsdata,raw_data,buff_size);

	return okay;
}

bool
check_gpsd_proto_compat(
	const json &gpsd_ver_msg)
{
	bool compat = true;

	unsigned int major_ver = gpsd_ver_msg["proto_major"];
	unsigned int minor_ver = gpsd_ver_msg["proto_minor"];
	if (major_ver != GPSD_COMPAT_PROTO_MAJOR_VER)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"GPSD major protocol version compatible! "
			"server major_ver = " << GPSD_COMPAT_PROTO_MAJOR_VER << "; "
			"daemon major_ver = " << major_ver);
		compat = false;
	}

	if (minor_ver != GPSD_COMPAT_PROTO_MINOR_VER)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"GPSD minor protocol version compatible! "
			"server minor_ver = " << GPSD_COMPAT_PROTO_MINOR_VER << "; "
			"daemon minor_ver = " << minor_ver);
		compat = false;
	}

	return compat;
}

void
send_sample(
	void *publisher,
	const solodata::NavSample &datum)
{
	// serialize datum into buffer
	size_t samp_size = datum.ByteSizeLong();
	if (samp_size > navsamp_buffer.size())
	{
		resize_navsamp_buffer(samp_size);
	}
	datum.SerializeToArray((void*)navsamp_buffer.data(),samp_size);

	int rc = zmq_send(
		publisher,
		(const void*)navsamp_buffer.data(),
		samp_size,
		ZMQ_DONTWAIT);
	if (rc < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"zmq_send failed for message '" << datum.DebugString() << "'");
		return;
	}
}

void
build_sample(
	const solodata::IMU_Sample &imu_samp,
	void *publisher)
{
	LOG4CXX_TRACE(boilerplate.getLogger(),imu_samp.DebugString());

	if ( ! saw_first_sample)
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"First IMU sample received! Nav data is streaming.");
		saw_first_sample = true;
	}

	// make a NavSample and set timestamp to processor time
	static uint32_t sequence_number = 0;
	solodata::NavSample ns;
	ns.set_utc_us(imu_samp.timestamp());
	ns.set_sequence_num(sequence_number++);

	// add the IMU sample
	solodata::IMU_Sample *imu_samp_ptr = ns.add_imu_samp();
	*imu_samp_ptr = imu_samp;

	// add gps sample data if available/valid
	static timespec_t prev_tpv_time = {0,0};
	static bool prev_tpv_time_set = false;
	bool handle_gps = true;
	handle_gps = handle_gps && gpsdata.set & TIME_SET;
	handle_gps = handle_gps && gpsdata.set & MODE_SET;

	// add a check to see if the GPS measurement rate changes
	static bool rate_changed_once = false;
	if (gpsdata.set & DEVICELIST_SET)
	{
		unsigned int n_devs = gpsdata.devices.ndevices;
		for (unsigned int d=0; d<n_devs; d++)
		{
			const devconfig_t &dev = gpsdata.devices.list[d];

			// see if this is the GPS device we're listening to
			if (strcmp(dev.path,gps_dev.path) == 0)
			{
				double cycle = dev.cycle.tv_sec + dev.cycle.tv_nsec / 1000000000.0;
				double actual_rate_hz = 1 / cycle;
				if (abs(actual_rate_hz - curr_gps_rate_hz) > 0.001)
				{
					auto level = (rate_changed_once ? log4cxx::Level::getError() : log4cxx::Level::getWarn());
					LOG4CXX_LOG(boilerplate.getLogger(),level,
						"detected GPS rate change from " << curr_gps_rate_hz << "Hz"
						" to " << actual_rate_hz << "Hz. This can occur once at"
						" startup because gpsd has a cycle reporting bug in it.");
					// assume new rate
					curr_gps_rate_hz = actual_rate_hz;
					rate_changed_once = true;
				}
			}
		}
	}

	if (handle_gps)
	{
		if (prev_tpv_time_set)
		{
			// gpsd can send multiple TPV for the same epoch sometimes
			// this code is here to ignore the duplicate TPV
			int64_t diff_ns = timespec_diff_ns(gpsdata.fix.time,prev_tpv_time);
			int64_t ideal_period_ns = 1.0 / curr_gps_rate_hz * NS_IN_SEC;
			int64_t uncertainty_ns = diff_ns - ideal_period_ns;
			// expected time uncertainty in seconds; only valid if TIMERR_SET
			int64_t ept_ns = gpsdata.fix.ept * NS_IN_SEC;
			if (diff_ns == 0)
			{
				LOG4CXX_TRACE(boilerplate.getLogger(),
					"ignoring duplicate TPV message");
				handle_gps = false;
			}
			else if (gpsdata.set & TIMERR_SET && abs(uncertainty_ns) > ept_ns)
			{
				LOG4CXX_WARN(boilerplate.getLogger(),
					"detected " << diff_ns << "ns skip in TPV fixes."
					" ideal fix period = " << ideal_period_ns << "ns,"
					" actual fix period = " << diff_ns << "ns. Uncertainty is"
					" larger than expected " << ept_ns << "ns. GPS data may"
					" have been dropped!");
			}
		}
		prev_tpv_time = gpsdata.fix.time;
		prev_tpv_time_set = true;
	}

	// make sure we currently have at least a 2D or 3D fix
	bool fixed = false;
	if (handle_gps)
	{
		fixed = fixed || gpsdata.fix.mode == MODE_2D;// 2D fix
		fixed = fixed || gpsdata.fix.mode == MODE_3D;// 3D fix

		if (prev_fix_mode != gpsdata.fix.mode)
		{
			log4cxx::LevelPtr level = (
				prev_fix_mode < gpsdata.fix.mode ?
				log4cxx::Level::getInfo()        :
				log4cxx::Level::getWarn());
			LOG4CXX_LOG(boilerplate.getLogger(),level,
				"GPS fix mode transition: " <<
				fixModeToString(prev_fix_mode) << " -> " << fixModeToString(gpsdata.fix.mode));

			prev_fix_mode = gpsdata.fix.mode;
		}
	}

	if (handle_gps)
	{
		tracepoint(navserver_ust,gps_handle);

		auto gps_info_ptr = ns.mutable_gps();
		gps_info_ptr->set_fixed(fixed);
		gps_info_ptr->set_fix_mode(gpsdFixModeToProto(gpsdata.fix.mode));

		// Assign UTC in microseconds
		double utc_us = gpsdata.fix.time.tv_sec * US_IN_SEC +
			gpsdata.fix.time.tv_nsec / 1000ULL;
		gps_info_ptr->set_utc_us(utc_us);

		auto now = std::chrono::system_clock::now();
		auto now_epoch = now.time_since_epoch();
		auto now_epoch_us = std::chrono::duration_cast<std::chrono::microseconds>(now_epoch);
		long long delta_us = (long long)gps_info_ptr->utc_us() - now_epoch_us.count();
		LOG4CXX_DEBUG(boilerplate.getLogger(),
			"GPS time: " << (long long)gps_info_ptr->utc_us() << "; "
			"CPU time: " << now_epoch_us.count() << "; "
			"delta_us: " << delta_us << "; ");

		double gps_imu_delta_us = gps_info_ptr->utc_us() - imu_samp.timestamp();
		const double GPS_IMU_DELTA_LIMIT_US = 100000.0;// 100ms
		if (std::abs(gps_imu_delta_us) > GPS_IMU_DELTA_LIMIT_US)
		{
			LOG4CXX_WARN(boilerplate.getLogger(),
				"excessive skew between GPS & IMU data. is the system clock synced to GPS?"
				" gps_imu_delta_us = " << std::fixed << std::setprecision(6) << gps_imu_delta_us);
		}

		if (gpsdata.set & LATLON_SET)
		{
			gps_info_ptr->mutable_position()->set_lat_dd(gpsdata.fix.latitude);
			gps_info_ptr->mutable_position()->set_lon_dd(gpsdata.fix.longitude);
			gps_info_ptr->mutable_position()->set_lat_error(gpsdata.fix.epx);
			gps_info_ptr->mutable_position()->set_lon_error(gpsdata.fix.epy);
		}
		if (gpsdata.set & ALTITUDE_SET)
		{
			gps_info_ptr->set_altitude(gpsdata.fix.altHAE);
		}
		if (gpsdata.set & VERR_SET)
		{
			gps_info_ptr->set_altitude_err(gpsdata.fix.epv);
		}
		if (gpsdata.set & SPEED_SET)
		{
			gps_info_ptr->set_speed(gpsdata.fix.speed);
		}
		if (gpsdata.set & SPEEDERR_SET)
		{
			gps_info_ptr->set_speed_err(gpsdata.fix.eps);
		}
		if (gpsdata.set & TRACK_SET)
		{
			gps_info_ptr->set_true_course(gpsdata.fix.track);
		}

		LOG4CXX_DEBUG(boilerplate.getLogger(),
			"gps_info = " << gps_info_ptr->DebugString());
	}

	// FIXME add version check code back
	// if (msg_class == "VERSION")
	// {
	// 	if ( ! check_gpsd_proto_compat(gps_msg))
	// 	{
	// 		LOG4CXX_WARN(boilerplate.getLogger(),
	// 			"gpsd protocol incompatible!");
	// 		// TODO not sure what to do... shut down server?!?!
	// 	}
	// }

	send_sample(publisher,ns);
}

void
on_imu_sample(
	const solodata::IMU_Sample &datum)
{
	tracepoint(navserver_ust,on_imu_sample);

	// see if there is any GPS data available
	int ret = gps_waiting(&gpsdata,1);// 1us timeout
	if (ret > 0)
	{
		tracepoint(navserver_ust,gps_read);

		LOG4CXX_DEBUG(boilerplate.getLogger(),"GPS data available!");
		if ( ! wrapped_gps_read(&gpsdata,ns_config.log_gpsdata))
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"Failed to read GPS data!"
				" received errno " << errno << " '" << gps_errstr(errno) << "'");
		}
	}
	else
	{
		/**
		 * Since we poll the GPS when we get an IMU sample, we're expecting
		 * the gps_waiting() method to timeout in most cases.
		 */
	}

	build_sample(datum,nav_pub);
}

double
tstos(
	const timespec_t *ts)
{
	return (double)ts->tv_sec + (double)ts->tv_nsec / 1000000000.0;
}

bool
init_gps(
	const solodata::navserver_config_t::gpsd_t &cfg)
{
	bool okay = true;

	LOG4CXX_INFO(boilerplate.getLogger(),
		"gpsd API version " <<
		GPSD_API_MAJOR_VERSION << "." <<
		GPSD_API_MINOR_VERSION);

	// set libgps debug level
	if (cfg.debug_level > 0)
	{
		gps_enable_debug(cfg.debug_level,stderr);
	}

	// connect to gpsd
	int ret = gps_open(cfg.hostname.c_str(),cfg.port.c_str(),&gpsdata);
	if (ret == 0)
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"succesfully connected to gpsd"
			" at " << cfg.hostname << ":" << cfg.port);
		gps_opened = true;
	}
	else
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"failed to connected to gpsd"
			" at " << cfg.hostname << ":" << cfg.port <<
			". received error '" << gps_errstr(errno) << "'");
		okay = false;
	}

	// the device path of the GPS we discover
	bool found_gps = false;
	LOG4CXX_INFO(boilerplate.getLogger(),
		"waiting for GPS device...");
	while (okay && ! found_gps && stay_alive)
	{
		// Configure GPS data rate and baud
		if (okay && gps_send(&gpsdata,"?DEVICES;") != 0)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"failed to send device query command to gpsd."
				" received error '" << gps_errstr(errno) << "'");
			okay = false;
		}

		// keep requesting gpsd. silently timing out until GPS is found
		if (okay && ! gps_wait_and_read(&gpsdata,100000,false,nullptr,0,false))
		{
			continue;
		}

		LOG4CXX_DEBUG(boilerplate.getLogger(),
			gps_utils::gpsdata_to_string(gpsdata));

		if (okay && gpsdata.set & DEVICELIST_SET)
		{
			unsigned int n_dev = gpsdata.devices.ndevices;
			LOG4CXX_INFO(boilerplate.getLogger(),
				"there are " << n_dev << " device(s)");
			for (unsigned int d=0; d<n_dev; d++)
			{
				const devconfig_t *dev = gpsdata.devices.list + d;
				LOG4CXX_INFO(boilerplate.getLogger(),
					"driver: " << dev->driver << "; "
					"flag: " << dev->flags << "; "
					"path: " << dev->path << "; "
					"baudrate: \"" << dev->baudrate << "\"; "
					"cycle: " << tstos(&dev->cycle) << "s; "
					"mincycle: " << tstos(&dev->mincycle) << "s; ");

				if ( ! found_gps && dev->flags & SEEN_GPS)
				{
					found_gps = true;
					gps_dev = *dev;
				}
			}
		}

		if (okay && found_gps)
		{
			LOG4CXX_INFO(boilerplate.getLogger(),
				"found " << gps_dev.driver << " GPS device with "
				"path: \"" << gps_dev.path << "\"; "
				"baudrate: \"" << gps_dev.baudrate << "\"; "
				"cycle: \"" << gps_dev.cycle.tv_sec << "s " << gps_dev.cycle.tv_nsec << "ns\"; "
				"mincycle: \"" << gps_dev.mincycle.tv_sec << "s " << gps_dev.mincycle.tv_nsec << "ns\"; ");
		}
		else if (okay)
		{
			// GPS still not found... wait a little before retrying
			usleep(1000000);// 1s
		}
	}

	if (okay && found_gps)
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"setting baud to " << cfg.gps_baud << " and rate to " << cfg.gps_rate_hz << "Hz...");

		const char * COMMAND = "?DEVICE={\"class\":\"DEVICE\",\"path\":\"%s\",\"bps\":%d,\"cycle\":%0.3f}";
		curr_gps_rate_hz = cfg.gps_rate_hz;
		ret = gps_send(
			&gpsdata,
			COMMAND,
			gps_dev.path,
			cfg.gps_baud,
			1.0 / curr_gps_rate_hz);
		if (ret != 0)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"failed to configure GPS \"" << gps_dev.path << "\" with"
				" baud = " << cfg.gps_baud << "; "
				" rate = " << curr_gps_rate_hz << "Hz. "
				" received error '" << gps_errstr(errno) << "'");
			okay = false;
		}

		// it can take ~1s for the rate changes to take effect
		okay = okay && gps_wait_and_read(&gpsdata,2000000);

		if (okay && ! gpsdata.set & DEVICE_SET)
		{
			LOG4CXX_WARN(boilerplate.getLogger(),
				"reponse doesn't contain device info. unable to confirm"
				" rate changes were applied."
				" received error '" << gps_errstr(errno) << "'");
		}
		else if (okay)
		{
			double cycle = gpsdata.dev.cycle.tv_sec + gpsdata.dev.cycle.tv_nsec / 1000000000.0;
			double actual_rate_hz = 1 / cycle;
			if (fabs(actual_rate_hz - curr_gps_rate_hz) > 0.1)
			{
				LOG4CXX_WARN(boilerplate.getLogger(),
					"GPS rate change not applied. "
					"cycle = " << tstos(&gpsdata.dev.cycle) << "s; "
					"mincycle = " << tstos(&gpsdata.dev.mincycle) << "s. "
					"current rate is " << actual_rate_hz << "Hz");
				// update our understanding of the gps sample rate
				curr_gps_rate_hz = actual_rate_hz;
			}
		}
	}

	// configure gpsd to stream GPS data to NavServer
	if (okay && found_gps && (ret = gps_stream(&gpsdata,WATCH_ENABLE | WATCH_JSON,NULL)) != 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to configure gpsd for streaming."
			" received error '" << gps_errstr(errno) << "'");
		okay = false;
	}

	if (okay && found_gps)
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"GPS initialized!");
	}

	return okay;
}

bool
init_zmq(
	const solodata::navserver_config_t::zmq_t &ns_cfg,
	const solodata::imud_config_t::zmq_t &imud_cfg)
{
	bool okay = true;

	// Socket to listen to publishers
	imu_sub = boilerplate.create_socket(ZMQ_SUB);
	nav_pub = boilerplate.create_socket(ZMQ_PUB);
	if (okay && zmq_connect(imu_sub,imud_cfg.hostname.c_str()) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to connect imu socket to " << imud_cfg.hostname << "."
			" error = " << zmq_strerror(errno));
		okay = false;
	}
	if (okay && zmq_setsockopt(imu_sub,ZMQ_SUBSCRIBE,"",0) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to create imu subscriber."
			" error = " << zmq_strerror(errno));
		okay = false;
	}
	if (okay && ! reactor.add_pb_listener(imu_sub,on_imu_sample))
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to add imu subscriber listener to reactor.");
		okay = false;
	}

	/**
	 * Set high water mark (HWM) for NavSample publisher
	 *
	 * Setting this will cause the publisher to drop messages when
	 * the HWM is reached. When this occurs, slow subscribers will
	 * detect a skip in their received sequence numbers and should
	 * proceed to disconnect. This implements the "Suicidal Snail"
	 * design pattern.
	 */
	if (okay && zmq_setsockopt(nav_pub,ZMQ_SNDHWM,&ns_cfg.nav_samp_hwm,sizeof(ns_cfg.nav_samp_hwm)) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to set HWM of " << ns_cfg.nav_samp_hwm <<
			" on NavSample publisher socket."
			" error = " << zmq_strerror(errno));
		okay = false;
	}
	else
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"Set NavSample publisher HWM to " << ns_cfg.nav_samp_hwm << " message(s)");
	}
	if (okay && zmq_bind(nav_pub,ns_cfg.nav_samp_hostname.c_str()) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to bind nav sample socket to " << ns_cfg.nav_samp_hostname << "."
			" error = " << zmq_strerror(errno));
		okay = false;
	}
	if (okay && ns_cfg.nav_samp_hostname.find("ipc://") == 0)
	{
		// allow all users to connect and receive data from socket
		chmod(&ns_cfg.nav_samp_hostname.c_str()[6],0777);
	}

	return okay;
}

void
shutdown_signal(
	int signal)
{
	// DynamicMemoryGuard::thread_disable();
	LOG4CXX_INFO(boilerplate.getLogger(),"shutdown requested!");
	stay_alive = false;

	// reactor will catch it's own interrupt condition and exit
}

void
display_usage()
{
	printf("NavServer [options]\n");
	printf(" -f                : path to config file\n");
	printf(" --daemon          : run as daemon process\n");
	printf(" -h --help         : display this menu\n");
}

int
main(
	int argc,
	char **argv)
{
	bool okay = true;
	int daemon_flag = 0;
	std::string cfg_filepath = "";
	static struct option long_options[] =
	{
		{"daemon"         , no_argument      , &daemon_flag    , 1  },
		{"help"           , no_argument      , 0               , 'h'},
		{0, 0, 0, 0}
	};

	while (true)
	{
		int option_index = 0;
		int c = getopt_long(
			argc,
			argv,
			"hf:",
			long_options,
			&option_index);

		// detect the end of the options
		if (c == -1)
		{
			break;
		}

		switch (c)
		{
			case 0:
				// flag setting
				break;
			case 'f':
				cfg_filepath = optarg;
				break;
			case 'h':
			case '?':
				display_usage();
				exit(0);
				break;
			default:
				printf("Unsupported option '%c'\n",(char)c);
				display_usage();
				exit(1);
				break;
		}
	}

	// init classes logger prefixes
	reactor.setLoggerPrefix(boilerplate.getLoggerNameFull());

	if (daemon_flag)
	{
		boilerplate.daemonize();
	}

	// setup graceful shutdown signals
	signal(SIGINT, shutdown_signal);
	signal(SIGHUP, shutdown_signal);

	// setup/parse config options
	solodata::imud_config_t imud_config;
	if (cfg_filepath != "")
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"parsing config from '" << cfg_filepath << "'");
		YAML::Node cfg_node = YAML::LoadFile(cfg_filepath);
		ns_config = cfg_node["navserver"].as<solodata::navserver_config_t>();
		imud_config = cfg_node["imud"].as<solodata::imud_config_t>();
	}

	// allocate memory to sample buffer
	navsamp_buffer.resize(ns_config.zmq.init_buff_size_byptes);

	// init gpsd connection
	okay = okay && stay_alive && init_gps(ns_config.gpsd);

	// init pub/sub connections
	okay = okay && stay_alive && init_zmq(ns_config.zmq,imud_config.zmq);

	// start listening to imu and gps
	if (okay && ! reactor.start())
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"failed to listen to IMU/GPS samples!");
		okay = false;
	}
	LOG4CXX_INFO(boilerplate.getLogger(),
		"shutting down...");

	if (gps_opened)
	{
		// close gpsd connection
		gps_stream(&gpsdata, WATCH_DISABLE, NULL);
		gps_close(&gpsdata);
		gps_opened = false;
	}

	return okay ? EXIT_SUCCESS : EXIT_FAILURE;
}