#include "gps_utils.h"

#include <iomanip>
#include <sstream>

namespace gps_utils
{
	std::string
	set_to_string(
		const gps_mask_t &set_mask)
	{
		std::stringstream ss;

		ss << "[";
		if (set_mask & ONLINE_SET) ss << "ONLINE,";
		if (set_mask & TIME_SET)   ss << "TIME,";
		if (set_mask & TIMERR_SET) ss << "TIMERR,";
		if (set_mask & LATLON_SET) ss << "LATLON,";
		if (set_mask & ALTITUDE_SET) ss << "ALTITUDE,";
		if (set_mask & SPEED_SET) ss << "SPEED,";
		if (set_mask & TRACK_SET) ss << "TRACK,";
		if (set_mask & CLIMB_SET) ss << "CLIMB,";
		if (set_mask & STATUS_SET) ss << "STATUS,";
		if (set_mask & MODE_SET) ss << "MODE,";
		if (set_mask & DOP_SET) ss << "DOP,";
		if (set_mask & HERR_SET) ss << "HERR,";
		if (set_mask & VERR_SET) ss << "VERR,";
		if (set_mask & ATTITUDE_SET) ss << "ATTITUDE,";
		if (set_mask & SATELLITE_SET) ss << "SATELLITE,";
		if (set_mask & SPEEDERR_SET) ss << "SPEEDERR,";
		if (set_mask & TRACKERR_SET) ss << "TRACKERR,";
		if (set_mask & CLIMBERR_SET) ss << "CLIMBERR,";
		if (set_mask & DEVICE_SET) ss << "DEVICE,";
		if (set_mask & DEVICELIST_SET) ss << "DEVICELIST,";
		if (set_mask & DEVICEID_SET) ss << "DEVICEID,";
		if (set_mask & RTCM2_SET) ss << "RTCM2,";
		if (set_mask & RTCM3_SET) ss << "RTCM3,";
		if (set_mask & AIS_SET) ss << "AIS,";
		if (set_mask & PACKET_SET) ss << "PACKET,";
		if (set_mask & SUBFRAME_SET) ss << "SUBFRAME,";
		if (set_mask & GST_SET) ss << "GST,";
		if (set_mask & VERSION_SET) ss << "VERSION,";
		if (set_mask & POLICY_SET) ss << "POLICY,";
		if (set_mask & LOGMESSAGE_SET) ss << "LOGMESSAGE,";
		if (set_mask & ERROR_SET) ss << "ERROR,";
		if (set_mask & TOFF_SET) ss << "TOFF,";
		if (set_mask & PPS_SET) ss << "PPS,";
		if (set_mask & NAVDATA_SET) ss << "NAVDATA,";
		if (set_mask & OSCILLATOR_SET) ss << "OSCILLATOR,";
		if (set_mask & ECEF_SET) ss << "ECEF,";
		if (set_mask & VECEF_SET) ss << "VECEF,";
		if (set_mask & MAGNETIC_TRACK_SET) ss << "MAGNETIC_TRACK,";
		if (set_mask & RAW_SET) ss << "RAW,";
		if (set_mask & NED_SET) ss << "NED,";
		if (set_mask & VNED_SET) ss << "VNED,";
		if (set_mask & LOG_SET) ss << "LOG,";
		if (set_mask & IMU_SET) ss << "IMU,";
		ss << "]";

		return ss.str();
	}
	
	std::string
	timespec_to_string(
		const timespec &ts)
	{
		std::stringstream ss;
		ss << "{" << ts.tv_sec << "," << ts.tv_nsec << "}";
		return ss.str();
	}

	std::string
	devconfig_to_string(
		const devconfig_t &dev,
		std::string tab,
		bool newlines)
	{
		std::stringstream ss;

		std::string ending = "\n";
		std::string mytab = "  ";
		if ( ! newlines)
		{
			ending = "";
			tab = "";
			mytab = "";
		}

		ss << tab << "{" << ending;
		ss << tab << mytab << "path: " << dev.path << "; " << ending;
		ss << tab << mytab << "flags: 0x" << std::hex << dev.flags << std::dec << "; " << ending;
		ss << tab << mytab << "driver: " << dev.driver << "; " << ending;
		ss << tab << mytab << "subtype: " << dev.subtype << "; " << ending;
		ss << tab << mytab << "subtype1: " << dev.subtype1 << "; " << ending;
		ss << tab << mytab << "activated: " << timespec_to_string(dev.activated) << "; " << ending;
		ss << tab << mytab << "baudrate: " << dev.baudrate << "; " << ending;
		ss << tab << mytab << "stopbits: " << dev.stopbits << "; " << ending;
		ss << tab << mytab << "parity: " << dev.parity << "; " << ending;
		ss << tab << mytab << "cycle: " << timespec_to_string(dev.cycle) << "; " << ending;
		ss << tab << mytab << "mincycle: " << timespec_to_string(dev.mincycle) << "; " << ending;
		ss << tab << mytab << "driver_mode: " << dev.driver_mode << "; " << ending;
		ss << tab << "}" << ending;

		return ss.str();
	}

	std::string
	gpsdata_to_string(
		const gps_data_t &gd,
		std::string tab,
		bool newlines)
	{
		std::stringstream ss;

		std::string ending = "\n";
		std::string mytab = "  ";
		if ( ! newlines)
		{
			ending = "";
			tab = "";
			mytab = "";
		}

		ss << std::fixed;

		ss << tab << "{" << ending;
		ss << tab << mytab << "set: " << set_to_string(gd.set) << "; " << ending;
		if (gd.set & DEVICE_SET)
		{
			ss << tab << mytab << "device: " << devconfig_to_string(gd.dev,"",false) << "; " << ending;
		}
		if (gd.set & DEVICELIST_SET)
		{
			ss << tab << mytab << "devices: [" << ending;
			unsigned int n_devs = gd.devices.ndevices;
			for (unsigned int d=0; d<n_devs; d++)
			{
				ss << tab << mytab << mytab << devconfig_to_string(gd.devices.list[d],"",false) << ", " << ending;
			}
			ss << tab << mytab << "]; " << ending;
		}
		if (gd.set & TIME_SET)
		{
			ss << tab << mytab << "time: " << timespec_to_string(gd.fix.time) << "; " << ending;
		}
		if (gd.set & TIMERR_SET)
		{
			ss << tab << mytab << "ept (time uncertainty): " << gd.fix.ept << "s; " << ending;
		}
		if (gd.set & MODE_SET)
		{
			ss << tab << mytab << "mode: " << gd.fix.mode << "; " << ending;
		}
		if (gd.set & STATUS_SET)
		{
			ss << tab << mytab << "status: " << gd.fix.status << "; " << ending;
		}
		if (gd.set & LATLON_SET)
		{
			ss << tab << mytab << "lat: " << std::setprecision(9) << gd.fix.latitude << "; " << ending;
			ss << tab << mytab << "epx (lat uncertainty): " << std::setprecision(3) << gd.fix.epx << "m; " << ending;
			ss << tab << mytab << "lon: " << std::setprecision(9) << gd.fix.longitude << "; " << ending;
			ss << tab << mytab << "epy (lon uncertainty): " << std::setprecision(3)<< gd.fix.epy << "m; " << ending;
		}
		if (gd.set & HERR_SET)
		{
			ss << tab << mytab << "eph (2D position uncertainty): " << gd.fix.eph << "m; " << ending;
		}
		if (gd.set & TRACK_SET)
		{
			ss << tab << mytab << "track: " << gd.fix.track << "degrees (relative to true north); " << ending;
		}
		if (gd.set & TRACKERR_SET)
		{
			ss << tab << mytab << "epd (track uncertainty): " << gd.fix.epd << "degrees " << ending;
		}
		if (gd.set & MAGNETIC_TRACK_SET)
		{
			ss << tab << mytab << "magnetic_track: " << gd.fix.magnetic_track << "degrees (relative to mag north); " << ending;
			ss << tab << mytab << "magnetic_var: " << gd.fix.magnetic_var << "degrees " << ending;
		}
		if (gd.set & ALTITUDE_SET)
		{
			ss << tab << mytab << "altHAE (height above ellipsoid): " << gd.fix.altHAE << "m; " << ending;
		}
		if (gd.set & VERR_SET)
		{
			ss << tab << mytab << "epv (altitude uncertainty): " << gd.fix.epv << "m; " << ending;
		}
		if (gd.set & SPEED_SET)
		{
			ss << tab << mytab << "speed: " << gd.fix.speed << "m/s; " << ending;
		}
		if (gd.set & SPEEDERR_SET)
		{
			ss << tab << mytab << "eps (speed uncertainty): " << gd.fix.eps << "m/s; " << ending;
		}
		if (gd.set & CLIMB_SET)
		{
			ss << tab << mytab << "climb: " << gd.fix.climb << "m/s; " << ending;
		}
		if (gd.set & CLIMBERR_SET)
		{
			ss << tab << mytab << "epc (climb uncertainty): " << gd.fix.epc << "m/s; " << ending;
		}
		if (gd.set & ERROR_SET)
		{
			ss << tab << mytab << "error: " << gd.error << "; " << ending;
		}
		ss << tab << "}" << ending;

		return ss.str();
	}
}