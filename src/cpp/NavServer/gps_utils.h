#pragma once

#include <gps.h>
#include <iostream>
#include <string>

namespace gps_utils
{
	/**
	 * Converts a gpsdata `set` mask to a list of string
	 * showing which fields are set.
	 */
	std::string
	set_to_string(
		const gps_mask_t &set_mask);

	std::string
	timespec_to_string(
		const timespec &ts);

	/**
	 * Converts a devconfig_t to a string
	 */
	std::string
	devconfig_to_string(
		const devconfig_t &dev,
		std::string tab = "",
		bool newlines = true);

	/**
	 * Converts a gps_data_t to a string
	 */
	std::string
	gpsdata_to_string(
		const gps_data_t &gd,
		std::string tab = "",
		bool newlines = true);
}