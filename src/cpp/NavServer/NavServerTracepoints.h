#undef TRACEPOINT_PROVIDER
#define TRACEPOINT_PROVIDER navserver_ust

#undef TRACEPOINT_INCLUDE
#define TRACEPOINT_INCLUDE "NavServerTracepoints.h"

#if !defined(_TP_H) || defined(TRACEPOINT_HEADER_MULTI_READ)
#define _TP_H

#include <lttng/tracepoint.h>

//---------------------------------------------------------
// NavServer general events
//---------------------------------------------------------

TRACEPOINT_EVENT_CLASS(
	/* Tracepoint provider name */
	navserver_ust,

	/* Tracepoint class name */
	navserver_event_class,

	/* Input arguments */
	TP_ARGS(
	),

	/* Output event fields */
	TP_FIELDS(
	)
)

TRACEPOINT_EVENT_INSTANCE(
	navserver_ust,
	navserver_event_class,
	gps_read,
	TP_ARGS(
	)
)

TRACEPOINT_EVENT_INSTANCE(
	navserver_ust,
	navserver_event_class,
	gps_handle,
	TP_ARGS(
	)
)

TRACEPOINT_EVENT_INSTANCE(
	navserver_ust,
	navserver_event_class,
	on_imu_sample,
	TP_ARGS(
	)
)

#endif /* _TP_H */

#include <lttng/tracepoint-event.h>
