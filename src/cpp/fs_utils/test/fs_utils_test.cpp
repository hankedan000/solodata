#include "fs_utils_test.h"

#include <cstdlib>// system()
#include "fs_utils.h"

void
fs_utils_test::setUp()
{
	// make sure the test directory is clean for each test
	system("rm -r /tmp/fs_utils_test");
}

void
fs_utils_test::tearDown()
{
}

void
fs_utils_test::mkdir()
{
	// ----------------------------------------------------------
	// test simple case where last entry is directory

	// make sure /tmp/ exists because we need it to perform tests
	CPPUNIT_ASSERT_MESSAGE(
		"test requires /tmp/ to exist",
		fs_utils::dir_exists("/tmp/"));

	// make sure dir doesn't exist already
	CPPUNIT_ASSERT( ! fs_utils::dir_exists("/tmp/fs_utils_test/dir1/"));
	// create dir
	CPPUNIT_ASSERT_NO_THROW(fs_utils::mkdir("/tmp/fs_utils_test/dir1/"));
	// make sure dir exists now
	CPPUNIT_ASSERT(fs_utils::dir_exists("/tmp/fs_utils_test/dir1/"));

	// ----------------------------------------------------------
	// test case where last entry in path is actually a file

	// make sure dir doesn't exist already
	CPPUNIT_ASSERT( ! fs_utils::dir_exists("/tmp/fs_utils_test/dir2/"));
	CPPUNIT_ASSERT( ! fs_utils::dir_exists("/tmp/fs_utils_test/dir2/file.txt"));
	// create dir
	CPPUNIT_ASSERT_NO_THROW(fs_utils::mkdir("/tmp/fs_utils_test/dir2/file.txt"));
	// make sure dir exists now
	CPPUNIT_ASSERT(fs_utils::dir_exists("/tmp/fs_utils_test/dir2/"));
	CPPUNIT_ASSERT( ! fs_utils::dir_exists("/tmp/fs_utils_test/dir2/file.txt"));

	// ----------------------------------------------------------
	// test case where dir already exists. make sure it doesn't fail on second try

	CPPUNIT_ASSERT_NO_THROW(fs_utils::mkdir("/tmp/fs_utils_test/double_mkdir/"));
	CPPUNIT_ASSERT_NO_THROW(fs_utils::mkdir("/tmp/fs_utils_test/double_mkdir/"));
}

int main()
{
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(fs_utils_test::suite());
	return runner.run() ? 0 : 1;
}
