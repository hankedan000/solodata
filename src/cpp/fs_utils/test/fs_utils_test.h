#pragma once

#include <unistd.h>

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class fs_utils_test : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(fs_utils_test);
	CPPUNIT_TEST(mkdir);
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();
	void tearDown();

protected:
	void mkdir();

private:

};
