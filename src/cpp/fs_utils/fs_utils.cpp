#include "fs_utils.h"

#include <cstdlib>// system()
#include <sys/stat.h>

#define OS_PATH_SEPARATOR "/"

namespace fs_utils
{

bool
dir_exists(
	const std::string &path)
{
	struct stat info;
	if( stat( path.c_str(), &info ) != 0 ) {
		return false;
	} else if( info.st_mode & S_IFDIR ) {
		// path exists, and it's a directory!
		return true;
	} else {
		// path exists, but it's not a directory
		return false;
	}
}

void
mkdir(
	const std::string &path)
{
	const std::string parent_dir_path = path.substr(0, path.find_last_of(OS_PATH_SEPARATOR));
	const std::string cmd("mkdir -p " + parent_dir_path);
	
	// FIXME it's not safe to use sysmte()
	// someone could exploit this to run commands
	system(cmd.c_str());
}

}