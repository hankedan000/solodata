#pragma once

#include <string>

namespace fs_utils
{

bool
dir_exists(
	const std::string &path);

/**
 * Will create all parent directories of the given file or directory path
 * 
 * Ex:
 *   path = "/home/user/file.txt"
 *   creates all directories up to "/home/user/"
 * 
 *   path = "/home/user/file" <-- notice no trailing '/'
 *   creates all directories up to "/home/user/"
 * 
 *   path = "dir1/dir2/dir3/"
 *   creates all directories up to "./dir1/dir2/dir3/"
 */
void
mkdir(
	const std::string &path);

}