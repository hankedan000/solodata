add_library(fs_utils STATIC fs_utils.cpp)
target_include_directories(fs_utils PUBLIC include)
install(
	TARGETS fs_utils
	LIBRARY
		DESTINATION lib
)

add_subdirectory(test)