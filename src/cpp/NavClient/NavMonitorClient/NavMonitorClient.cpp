#include <csignal>

#include "NavClient.h"

// include log4cxx header files
#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/helpers/stringhelper.h>
#include <log4cxx/helpers/transform.h>
#include <log4cxx/net/socketappender.h>

log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("NavMonitorClient"));
solodata::NavClient client;

void
handle_sample(
	const solodata::NavSample &samp)
{
	LOG4CXX_INFO(logger, samp.DebugString());
}

void
handle_sigint(
	int)
{
	LOG4CXX_INFO(logger,"Requsted shutdown!");
	client.stop();
}

int
main(
	int argc,
	char **argv)
{
	bool okay = true;
	signal(SIGINT,handle_sigint);
	// setup a simple configuration that logs to console
	log4cxx::BasicConfigurator::configure();

	// set logging level threshold
	logger->setLevel(log4cxx::Level::getInfo());

	client.add_listener(
		"NavMonitorClient",
		solodata::NavClient::SampleListener(handle_sample));
	if (client.start("localhost",6060))
	{
		client.wait();
	}

	return okay ? 0 : 1;
}