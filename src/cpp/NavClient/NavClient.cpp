#include "NavClient.h"
#include <unistd.h>

// include log4cxx header files
#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/helpers/stringhelper.h>
#include <log4cxx/helpers/transform.h>
#include <log4cxx/net/socketappender.h>

// include ZeroMQ headers
#include <zmq.h>

// used to quantize buffer growth in number of bytes
#define BUFFER_GROW_STEPS 1024
// size in bytes to initial the NavSample buffer to
#define INIT_BUFFER_SIZE (1 * BUFFER_GROW_STEPS)
// the host ip where the NavServer is running
#define NAV_SERVER_HOSTNAME "tcp://localhost:6060"

namespace solodata
{
	NavClient::NavClient()
	 : logger_(log4cxx::Logger::getLogger("NavClient"))
	 , thread_()
	 , context_(nullptr)
	 , subscriber_(nullptr)
	 , buffer_()
	{
		logger_->setLevel(log4cxx::Level::getInfo());
		buffer_.resize(INIT_BUFFER_SIZE);
	}

	NavClient::~NavClient()
	{
		// send signal to stop
		stop();
		// wait for thread to join
		wait();
		if (subscriber_ != nullptr)
		{
			zmq_close(subscriber_);
			subscriber_ = nullptr;
		}
		if (context_ != nullptr)
		{
			zmq_ctx_destroy(context_);
			context_ = nullptr;
		}
	}

	bool
	NavClient::start(
			const std::string &host_ip,
			unsigned int host_port)
	{
		bool okay = true;

		// Socket to listen to publishers
		context_ = zmq_ctx_new();
		subscriber_ = zmq_socket(context_,ZMQ_SUB);
		std::string hostname = "tcp://" + host_ip + ":" + std::to_string(host_port);
		if (okay && zmq_connect(subscriber_,hostname.c_str()) < 0)
		{
			LOG4CXX_ERROR(logger_,"Failed to bind socket to " << hostname);
			okay = false;
		}
		if (okay && zmq_setsockopt(subscriber_,ZMQ_SUBSCRIBE,"",0) < 0)
		{
			LOG4CXX_ERROR(logger_,"Failed to create subscriber");
			okay = false;
		}

		if (okay)
		{
			stay_alive_ = true;
			thread_ = std::thread(&NavClient::event_loop,this);
		}

		return okay;
	}

	void
	NavClient::stop()
	{
		stay_alive_ = false;
		if (subscriber_ != nullptr)
		{
			zmq_close(subscriber_);
			subscriber_ = nullptr;
		}
		if (context_ != nullptr)
		{
			zmq_ctx_destroy(context_);
			context_ = nullptr;
		}
	}

	void
	NavClient::wait()
	{
		if (thread_.joinable())
		{
			thread_.join();
		}
	}

	void
	NavClient::add_listener(
		const std::string &key,
		SampleListener listener)
	{
		mutex_.lock();
		listeners_[key] = listener;
		mutex_.unlock();
	}

	void
	NavClient::event_loop()
	{
		LOG4CXX_INFO(logger_,"Listening to NavServer...");
		while (stay_alive_)
		{
			int rbytes = zmq_recv(
				subscriber_,
				(void*)buffer_.data(),
				buffer_.size(),
				0);
			LOG4CXX_DEBUG(logger_,"received " << rbytes << " bytes");

			if ( ! stay_alive_)
			{
				// we can get in here if interrupted while in zmq_recv()
				continue;
			}
			else if (rbytes < 0)
			{
				LOG4CXX_ERROR(logger_,"zmq_recv failed");
				continue;
			}
			else if (rbytes > buffer_.size())
			{
				LOG4CXX_ERROR(logger_,"zmq_recv message was truncated!");
				resize_buffer(rbytes);
				continue;
			}

			// deserialize NavSample
			solodata::NavSample datum;
			if ( ! datum.ParseFromArray((const void *)buffer_.data(),rbytes))
			{
				LOG4CXX_ERROR(logger_,"datum.ParseFromArray() failed!");
				continue;
			}

			LOG4CXX_DEBUG(logger_,datum.DebugString());

			// notify listeners
			mutex_.lock();
			for (auto listener : listeners_)
			{
				listener.second(datum);
			}
			mutex_.unlock();
		}
	}


	void
	NavClient::resize_buffer(
		size_t required_bytes)
	{
		size_t curr_size = buffer_.size();
		size_t next_size = (required_bytes / BUFFER_GROW_STEPS + 1) * BUFFER_GROW_STEPS;
		LOG4CXX_WARN(logger_,
			"NavSample deserialization buffer resized"
			" from " << curr_size << " bytes"
			" to "   << next_size << " bytes.");
		buffer_.resize(next_size);
	}

}
