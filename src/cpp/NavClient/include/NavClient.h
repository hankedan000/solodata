#pragma once

#include <functional>
#include <map>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

// include protobuf header files
#include "NavSample.pb.h"

// include log4cxx header files
#include <log4cxx/logger.h>

namespace solodata
{
	class NavClient
	{
	public:
		typedef std::function<void (const solodata::NavSample &)> SampleListener;

	public:
		NavClient();

		~NavClient();

		bool
		start(
			const std::string &host_ip,
			unsigned int host_port);

		void
		stop();

		void
		wait();

		void
		add_listener(
			const std::string &key,
			SampleListener listener);

		void
		event_loop();

	private:
		/**
		 * method that encapsulates deserialzation buffer growth
		 *
		 * @param[in] required_bytes
		 * minimum size of buffer in bytes after growth
		 */
		void
		resize_buffer(
			size_t required_bytes);

	private:
		log4cxx::LoggerPtr logger_;
		std::thread thread_;
		std::mutex mutex_;
		std::map<std::string,SampleListener> listeners_;
		bool stay_alive_;
		void *context_;
		void *subscriber_;
		std::vector<uint8_t> buffer_;

	};
}// solodata