#pragma once

#include <cmath>
#include <iostream>
#include <sstream>
#include "Nav_defn.pb.h"

namespace solodata
{

	std::string
	vf_to_str(
		const VectorFloat &vf);

	std::string
	quat_to_str(
		const Quaternion &q);

	VectorFloat
	operator+(
		const VectorFloat &lhs,
		const VectorFloat &rhs);

	VectorFloat
	operator-(
		const VectorFloat &lhs,
		const VectorFloat &rhs);

	template<typename T>
	VectorFloat
	operator*(
		const VectorFloat &lhs,
		const T &c)
	{
		VectorFloat res;
		res.set_x(lhs.x() * c);
		res.set_y(lhs.y() * c);
		res.set_z(lhs.z() * c);
		return res;
	}

	template<typename T>
	VectorFloat
	operator*(
		const T &c,
		const VectorFloat &rhs)
	{
		return rhs * c;
	}

	template<typename T>
	VectorFloat
	operator/(
		const VectorFloat &lhs,
		const T &c)
	{
		VectorFloat res;
		res.set_x(lhs.x() / c);
		res.set_y(lhs.y() / c);
		res.set_z(lhs.z() / c);
		return res;
	}

	template<typename T>
	VectorFloat
	operator/(
		const T &c,
		const VectorFloat &rhs)
	{
		VectorFloat res;
		res.set_x(c / rhs.x());
		res.set_y(c / rhs.y());
		res.set_z(c / rhs.z());
		return res;
	}

	/**
	 * Computes the hadamard product of the two vectors
	 * Ex: had(A,B) = [Ax*Bx,Ay*By,Az*Bz]
	 */
	VectorFloat
	had(
		const VectorFloat &A,
		const VectorFloat &B);

	VectorFloat
	get_gravity(
		const Quaternion &q);

}// namespace solodata