if (BUILD_TESTING AND CPPUNIT_FOUND)
	add_executable(NavMathTest NavMathTest.cpp)
	add_test(NAME NavMathTest COMMAND NavMathTest)

	# Link the Stopwatch library to the demo
	target_link_libraries(NavMathTest NavMath ${CPPUNIT_LIBRARIES})
endif()
