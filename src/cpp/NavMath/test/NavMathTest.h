/**
 * File: NavMathTest.h
 * Author:  Daniel Hankewycz
 * Description:
 * CppUnit test for the NavMath library
 */
#pragma once

#include <unistd.h>

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "NavMath.h"

class NavMathTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(NavMathTest);
	CPPUNIT_TEST(testAddSub);
	CPPUNIT_TEST(testConstMultDiv);
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();
	void tearDown();

protected:
	void testAddSub();
	void testConstMultDiv();

private:

};
