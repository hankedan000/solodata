/**
 * File: NavMathTest.cpp
 * Author:  Daniel Hankewycz
 * Description:
 * CppUnit test for the NavMath library
 */
#include "NavMathTest.h"

void
NavMathTest::setUp()
{
}

void
NavMathTest::tearDown()
{
}

void
NavMathTest::testAddSub()
{
	solodata::VectorFloat res,l,r;

	l.set_x(1);
	l.set_y(2);
	l.set_z(3);
	r.set_x(3);
	r.set_y(3);
	r.set_z(3);
	res = l + r;
	CPPUNIT_ASSERT_DOUBLES_EQUAL(+4.0,res.x(),0.1);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(+5.0,res.y(),0.1);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(+6.0,res.z(),0.1);

	res = l - r;
	CPPUNIT_ASSERT_DOUBLES_EQUAL(-2.0,res.x(),0.1);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(-1.0,res.y(),0.1);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(-0.0,res.z(),0.1);
}

void
NavMathTest::testConstMultDiv()
{
	solodata::VectorFloat res,l,r;

	l.set_x(1);
	l.set_y(2);
	l.set_z(3);
	res = l * 3.0;
	CPPUNIT_ASSERT_DOUBLES_EQUAL(+3.0,res.x(),0.1);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(+6.0,res.y(),0.1);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(+9.0,res.z(),0.1);

	res = -1 * l;
	CPPUNIT_ASSERT_DOUBLES_EQUAL(-1.0,res.x(),0.1);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(-2.0,res.y(),0.1);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(-3.0,res.z(),0.1);

	res = l / 3.0;
	CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0 / 3.0,res.x(),0.1);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(2.0 / 3.0,res.y(),0.1);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(3.0 / 3.0,res.z(),0.1);
}

int main()
{
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(NavMathTest::suite());
	return runner.run() ? 0 : 1;
}
