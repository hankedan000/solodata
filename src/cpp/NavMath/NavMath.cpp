#include "NavMath.h"

namespace solodata
{

	std::string
	vf_to_str(
		const VectorFloat &vf)
	{
		std::stringstream ss;
		ss << std::fixed << std::showpos << "{";
		ss << "x: " << vf.x() << "; ";
		ss << "y: " << vf.y() << "; ";
		ss << "z: " << vf.z() << "}";
		return ss.str();
	}

	std::string
	quat_to_str(
		const Quaternion &q)
	{
		std::stringstream ss;
		ss << std::fixed << std::showpos << "{";
		ss << "w: " << q.w() << "; ";
		ss << "x: " << q.x() << "; ";
		ss << "y: " << q.y() << "; ";
		ss << "z: " << q.z() << "}";
		return ss.str();
	}

	VectorFloat
	operator+(
		const VectorFloat &lhs,
		const VectorFloat &rhs)
	{
		VectorFloat res;
		res.set_x(lhs.x() + rhs.x());
		res.set_y(lhs.y() + rhs.y());
		res.set_z(lhs.z() + rhs.z());
		return res;
	}

	VectorFloat
	operator-(
		const VectorFloat &lhs,
		const VectorFloat &rhs)
	{
		VectorFloat res;
		res.set_x(lhs.x() - rhs.x());
		res.set_y(lhs.y() - rhs.y());
		res.set_z(lhs.z() - rhs.z());
		return res;
	}

	VectorFloat
	had(
		const VectorFloat &A,
		const VectorFloat &B)
	{
		VectorFloat res;
		res.set_x(A.x() * B.x());
		res.set_y(A.y() * B.y());
		res.set_z(A.z() * B.z());
		return res;
	}

	VectorFloat
	get_gravity(
		const Quaternion &q)
	{
		VectorFloat g;
		g.set_x(2 * (q.x()*q.z() - q.w()*q.y()));
		g.set_y(2 * (q.w()*q.x() + q.y()*q.z()));
		g.set_z(q.w()*q.w() - q.x()*q.x() - q.y()*q.y() + q.z()*q.z());
		return g;
	}

}// namespace solodata