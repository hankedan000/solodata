#pragma once

#include <string>
#include "yaml_utils.h"
#include <yaml-cpp/yaml.h>

namespace solodata
{

struct imud_config_t
{
	struct zmq_t
	{
		std::string hostname = "ipc:///var/run/imud.sock";
		size_t buff_size_bytes = 1000;
	} zmq;

	struct imu_t
	{
		unsigned int gyr_rate_hz  = 100;
		unsigned int acc_rate_hz  = 100;
		unsigned int mag_rate_hz  = 100;
		unsigned int quat_rate_hz = 100;

		// Full-Scale Range settings
		unsigned int acc_fsr = 4;   // Default = +/- 4g. Valid ranges: 2, 4, 8, 16
		unsigned int gyr_fsr = 2000;// Default = +/- 2000dps. Valid ranges: 250, 500, 1000, 2000

		unsigned int watchdog_limit = 10;// sleep cycles
		unsigned int junk_filter = 50;// number of samples to ignore after init

		unsigned int intr_pin = 2;// GPIO 27 -> Pin 2 (wiringPi API)
	} imu;

	struct sched_t
	{
		// static scheduling priority
		unsigned int priority = 99;

		// CPU core affinity
		// -1 means don't set the core affinity
		int cpu_affinity = -1;
	} sched;

	struct status_t
	{
		// enables thread that periodically writes debug info to disk
		bool enabled = true;

		// the file to write status info to
		std::string file = "/var/run/imud/status";

		// status report interval in milliseconds
		unsigned int interval_ms = 1000;
	} stat;

	unsigned int samp_pool_size = 16;
};

}// namespace solodata

// overrides for yaml config encode/decode
namespace YAML
{

template<>
struct convert<solodata::imud_config_t::zmq_t>
{
	static Node
	encode(
		const solodata::imud_config_t::zmq_t& rhs)
	{
		Node node;
		node["hostname"] = rhs.hostname;
		node["buff_size_bytes"] = rhs.buff_size_bytes;
		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::imud_config_t::zmq_t& rhs)
	{
		rhs.hostname = node["hostname"].as<std::string>();
		rhs.buff_size_bytes = node["buff_size_bytes"].as<unsigned int>();
		return true;
	}
};

template<>
struct convert<solodata::imud_config_t::imu_t>
{
	static Node
	encode(
		const solodata::imud_config_t::imu_t& rhs)
	{
		Node node;
		node["gyr_rate_hz"] = rhs.gyr_rate_hz;
		node["acc_rate_hz"] = rhs.acc_rate_hz;
		node["mag_rate_hz"] = rhs.mag_rate_hz;
		node["quat_rate_hz"] = rhs.quat_rate_hz;

		node["acc_fsr"] = rhs.acc_fsr;
		node["gyr_fsr"] = rhs.gyr_fsr;

		node["watchdog_limit"] = rhs.watchdog_limit;
		node["junk_filter"] = rhs.junk_filter;

		node["intr_pin"] = rhs.intr_pin;

		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::imud_config_t::imu_t& rhs)
	{
		YAML_TO_FIELD(node,"gyr_rate_hz",rhs.gyr_rate_hz);
		YAML_TO_FIELD(node,"acc_rate_hz",rhs.acc_rate_hz);
		YAML_TO_FIELD(node,"mag_rate_hz",rhs.mag_rate_hz);
		YAML_TO_FIELD(node,"quat_rate_hz",rhs.quat_rate_hz);

		YAML_TO_FIELD(node,"acc_fsr",rhs.acc_fsr);
		YAML_TO_FIELD(node,"gyr_fsr",rhs.gyr_fsr);

		YAML_TO_FIELD(node,"watchdog_limit",rhs.watchdog_limit);
		YAML_TO_FIELD(node,"junk_filter",rhs.junk_filter);

		YAML_TO_FIELD(node,"intr_pin",rhs.intr_pin);

		return true;
	}
};

template<>
struct convert<solodata::imud_config_t::sched_t>
{
	static Node
	encode(
		const solodata::imud_config_t::sched_t& rhs)
	{
		Node node;
		node["priority"] = rhs.priority;
		node["cpu_affinity"] = rhs.cpu_affinity;

		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::imud_config_t::sched_t& rhs)
	{
		YAML_TO_FIELD(node,"priority",rhs.priority);
		YAML_TO_FIELD(node,"cpu_affinity",rhs.cpu_affinity);

		return true;
	}
};

template<>
struct convert<solodata::imud_config_t::status_t>
{
	static Node
	encode(
		const solodata::imud_config_t::status_t& rhs)
	{
		Node node;
		node["enabled"] = rhs.enabled;
		node["file"] = rhs.file;
		node["interval_ms"] = rhs.interval_ms;

		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::imud_config_t::status_t& rhs)
	{
		YAML_TO_FIELD(node,"enabled",rhs.enabled);
		YAML_TO_FIELD(node,"file",rhs.file);
		YAML_TO_FIELD(node,"interval_ms",rhs.interval_ms);

		return true;
	}
};

template<>
struct convert<solodata::imud_config_t>
{
	static Node
	encode(
		const solodata::imud_config_t& rhs)
	{
		Node node;
		node["zmq"] = rhs.zmq;
		node["imu"] = rhs.imu;
		node["sched"] = rhs.sched;

		node["samp_pool_size"] = rhs.samp_pool_size;

		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::imud_config_t& rhs)
	{
		YAML_TO_FIELD(node,"zmq",rhs.zmq);
		YAML_TO_FIELD(node,"imu",rhs.imu);
		YAML_TO_FIELD(node,"sched",rhs.sched);
		YAML_TO_FIELD(node,"stat",rhs.stat);

		YAML_TO_FIELD(node,"samp_pool_size",rhs.samp_pool_size);
		
		return true;
	}
};

}// namespace YAML