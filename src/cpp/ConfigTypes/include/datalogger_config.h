#pragma once

#include <string>
#include "yaml_utils.h"
#include <yaml-cpp/yaml.h>

namespace solodata
{

struct datalogger_config_t
{
	std::string record_dir = "/tmp/DataLogger";

	bool enable_stopwatch = false;
};

}// namespace solodata

// overrides for yaml config encode/decode
namespace YAML
{

template<>
struct convert<solodata::datalogger_config_t>
{
	static Node
	encode(
		const solodata::datalogger_config_t& rhs)
	{
		Node node;
		node["record_dir"] = rhs.record_dir;
		node["enable_stopwatch"] = rhs.enable_stopwatch;

		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::datalogger_config_t& rhs)
	{
		YAML_TO_FIELD(node,"record_dir",rhs.record_dir);
		YAML_TO_FIELD(node,"enable_stopwatch",rhs.enable_stopwatch);

		return true;
	}
};

}// namespace YAML