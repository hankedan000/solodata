#pragma once

#include <string>
#include <vector>
#include "yaml_utils.h"
#include <yaml-cpp/yaml.h>

namespace solodata
{

struct cand_config_t
{
	struct zmq_t
	{
		std::string hostname = "ipc:///var/run/cand.sock";
		size_t buff_size_bytes = 1000;

		// control socket's tcp port #
		unsigned int ctrl_port = 7685;
	} zmq;

	struct sched_t
	{
		// static scheduling priority
		unsigned int priority = 99;

		// CPU core affinity
		// -1 means don't set the core affinity
		int cpu_affinity = -1;
	} sched;

	struct signal_group_t
	{
		std::string name;
		std::string dbc;
		unsigned int pool_size;
		unsigned int rate_hz;
	};
	std::vector<signal_group_t> signal_groups;

	std::string if_name = "can0";

	unsigned int pub_pool_size = 8;
};

}// namespace solodata

// overrides for yaml config encode/decode
namespace YAML
{

template<>
struct convert<solodata::cand_config_t::zmq_t>
{
	static Node
	encode(
		const solodata::cand_config_t::zmq_t& rhs)
	{
		Node node;
		node["hostname"] = rhs.hostname;
		node["buff_size_bytes"] = rhs.buff_size_bytes;
		node["ctrl_port"] = rhs.ctrl_port;
		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::cand_config_t::zmq_t& rhs)
	{
		YAML_TO_FIELD(node,"hostname",rhs.hostname);
		YAML_TO_FIELD(node,"buff_size_bytes",rhs.buff_size_bytes);
		YAML_TO_FIELD(node,"ctrl_port",rhs.ctrl_port);
		return true;
	}
};

template<>
struct convert<solodata::cand_config_t::sched_t>
{
	static Node
	encode(
		const solodata::cand_config_t::sched_t& rhs)
	{
		Node node;
		node["priority"] = rhs.priority;
		node["cpu_affinity"] = rhs.cpu_affinity;

		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::cand_config_t::sched_t& rhs)
	{
		YAML_TO_FIELD(node,"priority",rhs.priority);
		YAML_TO_FIELD(node,"cpu_affinity",rhs.cpu_affinity);

		return true;
	}
};

template<>
struct convert<solodata::cand_config_t::signal_group_t>
{
	static Node
	encode(
		const solodata::cand_config_t::signal_group_t& rhs)
	{
		Node node;
		node["name"] = rhs.name;
		node["dbc"] = rhs.dbc;
		node["pool_size"] = rhs.pool_size;
		node["rate_hz"] = rhs.rate_hz;

		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::cand_config_t::signal_group_t& rhs)
	{
		YAML_TO_FIELD(node,"name",rhs.name);
		YAML_TO_FIELD(node,"dbc",rhs.dbc);
		YAML_TO_FIELD(node,"pool_size",rhs.pool_size);
		YAML_TO_FIELD(node,"rate_hz",rhs.rate_hz);

		return true;
	}
};

template<>
struct convert<solodata::cand_config_t>
{
	static Node
	encode(
		const solodata::cand_config_t& rhs)
	{
		Node node;
		node["zmq"] = rhs.zmq;
		node["sched"] = rhs.sched;
		vector_to_yaml(rhs.signal_groups, node["signal_groups"]);

		node["if_name"] = rhs.if_name;
		node["pub_pool_size"] = rhs.pub_pool_size;

		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::cand_config_t& rhs)
	{
		YAML_TO_FIELD(node,"zmq",rhs.zmq);
		YAML_TO_FIELD(node,"sched",rhs.sched);
		yaml_to_vector(node["signal_groups"], rhs.signal_groups);

		YAML_TO_FIELD(node,"if_name",rhs.if_name);
		YAML_TO_FIELD(node,"pub_pool_size",rhs.pub_pool_size);


		return true;
	}
};

}// namespace YAML