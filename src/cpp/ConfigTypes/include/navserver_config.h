#pragma once

#include <string>
#include <yaml-cpp/yaml.h>

namespace solodata
{

struct navserver_config_t
{
	struct gpsd_t
	{
		std::string hostname = "localhost";
		std::string port = "2947";
		unsigned int debug_level = 0;// 0 -> 10?; 0 is no debug, 10 is max

		double gps_rate_hz = 10.0;
		unsigned int gps_baud = 115200;
	} gpsd;

	struct zmq_t
	{
		// publisher hostname to send NavSamples
		std::string nav_samp_hostname = "tcp://*:6060";
		// high water mark for nav sample publisher socket
		int nav_samp_hwm = 100;

		// initial size of buffer used to serialize NavSamples
		// will grow dynamically if necessary
		size_t init_buff_size_byptes = 1024;
	} zmq;

	// logs contents of gps_data_t received from libgps
	bool log_gpsdata = false;
};

}// namespace solodata

// overrides for yaml config encode/decode
namespace YAML
{

template<>
struct convert<solodata::navserver_config_t::gpsd_t>
{
	static Node
	encode(
		const solodata::navserver_config_t::gpsd_t& rhs)
	{
		Node node;
		node["hostname"] = rhs.hostname;
		node["port"] = rhs.port;
		node["debug_level"] = rhs.debug_level;

		node["gps_rate_hz"] = rhs.gps_rate_hz;
		node["gps_baud"] = rhs.gps_baud;

		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::navserver_config_t::gpsd_t& rhs)
	{
		rhs.hostname = node["hostname"].as<std::string>();
		rhs.port = node["port"].as<std::string>();
		rhs.debug_level = node["debug_level"].as<unsigned int>();

		rhs.gps_rate_hz = node["gps_rate_hz"].as<double>();
		rhs.gps_baud = node["gps_baud"].as<unsigned int>();

		return true;
	}
};

template<>
struct convert<solodata::navserver_config_t::zmq_t>
{
	static Node
	encode(
		const solodata::navserver_config_t::zmq_t& rhs)
	{
		Node node;
		node["nav_samp_hostname"] = rhs.nav_samp_hostname;
		node["nav_samp_hwm"] = rhs.nav_samp_hwm;

		node["init_buff_size_byptes"] = rhs.init_buff_size_byptes;
		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::navserver_config_t::zmq_t& rhs)
	{
		rhs.nav_samp_hostname = node["nav_samp_hostname"].as<std::string>();
		rhs.nav_samp_hwm = node["nav_samp_hwm"].as<int>();

		rhs.init_buff_size_byptes = node["init_buff_size_byptes"].as<size_t>();
		return true;
	}
};

template<>
struct convert<solodata::navserver_config_t>
{
	static Node
	encode(
		const solodata::navserver_config_t& rhs)
	{
		Node node;
		node["gpsd"] = rhs.gpsd;
		node["zmq"] = rhs.zmq;

		node["log_gpsdata"] = rhs.log_gpsdata;

		return node;
	}

	static bool
	decode(
		const Node& node,
		solodata::navserver_config_t& rhs)
	{
		rhs.gpsd = node["gpsd"].as<solodata::navserver_config_t::gpsd_t>();
		rhs.zmq = node["zmq"].as<solodata::navserver_config_t::zmq_t>();

		rhs.log_gpsdata = node["log_gpsdata"].as<bool>();
		
		return true;
	}
};

}// namespace YAML