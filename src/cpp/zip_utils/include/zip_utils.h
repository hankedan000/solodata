#pragma once

#include <string>
#include <vector>

namespace zip_utils
{

	/**
	 * @return
	 * True if dir is a directory, flase if it's a file.
	 */
	bool
	is_dir(
		const std::string &dir);

	/**
	 * @param[in] inputdir
	 * The directory to zip up. Including all files and subdirectories
	 * within it.
	 *
	 * @param[in] output_filename
	 * The name of the zip file to create
	 *
	 * @param[in] inclusive
	 * If set to true, then inputdir is included within the zip. If false
	 * then inputdir is treated as the root.
	 */
	void
	zip_dir(
		const std::string &inputdir,
		const std::string &output_filename,
		bool inclusive = true);

	/**
	 * @param[in] inputdirs
	 * The directories to zip up. Including all files and subdirectories
	 * within it them.
	 *
	 * @param[in] output_filename
	 * The name of the zip file to create
	 */
	void
	zip_multi_dir(
		const std::vector<std::string> &inputdirs,
		const std::string &output_filename);

}
