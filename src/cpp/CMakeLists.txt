add_subdirectory(NavMath)
add_subdirectory(ConfigTypes)
add_subdirectory(fs_utils)
add_subdirectory(zip_utils)
# don't bother running nlohmann_json tests
set(JSON_BuildTests OFF CACHE INTERNAL "")
# safe to set if running gcc greater than 7.1
# TODO add gcc compiler version check here
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-psabi")
add_subdirectory(nlohmann_json)
add_subdirectory(cand)
add_subdirectory(NavServer)
add_subdirectory(NavClient)
add_subdirectory(DataLogger)
