#include <zmq.h>
#include <iostream>

#include "DataLogger.pb.h"

using namespace solodata::DataLogger;

void
show_usage()
{
	std::cout << "usage: dlctrl <hostname> start|stop" << std::endl;
	std::cout << "  hostname | optional and defaults to localhost:5051" << std::endl;
	std::cout << "  start    | requests DataLogger to start recording" << std::endl;
	std::cout << "  stop     | requests DataLogger to stop recording" << std::endl;
}

int
main(
	int argc,
	const char **argv)
{
	// parse host argument
	std::string hostname;
	int cmd_idx = 1;
	if (argc >= 3)
	{
		hostname = argv[1];
		cmd_idx = 2;
	}
	else if (argc == 2)
	{
		// assume localhost
		hostname = "localhost:5051";
	}
	else
	{
		std::cerr << "Too few arguments!" << std::endl;
		show_usage();
		exit(EXIT_FAILURE);
	}

	// ---------------------------

	// parse command argument
	bool valid_cmd = false;
	ControlRequest_msg control;
	control.set_record_nav(true);
	control.set_record_can(true);
	std::string argv_str(argv[cmd_idx]);
	if (argv_str == "start")
	{
		control.set_record_control(RecordControl_E::START);
		valid_cmd = true;
	}
	else if (argv_str == "stop")
	{
		control.set_record_control(RecordControl_E::STOP);
		valid_cmd = true;
	}

	if ( ! valid_cmd)
	{
		std::cerr << "No valid command specified!" << std::endl;
		show_usage();
		exit(EXIT_FAILURE);
	}

	std::string full_hostname("tcp://");
	full_hostname += hostname;
	std::cout << "Connecting to DataLogger @ " << full_hostname << std::endl;
	// Socket to talk to responder
	void *context = zmq_ctx_new();
	void *control_req = zmq_socket(context,ZMQ_REQ);
	if (zmq_connect(control_req,full_hostname.c_str()) < 0)
	{
		std::cerr << "zmq_connect failed!" << zmq_strerror(errno) << std::endl;
	}
	else
	{
		uint8_t buffer[1024];
		control.SerializeToArray((void*)buffer,sizeof(buffer));
		std::cout << "Sending request and waiting for reply..." << std::endl;
		zmq_send(control_req,(const void *)buffer,control.ByteSizeLong(),0);
		int rbytes = zmq_recv(control_req,buffer,sizeof(buffer),0);
		if (rbytes < 0)
		{
			std::cerr << "zmq_recv failed!" << zmq_strerror(errno) << std::endl;
		}
		else
		{
			ControlReply_msg reply;
			if (reply.ParseFromArray(buffer,rbytes))
			{
				std::cout << "Received Reply!" << std::endl;
				std::cout << reply.DebugString() << std::endl;
			}
		}
	}

	zmq_close(control_req);
	zmq_ctx_destroy(context);

	return 0;
}