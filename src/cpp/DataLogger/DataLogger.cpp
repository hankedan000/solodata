#include <ctime>
#include "cand_client.h"
#include "datalogger_config.h"
#include <getopt.h>
#include <list>
#include <Logging.h>
#include <protorecord.h>
#include <ZMQ_Boilerplate.h>
#include <SharedStopwatch.h>
#include <fineftp/server.h>
#include <thread>
#include "zip_utils.h"
#include "file_utils.h"
#include "cand_config.h"
#include "navserver_config.h"

// include protobuf header files
#include "CAN_Sample.pb.h"
#include "DataLogger.pb.h"
#include "NavSample.pb.h"

// include ZeroMQ headers
#include <zmq.h>

#define STATUS_HOSTNAME "tcp://*:5050"
#define CONTROL_HOSTNAME "tcp://*:5051"

using namespace solodata::DataLogger;

SET_LOGGING_CFG_NAME("solodata_logging.cfg");
ZMQ_Boilerplate boilerplate("DataLogger");
solodata::datalogger_config_t config;
// Create an FTP Server on port 2121. We use 2121 instead of the default port
// 21, as we would need root privileges to open port 21.
fineftp::FtpServer ftp_server(2121);
std::string curr_record_dir;
protorecord::Writer nav_sample_writer;
protorecord::Writer can_sample_writer;
protorecord::Writer can_index_writer;
StopwatchThrottle throttle(10.0);// log summary every 10s

bool daemonize = false;
// set to false once we receive the first sample from publisher
bool first_nav_sample = true;
bool first_can_sample = true;
// set if a droped sample was detect. clear when status is sent
bool nav_drop_occurred = false;
bool can_drop_occurred = false;
uint64_t total_nav_samples_seen = 0;
uint64_t total_can_samples_seen = 0;
// if the data logger should record nav samples
bool record_nav = false;
// if the data logger should record CAN samples
bool record_can = false;
// current state of the recording
RecordState_E record_state = RecordState_E::STOPPED;

cand::Client cand_client;

// zmq sockets
void *nav_sub = nullptr;
void *can_sub = nullptr;
void *status_pub = nullptr;
void *control_rep = nullptr;

class ZippingTask
{
public:
	ZippingTask()
	 : thread_()
	 , record_name_()
	 , record_zip_path_()
	 , okay_(true)
	 , complete_(false)
	{
	}

	void
	run()
	{
		okay_ = true;

		// Zip record and then delete it if zip was successful
		try
		{
			LOG4CXX_DEBUG(boilerplate.getLogger(),
				__func__ << " - "
				"Zipping... " << record_zip_path_);

			zip_utils::zip_dir(record_name_,record_zip_path_);

			LOG4CXX_INFO(boilerplate.getLogger(),
				__func__ << " - "
				"Zipping complete for " << record_zip_path_);

			if (file_utils::rmrf(record_name_.c_str()) < 0)
			{
				LOG4CXX_ERROR(boilerplate.getLogger(),
					__func__ << " - "
					"Failed to remove " << record_name_ << "!"
					"error = " << strerror(errno));
				okay_ = false;
			}
		}
		catch (const std::runtime_error &re)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				__func__ << " - "
				"Failed to zip " << record_zip_path_ << "! " << re.what());
			okay_ = false;
		}

		// Mark completion flag so main thread can clean up
		complete_ = true;
	}

	void
	start(
		const std::string &record_name)
	{
		record_name_ = record_name;

		// Build the record's zip file path
		record_zip_path_ = config.record_dir;
		const char OS_PATH_SEPARATOR = '/';
		if (record_zip_path_.size() > 0 && record_zip_path_.back() != OS_PATH_SEPARATOR)
		{
			record_zip_path_ += OS_PATH_SEPARATOR;
		}
		record_zip_path_ += record_name_ + ".zip";

		// Start zipping task in a new thread
		complete_ = false;
		thread_ = std::thread(&ZippingTask::run,this);
	}

	void
	join()
	{
		// Chec if it's not a default constructed thread
		if (thread_.joinable())
		{
			thread_.join();
		}
	}

	bool
	is_complete() const
	{
		return complete_;
	}

	bool
	is_okay() const
	{
		return okay_;
	}

	std::string
	zip_name() const
	{
		return record_name_ + ".zip";
	}

private:
	// the thread that this task runs in
	std::thread thread_;

	// the record name this task is zipping
	std::string record_name_;

	// the zipped record path
	std::string record_zip_path_;

	// set to true if the task completed successfully
	bool okay_;

	// set to true once the ZippingTask as completed execution
	bool complete_;

};

// A list of ZippingTasks that are running. Once a task completes, the
// the status timer will check, notify record completion, and then remove
// these completed tasks from the list.
std::list<ZippingTask *> zip_tasks;

// forward declaration
bool
publish_status();

void
on_nav_sample(
	const solodata::NavSample &datum)
{
	TIME_IT();
	// used to detect sample drops from NavServer
	static uint32_t prev_sequence_num = 0;

	LOG4CXX_DEBUG(boilerplate.getLogger(),
		datum.DebugString());

	if ( ! first_nav_sample && datum.has_sequence_num() &&
		datum.sequence_num() != prev_sequence_num + 1)
	{
		LOG4CXX_WARN(boilerplate.getLogger(),
			"NavSample drop detected! DataLogger is running too slowly."
			" prev_sequence_num = " << prev_sequence_num << "; "
			" curr_sequence_num = " << datum.sequence_num() << "; ");
		nav_drop_occurred = true;
	}

	if (config.enable_stopwatch && throttle.ready())
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			TheSharedStopwatch->getSummary());
	}
	if (record_state == RecordState_E::RECORDING && record_nav)
	{
		nav_sample_writer.write(datum);
	}

	// store for next time
	if (datum.has_sequence_num())
	{
		prev_sequence_num = datum.sequence_num();
	}

	// book keeping
	first_nav_sample = false;
	total_nav_samples_seen++;
}

void
handle_can_sample(
	const solodata::CAN_Sample &datum)
{
	TIME_IT();
	// used to detect sample drops from cand
	static uint32_t prev_sequence_num = 0;

	LOG4CXX_DEBUG(boilerplate.getLogger(),
		datum.DebugString());

	if ( ! first_can_sample && datum.has_sequence_num() &&
		datum.sequence_num() != prev_sequence_num + 1)
	{
		LOG4CXX_WARN(boilerplate.getLogger(),
			"CAN_Sample drop detected! DataLogger is running too slowly."
			" prev_sequence_num = " << prev_sequence_num << "; "
			" curr_sequence_num = " << datum.sequence_num() << "; ");
		can_drop_occurred = true;
	}

	if (config.enable_stopwatch && throttle.ready())
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			TheSharedStopwatch->getSummary());
	}

	if (record_state == RecordState_E::RECORDING && record_can)
	{
		can_sample_writer.write(datum);
	}

	// store for next time
	if (datum.has_sequence_num())
	{
		prev_sequence_num = datum.sequence_num();
	}

	// book keeping
	first_can_sample = false;
	total_can_samples_seen++;
}

std::vector<uint8_t> can_buffer(8192);
void
on_can_sample(
	void *socket,
	void *arg)
{
	void *buff_data = (void *)can_buffer.data();
	size_t buff_size = can_buffer.size();
	int rc = cand::Client::recv_sample_data(socket,buff_data,buff_size,0);
	if (rc > buff_size)
	{
		std::cout << __func__ << " - "
			"listener buffer size too small to receive sample. "
			"buff_size: " << buff_size << "; "
			"recv_size: " << rc << ". dropping sample, but resizing "
			" buffer to account." << std::endl;
		can_buffer.resize(rc * 2);// double it just to be safe
		return;
	}
	else if (rc > 0)
	{
		solodata::CAN_Sample sample;
		if ( ! sample.ParseFromArray(buff_data,rc))
		{
			std::cerr << "failed to parse sample data!" << std::endl;
		}
		else
		{
			handle_can_sample(sample);
		}
	}
	else if (rc == -1 && errno != EAGAIN)
	{
		std::cerr << __func__ << " - "
			"recv_sample_data() return error " << zmq_strerror(errno) << std::endl;
		return;
	}
}

void
on_status_timer(
	int timer_id,
	void *vargs)
{
	bool okay = true;

	Status_msg msg;
	msg.set_record_state(record_state);
	msg.set_nav_recording_on(record_nav);
	msg.set_can_recording_on(record_can);
	msg.set_nav_data_valid(total_nav_samples_seen > 0);// TODO set these better
	msg.set_can_data_valid(total_can_samples_seen > 0);// TODO set these better
	msg.set_nav_drop_occurred(nav_drop_occurred);
	msg.set_can_drop_occurred(can_drop_occurred);
	msg.set_total_nav_samples_seen(total_nav_samples_seen);
	msg.set_total_can_samples_seen(total_can_samples_seen);
	msg.set_record_nav_samples_count(nav_sample_writer.size());
	msg.set_record_can_samples_count(can_sample_writer.size());

	// Notify of any new records
	for (auto task_itr=zip_tasks.begin(); task_itr!=zip_tasks.end(); task_itr++)
	{
		ZippingTask *task_ptr = *task_itr;

		if (task_ptr->is_complete())
		{
			LOG4CXX_DEBUG(boilerplate.getLogger(),
				__func__ << " - " <<
				"ZippingTask " << task_ptr->zip_name() << " completed!");

			task_ptr->join();

			// If task completed ok, then append record to status
			if (task_ptr->is_okay())
			{
				msg.add_new_records(task_ptr->zip_name());
			}

			// Remove completed tasks
			delete task_ptr;
			task_itr = zip_tasks.erase(task_itr);
		}
	}

	// Set flag is there are still active zipping task running
	msg.set_zipping_in_progress(zip_tasks.size() > 0);

	LOG4CXX_DEBUG(boilerplate.getLogger(),
		__func__ << " - " <<
		"Status_msg: " << msg.DebugString());

	static uint8_t buffer[1024];
	if (okay && ! msg.SerializeToArray((void*)buffer,sizeof(buffer)))
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			__func__ << " - "
			"Failed to serialize status message");
		okay = false;
	}

	if (okay && status_pub != nullptr)
	{
		int rc = zmq_send(
			status_pub,
			(const void *)buffer,
			msg.ByteSizeLong(),
			ZMQ_DONTWAIT);
		if (rc < 0)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				__func__ << " - "
				"zmq_send failed. " << zmq_strerror(errno));
			okay = false;
		}
	}
	else if (status_pub == nullptr)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			__func__ << " - status_pub is null!");
		okay = false;
	}

	// clear flags for next status
	nav_drop_occurred = false;
	can_drop_occurred = false;
}

bool
setup_record()
{
	bool okay = true;
	
	time_t rawtime = time(NULL);
	struct tm *timeinfo = localtime(&rawtime);
	char record_dir[1024];
	strftime(record_dir,sizeof(record_dir),"%F_%H.%M.%S",timeinfo);

	int status = mkdir(record_dir,0777);
	curr_record_dir = record_dir;
	if (okay && status < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			__func__ << " - "
			"Failed to create record directory." <<
			" error: " << strerror(errno) << "; "
			" record_dir: '" << record_dir << "'");
		okay = false;
	}

	std::string nav_path = record_dir;
	nav_path += "/nav_samples";
	if (okay && ! nav_sample_writer.open(nav_path))
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			__func__ << " - "
			"Failed to open nav_sample_writer with path " << nav_path);
		okay = false;
	}

	std::string can_samp_path = record_dir;
	can_samp_path += "/can_samples";
	if (okay && ! can_sample_writer.open(can_samp_path))
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			__func__ << " - "
			"Failed to open can_sample_writer with path " << can_samp_path);
		okay = false;
	}

	std::string can_idx_path = record_dir;
	can_idx_path += "/can_index";
	if (okay && ! can_index_writer.open(can_idx_path))
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			__func__ << " - "
			"Failed to open can_index_writer with path " << can_idx_path);
		okay = false;
	}
	else if (okay)
	{
		solodata::cand::Index can_index;
		if (cand_client.get_index(can_index) == cand::Client::OK)
		{
			can_index_writer.write(can_index);
		}
		else
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				__func__ << " - "
				"Failed to get CAN index");
			okay = false;
		}
	}

	if ( ! okay)
	{
		curr_record_dir = "";
	}

	return okay;
}

bool
stop_record()
{
	bool okay = true;

	nav_sample_writer.close();
	can_sample_writer.close();
	can_index_writer.close();

	// Start a task to zip up the record
	ZippingTask *task = new ZippingTask();
	task->start(curr_record_dir);
	zip_tasks.push_back(task);

	return okay;
}

// method called when the DataLogger received a control request from client
void
on_control_request(
	const solodata::DataLogger::ControlRequest_msg &datum)
{
	LOG4CXX_INFO(boilerplate.getLogger(),
		__func__ << " - " <<
		datum.DebugString());

	/**
	 * Set to true if the control was "accepted". For example,
	 * you can't START a recording while a recording is already
	 * in progress. In that case, the accepted_control would
	 * be left false.
	 */
	bool accepted_control = false;

	switch (datum.record_control())
	{
		case RecordControl_E::START:
			if (record_state == RecordState_E::STOPPED ||
				record_state == RecordState_E::PAUSED)
			{
				if (setup_record())
				{
					// update record state
					record_state = RecordState_E::RECORDING;
					accepted_control = true;
				}
			}
			else
			{
				LOG4CXX_ERROR(boilerplate.getLogger(),
					__func__ << " - "
					"Received request to START recording while recording is already"
					" active. Request being ignored.");
			}
			break;
		case RecordControl_E::STOP:
			if (record_state == RecordState_E::RECORDING ||
				record_state == RecordState_E::PAUSED)
			{
				stop_record();

				// update record state
				record_state = RecordState_E::STOPPED;
				accepted_control = true;
			}
			else
			{
				LOG4CXX_ERROR(boilerplate.getLogger(),
					__func__ << " - "
					"Received request to STOP recording while recording is already"
					" stopped. Request being ignored.");
			}
			break;
		case RecordControl_E::PAUSE:
			if (record_state == RecordState_E::RECORDING)
			{
				// update record state
				record_state = RecordState_E::PAUSED;
				accepted_control = true;
			}
			else
			{
				LOG4CXX_ERROR(boilerplate.getLogger(),
					__func__ << " - "
					"Received request to PAUSE recording while recording is not even"
					" active. Request being ignored.");
			}
			break;
		default:
			LOG4CXX_ERROR(boilerplate.getLogger(),
				__func__ << " - "
				"Received unsupported record_control variant!"
				" Maintaining current record_state " << record_state);
			record_state = record_state;
			break;
	}

	// above state logic has accepted the control; store current controls
	if (accepted_control)
	{
		record_nav = datum.has_record_nav() && datum.record_nav();
		record_can = datum.has_record_can() && datum.record_can();
	}

	// --------------------------------------

	// build and send the reply to request with new record state
	ControlReply_msg reply;
	reply.set_record_state(record_state);
	
	static uint8_t reply_buffer[1024];
	if (reply.SerializeToArray((void*)reply_buffer,sizeof(reply_buffer)))
	{
		int rc = zmq_send(
			control_rep,
			(const void *)reply_buffer,
			reply.ByteSizeLong(),
			ZMQ_DONTWAIT);
		if (rc < 0)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				__func__ << " - "
				"zmq_send failed. " << zmq_strerror(errno));
		}
	}
	else
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			__func__ << " - "
			"Failed to serialize reply message");
	}
}

bool
init_zmq(
	const solodata::navserver_config_t::zmq_t &ns_zmq_cfg,
	const solodata::cand_config_t::zmq_t &cand_zmq_cfg)
{
	bool okay = true;

	// Socket to listen to NavSamples
	nav_sub = boilerplate.create_socket(ZMQ_SUB);
	if (okay && zmq_connect(nav_sub,ns_zmq_cfg.nav_samp_hostname.c_str()) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to connect nav socket to " << ns_zmq_cfg.nav_samp_hostname << "."
			" error = " << zmq_strerror(errno));
		okay = false;
	}
	if (okay && zmq_setsockopt(nav_sub,ZMQ_SUBSCRIBE,"",0) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to create nav subscriber."
			" error = " << zmq_strerror(errno));
		okay = false;
	}
	if (okay && ! boilerplate.reactor().add_pb_listener(nav_sub,on_nav_sample))
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to add nav subscriber listener to reactor.");
		okay = false;
	}

	if (okay)
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"Connected to NavServer @ " << ns_zmq_cfg.nav_samp_hostname);
	}

	// -----------------------

	// Socket to listen to CAN_Samples
	if (cand_client.connect(boilerplate.context(),nullptr) != cand::Client::OK)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to connect to cand via cand::Client");
		okay = false;
	}
	if (okay && cand_client.get_sample_subscriber(boilerplate.context(),can_sub) != cand::Client::OK)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to get CAN sample subscriber socket form cand::Client");
		okay = false;
	}
	if (okay && ! boilerplate.reactor().add_user_socket_listener(can_sub,on_can_sample,nullptr))
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to add CAN subscriber listener to reactor.");
		okay = false;
	}

	if (okay)
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"Connected to cand!");
	}

	// -----------------------

	// Socket to send period status messages back to client
	status_pub = boilerplate.create_socket(ZMQ_PUB);
	if (okay && zmq_bind(status_pub,STATUS_HOSTNAME) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to create status publisher."
			" error = " << zmq_strerror(errno));
		okay = false;
	}

	// -----------------------

	// Socket to listen and reply to control requests from
	control_rep = boilerplate.create_socket(ZMQ_REP);
	if (okay && zmq_bind(control_rep,CONTROL_HOSTNAME) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to create control reply socket. "
			"host = " << CONTROL_HOSTNAME << "; "
			"error = " << zmq_strerror(errno));
		okay = false;
	}
	if (okay && ! boilerplate.reactor().add_pb_listener(control_rep,on_control_request))
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to add control request listener to reactor.");
		okay = false;
	}

	return okay;
}

bool
init_ftp()
{
	bool okay = true;

	// create the output recording directory
	int status = mkdir(config.record_dir.c_str(),0777);
	if (okay && status < 0 && errno != EEXIST)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			__func__ << " - "
			"Failed to create record output directory." <<
			" error: " << strerror(errno) << "; "
			" record_dir: '" << config.record_dir << "'");
		okay = false;
	}

	if (okay && ! ftp_server.addUser(
		"logger",
		"password",
		config.record_dir,
		fineftp::Permission::ReadOnly))
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			__func__ << " - "
			"Failed to add user/path to FTP server");
		okay = false;
	}

	// Start the FTP Server with a thread-pool size of 4.
	if (okay && ! ftp_server.start(4))
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			__func__ << " - "
			"Failed to start the FTP server");
		okay = false;
	}

	return okay;
}

void
display_usage()
{
	printf("DataLogger [options]\n");
	printf(" -f                : path to config file\n");
	printf(" --daemon          : run as daemon process\n");
	printf(" -h --help         : display this menu\n");
}

int
main(
	int argc,
	char **argv)
{
	bool okay = true;
	int daemon_flag = 0;
	std::string cfg_filepath = "";
	static struct option long_options[] =
	{
		{"daemon"         , no_argument      , &daemon_flag    , 1  },
		{"help"           , no_argument      , 0               , 'h'},
		{0, 0, 0, 0}
	};

	while (true)
	{
		int option_index = 0;
		int c = getopt_long(
			argc,
			argv,
			"hf:",
			long_options,
			&option_index);

		// detect the end of the options
		if (c == -1)
		{
			break;
		}

		switch (c)
		{
			case 0:
				// flag setting
				break;
			case 'f':
				cfg_filepath = optarg;
				break;
			case 'h':
			case '?':
				display_usage();
				exit(0);
				break;
			default:
				printf("Unsupported option '%c'\n",(char)c);
				display_usage();
				exit(1);
				break;
		}
	}

	if ( ! Logging::was_init_from_config())
	{
		// reduce default logging level
		boilerplate.getLogger()->setLevel(log4cxx::Level::getInfo());
	}

	if (daemon_flag)
	{
		boilerplate.daemonize();
	}

	// setup/parse config options
	solodata::navserver_config_t ns_config;
	solodata::cand_config_t cand_config;
	if (cfg_filepath != "")
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"parsing config from '" << cfg_filepath << "'");
		YAML::Node cfg_node = YAML::LoadFile(cfg_filepath);
		config = cfg_node["datalogger"].as<solodata::datalogger_config_t>();
		ns_config = cfg_node["navserver"].as<solodata::navserver_config_t>();
		cand_config = cfg_node["cand"].as<solodata::cand_config_t>();
	}

	if (okay && config.enable_stopwatch)
	{
		TheSharedStopwatch->enabled(config.enable_stopwatch);
		LOG4CXX_WARN(boilerplate.getLogger(),
			"Stopwatch is enabled.");
	}

	// init pub/sub connections
	okay = okay && init_zmq(ns_config.zmq,cand_config.zmq);

	// setup ftp server
	okay = okay && init_ftp();

	// schedule periodic timer to publish status
	const size_t STATUS_TIMER_INTERVAL_MS = 1000;
	int timer_id;
	okay = okay && boilerplate.reactor().add_timer_listener(
		STATUS_TIMER_INTERVAL_MS,
		-1,
		timer_id,
		on_status_timer,
		nullptr);

	// start listening to imu and gps
	if (okay && ! boilerplate.reactor().start())
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"failed to start reactor!");
		okay = false;
	}
	LOG4CXX_INFO(boilerplate.getLogger(),
		"Shutting down...");

	// Wait for all zipping task to complete and join
	if ( ! zip_tasks.empty())
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"Waiting for " << zip_tasks.size() << " ZippingTask(s) to complete...");
	}

	while ( ! zip_tasks.empty())
	{
		ZippingTask *task = zip_tasks.front();
		if (task->is_complete())
		{
			task->join();
			delete task;
			zip_tasks.pop_front();
		}
	}

	ftp_server.stop();
	cand_client.close();
	if (can_sub != nullptr)
	{
		zmq_close(can_sub);
		can_sub = nullptr;
	}

	return okay ? EXIT_SUCCESS : EXIT_FAILURE;
}