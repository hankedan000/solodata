#include <ftw.h>

namespace file_utils
{

	inline
	int unlink_cb(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
	{
		return remove(fpath);
	}

	/**
	 * Recursively deletes a directory
	 *
	 * @param[in] path
	 * directory path
	 *
	 * @return
	 * On success, 0 is returned. On fail, -1 is returned and errno is set appropriately.
	 */
	inline
	int rmrf(const char *path)
	{
		return nftw(path, unlink_cb, 64, FTW_DEPTH | FTW_PHYS);
	}

}