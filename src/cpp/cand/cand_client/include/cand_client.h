#pragma once

#include <functional>
#include <memory>
#include <stdint.h>
#include <thread>
#include <vector>
#include <zmq.h>

#include "cand_ctrl.pb.h"
#include "CAN_Sample.pb.h"

namespace cand
{

	class Client
	{
	public:
		static const int OK            = 0;
		static const int FAILED        = -10;
		static const int NOT_CONNECTED = -20;
		static const int TIMEOUT       = -30;

		using SampleCallbackFunc = std::function<void (const solodata::CAN_Sample &)>;

	public:
		Client();

		~Client();

		void
		close();

		/**
		 * @param[in] zmq_ctx
		 * a ZMQ context to use.
		 * if null, one will be created internally
		 * 
		 * @param[in] hostname
		 * the control socket hostname to connect to.
		 * if null, the default of "tcp://localhost:<CAND_DEFAULT_CTRL_PORT>"
		 * will be used.
		 * 
		 * @return
		 * OK on success
		 * FAILED on failure
		 */
		int
		connect(
			void *zmq_ctx,
			const char *hostname);

		/**
		 * Creates and connects a sample subscriber socket.
		 * 
		 * @param[in] zmq_ctx
		 * the zmq context to use when creating the socket.
		 * if NULL, the internal context will be used.
		 * 
		 * @param[out] socket
		 * the created socket. the caller owns the socket, so it's
		 * responsible for calling zmq_close() on the socket.
		 * 
		 * @return
		 * OK - on success
		 * NOT_CONNECT - client hasn't been connected to cand yet via connect()
		 * FAILED - on connection failure
		 */
		int
		get_sample_subscriber(
			void *zmq_ctx,
			void *&socket);

		/**
		 * Receives a cand sample message from the provided socket
		 * 
		 * @param[in] socket
		 * the sample subscriber socket to recv a sample from
		 * 
		 * @param[in] zmsg
		 * the zmq_msg_t to use when receiving the sample. caller is responsible
		 * for initializing the message via zmq_msg_init(), zmq_msg_init_data(),
		 * or zmq_msg_init_size()
		 * 
		 * @return
		 * return number of bytes in the message if successful. Note that the value
		 * can exceed the value of the len parameter in case the message was truncated.
		 * If not successful the function shall return -1 and set errno to one of the
		 * values defined below.
		 */
		static int
		recv_sample_data(
			void *socket,
			void *data,
			size_t size,
			int flags = ZMQ_DONTWAIT);

		int
		listen(
			const SampleCallbackFunc &cb);

		int
		unlisten();

		/**
		 * @param[out] index
		 * the returned cand index if successful
		 * 
		 * @return
		 * OK on success
		 */
		int
		get_index(
			solodata::cand::Index &index);

		/**
		 * @param[out] cand_status
		 * the returned CAND_Status if successful
		 * 
		 * @return
		 * OK on success
		 */
		int
		get_status(
			solodata::cand::CAND_Status &cand_status);

	private:
		int
		wait_for_connect();

		int
		do_request(
			const solodata::cand::Control &ctrl,
			solodata::cand::Response &resp);

		// forward declare
		struct ListenerObjects;

		void
		listener_thread_main(
			std::shared_ptr<ListenerObjects> listener_objs_ptr);

	private:
		// the zmq context to use
		void *zmq_ctx_;

		// true if this class owns the zmq context
		bool ctx_owned_;

		// the ZMQ REQ socket used to make requests to cand
		void *req_;
		void *mon_socket_;// monitor socket for req_

		std::vector<uint8_t> buffer_;

		// set false to request listeners to stop
		bool listeners_stay_alive_;
		struct ListenerObjects
		{
			std::thread thread;
			void *socket;// SUB socket
			SampleCallbackFunc callback;

			// buffer used to receive sample data from socket
			std::vector<uint8_t> buffer;
		};

		std::vector<std::shared_ptr<ListenerObjects>> listeners_;

	};

}