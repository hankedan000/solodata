#include "cand_client.h"

#include "../cand_constants.h"

namespace cand
{

	Client::Client()
	 : zmq_ctx_(nullptr)
	 , ctx_owned_(false)
	 , req_(nullptr)
	 , mon_socket_(nullptr)
	 , buffer_()
	 , listeners_stay_alive_(true)
	 , listeners_()
	{
		buffer_.resize(8192);
	}

	Client::~Client()
	{
		close();
	}

	void
	Client::close()
	{
		// wait for listeners to join
		listeners_stay_alive_ = false;
		for (auto &listener : listeners_)
		{
			if (listener->thread.joinable())
			{
				listener->thread.join();
			}
			if (listener->socket != nullptr)
			{
				zmq_close(listener->socket);
				listener->socket = nullptr;
			}
		}
		listeners_.clear();

		if (req_ != nullptr)
		{
			zmq_close(req_);
			req_ = nullptr;
		}
		if (mon_socket_ != nullptr)
		{
			zmq_close(mon_socket_);
			mon_socket_ = nullptr;
		}
		if (ctx_owned_ && zmq_ctx_ != nullptr)
		{
			zmq_ctx_destroy(zmq_ctx_);
			ctx_owned_ = false;
		}
		zmq_ctx_ = nullptr;
	}

	/**
	 * @param[in] zmq_ctx
	 * a ZMQ context to use.
	 * if null, one will be created internally
	 * 
	 * @param[in] hostname
	 * the control socket hostname to connect to.
	 * if null, the default of "tcp://localhost:<CAND_DEFAULT_CTRL_PORT>"
	 * will be used.
	 */
	int
	Client::connect(
		void *zmq_ctx,
		const char *hostname)
	{
		// try to use the user provided context
		zmq_ctx_ = zmq_ctx;
		if (zmq_ctx_ == nullptr)
		{
			// still null, then make our own context
			zmq_ctx_ = zmq_ctx_new();
			ctx_owned_ = true;
		}

		const std::string DEFAULT_HOSTNAME(
			std::string("tcp://localhost:") + std::to_string(CAND_DEFAULT_CTRL_PORT));
		std::string host = DEFAULT_HOSTNAME;
		if (hostname != nullptr)
		{
			host = hostname;
		}
		req_ = zmq_socket(zmq_ctx_,ZMQ_REQ);
		if (req_ == nullptr)
		{
			std::cerr << __func__ << "() - zmq_socket failed! error: " << zmq_strerror(errno) << std::endl;
			return FAILED;
		}

		// create a monitor socket to watch for connect event
		char mon_endpoint[1024];
		int events = ZMQ_EVENT_ALL;
		sprintf(mon_endpoint, "inproc://cand_client_monitor-%p", this);
		int rc = zmq_socket_monitor(req_, mon_endpoint, events);
		if (rc < 0)
		{
			std::cerr << __func__ << "() - "
				"zmq_socket_monitor() failed! "
				"error: " << zmq_strerror(errno) << "; "
				"mon_endpoint: " << mon_endpoint << "; " << std::endl;
			return FAILED;
		}
		mon_socket_ = zmq_socket(zmq_ctx_, ZMQ_PAIR);
		rc = zmq_connect(mon_socket_, mon_endpoint);
		if (rc < 0)
		{
			std::cerr << __func__ << "() - "
				"zmq_connect() failed! "
				"error: " << zmq_strerror(errno) << "; "
				"mon_endpoint: " << mon_endpoint << "; " << std::endl;
			zmq_close(mon_socket_);
			return FAILED;
		}

		// connect to cand
		if (zmq_connect(req_,host.c_str()) < 0)
		{
			std::cerr << __func__ << "() - zmq_connect failed! error: " << zmq_strerror(errno) << std::endl;
			zmq_close(req_);
			req_ = nullptr;
			return FAILED;
		}

		const int timeout = 1000;
		if (zmq_setsockopt(req_, ZMQ_RCVTIMEO, &timeout, sizeof(timeout)) < 0)
		{
			std::cerr << __func__ << "() - failed to set recv socket timeout! error: " << zmq_strerror(errno) << std::endl;
			zmq_close(req_);
			req_ = nullptr;
			return FAILED;
		}

		return wait_for_connect();
	}

	int
	Client::get_sample_subscriber(
		void *zmq_ctx,
		void *&socket)
	{
		// get status from cand so we can acquire it's sample socket hostname
		solodata::cand::CAND_Status cand_status;
		int rc = get_status(cand_status);
		if (rc != OK)
		{
			return rc;
		}

		// use internal context if one wasn't provided
		if (zmq_ctx == nullptr)
		{
			zmq_ctx = zmq_ctx_;
		}

		// create the sample subscriber socket
		socket = zmq_socket(zmq_ctx, ZMQ_SUB);
		if (socket == nullptr)
		{
			std::cerr << __func__ << "() - "
				"failed to create sub socket! "
				"error: " << zmq_strerror(errno) << std::endl;
			return FAILED;
		}

		// connect the sample subscriber to cand
		int zmq_rc = zmq_connect(socket,cand_status.sample_hostname().c_str());
		if (zmq_rc < 0)
		{
			std::cerr << __func__ << "() - "
				"failed to connect sub socket! "
				"sample_hostname: " << cand_status.sample_hostname() << "; "
				"error: " << zmq_strerror(errno) << std::endl;
			zmq_close(socket);
			socket = nullptr;
			return FAILED;
		}
		zmq_rc = zmq_setsockopt(socket,ZMQ_SUBSCRIBE,"",0);
		if (zmq_rc < 0)
		{
			std::cerr << __func__ << "() - "
				"failed to set subscriber filter on socket! "
				"error: " << zmq_strerror(errno) << std::endl;
			zmq_close(socket);
			socket = nullptr;
			return FAILED;
		}

		return OK;
	}

	int
	Client::recv_sample_data(
		void *socket,
		void *data,
		size_t size,
		int flags)
	{
		// TODO add logic to receive multi-part message when device filtering is implemented
		// device name string will be in the 1st part; sample data in 2nd part
		return zmq_recv(socket,data,size,flags);
	}

	int
	Client::listen(
		const SampleCallbackFunc &cb)
	{
		void *socket;
		int rc = get_sample_subscriber(zmq_ctx_,socket);
		if (rc != OK)
		{
			return rc;
		}

		auto new_lobjs = std::shared_ptr<ListenerObjects>(new ListenerObjects());
		new_lobjs->callback = cb;
		new_lobjs->socket = socket;
		new_lobjs->thread = std::thread(&Client::listener_thread_main,this,new_lobjs);
		new_lobjs->buffer.resize(8192);
		listeners_.push_back(new_lobjs);

		return OK;
	}

	int
	Client::get_index(
		solodata::cand::Index &index)
	{
		solodata::cand::Control ctrl;
		solodata::cand::Response resp;
		ctrl.set_request_type(solodata::cand::RequestType_E::GET_INDEX);

		int rc = do_request(ctrl,resp);
		if (rc != OK)
		{
			return rc;
		}

		index.Clear();
		if (resp.has_index())
		{
			index = resp.index();
		}

		return OK;
	}

	int
	Client::get_status(
		solodata::cand::CAND_Status &cand_status)
	{
		solodata::cand::Control ctrl;
		solodata::cand::Response resp;
		ctrl.set_request_type(solodata::cand::RequestType_E::GET_STATUS);

		int rc = do_request(ctrl,resp);
		if (rc != OK)
		{
			return rc;
		}

		if ( ! resp.has_cand_status())
		{
			return FAILED;
		}

		cand_status = resp.cand_status();
		return OK;
	}

	int
	Client::wait_for_connect()
	{
		bool connected = false;
		unsigned int itr = 0;
		while ( ! connected && itr++ < 10)
		{
			// First frame in message contains event number and value
			zmq_msg_t msg;
			zmq_msg_init (&msg);
			if (zmq_msg_recv (&msg, mon_socket_, 0) == -1)
				return FAILED; // Interruped, presumably
			assert (zmq_msg_more (&msg));

			uint8_t *data = (uint8_t *) zmq_msg_data (&msg);
			uint16_t event = *(uint16_t *) (data);

			// Second frame in message contains event address
			zmq_msg_init (&msg);
			if (zmq_msg_recv (&msg, mon_socket_, 0) == -1)
				return FAILED; // Interruped, presumably
			assert (!zmq_msg_more (&msg));

			char address[1024];
			data = (uint8_t *) zmq_msg_data (&msg);
			size_t size = zmq_msg_size (&msg);
			if (sizeof(address) < (size+1)) {
				std::cerr <<
					"insufficient buffer size for address of size " << size << std::endl;
				return FAILED;
			}
			memcpy(address, data, size);
			address[size] = 0;

			connected = (event == ZMQ_EVENT_CONNECTED);
		}

		return (connected ? OK : NOT_CONNECTED);
	}

	int
	Client::do_request(
		const solodata::cand::Control &ctrl,
		solodata::cand::Response &resp)
	{
		if (req_ == nullptr)
		{
			return NOT_CONNECTED;
		}

		auto ctrl_size = ctrl.ByteSizeLong();
		if (buffer_.size() < ctrl_size)
		{
			buffer_.resize(ctrl_size);
		}
		ctrl.SerializeToArray((void*)buffer_.data(),ctrl_size);
		zmq_send(req_,(const void *)buffer_.data(),ctrl_size,0);
		int rbytes = zmq_recv(req_,buffer_.data(),buffer_.size(),0);
		if (rbytes < 0)
		{
			if (errno == EAGAIN)
			{
				std::cerr << __func__ << "() - request timed out!" << std::endl;
				return TIMEOUT;
			}
			else
			{
				std::cerr << __func__ << "() - zmq_recv failed!" << zmq_strerror(errno) << std::endl;
				return FAILED;
			}
		}
		else
		{
			if ( ! resp.ParseFromArray(buffer_.data(),rbytes))
			{
				return FAILED;
			}

			if (resp.status() != solodata::cand::ResponseStatus_E::RS_OK)
			{
				std::cerr << __func__ << "() - received bad response status! resp = " << resp.DebugString() << std::endl;
				return FAILED;
			}
		}

		return OK;
	}

	void
	Client::listener_thread_main(
		std::shared_ptr<ListenerObjects> listener_objs_ptr)
	{
		auto socket = listener_objs_ptr->socket;
		const int timeout = 1000;
		if (zmq_setsockopt(socket, ZMQ_RCVTIMEO, &timeout, sizeof(timeout)) < 0)
		{
			std::cerr << __func__ << "() - "
				"failed to set recv socket timeout! "
				"error: " << zmq_strerror(errno) << std::endl;
			return;
		}

		while (listeners_stay_alive_)
		{
			void *buff_data = (void *)listener_objs_ptr->buffer.data();
			size_t buff_size = listener_objs_ptr->buffer.size();
			int rc = recv_sample_data(socket,buff_data,buff_size,0);
			if (rc > buff_size)
			{
				std::cout << __func__ << " - "
					"listener buffer size too small to receive sample. "
					"buff_size: " << buff_size << "; "
					"recv_size: " << rc << ". dropping sample, but resizing "
					" buffer to account." << std::endl;
				listener_objs_ptr->buffer.resize(rc * 2);// double it just to be safe
				continue;
			}
			else if (rc > 0)
			{
				solodata::CAN_Sample sample;
				if ( ! sample.ParseFromArray(buff_data,rc))
				{
					std::cerr << "failed to parse sample data!" << std::endl;
				}
				else
				{
					listener_objs_ptr->callback(sample);
				}
			}
			else if (rc == -1 && errno != EAGAIN)
			{
				std::cerr << __func__ << " - "
					"recv_sample_data() return error " << zmq_strerror(errno) << std::endl;
				continue;
			}
		}
	}

}