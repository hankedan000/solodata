#pragma once

#include <LoggingObject.h>
#include <ZMQ_Boilerplate.h>

#include "cand_ctrl.pb.h"
#include "cand_globals.h"
#include "ThreadWrapper.h"

namespace cand
{

class ControlThread : public LoggingObject, public concrt::ThreadWrapper
{
public:
	ControlThread() = delete;

	ControlThread(
		cand::GlobalsPtr globals,
		ZMQ_Boilerplate &boilerplate,
		const std::string &logging_prefix = "");

	void
	on_control(
		const solodata::cand::Control &ctrl);

protected:
	virtual
	void
	thread_main();

private:
	cand::GlobalsPtr globals_;

	// zmq REP sockets used to wait for requests
	void *rep_;

};

}