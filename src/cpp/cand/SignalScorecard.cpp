#include "SignalScorecard.h"

#include <chrono>

namespace solodata
{

// ----------------------------------
// Public Methods
// ----------------------------------
SignalScorecard::SignalScorecard(
	const CppCAN::CANDatabase &cdb)
 : LoggingObject("SignalScorecard")
 , expected_ids_()
 , training_map_()
 , first_id_(-1)
 , last_id_(-1)
 , training_complete_(false)
{
	for (const auto &entry : cdb)
	{
		const CppCAN::CANFrame &frame = entry.second;
		training_map_.insert({
			frame.can_id(),
			{(uint32_t)(frame.can_id())}});
	}
}

int
SignalScorecard::add_frame_training(
	const can_frame &frame)
{
	// get timestamp for when signal was received
	auto samp_time = std::chrono::system_clock::now();
	auto samp_time_us = std::chrono::time_point_cast<std::chrono::microseconds>(samp_time);
	auto epoch = samp_time_us.time_since_epoch();

	if (training_complete_)
	{
		LOG4CXX_WARN(getLogger(),
			__func__ << " - "
			"training was already complete. bug???");
		return TRAINING_COMPLETE;
	}

	auto entry = training_map_.find(frame.can_id);
	if (entry == training_map_.end())
	{
		return UNKNOWN_FRAME;
	}

	if (entry->second.timestamps.size() == MAX_TRAINING_INTERVALS)
	{
		do_training();
		if (training_complete_)
		{
			return TRAINING_COMPLETE;
		}
	}

	entry->second.timestamps.push_back(std::chrono::duration_cast<std::chrono::microseconds>(epoch));

	return TRAINING_NOT_COMPLETE;
}

int
SignalScorecard::add_frame(
	FrameMap &fm,
	const can_frame &frame)
{
	// get timestamp for when signal was received
	auto samp_time = std::chrono::system_clock::now();
	auto samp_time_us = std::chrono::time_point_cast<std::chrono::microseconds>(samp_time);
	auto epoch = samp_time_us.time_since_epoch();

	if ( ! training_complete_)
	{
		LOG4CXX_WARN(getLogger(),
			__func__ << " - "
			"training not complete. can't add_frame(). bug???");
		return TRAINING_NOT_COMPLETE;
	}

	auto entry = fm.find(frame.can_id);
	if (entry == fm.end())
	{
		return UNKNOWN_FRAME;
	}
	else if (entry->second.received)
	{
		return DUPLICATE_ADD;
	}

	entry->second.timestamp = std::chrono::duration_cast<std::chrono::microseconds>(epoch);

	// copy frame data and mark received
	memcpy(entry->second.data,frame.data,entry->second.dlc);
	entry->second.received = true;

	// check for sentinal frame completion
	if (frame.can_id == last_id_)
	{
		return READY;
	}

	return NOT_READY;
}

void
SignalScorecard::init_frame_map(
	const CppCAN::CANDatabase &cdb,
	FrameMap &fm) const
{
	if ( ! training_complete_)
	{
		throw std::runtime_error("can't init because training isn't complete yet");
	}
	fm.clear();
	for (auto expected_id : expected_ids_)
	{
		const CppCAN::CANFrame &db_frame = cdb.at(expected_id);
		fm.insert({
			expected_id,
			{expected_id,db_frame.dlc()}});
	}
}

void
SignalScorecard::reset(
	FrameMap &fm) const
{
	for (auto &entry: fm)
	{
		entry.second.received = false;
	}
}

bool
SignalScorecard::is_training() const
{
	return ! training_complete_;
}

const std::set<uint32_t> &
SignalScorecard::get_expected_ids() const
{
	if (is_training())
	{
		throw std::runtime_error("can't call get_expected_ids() while scorecard is training");
	}
	return expected_ids_;
}

void
SignalScorecard::do_training()
{
	/**
	 * Training is trying to do two things...
	 * 
	 * 1) Learn which frames in the database we will expect
	 * to see as part of a "burst". A burst is a contiguous
	 * stream of CAN frames transmitted by a node at a regular
	 * interval.
	 * 
	 * 2) Learn which frames mark the beginning and end of
	 * a burst.
	 */
	if (training_complete_)
	{
		LOG4CXX_WARN(getLogger(),
			__func__ << " - "
			"training has already been completed. not doing it again. bug???");
		return;
	}

	// start by sorting all frame ids based on time
	std::map<std::chrono::microseconds, uint32_t> ids_by_time;
	for (auto &entry : training_map_)
	{
		if (entry.second.timestamps.size() > 0)
		{
			expected_ids_.insert(entry.first);
			for (const auto &ts : entry.second.timestamps)
			{
				ids_by_time[ts] = entry.second.can_id;
			}
		}
	}

	if (ids_by_time.size() == 0)
	{
		LOG4CXX_WARN(getLogger(),
			__func__ << " - "
			"do_training() called, but there are no frames to train with!");
		return;
	}

	// measure delta times between frames to find the first/last frame in the burst
	std::chrono::microseconds max_delta_sum = std::chrono::microseconds().min();
	bool is_first = true;
	bool first_last_found = false;
	std::chrono::microseconds prev_ts = std::chrono::microseconds{0};
	unsigned int idx = 0;
	uint32_t prev_id = 0;
	for (const auto &entry : ids_by_time)
	{
		auto curr_ts = entry.first;
		auto &stats = training_map_.at(entry.second);
		stats.count++;
		if ( ! is_first)
		{
			auto delta = curr_ts - prev_ts;
			if (false)
			{
				printf("[%-4d] - cand_id: 0x%04x; delta: %5ldus\n",
					idx,
					entry.second,
					delta.count());
			}

			stats.predecessors.insert(prev_id);
			if (stats.count > 1)
			{
				stats.delta_sum += delta;
				if (stats.delta_sum > max_delta_sum)
				{
					max_delta_sum = stats.delta_sum;
					first_id_ = entry.second;
					last_id_ = prev_id;
					first_last_found = true;
				}
			}
		}

		prev_ts = curr_ts;
		prev_id = entry.second;
		is_first = false;
		idx++;
	}

	bool show_stats = false;
	if (first_last_found)
	{
		LOG4CXX_INFO(getLogger(),
			__func__ << " - "
			"training complete! "
			"first_frame_id: 0x" << std::hex << first_id_ << "; "
			"last_frame_id: 0x" << std::hex << last_id_ << "; ");

		auto &stats = training_map_.at(first_id_);
		if (stats.predecessors.size() > 1)
		{
			LOG4CXX_WARN(getLogger(),
				"first frame (0x" << std::hex << first_id_ << ") had more than 1"
				" predecessor. Seems like frames were dropped during the training"
				" phase.");
			show_stats = true;
		}
		training_complete_ = true;
	}
	else
	{
		LOG4CXX_ERROR(getLogger(),
			__func__ << " - "
			"could not find first and last frames in burst. training incomplete!");
		training_complete_ = false;
	}

	if (show_stats)
	{
		printf("training_map_:\n");
		for (const auto &entry : training_map_)
		{
			if (entry.second.count == 0)
			{
				continue;
			}

			const char *comment = "";
			if (entry.first == first_id_)
			{
				comment = "// first frame in burst";
			}
			else if (entry.first == last_id_)
			{
				comment = "// last frame in burst";
			}
			printf("0x%04x - count: %2d; delta_sum: %6ldus; predecessors: %ld;%s\n",
				entry.first,// can_id
				entry.second.count,
				entry.second.delta_sum.count(),
				entry.second.predecessors.size(),
				comment);
		}
	}
}

// ----------------------------------
// Private Methods
// ----------------------------------

}// namespace "solodata"
