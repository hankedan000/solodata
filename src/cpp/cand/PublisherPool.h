#pragma once

#include <ResourcePool.h>

#include "CAN_Sample.pb.h"

namespace cand
{
	struct PublisherResource
	{
		solodata::CAN_Sample sample;
	};

	using PublisherPool = concrt::ResourcePool<PublisherResource,concrt::PC_Model::MPSC>;
}
