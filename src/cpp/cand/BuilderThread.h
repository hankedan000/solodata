#pragma once

#include <cpp-can-parser/CANDatabase.h>
#include <LoggingObject.h>
#include <memory>
#include <ResourcePool.h>
#include <unordered_map>

#include "cand_config.h"
#include "cand_globals.h"
#include "SignalScorecard.h"
#include "ThreadWrapper.h"

namespace cand
{

struct BuilderResource
{
	solodata::SignalScorecard::FrameMap frames;
};
using BuilderPool = concrt::ResourcePool<BuilderResource,concrt::PC_Model::SPSC>;

class BuilderThread : public LoggingObject, public concrt::ThreadWrapper
{
public:
	static const int POST_TAKEN    = 0;
	static const int FRAME_UNKNOWN = 10;

public:
	BuilderThread() = delete;

	BuilderThread(
		cand::GlobalsPtr globals,
		const solodata::cand_config_t::signal_group_t &group_config,
		const std::string &logging_prefix = "");

	/**
	 * @parma[in] frame
	 * the CAN frame to try to post to the thread
	 * 
	 * @return
	 * POST_TAKEN - if frame was known
	 * FRAME_UNKNOWN - if frame isn't something this builder knows how to
	 * handle
	 */
	int
	post_frame(
		const can_frame &frame);

	const std::string &
	group_name() const;

	bool
	is_training() const;

	/**
	 * @param[in] sgd
	 * the descriptor to populate.
	 * if builder is currently training, then signals will be empty.
	 * 
	 * @return
	 * ture on success, false on failure
	 */
	void
	get_group_descriptor(
		solodata::cand::SignalGroupDescriptor &sgd) const;

	void
	get_status(
		solodata::cand::BuilderStatus &status) const;

protected:
	virtual
	void
	thread_main();

private:
	cand::GlobalsPtr globals_;

	const solodata::cand_config_t::signal_group_t group_config_;

	CppCAN::CANDatabase cdb_;

	std::unique_ptr<solodata::SignalScorecard> scorecard_;

	BuilderResource *active_res_;
	std::unique_ptr<BuilderPool> pool_;

	solodata::cand::BuilderState_E state_;

	struct Stats
	{
		Stats()
		 : frames_taken(0)
		 , samples_built(0)
		 , pool_exhaustion_count(0)
		{}

		// total number of CAN frames this builder has processed
		uint64_t frames_taken;

		// total number of CAN samples produced by builder
		uint64_t samples_built;

		// number of times BuilderPool::acquire() returned nullptr. indicates
		// that there are no resources available in the pool due to a slow builder.
		uint64_t pool_exhaustion_count;

		// map of dropped frame counters keyed by frame id
		std::unordered_map<uint32_t, uint64_t> frame_drop_count;
	} stats_;

	// stopwatch records
	int SW_RECORD_BUILD;
	int SW_RECORD_ADD_FRAME;

};

}