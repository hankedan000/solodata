#include "PublisherThread.h"

// used to quantize buffer growth in number of bytes
#define BUFFER_GROW_STEPS 1024

namespace cand
{
	PublisherThread::PublisherThread()
	 : LoggingObject("PublisherThread")
	 , show_signals_(false)
	 , samp_pub_(nullptr)
	 , buffer_()
	 , SW_RECORD_SEND_SAMPLE(-1)
	{
	}

	int
	PublisherThread::init(
		cand::GlobalsPtr globals,
		ZMQ_Boilerplate &boilerplate,
		bool show_signals)
	{
		globals_ = globals;
		show_signals_ = show_signals;

		SW_RECORD_SEND_SAMPLE = globals_->msw.addRecord("PublisherThread::send_sample",1000);

		buffer_.resize(globals_->config.zmq.buff_size_bytes);

		samp_pub_ = boilerplate.create_socket(ZMQ_PUB);
		if (samp_pub_ == nullptr)
		{
			LOG4CXX_ERROR(getLogger(),
				"create_socket() failed!");
			return -1;
		}

		if (zmq_bind(samp_pub_,globals->config.zmq.hostname.c_str()) >= 0)
		{
			LOG4CXX_INFO(getLogger(),
				"Successfully bound to \"" << globals->config.zmq.hostname << "\"");
		}
		else
		{
			LOG4CXX_ERROR(getLogger(),
				"Failed to create CAN publisher using"
				" hostname \"" << globals->config.zmq.hostname << "\"");
			return -1;
		}

		if (globals->config.zmq.hostname.find("ipc://") == 0)
		{
			// allow all users to connect and receive data from socket
			chmod(&globals->config.zmq.hostname.c_str()[6],0777);
		}

		return 0;
	}

	void
	PublisherThread::thread_main()
	{
		cand::PublisherResource *pub_res = nullptr;

		while (globals_->stay_alive && ! stop_thread_)
		{
			int rc = globals_->pub_pool->consume_wait(pub_res,1);// 1s timeout
			if (rc == concrt::ERR_TIMEOUT)
			{
				// retry or shutdown thread
				continue;
			}
			else if (rc == concrt::ERR_INTERNAL)
			{
				LOG4CXX_ERROR(getLogger(),
					"consume_wait() failed");
				continue;
			}
			else if (rc != 0)
			{
				LOG4CXX_ERROR(getLogger(),
					"unhandled consume_wait() error code " << rc);
				continue;
			}

			globals_->msw.start(SW_RECORD_SEND_SAMPLE);
			send_sample(pub_res->sample, show_signals_);
			globals_->msw.stop(SW_RECORD_SEND_SAMPLE);

			// cleanup and then release the resource
			pub_res->sample.Clear();// so producer can start fresh
			globals_->pub_pool->release(pub_res);
			pub_res = nullptr;
		}

		LOG4CXX_INFO(getLogger(),
			"PublisherThread stopped");
	}

	void
	PublisherThread::resize_cansamp_buffer(
		size_t required_bytes)
	{
		size_t curr_size = buffer_.size();
		size_t next_size = (required_bytes / BUFFER_GROW_STEPS + 1) * BUFFER_GROW_STEPS;
		LOG4CXX_DEBUG(getLogger(),
			"buffer_ resized"
			" from " << curr_size << " bytes"
			" to "   << next_size << " bytes.");
		buffer_.resize(next_size);
	}

	void
	PublisherThread::send_sample(
		const solodata::CAN_Sample &datum,
		bool show_signals)
	{
		if (show_signals)
		{
			LOG4CXX_INFO(getLogger(),
				datum.DebugString());
		}

		// serialize datum into buffer
		size_t samp_size = datum.ByteSizeLong();
		if (samp_size > buffer_.size())
		{
			resize_cansamp_buffer(samp_size);
		}
		datum.SerializeToArray((void*)buffer_.data(),samp_size);

		unsigned int name_bytes = 0;
		unsigned int value_bytes = 0;
		for (const auto &signal : datum.signals())
		{
			if (signal.has_name())
			{
				name_bytes += signal.name().size();
			}
			value_bytes += sizeof(double);
		}
		LOG4CXX_DEBUG(getLogger(),
			"signal_count = " << datum.signals().size() << "; "
			"samp_size = " << samp_size << "; "
			"name_bytes = " << name_bytes << "; "
			"value_bytes = " << value_bytes << "; ");

		int rc = zmq_send(
			samp_pub_,
			(const void*)buffer_.data(),
			samp_size,
			ZMQ_DONTWAIT);
		if (rc < 0)
		{
			LOG4CXX_ERROR(getLogger(),
				"zmq_send() failed for message '" << datum.DebugString() << "'");
		}
	}

}