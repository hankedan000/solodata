#pragma once

#include <cpp-can-parser/CANDatabase.h>
#include <linux/can.h>
#include <LoggingObject.h>
#include <map>
#include <set>

#include "CAN_Sample.pb.h"

namespace solodata
{

class SignalScorecard : public LoggingObject
{
public:
	static const int NOT_READY             = 0;
	static const int READY                 = 10;
	static const int TRAINING_COMPLETE     = 20;
	static const int TRAINING_NOT_COMPLETE = 30;
	static const int UNKNOWN_FRAME         = -10;
	static const int DUPLICATE_ADD         = -11;

public:
	struct FrameEntry
	{
		FrameEntry(
			uint32_t can_id,
			size_t dlc)
		 : can_id(can_id)
		 , dlc(dlc)
		 , timestamp()
		 , received(false)
		{}

		static const size_t MAX_CAN_DLC = 8;

		// the 11bit or 32bit can identifier
		const uint32_t can_id;

		// the number of data bytes in the can frame
		const size_t dlc;

		/**
		 * timestamp for when the frame was received
		 * a vector is used only for the bus training stage,
		 * normally only 1 timestamp entry exists in the field.
		 */
		std::chrono::microseconds timestamp;

		/**
		 * set to true if the frame was received, false otherwise
		 */
		bool received;

		// the CAN frame data. only 'dlc' # of bytes are valid
		uint8_t data[MAX_CAN_DLC];
	};

	using FrameMap = std::map<uint32_t, SignalScorecard::FrameEntry>;

private:
	static const size_t MAX_TRAINING_INTERVALS = 4;

	/**
	 * A structure of information that's collected or measured during that
	 * bus training phase.
	 */
	struct FrameTrainingStats
	{
		FrameTrainingStats(
			uint32_t can_id)
		 : can_id(can_id)
		 , timestamps()
		 , predecessors()
		 , count(0)
		 , delta_sum(std::chrono::microseconds{0})
		{
			timestamps.reserve(MAX_TRAINING_INTERVALS);
		}


		// the 11bit or 32bit can identifier
		const uint32_t can_id;

		// timestamps for when the frames were received
		std::vector<std::chrono::microseconds> timestamps;

		// can ids that were received immediately before this one.
		// ideally, this set should contain one entry. if there are more than
		// one, then it's an indication of dropped frames, or the source device
		// isn't transmitting frames in a linear/logical fashion.
		std::unordered_set<uint32_t> predecessors;

		// number of times we saw this frame
		unsigned int count;

		// summation of delta timestamps from this from to its predecessor.
		// ideally, the first frame in a burst should have the highest delta_sum.
		std::chrono::microseconds delta_sum;
	};

	using TrainingStatsMap = std::map<uint32_t /*can_id*/, SignalScorecard::FrameTrainingStats>;

public:
	/**
	 * @param[in] cdb
	 * CANDatabase used to prep the scorecard for which CAN identifiers
	 * it will be expected to receive.
	 * 
	 * @param[in] burst_rate_hz
	 * if the CANDatabase defines frames that a node will be transmitting
	 * at a regular interval, then this parameter specifies that rate in Hz.
	 * if the CANDatabase defines frames that a node could send asynchronously
	 * or adhoc, then set this parameter to 0.0
	 */
	SignalScorecard(
		const CppCAN::CANDatabase &cdb);

	int
	add_frame_training(
		const can_frame &frame);

	/**
	 * Adds a raw can frame to signal scorecard
	 *
	 * @param[in] frame
	 * A reference to the can frame to add. It's assumed that the data
	 * that the can_frame points to is valid and not null.
	 * 
	 * @return
	 * OK if frame was added successfully
	 * UNKNOWN_FRAME if frame was not known (add ignored)
	 * DUPLICATE_ADD if frame was already added for this cycle (add ignored)
	 * ALREADY_READY if scorecard was already marked ready (add ignored)
	 */
	int
	add_frame(
		FrameMap &fm,
		const can_frame &frame);

	void
	init_frame_map(
		const CppCAN::CANDatabase &cdb,
		FrameMap &fm) const;

	/**
	 * Resets all internal tracking data structures. Sample batching
	 * will resume once this function has been called.
	 */
	void
	reset(
		FrameMap &fm) const;

	bool
	is_training() const;

	const std::set<uint32_t> &
	get_expected_ids() const;

private:
	void
	do_training();

private:
	std::set<uint32_t> expected_ids_;

	TrainingStatsMap training_map_;

	/**
	 * CAN identifiers that are transmitted at the beginning and end of a
	 * frame burst. These members are discovered and assigned during the
	 * bus training phase.
	 */
	uint32_t first_id_;
	uint32_t last_id_;

	bool training_complete_;

};

}// namespace "solodata"
