#pragma once

#include <atomic>
#include <thread>

namespace concrt
{

class ThreadWrapper
{
public:
	ThreadWrapper()
	 : stop_thread_(false)
	 , the_thread_()
	{}

	~ThreadWrapper()
	{
		stop();
		join();
	}

	// starts the thread's execution
	void
	start()
	{
		the_thread_ = std::thread(&ThreadWrapper::thread_main, this);
	}

	// notifies thread to stop execution
	void
	stop()
	{
		stop_thread_ = true;
	}

	// blocks until thread exits execution
	void
	join()
	{
		if (the_thread_.joinable())
		{
			the_thread_.join();
		}
	}

protected:
	// the function that will run in thread context
	virtual
	void
	thread_main() = 0;

protected:
	std::atomic<bool> stop_thread_;

private:
	std::thread the_thread_;

};

}