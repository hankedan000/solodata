#include <iostream>
#include <unistd.h>

#include "cand_client.h"

cand::Client client;

void
show_usage()
{
	std::cout << "usage: candtool" << std::endl;
}

void
print_sgd(
	const solodata::cand::SignalGroupDescriptor &sgd)
{
	printf("group_name: %s\n", sgd.group_name().c_str());
	printf("  signals:\n");
	unsigned int idx = 0;
	for (auto &signal : sgd.signals())
	{
		printf("    [%-3d] 0x%08x %s: %s\n",idx,signal.frame_id(),signal.name().c_str(),signal.comment().c_str());
		idx++;
	}
}

void
print_status(
	const solodata::cand::CAND_Status &cand_status)
{
	// std::cout << "cand_status = " << cand_status.DebugString() << std::endl;

	for (auto &group_status : cand_status.builder_statuses())
	{
		printf("group_name: %s\n", group_status.group_name().c_str());
		printf("state: %d\n", group_status.state());

		auto &stats = group_status.stats();
		const auto frames_per_sample = stats.frame_drop_count().size();
		const auto frames_taken_adjusted = (stats.frames_taken() / frames_per_sample) * frames_per_sample;
		const auto pending_frames = stats.frames_taken() - frames_taken_adjusted;
		const auto frames_needed = frames_per_sample * stats.samples_built();
		uint64_t total_dropped_frames = 0;
		std::map<uint32_t,uint64_t> sorted_frame_drops;
		for (auto &drop_entry : stats.frame_drop_count())
		{
			total_dropped_frames += drop_entry.second;
			sorted_frame_drops[drop_entry.first] = drop_entry.second;
		}
		printf("stats:\n");
		printf("  pool_exhaustion_count:   %ld\n",stats.pool_exhaustion_count());
		printf("  frames_per_sample:       %ld\n",frames_per_sample);
		printf("  samples_built:           %ld\n",stats.samples_built());
		printf("  frames_taken:            %ld (%ld pending)\n",stats.frames_taken(),pending_frames);
		printf("  frames_taken_adjusted:   %ld\n",frames_taken_adjusted);
		printf("  total_dropped_frames:    %ld\n",total_dropped_frames);
		printf("  frames_needed(ideal):    %ld\n",frames_needed);
		printf("  frames_needed(measured): %ld\n",(frames_taken_adjusted + total_dropped_frames));
		printf("  dropped frames:\n");
		for (auto &drop_entry : sorted_frame_drops)
		{
			printf("    0x%08x: %4ld\n",drop_entry.first,drop_entry.second);
		}
	}
}

void
print_index(
	const solodata::cand::Index &index)
{
	for (auto &sgd : index.groups())
	{
		print_sgd(sgd.second);
	}
}

void
print_sample_w_index(
	const solodata::CAN_Sample &sample,
	const solodata::cand::Index &index)
{
	auto sgd_itr = index.groups().find(sample.group_name());
	if (sgd_itr != index.groups().end())
	{
		if (sgd_itr->second.signals().size() != sample.signals().size())
		{
			std::cerr << "mismatch in "
				"index signal count " << sgd_itr->second.signals().size() << " vs "
				"sample signal count " << sample.signals().size() << std::endl;
			return;
		}
		for (unsigned int i=0; i<sample.signals().size(); i++)
		{
			const auto &signal_desc = sgd_itr->second.signals()[i];
			const auto &signal = sample.signals()[i];
			printf("[%3d] - %0.3f: %s (%s)\n",
				i,
				signal.value(),
				signal_desc.name().c_str(),
				signal_desc.comment().c_str());
		}
	}
}

void
on_sample(
	const solodata::CAN_Sample &sample)
{
	// printf("%s\n",sample.DebugString().c_str());

	solodata::cand::Index index;
	if (client.get_index(index) == cand::Client::OK)
	{
		print_sample_w_index(sample,index);
	}
}

int
main(
	int argc,
	const char **argv)
{
	std::string cmd = "status";
	if (argc > 1)
	{
		cmd = argv[1];
	}

	int rc = client.connect(0,0);
	if (rc == cand::Client::OK)
	{
		if (cmd == "status")
		{
			solodata::cand::CAND_Status cand_status;
			if (client.get_status(cand_status) == cand::Client::OK)
			{
				print_status(cand_status);
			}
		}
		else if (cmd == "index")
		{
			solodata::cand::Index index;
			if (client.get_index(index) == cand::Client::OK)
			{
				print_index(index);
			}
		}
		else if (cmd == "listen")
		{
			if (client.listen(on_sample) == cand::Client::OK)
			{
				while (true)
				{
					usleep(1000);
				}
			}
		}
	}
	else
	{
		std::cout << "failed to connect to cand. is it running?" << std::endl;
	}

	return 0;
}