add_executable(candtool candtool.cpp)

target_link_libraries(candtool
	PRIVATE
		cand_client
		cand_ctrl_pb
		${ZMQ_LIBRARIES}
)

install(
	TARGETS candtool
	RUNTIME
		DESTINATION bin
)
