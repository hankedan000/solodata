#include "ControlThread.h"

#include <unistd.h>

#include "BuilderThread.h"
#include "cand_constants.h"

// define this to show control communication on stdout
#undef CONTROL_THREAD_SHOW_COMMS 

namespace cand
{

	ControlThread::ControlThread(
		cand::GlobalsPtr globals,
		ZMQ_Boilerplate &boilerplate,
		const std::string &logging_prefix)
	 : LoggingObject("ControlThread")
	 , globals_(globals)
	 , rep_(nullptr)
	{
		setLoggerPrefix(logging_prefix);

		rep_ = boilerplate.create_socket(ZMQ_REP);
		if (rep_ == nullptr)
		{
			throw std::runtime_error("create_socket(ZMQ_REP) failed!");
		}

		std::string hostname("tcp://*:");
		hostname += std::to_string(globals_->config.zmq.ctrl_port);
		if (zmq_bind(rep_,hostname.c_str()) >= 0)
		{
			LOG4CXX_INFO(getLogger(),
				"Successfully bound to \"" << hostname << "\"");
		}
		else
		{
			throw std::runtime_error("failed to bind rep_ socket using hostname " + hostname);
		}

		const int timeout = 1000;
		if (zmq_setsockopt(rep_, ZMQ_RCVTIMEO, &timeout, sizeof(timeout)) < 0)
		{
			throw std::runtime_error("failed to set recv socket timeout");
		}
	}

	void
	ControlThread::on_control(
		const solodata::cand::Control &ctrl)
	{
		solodata::cand::Response resp;

		resp.set_status(solodata::cand::ResponseStatus_E::RS_OK);
		switch (ctrl.request_type())
		{
			case solodata::cand::RequestType_E::GET_STATUS:
			{
				auto status = resp.mutable_cand_status();
				status->set_state(globals_->state);
				status->set_sample_hostname(globals_->config.zmq.hostname);
				// it's only safe to access the builders outside of CS_INITIALIZING
				// because the main thread could still be spinning up builders
				if (globals_->state != solodata::cand::CAND_State_E::CS_INITIALIZING)
				{
					for (const auto &builder : globals_->builders)
					{
						auto builder_status = status->add_builder_statuses();
						builder->get_status(*builder_status);
					}
				}
				break;
			}
			case solodata::cand::RequestType_E::GET_INDEX:
			{
				// it's only safe to access the builders outside of CS_INITIALIZING
				// because the main thread could still be spinning up builders
				if (globals_->state != solodata::cand::CAND_State_E::CS_INITIALIZING)
				{
					solodata::cand::Index *index = resp.mutable_index();
					auto groups_map_ptr = index->mutable_groups();
					for (const auto &builder : globals_->builders)
					{
						if (builder->is_training())
						{
							// don't populate index for those that are training
							continue;
						}

						solodata::cand::SignalGroupDescriptor cgd;
						builder->get_group_descriptor(cgd);
						(*groups_map_ptr)[cgd.group_name()] = cgd;
					}
				}
				break;
			}
			default:
				LOG4CXX_WARN(getLogger(),
					__func__ << " - "
					"unsupported request_type! ctrl: " << ctrl.DebugString());
				resp.set_status(solodata::cand::ResponseStatus_E::RS_FAILED);
				break;
		}

		std::vector<uint8_t> rsp_buffer;
		rsp_buffer.resize(resp.ByteSizeLong());
		if (resp.SerializeToArray(rsp_buffer.data(),rsp_buffer.size()))
		{
#ifdef CONTROL_THREAD_SHOW_COMMS
			std::cout << ">>> resp:\n" << resp.DebugString();
#endif
			int rc = zmq_send(
				rep_,
				(const void *)(rsp_buffer.data()),
				rsp_buffer.size(),
				ZMQ_DONTWAIT);
			if (rc < 0)
			{
				LOG4CXX_ERROR(getLogger(),
					__func__ << " - "
					"zmq_send() failed. " << zmq_strerror(errno));
			}
		}
		else
		{
			LOG4CXX_ERROR(getLogger(),
				__func__ << " - "
				"Failed to serialize reply message");
		}
	}

	void
	ControlThread::thread_main()
	{
		uint8_t buffer[1024];

		while (globals_->stay_alive && ! stop_thread_)
		{
			int rbytes = zmq_recv(rep_,buffer,sizeof(buffer),0);
			if (rbytes < 0)
			{
				if (errno == EAGAIN)
				{
					LOG4CXX_DEBUG(getLogger(),
						"zmq_recv() timed out");
				}
				else
				{
					LOG4CXX_ERROR(getLogger(),
						"zmq_recv() returned error: " << zmq_strerror(errno));
				}
			}
			else
			{
				LOG4CXX_DEBUG(getLogger(),
					"zmq_recv() received " << rbytes << "bytes");

				solodata::cand::Control ctrl;
				if (ctrl.ParseFromArray(buffer,rbytes))
				{
#ifdef CONTROL_THREAD_SHOW_COMMS
					std::cout << "<<< ctrl:\n" << ctrl.DebugString();
#endif
					on_control(ctrl);
				}
			}
		}

		LOG4CXX_INFO(getLogger(),
			"ControlThread stopped");
	}

}// namespace cand