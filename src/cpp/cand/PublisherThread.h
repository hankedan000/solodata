#pragma once

#include <vector>
#include <ZMQ_Boilerplate.h>

#include "cand_globals.h"
#include "ThreadWrapper.h"

namespace cand
{

class PublisherThread : public LoggingObject, public concrt::ThreadWrapper
{
public:
	PublisherThread();

	/**
	 * @return
	 * 0 on success, -1 on fail
	 */
	int
	init(
		cand::GlobalsPtr globals,
		ZMQ_Boilerplate &boilerplate,
		bool show_signals);

protected:
	virtual
	void
	thread_main();

private:
	void
	resize_cansamp_buffer(
		size_t required_bytes);

	void
	send_sample(
		const solodata::CAN_Sample &datum,
		bool show_signals);

private:
	cand::GlobalsPtr globals_;

	bool show_signals_;

	// ZMQ_PUB socket to send samples on
	void *samp_pub_;

	// buffer used to serial data to sockets
	std::vector<uint8_t> buffer_;

	// stopwatch records
	int SW_RECORD_SEND_SAMPLE;

};

}