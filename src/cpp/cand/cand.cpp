#include <csignal>
#include <getopt.h>
#include <linux/can.h>
#include <Logging.h>
#include <LoggingObject.h>
#include <memory>
#include <net/if.h>

#include <sched.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <thread>
#include <unistd.h>
#include <unordered_set>
#include <ZMQ_Boilerplate.h>

#include "BuilderThread.h"
#include "cand_config.h"
#include "cand_globals.h"
#include "ControlThread.h"
#include "PublisherThread.h"

// include protobuf header files
#include "CAN_Sample.pb.h"

SET_LOGGING_CFG_NAME("solodata_logging.cfg");

// process boilerplate class
ZMQ_Boilerplate boilerplate("cand");

// instantiate globals
cand::GlobalsPtr globals;

std::unique_ptr<cand::ControlThread> ctrl_thread;
cand::PublisherThread pub_thread;

int SW_RECORD_FRAME_HANDLE = -1;

void
shutdown_signal(
	int signal)
{
	LOG4CXX_INFO(boilerplate.getLogger(),"shutdown requested!");
	globals->stay_alive = false;
}

void
setup_signals()
{
	signal(SIGINT, shutdown_signal);
	signal(SIGHUP, shutdown_signal);
}

void
display_usage()
{
	printf("cand [options]\n");
	printf(" --daemon         : run as daemon process\n");
	printf(" -f               : path to config file\n");
	printf(" --show-raw-frame : displays raw hex frame to console\n");
	printf(" --show-signals   : displays CAN signals to console\n");
	printf(" --stopwatch      : enable stopwatching of critical functions\n");
	printf(" -h,--help        : display this menu\n");
}

int
main(int argc, char **argv)
{
	globals.reset(new cand::Globals());
	globals->state = solodata::cand::CAND_State_E::CS_INITIALIZING;
	int daemon_flag = 0;
	int show_raw_frame = 0;
	int show_signals = 0;
	int enable_sw = 0;
	std::string cfg_filepath = "";
	static struct option long_options[] =
	{
		{"daemon"        , no_argument      , &daemon_flag   , 1  },
		{"show-raw-frame", no_argument      , &show_raw_frame, 1  },
		{"show-signals"  , no_argument      , &show_signals  , 1  },
		{"stopwatch"     , no_argument      , &enable_sw     , 1  },
		{"help"          , no_argument      , 0              , 'h'},
		{0, 0, 0, 0}
	};

	while (true)
	{
		int option_index = 0;
		int c = getopt_long(
			argc,
			argv,
			"f:h",
			long_options,
			&option_index);

		// detect the end of the options
		if (c == -1)
		{
			break;
		}

		switch (c)
		{
			case 0:
				// flag setting
				break;
			case 'f':
				cfg_filepath = optarg;
				break;
			case 'h':
			case '?':
				display_usage();
				exit(0);
				break;
			default:
				LOG4CXX_WARN(boilerplate.getLogger(),
					"Unsupported option '" << (char)(c) << "'");
				display_usage();
				exit(1);
				break;
		}
	}

	// start the daemon in another process
	if (daemon_flag)
	{
		boilerplate.daemonize();
	}

	// main process can wait for signals
	setup_signals();

	bool okay = true;

	// setup/parse config options
	if (cfg_filepath != "")
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"parsing config from '" << cfg_filepath << "'");
		YAML::Node cfg_node = YAML::LoadFile(cfg_filepath);
		globals->config = cfg_node["cand"].as<solodata::cand_config_t>();
	}

	// initialize globals
	globals->pub_pool.reset(new cand::PublisherPool(globals->config.pub_pool_size));

	// setup realtime scheduling policy/priorities
	struct sched_param sched_p;
	sched_p.sched_priority = globals->config.sched.priority;
	LOG4CXX_INFO(boilerplate.getLogger(),
		"setting scheduler policy to 'SCHED_FIFO' with"
		" priority = " << globals->config.sched.priority);
	if (sched_setscheduler(0,SCHED_FIFO,&sched_p) < 0)
	{
		if (errno == EPERM)
		{
			LOG4CXX_WARN(boilerplate.getLogger(),
				"unable to set scheduling policy to 'SCHED_FIFO'."
				" root permissions are required. defaulting to 'SCHED_OTHER' policy.");
		}
		else
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"uncaught sched_setscheduler() error!"
				" errno = " << errno << "(" << strerror(errno) << ")");
		}
	}

	if (globals->config.sched.cpu_affinity >= 0)
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			"setting CPU affinity to core " << globals->config.sched.cpu_affinity);
		cpu_set_t cpu_set;
		CPU_ZERO(&cpu_set);
		CPU_SET(globals->config.sched.cpu_affinity,&cpu_set);
		if (sched_setaffinity(0,sizeof(cpu_set),&cpu_set) < 0)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"uncaught sched_setaffinity() error!"
				" errno = " << errno << "(" << strerror(errno) << ")");
		}
	}

	globals->msw.enabled(enable_sw);
	SW_RECORD_FRAME_HANDLE = globals->msw.addRecord("FrameHandle",10000);

	// --------------------------
	// connect to CAN socket

	int can_s = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if (can_s < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to create CAN socket connection!"
			" errno = " << strerror(errno) << " (" << errno << ")");
		okay = false;
	}

	struct timeval tv;
	tv.tv_sec = 1;
	tv.tv_usec = 0;
	if (okay)
	{
		setsockopt(can_s,SOL_SOCKET,SO_RCVTIMEO,(const char*)&tv,sizeof(tv));
	}

	struct ifreq ifr;
	if (okay)
	{
		strcpy(ifr.ifr_name, globals->config.if_name.c_str());
		if (ioctl(can_s, SIOCGIFINDEX, &ifr) < 0)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"Failed to get CAN interface index for"
				" can interface \"" << globals->config.if_name << "\"."
				" errno = " << strerror(errno) << " (" << errno << ")");
			okay = false;
		}
	}

	if (okay)
	{
		struct sockaddr_can addr;
		memset(&addr, 0, sizeof(addr));
		addr.can_family = AF_CAN;
		addr.can_ifindex = ifr.ifr_ifindex;
		if (bind(can_s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"Failed to bind to CAN socket for"
				" device " << globals->config.if_name << "."
				" errno = " << strerror(errno) << " (" << errno << ")");
			okay = false;
		}
		else
		{
			LOG4CXX_INFO(boilerplate.getLogger(),
				"Successfully bound to " << globals->config.if_name << "!");
		}
	}

	// --------------------------
	// spin up builder threads for each DBC file
	for (const auto &signal_group : globals->config.signal_groups)
	{
		try
		{
			std::shared_ptr<cand::BuilderThread> new_builder(new cand::BuilderThread(
				globals,
				signal_group,
				boilerplate.getLoggerNameFull()));
			new_builder->start();
			globals->builders.push_back(new_builder);
		}
		catch (const std::exception &e)
		{
			LOG4CXX_ERROR(boilerplate.getLogger(),
				"caught exception while creating BuilderThread. " << e.what());
			return -1;
		}
	}

	// start control thread
	ctrl_thread.reset(new cand::ControlThread(
		globals,
		boilerplate,
		boilerplate.getLoggerNameFull()));
	ctrl_thread->start();

	// start the publisher thread
	pub_thread.setLoggerPrefix(boilerplate.getLoggerNameFull());
	if (pub_thread.init(globals,boilerplate,show_signals) == 0)
	{
		pub_thread.start();
	}
	else
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to init publisher thread!");
		okay = false;
	}

	/**
	 * A set of can_ids that this daemon has learned to ignore because
	 * it has failed to lookup the id in the CAN DBC file.
	 */
	std::unordered_set<canid_t> ignored_frames;
	globals->state = solodata::cand::CAND_State_E::CS_RUNNING;

	while (okay && globals->stay_alive)
	{
		int nbytes;
		struct can_frame frame;
		nbytes = read(can_s, &frame, sizeof(struct can_frame));
		globals->msw.start(SW_RECORD_FRAME_HANDLE);
		if (nbytes < 0)
		{
			if (errno == EAGAIN)
			{
				// read timed out
				continue;
			}
			else if (errno == EINTR)
			{
				if (globals->stay_alive)
				{
					LOG4CXX_WARN(boilerplate.getLogger(),
						"read() interrupted but process still expected to be alive...");
				}
				continue;
			}
			else
			{
				LOG4CXX_ERROR(boilerplate.getLogger(),
					"read() failed! nbytes = " << nbytes << "."
					" errno = " << strerror(errno) << " (" << errno << ")");
				continue;
			}
		}
		else
		{
			if (ignored_frames.find(frame.can_id) != ignored_frames.end())
			{
				LOG4CXX_TRACE(boilerplate.getLogger(),
					"Ignoring frame with can_id 0x" << std::hex << frame.can_id);
				continue;
			}
		}

		bool taken = false;
		for (auto &builder : globals->builders)
		{
			if (builder->post_frame(frame) == cand::BuilderThread::POST_TAKEN)
			{
				taken = true;
				break;// stop once one builder has taken the frame
			}
		}

		if ( ! taken)
		{
			LOG4CXX_WARN(boilerplate.getLogger(),
				"Unknown can_id 0x" << std::hex << frame.can_id << "!" <<
				" This can_id will be ignored. Consider updating a dbc.");
			ignored_frames.insert(frame.can_id);
			continue;
		}

		if (show_raw_frame)
		{
			char sbuf[1024];
			sprintf(sbuf,"0x%03X [%d] ",frame.can_id, frame.can_dlc);
			for (int i = 0; i < frame.can_dlc; i++)
			{
				sprintf(sbuf + strlen(sbuf),"%02X ",frame.data[i]);
			}
			LOG4CXX_INFO(boilerplate.getLogger(),sbuf);
		}
		globals->msw.stop(SW_RECORD_FRAME_HANDLE);
	}

	// wait for all threads to join (builders will join on destruction)
	pub_thread.stop();
	pub_thread.join();

	if (globals->msw.enabled())
	{
		LOG4CXX_INFO(boilerplate.getLogger(),
			globals->msw.getSummary());
	}

	// clean up SocketCAN connection
	if (can_s > 0)
	{
		close(can_s);
		can_s = 0;
	}

	return okay ? 0 : -1;
}