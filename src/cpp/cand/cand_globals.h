#pragma once

#include <memory>
#include <MultiStopwatch.h>

#include "cand_ctrl.pb.h"
#include "cand_config.h"
#include "PublisherPool.h"

namespace cand
{

	// forward declare
	class BuilderThread;

	class Globals
	{
	public:
		solodata::cand_config_t config;

		// used to shutdown the process when interrupt signal is received
		bool stay_alive = true;

		solodata::cand::CAND_State_E state;

		std::shared_ptr<cand::PublisherPool> pub_pool;

		MultiStopwatch msw;

		std::vector<std::shared_ptr<cand::BuilderThread>> builders;
	};

	using GlobalsPtr = std::shared_ptr<Globals>;

}