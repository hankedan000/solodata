#include "BuilderThread.h"

#include <limits>

namespace cand
{

	BuilderThread::BuilderThread(
		cand::GlobalsPtr globals,
		const solodata::cand_config_t::signal_group_t &group_config,
		const std::string &logging_prefix)
	 : LoggingObject(std::string("BuilderThread(" + group_config.name + ")"))
	 , globals_(globals)
	 , group_config_(group_config)
	 , cdb_()
	 , scorecard_()
	 , active_res_(nullptr)
	 , pool_(new BuilderPool(group_config.pool_size))
	 , state_(solodata::cand::BuilderState_E::BS_TRAINING)
	 , stats_()
	 , SW_RECORD_BUILD(-1)
	 , SW_RECORD_ADD_FRAME(-1)
	{
		setLoggerPrefix(logging_prefix);

		const std::string record_prefix(std::string("BuilderThread(") + group_config.name + ")");
		const std::string record_name_build(record_prefix + "::build");
		const std::string record_name_add_frame(record_prefix + "::add_frame");
		SW_RECORD_BUILD = globals_->msw.addRecord(record_name_build,1000);
		SW_RECORD_ADD_FRAME = globals_->msw.addRecord(record_name_add_frame,10000);

		LOG4CXX_INFO(getLogger(),
			"initializing CAN database from file: " << group_config.dbc);
		cdb_ = CppCAN::CANDatabase::fromFile(group_config.dbc);
		scorecard_.reset(new solodata::SignalScorecard(cdb_));
		scorecard_->setLoggerPrefix(getLoggerNameFull());

		unsigned int frame_count = 0, signal_count = 0;
		for (const auto &frame : cdb_)
		{
			frame_count++;
			signal_count += frame.second.size();
		}
		LOG4CXX_INFO(getLogger(),
			"database contains " << frame_count << " frame(s), and " <<
			signal_count << " signal(s)");
	}

	int
	BuilderThread::post_frame(
		const can_frame &frame)
	{
		if ( ! cdb_.contains(frame.can_id))
		{
			return FRAME_UNKNOWN;
		}

		if (is_training())
		{
			int rc = scorecard_->add_frame_training(frame);
			if (rc == solodata::SignalScorecard::TRAINING_COMPLETE)
			{
				// init frame drop counter statistics
				stats_.frame_drop_count.clear();
				for (const auto &frame_id : scorecard_->get_expected_ids())
				{
					stats_.frame_drop_count[frame_id] = 0;
				}

				// init all resources in pool
				for (unsigned int i=0; i<group_config_.pool_size; i++)
				{
					BuilderResource *res;
					if (pool_->acquire_busy(res) == 0)
					{
						scorecard_->init_frame_map(cdb_,res->frames);
						pool_->release(res);
					}
					else
					{
						LOG4CXX_ERROR(getLogger(),
							"failed acquire res during initialization");
					}
				}
				LOG4CXX_INFO(getLogger(),
					"initialized " << group_config_.pool_size << " resource(s) in pool");
				state_ = solodata::cand::BuilderState_E::BS_RUNNING;
			}
		}
		else
		{
			stats_.frames_taken++;

			if (active_res_ == nullptr)
			{
				int acquire_rc = pool_->acquire_busy(active_res_,100);// 100us timeout
				if (acquire_rc != 0)
				{
					LOG4CXX_ERROR(getLogger(),
						"Failed to acquire resource. Dropping CAN frame.");
					stats_.pool_exhaustion_count++;
					return POST_TAKEN;
				}
			}

			try
			{
				globals_->msw.start(SW_RECORD_ADD_FRAME);
				int add_rc = scorecard_->add_frame(active_res_->frames, frame);
				globals_->msw.stop(SW_RECORD_ADD_FRAME);

				bool produce = false;
				if (add_rc == solodata::SignalScorecard::NOT_READY)// most common, so test first for performance
				{
					// keep waiting
				}
				else if (add_rc == solodata::SignalScorecard::READY)// 2nd most common case
				{
					// happy path!
					produce = true;
				}
				else if (add_rc == solodata::SignalScorecard::UNKNOWN_FRAME)
				{
					LOG4CXX_ERROR(getLogger(),
						__func__ << " - "
						"Unknown can_id 0x" << std::hex << frame.can_id << "!" <<
						" This should have been caught alreayd... Is there a bug?");
				}
				else if (add_rc == solodata::SignalScorecard::DUPLICATE_ADD)
				{
					LOG4CXX_ERROR(getLogger(),
						"duplicate add of can_id 0x" << std::hex << frame.can_id << "!" <<
						" This could cause partial frame aggregation.");
					produce = true;
				}

				if (produce)
				{
					pool_->produce(active_res_);
					stats_.samples_built++;
					active_res_ = nullptr;
				}
			}
			catch (const std::exception &e)
			{
				LOG4CXX_WARN(getLogger(),
					__func__ << " - "
					"caught std::exception while handling frame with"
					" can_id 0x" << std::hex << frame.can_id << "!\n" <<
					e.what());
			}
			catch (...)
			{
				LOG4CXX_WARN(getLogger(),
					__func__ << " - "
					"caught unknown expection while handling frame with"
					" can_id 0x" << std::hex << frame.can_id << "!\n");
			}
		}

		return POST_TAKEN;
	}

	const std::string &
	BuilderThread::group_name() const
	{
		return group_config_.name;
	}

	bool
	BuilderThread::is_training() const
	{
		return state_ == solodata::cand::BuilderState_E::BS_TRAINING;
	}

	void
	BuilderThread::get_group_descriptor(
		solodata::cand::SignalGroupDescriptor &sgd) const
	{
		sgd.set_group_name(group_config_.name);
		sgd.set_rate_hz(group_config_.rate_hz);
		sgd.clear_signals();
		if ( ! is_training())
		{
			for (auto can_id : scorecard_->get_expected_ids())
			{
				const auto &db_frame = cdb_.at(can_id);
				for (const auto &db_signal : db_frame)
				{
					solodata::cand::SignalDescriptor *sd = sgd.add_signals();
					sd->set_name(db_signal.second.name());
					sd->set_comment(db_signal.second.comment());
					sd->set_frame_id(can_id);
				}
			}
		}
	}

	void
	BuilderThread::get_status(
		solodata::cand::BuilderStatus &status) const
	{
		status.set_state(state_);
		status.set_group_name(group_name());

		auto stats_ptr = status.mutable_stats();
		stats_ptr->set_frames_taken(stats_.frames_taken);
		stats_ptr->set_samples_built(stats_.samples_built);
		stats_ptr->set_pool_exhaustion_count(stats_.pool_exhaustion_count);

		auto frame_drop_count_map_ptr = stats_ptr->mutable_frame_drop_count();
		for (const auto &entry : stats_.frame_drop_count)
		{
			(*frame_drop_count_map_ptr)[entry.first] = entry.second;
		}
	}

	void
	BuilderThread::thread_main()
	{
		cand::BuilderResource *res = nullptr;

		while (globals_->stay_alive && ! stop_thread_)
		{
			int rc = pool_->consume_wait(res,1);// 1s timeout
			if (rc == concrt::ERR_TIMEOUT)
			{
				// retry or shutdown thread
				continue;
			}
			else if (rc == concrt::ERR_INTERNAL)
			{
				LOG4CXX_ERROR(getLogger(),
					"consume_wait() failed");
				continue;
			}
			else if (rc != 0)
			{
				LOG4CXX_ERROR(getLogger(),
					"unhandled consume_wait() error code " << rc);
				continue;
			}

			// build the sample based on frames that were aggregated
			cand::PublisherResource *pub_res = nullptr;
			if (globals_->pub_pool->acquire_busy(pub_res,100) == concrt::OK)
			{
				globals_->msw.start(SW_RECORD_BUILD);
				auto &samp = pub_res->sample;
				samp.set_group_name(group_config_.name);
				uint64_t earliest_ts = -1;

				// add all signals
				for (const auto &frame_entry : res->frames)
				{
					bool frame_received = false;
					uint64_t frame_ts = -1;
					if (frame_entry.second.received)
					{
						frame_received = true;
						frame_ts = frame_entry.second.timestamp.count();
						if (frame_ts < earliest_ts)
						{
							earliest_ts = frame_ts;
						}
					}
					else
					{
						// keep track of dropped frame stats
						stats_.frame_drop_count.at(frame_entry.second.can_id)++;
					}

					const CppCAN::CANFrame &frame = cdb_.at(frame_entry.second.can_id);
					for (const auto &signal_entry : frame)
					{
						const CppCAN::CANSignal &signal = signal_entry.second;

						// build new signal in sample
						solodata::CAN_Signal *new_signal = samp.add_signals();
						bool send_name = false;
						if (send_name)
						{
							new_signal->set_name(signal.name());
						}
						bool send_comment = false;
						if (send_comment)
						{
							new_signal->set_comment(signal.comment());
						}

						if (frame_received)
						{
							CppCAN::CANSignal::raw_t raw = signal.decode(frame_entry.second.data);
							double phys = signal.rawToPhys(raw);
							new_signal->set_value(phys);
						}
						else
						{
							new_signal->set_value(std::numeric_limits<double>::quiet_NaN());
						}
					}
				}
				samp.set_timestamp(earliest_ts);
				globals_->msw.stop(SW_RECORD_BUILD);

				// send sample for publishing
				globals_->pub_pool->produce(pub_res);
			}

			// reset frames so producer can start fresh and release the resource
			scorecard_->reset(res->frames);
			pool_->release(res);
			res = nullptr;
		}

		LOG4CXX_INFO(getLogger(),
			"BuilderThread stopped");
	}

}