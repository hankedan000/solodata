class Coord extends PVector
{
  public Coord(float lat, float lon)
  {
    this.y = lat;
    this.x = lon;
  }
  
  public float lat()
  {
    return y;
  }
  
  public float lon()
  {
    return x;
  }
  
  public void lat(float lat)
  {
    y = lat;
  }
  public void lon(float lon)
  {
    x = lon;
  }
}

float deg2meters(float d)
{
  // earth radius in meters
  float EARTH_RAD_M = 6371000.0;
  return EARTH_RAD_M * radians(d);
}

float meters2miles(float m)
{
  return m * 2 / 3218.69;
}

float sec2hrs(float s)
{
  return s / 60 / 60;
}

float knots2mph(float knots)
{
  return knots * 1.15078;
}

class NavSample
{
  public static final int SIZE = (32 + 32 + 32 + 32) / 8;
  int utc_sec;
  int utc_usec;
  Coord coord = new Coord(0,0);
  boolean velInferred = false;
  boolean velValid = false;
  float vel_mph = 0;
  
  public
  NavSample(ByteBuffer bb)
  {
    try
    {
      utc_sec   = bb.getInt(0);
      utc_usec  = bb.getInt(4);
      coord.lat(bb.getFloat(8));
      coord.lon(bb.getFloat(12));
    }
    catch (Exception e)
    {
      println(e);
    }
  }
  
  public NavSample(JSONObject obj)
  {
    double utc = obj.getDouble("utc");
    utc_sec = (int)Math.floor(utc);
    utc_usec = (int)((utc - utc_sec) * 1.0e6);
    coord.lat(obj.getFloat("lat"));
    coord.lon(obj.getFloat("lon"));
    velInferred = false;
    velValid = true;
    try
    {
      float vel_knots = obj.getFloat("speed_knots");
      vel_mph = knots2mph(vel_knots);
    }
    catch (RuntimeException ex)
    {
      vel_mph = 0.0;
      velValid = false;
    }
  }
  
  boolean isValid()
  {
    boolean valid = true;
    
    valid = valid && -90 < coord.lat() && coord.lat() < 90;
    valid = valid && -180 < coord.lon() && coord.lon() < 180;
    
    return valid;
  }
  
  boolean isVelocityValid()
  {
    return velValid;
  }
  
  double getTime()
  {
    double time = 0;
    time += utc_sec;
    time += utc_usec / 1.0e6;
    return time;
  }
  
  boolean inferVelocity(NavSample next)
  {
    boolean okay = true;
    
    if (velValid)
    {
      println("Warning! Inferring velocity, but velocity was already valid.");
    }
    
    float dt = (float)(next.getTime() - getTime());
    if (dt > 0)
    {
      Coord dc = new Coord(next.coord.y,next.coord.x);
      dc.sub(coord);
      
      float deg = dc.mag();
      float dist_meters = deg2meters(deg);
      float dist_miles = meters2miles(dist_meters);
      
      velInferred = true;
      velValid = true;
      vel_mph = dist_miles / sec2hrs(dt);
      //println("vel = " + vel_mph);
      //println("\t dt = " + dt);
      //println("\t dist_miles = " + dist_miles);
    }
    else
    {
      println("dt is " + dt + ". Can't compute velocity from negative time delta");
      okay = false;
    }
    
    return okay;
  }
  
  float getVelocity()
  {
    return vel_mph;
  }
  
  @Override
  String toString()
  {
    return toJSON().toString();
  }
  
  JSONObject toJSON()
  {
    JSONObject obj = new JSONObject();
    
    obj.setInt("utc_sec",utc_sec);
    obj.setInt("utc_usec",utc_usec);
    obj.setFloat("lat",coord.lat());
    obj.setFloat("lon",coord.lon());
    
    return obj;
  }
}
