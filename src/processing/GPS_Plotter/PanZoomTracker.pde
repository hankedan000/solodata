class PanZoomTracker extends PanTracker
{
  private float zoom = 1.0;
  
  void mouseWheel(MouseEvent event)
  {
    // compute the zoom scaling
    float e = -1 * event.getCount();
    float dZoom = e / 10.0;
    zoom += dZoom;
    
    // compute pan offset to recenter around mouse
    PVector zoomSpot = new PVector(event.getX(),event.getY());
    PVector dPan = zoomSpot.copy();
    dPan.mult(1 + dZoom);
    dPan.sub(zoomSpot);
    totalPan.sub(dPan);
  }
  
  float getZoom()
  {
    return zoom;
  }
}
