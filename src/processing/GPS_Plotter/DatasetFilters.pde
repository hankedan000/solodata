import java.util.*;

/**
 * Ramer Douglas Peucker algorithm
 * https://en.wikipedia.org/wiki/Ramer-Douglas-Peucker_algorithm
 * 
 * @param[in] track
 * the navsamples to filter
 * 
 * @param[in] epsilon
 * the distance away to filter out points
 */
List<NavSample> rdp(List<NavSample> track, float epsilon)
{
  List<NavSample> ods = new ArrayList();
  
  // Find the point with the maximum distance
  float dmax = 0;
  int index = 0;
  int end = track.size();
  for (int i = 2; i < end; i++)
  {
    float d = perpendicularDistance(track.get(i).coord, track.get(1).coord, track.get(end-1).coord);
    if ( d > dmax )
    {
      index = i;
      dmax = d;
    }
  }
  
  println("dmax = " + dmax);
    
  // If max distance is greater than epsilon, recursively simplify
  if ( dmax > epsilon )
  {
      // Recursive call
      List<NavSample> recResults1 = rdp(track.subList(0,index), epsilon);
      List<NavSample> recResults2 = rdp(track.subList(index,end), epsilon);

      // Build the result list
      ods.addAll(recResults1);
      ods.addAll(recResults2);
  }
  else
  {
    ods.add(track.get(0));
    ods.add(track.get(end-1));
  }
  
  return ods;
}

/**
 * @param[in] track
 * the navsamples to filter
 * 
 * @return
 * the filtered nav samples
 */
List<NavSample> rmZerodCoords(List<NavSample> track)
{
  List<NavSample> ods = new ArrayList();
  
  for (NavSample samp : track)
  {
    if (samp.coord.mag() <= 0.00001)
    {
      // practically (0,0), so drop sample
    }
    else
    {
      ods.add(samp);
    }
  }
  
  return ods;
}

/**
 * @param[in] p
 * a point in 2D space to compute the perpendicular distance to
 * 
 * @param[in] a
 * a point in 2D space that forms one endpoint of the line
 * 
 * @param[in] b
 * a point in 2D space that forms the other endpoint of the line
 *
 * @return
 * the perpendicular distance from point 'p' to the line formed
 * by points 'a' and 'b'
 */
float perpendicularDistance(PVector p, PVector a, PVector b)
{
  // https://www.intmath.com/plane-analytic-geometry/perpendicular-distance-point-line.php
  Line l = new Line(a,b);
  
  float numer = abs(l.m * p.x + -1 * p.y + l.b);
  float denom = sqrt(l.m * l.m + 1);
  
  return numer/denom;
}

class Line
{
  // y = mx + b
  float m,b;
  
  public Line(PVector a, PVector b)
  {
    this.m = (b.y - a.y) / (b.x - a.x);
    this.b = a.y - m * a.x;
  }
}
