import java.nio.*;

NavDataset ds = new NavDataset();

// points of interest
List<Coord> poi = new ArrayList();
DetectionGate dg;

// s -> scrub time with mouse
// n -> navigate (zoom/pan)
// r -> real time play back
// f -> fit to screen
char drawMode = 'f';
PanZoomTracker pzTracker = new PanZoomTracker();

double currTime = 0;

color BG = 70;
color TRACE = #A1EBFC;
color GATE = #C355C9;
color TEXT = #EAEAEA;

/*
 * +-----------------------------------+
 * |             MENU PANE             |
 * +-----------------------------------+
 * |                                   |
 * |           PLOTTING PANE           |
 * |                                   |
 * |                                   |
 * +-----------------------------------+
 */
 // these values are computed in setup
 float MENU_HEIGHT,MENU_WIDTH;
 float MENU_X,MENU_Y;
 float PLOT_HEIGHT,PLOT_WIDTH;
 float PLOT_X,PLOT_Y;

void setup() 
{
  poi.add(new Coord(39.209650, -76.656076));
  dg = new DetectionGate(new Coord(39.209532, -76.656389),new Coord(39.209846, -76.656250),true);
  ds.loadJSON_File("run2_inu.json");
  // TODO add smarts to determine when velocity should be computed
  boolean computeVelocity = false;
  if (computeVelocity)
  {
    ds.computeVelocity();
  }
  
  List<NavSample> simple = rmZerodCoords(ds.samplesClean);
  ds = new NavDataset();
  for (NavSample s : simple)
  {
    ds.add(s);
  }
  
  //for (int i=0; i<40; i++)
  //{
  //  String filename = String.format("scca/run%d_inu.json",i);
  //  try
  //  {
  //    ds.loadJSON_File(filename);
  //  }
  //  catch(Exception ex)
  //  {
  //    println("failed on " + filename);
  //  }
  //}
  println(ds);
  
  size(600,600);
  
  // compute pane dimensions
  MENU_HEIGHT = 30;
  MENU_WIDTH = width;
  MENU_X = 0;
  MENU_Y = 0;
  PLOT_HEIGHT = height - MENU_HEIGHT;
  PLOT_WIDTH = width;
  PLOT_X = 0;
  PLOT_Y = MENU_HEIGHT;
}

void mousePressed()
{
  pzTracker.mousePressed();
}

void mouseReleased()
{
  pzTracker.mouseReleased();
}

void mouseDragged()
{
  pzTracker.mouseDragged();
}

void mouseWheel(MouseEvent event)
{
  pzTracker.mouseWheel(event);
}

void keyPressed()
{
  switch (key)
  {
    case 's':
      drawMode = key;
      break;
    case 'n':
      drawMode = key;
      break;
    case 'r':
      drawMode = key;
      currTime = ds.startTime();
      break;
    case 'f':
      drawMode = key;
      break;
    default:
      // keep draw mode
  }
}

void draw()
{
  currTime += 1.0/frameRate;
  background(BG);
  
  drawMenuPane();
  drawPlotPane();
}

void drawMenuPane()
{
  String str = "";
  str += "s -> scrub; ";
  str += "n -> navigate; ";
  str += "r -> realtime; ";
  str += " f -> fit; ";
  fill(TEXT);
  text(str,0,MENU_HEIGHT/2);
}

void drawPlotPane()
{
  pushMatrix();
  PVector pan = pzTracker.getPan();
  float zoom = pzTracker.getZoom();
  translate(PLOT_X+pan.x,PLOT_Y+pan.y);
  scale(PLOT_WIDTH/width,PLOT_HEIGHT/height);
  scale(zoom);
  
  // display points of interest
  drawPOI(poi);
  
  drawDetectionGate(dg);
  
  // display the track data
  switch (drawMode)
  {
    case 's':
      drawScrub();
      break;
    case 'r':
      drawRealtime();
      break;
    case 'f':
    default:
      drawFit();
      break;
  }
  popMatrix();
}

PVector mapCoord(Coord c, BoundBox bb)
{
  float x = map(c.lon(),bb.a.x,bb.b.x,0,width);
  float y = map(c.lat(),bb.a.y,bb.b.y,height,0);
  
  return new PVector(x,y);
}

void drawTrackPoint(Coord c, BoundBox bb)
{
  drawTrackPoint(c,bb,TRACE);
}

void drawTrackPoint(Coord c, BoundBox bb, color fillColor)
{
  PVector p = mapCoord(c,bb);
  
  fill(fillColor);
  noStroke();
  ellipse(p.x,p.y,2,2);
}

color getVelocityColor(float vel_mph)
{
  color slow = color(255,0,0);
  color fast = color(0,255,0);
  float inter = map(vel_mph, 0, 100, 0, 1);
  return lerpColor(slow, fast, inter);
}

void drawPOI(Coord c, BoundBox bb)
{
  PVector p = mapCoord(c,bb);
  
  fill(255,0,0);
  noStroke();
  ellipse(p.x,p.y,10,10);
}

void drawScrub()
{
  int max = (int)map(mouseX,0,width,0,ds.samplesClean.size());
  int i = 0;
  for (NavSample samp : ds.samplesClean)
  {
    drawTrackPoint(samp.coord,ds.boundBox);
    dg.add(samp.coord);

    i++;
    if (i >= max)
    {
      break;
    }
  }
}

void drawRealtime()
{
  for (NavSample samp : ds.samplesClean)
  {
    drawTrackPoint(samp.coord,ds.boundBox);
    
    if (samp.getTime() > currTime)
    {
      break;
    }
  }
}

void drawFit()
{
  for (NavSample samp : ds.samplesClean)
  {
    drawTrackPoint(samp.coord,ds.boundBox,getVelocityColor(samp.getVelocity()));
  }
}

void drawPOI(List<Coord> poi)
{
  for (Coord c : poi)
  {
    drawPOI(c,ds.boundBox);
  }
}

void drawDetectionGate(DetectionGate dg)
{
  stroke(GATE);
  strokeWeight(2);
  PVector a = mapCoord(dg.a,ds.boundBox);
  PVector b = mapCoord(dg.b,ds.boundBox);
  line(a.x,a.y,b.x,b.y);
}
