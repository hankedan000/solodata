class PanTracker extends ClickDragTracker
{
  PVector totalPan = new PVector();
  
  @Override
  public void mouseReleased()
  {
    super.mouseReleased();
    totalPan.add(getDisplacement());
  }
  
  public PVector getPan()
  {
    PVector pan = totalPan.copy();
    if (isDragging())
    {
      pan.add(getDisplacement());
    }
    return pan;
  }
}
