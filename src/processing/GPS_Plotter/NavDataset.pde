import java.util.*;

class BoundBox
{
  PVector a = new PVector(500,500);
  PVector b = new PVector(-500,-500);
  
  void add(PVector p)
  {
    a.x = min(a.x,p.x);
    a.y = min(a.y,p.y);
    b.x = max(b.x,p.x);
    b.y = max(b.y,p.y);
  }
}

class NavDataset
{
  List<NavSample> samplesAll = new ArrayList();
  List<NavSample> samplesClean = new ArrayList();
  BoundBox boundBox = new BoundBox();
  
  private float sumLat = 0,sumLon = 0;
  
  void loadJSON_File(String filename)
  {
    JSONArray samps = loadJSONArray(filename);
    
    for (int i=0; i<samps.size(); i++)
    {
      JSONObject obj = samps.getJSONObject(i);
      add(new NavSample(obj));
    }
  }
  
  void add(NavSample sample)
  {
    samplesAll.add(sample);
    
    if (sample.isValid())
    {
      samplesClean.add(sample);
      
      // accumulate bounding box around clean samples
      boundBox.add(new PVector(sample.coord.lon(),sample.coord.lat()));
      
      // keep runing sum
      sumLat += sample.coord.lat();
      sumLon += sample.coord.lon();
    }
  }
  
  double startTime()
  {
    double time = 0;
    
    if (samplesClean.size() > 0)
    {
      time = samplesClean.get(0).getTime();
    }
    
    return time;
  }
  
  double endTime()
  {
    double time = 0;
    
    int size = samplesClean.size();
    if (size > 0)
    {
      time = samplesClean.get(size-1).getTime();
    }
    
    return time;
  }
  
  void computeVelocity()
  {
    if (samplesClean.size() > 1)
    {
      for (int i=1; i<samplesClean.size(); i++)
      {
        samplesClean.get(i-1).inferVelocity(samplesClean.get(i));
      }
    }
  }
  
  PVector mean()
  {
    PVector p = new PVector(sumLon,sumLat);
    p.x /= samplesClean.size();
    p.y /= samplesClean.size();
    return p;
  }
  
  String toString()
  {
    String str = "";
    str += "Total samples:       " + samplesAll.size()   + "\n";
    str += "Total clean samples: " + samplesClean.size() + "\n";
    str += "min coord:           " + boundBox.a          + "\n";
    str += "max coord:           " + boundBox.b          + "\n";
    str += "mean coord:          " + mean()              + "\n";
    return str;
  }
}
