class ClickDragTracker
{
  protected PVector i = new PVector();
  protected PVector f = new PVector();
  protected boolean hasFirst = false;
  
  void mousePressed()
  {
    hasFirst = true;
    i.x = mouseX;
    i.y = mouseY;
    f.x = i.x;
    f.y = i.y;
  }
  
  void mouseReleased()
  {
    hasFirst = false;
    f.x = mouseX;
    f.y = mouseY;
  }
  
  void mouseDragged()
  {
    if (hasFirst)
    {
      f.x = mouseX;
      f.y = mouseY;
    }
  }
  
  PVector getDisplacement()
  {
    PVector disp = f.copy();
    return disp.sub(i);
  }
  
  boolean isDragging()
  {
    return hasFirst;
  }
}
