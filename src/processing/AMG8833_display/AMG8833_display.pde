import processing.serial.*;
import java.nio.*;

Serial myPort;

void setup()
{
  printArray(Serial.list());
  
  myPort = new Serial(this, Serial.list()[0], 115200);
  
  size(800,800);
}

final int NUM_X_PIXELS = 8;
final int NUM_Y_PIXELS = 8;
final byte[] buffer = new byte[8*8*4];
boolean sawStart = false;
int idx = 0;

final color MIN_COLOR = color(0,0,255);
final color MAX_COLOR = color(255,0,0);

boolean autoTemp = true;

void drawThermalImage(FloatBuffer pxData)
{
  int w = width/NUM_X_PIXELS;
  int h = height/NUM_Y_PIXELS;
  
  float minTemp = Float.MAX_VALUE;
  float maxTemp = Float.MIN_VALUE;
  if (autoTemp)
  {
    for (int i=0; i<(NUM_X_PIXELS*NUM_Y_PIXELS); i++)
    {
      float temp = pxData.get(i);
      minTemp = Math.min(minTemp,temp);
      maxTemp = Math.max(maxTemp,temp);
    }
  }
  else
  {
    minTemp = 37;
    maxTemp = 48;
  }
  
  for (int y=0; y<NUM_Y_PIXELS; y++)
  {
    for (int x=0; x<NUM_X_PIXELS; x++)
    {
      float temp = pxData.get(y*NUM_X_PIXELS+x);
      float pxRatio = map(temp,minTemp,maxTemp,0.0,1.0);
      color pxColor = lerpColor(MIN_COLOR,MAX_COLOR,pxRatio);
      fill(pxColor);
      rect((NUM_Y_PIXELS-y-1)*w,x*h,w,h);// correcting for orientation of sensor here
      //print(temp);
      //print(",");
    }
    //println();
  }
}

void draw()
{
  
  while (myPort.available() > 0)
  {
    int c = myPort.read();
    if ( ! sawStart && idx == 0 && (char)(c) == '$')
    {
      //println("start");
      sawStart = true;
    }
    else if (sawStart && idx == 256 && (char)(c) == ';')
    {
      //println("finished! read %d bytes", idx);
      FloatBuffer pxData = ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).asFloatBuffer();
      drawThermalImage(pxData);
      sawStart = false;
      idx = 0;
    }
    else if (idx >= 256)
    {
      sawStart = false;
    }
    else if (sawStart)
    {
      if (idx < buffer.length)
      {
        buffer[idx++] = (byte)c;
      }
    }
  }
}
