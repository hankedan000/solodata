import can
import time
import random

bustype = 'socketcan_native'
channel = 'can0'

print('Initializing ' + channel + '...')
bus = can.interface.Bus(channel=channel,bustype=bustype)
print('CAN bus initialized!')

while True:
	r = random.randint(0,255)
	g = random.randint(0,255)
	b = random.randint(0,255)
	msg = can.Message(data=[r,g,b])
	bus.send(msg)
	time.sleep(1)

exit()