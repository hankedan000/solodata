# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'solodata.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from threading import Thread
from jou.Reader import Reader
import threading
import socket
import pynmea2
import sys
import ctypes

HOST = '192.168.1.178'
PORT = 1230

class NMEA_Thread(QtCore.QThread):
  gotNMEA = QtCore.pyqtSignal(object)
  
  def __init__(self):
    QtCore.QThread.__init__(self)
    self.s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    try:
        self.s.connect((HOST,PORT))
    except:
        try:
            self.s.connect(('localhost',PORT))
        except:
            self.s = None
    self.recording = False
    # file used for recording
    self.file = None

  def run(self):
      if self.s:
        try:
            streamreader = pynmea2.NMEAStreamReader()
            reader = Reader(self.s)

            # poke the streaming server to let it know we're here
            self.s.sendto('poke'.encode('utf-8'),(HOST,PORT))

            print('running...')
            while True:
                obj = reader.read()
                for nmea in streamreader.next(obj['nmea'] + '\n'):
                    # print(nmea)
                    self.gotNMEA.emit(nmea)
                    if self.recording:
                        self.file.write('%s\n' % (nmea))
        except Exception as ex:
            raise(ex)
        finally:
            # notify server that we're leaving
            self.s.sendto('quit'.encode('utf-8'),(HOST,PORT))
            self.s.close()

      return
      
  def onRecordRequest(self,start,runNum):
    okay = True
    
    if start:
      filename = 'run%d.nmea' % (runNum)
      print('Recording run%d to file "%s"!' % (runNum,filename))
      self.file = open(filename,'w')
    else:
      print('Saving run%d!' % (runNum))
      self.file.close()
      
    if okay:
      self.recording = start

class Ui_SoloData(object):
    def setupUi(self, SoloData):        
        SoloData.setObjectName("SoloData")
        SoloData.resize(789, 566)
        self.centralwidget = QtWidgets.QWidget(SoloData)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.run1 = QtWidgets.QRadioButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.run1.sizePolicy().hasHeightForWidth())
        self.run1.setSizePolicy(sizePolicy)
        self.run1.setMinimumSize(QtCore.QSize(0, 0))
        self.run1.setChecked(True)
        self.run1.setObjectName("run1")
        self.runRadios = QtWidgets.QButtonGroup(SoloData)
        self.runRadios.setObjectName("runRadios")
        self.runRadios.addButton(self.run1)
        self.horizontalLayout.addWidget(self.run1)
        self.run2 = QtWidgets.QRadioButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.run2.sizePolicy().hasHeightForWidth())
        self.run2.setSizePolicy(sizePolicy)
        self.run2.setObjectName("run2")
        self.runRadios.addButton(self.run2)
        self.horizontalLayout.addWidget(self.run2)
        self.run3 = QtWidgets.QRadioButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.run3.sizePolicy().hasHeightForWidth())
        self.run3.setSizePolicy(sizePolicy)
        self.run3.setChecked(False)
        self.run3.setObjectName("run3")
        self.runRadios.addButton(self.run3)
        self.horizontalLayout.addWidget(self.run3)
        self.run4 = QtWidgets.QRadioButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.run4.sizePolicy().hasHeightForWidth())
        self.run4.setSizePolicy(sizePolicy)
        self.run4.setObjectName("run4")
        self.runRadios.addButton(self.run4)
        self.horizontalLayout.addWidget(self.run4)
        self.run5 = QtWidgets.QRadioButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.run5.sizePolicy().hasHeightForWidth())
        self.run5.setSizePolicy(sizePolicy)
        self.run5.setObjectName("run5")
        self.runRadios.addButton(self.run5)
        self.horizontalLayout.addWidget(self.run5)
        self.run6 = QtWidgets.QRadioButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.run6.sizePolicy().hasHeightForWidth())
        self.run6.setSizePolicy(sizePolicy)
        self.run6.setMinimumSize(QtCore.QSize(0, 0))
        self.run6.setObjectName("run6")
        self.runRadios.addButton(self.run6)
        self.horizontalLayout.addWidget(self.run6)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setReadOnly(True)
        self.textEdit.setObjectName("textEdit")
        self.verticalLayout.addWidget(self.textEdit)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.newSessionButton = QtWidgets.QPushButton(self.centralwidget)
        self.newSessionButton.setMinimumSize(QtCore.QSize(0, 60))
        self.newSessionButton.setObjectName("newSessionButton")
        self.horizontalLayout_2.addWidget(self.newSessionButton)
        self.startStopButton = QtWidgets.QPushButton(self.centralwidget)
        self.startStopButton.setMinimumSize(QtCore.QSize(0, 60))
        self.startStopButton.setObjectName("startStopButton")
        self.horizontalLayout_2.addWidget(self.startStopButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        SoloData.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(SoloData)
        self.menubar.setEnabled(True)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 789, 22))
        self.menubar.setObjectName("menubar")
        SoloData.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(SoloData)
        self.statusbar.setEnabled(True)
        self.statusbar.setObjectName("statusbar")
        SoloData.setStatusBar(self.statusbar)

        self.retranslateUi(SoloData)
        QtCore.QMetaObject.connectSlotsByName(SoloData)
        
        self.startStopButton.clicked.connect(self.onStartStop)
        self.recording = False
        self.setStartStop(False)
        
        self.nmeaThread = NMEA_Thread()
        self.nmeaThread.gotNMEA.connect(self.onNMEA)
        self.nmeaThread.start()

    def onNMEA(self, nmea):
        self.textEdit.setText(str(nmea))

        if isinstance(nmea,pynmea2.types.talker.GSV):
            self.onGSV(nmea)

        # print(nmea)

    def onGSV(self, gsv):
        MAX_SV_PER_MSG = 4
        # print(gsv)

        def getSV(gsv,num):
            sv = {}
            sv['prn'] = int(getattr(gsv,'sv_prn_num_{}'.format(num)))
            sv['el']  = int(getattr(gsv,'elevation_deg_{}'.format(num)))
            sv['az']  = int(getattr(gsv,'azimuth_{}'.format(num)))
            try:
                sv['snr'] = int(getattr(gsv,'snr_{}'.format(num)))
            except ValueError:
                sv['snr'] = 0
            return sv

        def getNumSV(gsv):
            numSVsBeforeMe = (int(gsv.msg_num) - 1) * MAX_SV_PER_MSG
            numSVsInMe = int(gsv.num_sv_in_view) - numSVsBeforeMe
            if numSVsInMe > MAX_SV_PER_MSG:
                numSVsInMe = MAX_SV_PER_MSG
            return numSVsInMe

        svList = [getSV(gsv,i) for i in range(1,getNumSV(gsv))]
        
        for sv in svList:
            print(sv['snr'])

    def onStartStop(self):
        # toggle state
        self.recording = not self.recording
        
        currRun = self.getRunNum()
        # inform NMEA thread about recording state
        self.nmeaThread.onRecordRequest(self.recording,currRun)
        self.setStartStop(self.recording)
       
    def setStartStop(self,isRecording):
        if isRecording:
            self.startStopButton.setStyleSheet('background-color: red')
            self.startStopButton.setText('Stop')
            self.setRunRadiosEnabled(False)# disable run radio buttons
        else:
            self.startStopButton.setStyleSheet('background-color: green')
            self.startStopButton.setText('Start')
            self.setRunRadiosEnabled(True)# enable run radio buttons again
    
    def setRunRadiosEnabled(self,enabled):
      for button in self.runRadios.buttons():
        button.setEnabled(enabled)
    
    def getRunNum(self):
        for i in range(7):
          if self.runRadios.buttons()[i].isChecked():
            return i + 1
        return -1

    def retranslateUi(self, SoloData):
        _translate = QtCore.QCoreApplication.translate
        SoloData.setWindowTitle(_translate("SoloData", "SoloData"))
        self.run1.setText(_translate("SoloData", "Run1"))
        self.run2.setText(_translate("SoloData", "Run2"))
        self.run3.setText(_translate("SoloData", "Run3"))
        self.run4.setText(_translate("SoloData", "Run4"))
        self.run5.setText(_translate("SoloData", "Run5"))
        self.run6.setText(_translate("SoloData", "Run6"))
        self.newSessionButton.setText(_translate("SoloData", "New Session"))
        self.startStopButton.setText(_translate("SoloData", "Start"))

app = QtWidgets.QApplication(sys.argv)
SoloData = QtWidgets.QMainWindow()
ui = Ui_SoloData()
ui.setupUi(SoloData)
ui.textEdit.setText('hello')
SoloData.show()
app.exec_()
print('done!')

