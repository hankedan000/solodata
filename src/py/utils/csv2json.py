#!/usr/bin/env python3
import csv
import json
import argparse
import os

parser = argparse.ArgumentParser(prog='csv2json',
                                    usage='%(prog)s [options] csvfile',
                                    description='Convert csv files to json')

parser.add_argument(
    '--list-headers',
    action='store_true',
    help='prints column headers')

parser.add_argument(
    'csvfile',
    type=argparse.FileType('r'),
    nargs='+')

args = parser.parse_args()

# prints headers in a nice numbered list format
def listHeaders(headers):
    for i in range(len(headers)):
        print("%3d: %s" % (i,headers[i]))

for file in args.csvfile:
    # determine input/output file names
    ifname = file.name
    name = os.path.splitext(ifname)[0]
    ofname = name + '.json'

    # read data from csv file
    print('%s' % (ifname))
    data = dict ()
    reader = csv.reader(file)
    rowCount = 0
    headerRow = None
    sampleRows = []
    for row in reader:
        if rowCount is 0:
            headerRow = row

            # add all the headers to the dictionary
            for header in headerRow:
                data[header] = []
        else:
            sampleRows.append(row)

            # add samples to the dictionary
            for i in range(len(row)):
                header = headerRow[i]
                data[header].append(row[i])
        rowCount += 1
    print('\tThere are %d row(s)' % (rowCount))
    print('\tFound %d column(s)' % (len(headerRow)))

    # perform output operation
    if args.list_headers:
        listHeaders(list(data.keys()))
    else:
        with open(ofname, 'w') as of:
            json.dump(data,of)
