#!/usr/bin/env python3
import json
import argparse
import os
import datetime

parser = argparse.ArgumentParser(
    prog='json2inu',
    usage='%(prog)s [options] jsonfile',
    description='Convert json files containing arrays of nav data to INU samples')

parser.add_argument(
    '--list',
    action='store_true',
    help='prints array names')

parser.add_argument(
    '--lat',
    type=int,
    help='specify which array index contains latitude data')

parser.add_argument(
    '--lon',
    type=int,
    help='specify which array index contains longitude data')

parser.add_argument(
    '--ax',
    type=int,
    help='specify which array index contains acceleration x data')

parser.add_argument(
    '--ay',
    type=int,
    help='specify which array index contains acceleration y data')

parser.add_argument(
    '--az',
    type=int,
    help='specify which array index contains acceleration z data')

parser.add_argument(
    '--year',
    type=int,
    help='specify which array index contains current year')

parser.add_argument(
    '--month',
    type=int,
    help='specify which array index contains current month')

parser.add_argument(
    '--day',
    type=int,
    help='specify which array index contains current day')

parser.add_argument(
    '--hour',
    type=int,
    help='specify which array index contains current hour')

parser.add_argument(
    '--minute',
    type=int,
    help='specify which array index contains current minute')

parser.add_argument(
    '--second',
    type=int,
    help='specify which array index contains current second')

parser.add_argument(
    '--millisecond',
    type=int,
    help='specify which array index contains current millisecond')

parser.add_argument(
    'jsonfile',
    type=argparse.FileType('r'),
    nargs='+')

args = parser.parse_args()

arraysToParse = dict()
if args.lat:
    arraysToParse[args.lat] = 'lat'
if args.lon:
    arraysToParse[args.lon] = 'lon'
if args.ax:
    arraysToParse[args.ax] = 'ax'
if args.ay:
    arraysToParse[args.ay] = 'ay'
if args.az:
    arraysToParse[args.az] = 'az'
if args.year:
    arraysToParse[args.year] = 'utc.year'
if args.month:
    arraysToParse[args.month] = 'utc.month'
if args.day:
    arraysToParse[args.day] = 'utc.day'
if args.hour:
    arraysToParse[args.hour] = 'utc.hour'
if args.minute:
    arraysToParse[args.minute] = 'utc.minute'
if args.second:
    arraysToParse[args.second] = 'utc.second'
if args.millisecond:
    arraysToParse[args.millisecond] = 'utc.millisecond'

# prints names in a nice numbered list format
def listHeaders(headers):
    for i in range(len(headers)):
        print("%3d: %s" % (i,headers[i]))

def assignSampVal(samp,valName,valStr):
    typeByValName = {
        'lat':'float',
        'lon':'float',
        'ax':'float',
        'ay':'float',
        'az':'float',
        'utc.year':'int',
        'utc.month':'int',
        'utc.day':'int',
        'utc.hour':'int',
        'utc.minute':'int',
        'utc.second':'int',
        'utc.millisecond':'int'
    }

    typeName = typeByValName[valName]
    val = None
    if typeName is 'float':
        val = float(valStr)
    elif typeName is 'int':
        val = int(valStr)

    if 'utc' in valName:
        part = valName.split('.')[1]
        date = datetime.datetime.fromtimestamp(samp['utc'])
        if part == 'year':
            date = date.replace(year=val)
        elif part == 'month':
            date = date.replace(month=val)
        elif part == 'day':
            date = date.replace(day=val)
        elif part == 'hour':
            date = date.replace(hour=val)
        elif part == 'minute':
            date = date.replace(minute=val)
        elif part == 'second':
            date = date.replace(second=val)
        elif part == 'millisecond':
            date = date.replace(microsecond=val*1000)
        samp['utc'] = date.timestamp()
    else:
        samp[valName] = val

def getINU_Samps(data,arraysToParse):
    colNames = list(data.keys())
    srcToDest = dict()
    for colIdx,dest in arraysToParse.items():
        srcToDest[colNames[colIdx]] = dest

    samps = []
    for src,dest in srcToDest.items():
        srcArray = data[src]
        for i in range(len(srcArray)):
            try:
                # see if sample exists already
                samps[i]
            except IndexError:
                # make new empty brand new sample!
                samps.append({
                    'lat':0.0,
                    'lon':0.0,
                    'ax':0.0,
                    'ay':0.0,
                    'az':0.0,
                    'utc':0.0
                })
            assignSampVal(samps[i],dest,srcArray[i])

    return samps

for file in args.jsonfile:
    # determine input/output file names
    ifname = file.name
    name = os.path.splitext(ifname)[0]
    ofname = name + '_inu.json'

    # read data from json file
    print('%s' % (ifname))
    data = json.load(file)

    # perform output operation
    if args.list or not any(arraysToParse):
        listHeaders(list(data.keys()))
    elif any(arraysToParse):
        samps = getINU_Samps(data,arraysToParse)
        with open(ofname, 'w') as of:
            json.dump(samps,of)

    # headerRow = None
    # sampleRows = []
    # for row in reader:
    #     if rowCount is 0:
    #         headerRow = row
    #
    #         # add all the headers to the dictionary
    #         for header in headerRow:
    #             data[header] = []
    #     else:
    #         sampleRows.append(row)
    #
    #         # add samples to the dictionary
    #         for i in range(len(row)):
    #             header = headerRow[i]
    #             data[header].append(row[i])
    #     rowCount += 1
    # print('\tThere are %d row(s)' % (rowCount))
    # print('\tFound %d column(s)' % (len(headerRow)))
