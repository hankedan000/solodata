#!/usr/bin/env python3
import json
import argparse
import os
import datetime
import pynmea2
import datetime

parser = argparse.ArgumentParser(
    prog='json2inu',
    usage='%(prog)s [options] nmeafile',
    description='Convert json files containing arrays of nav data to INU samples')

parser.add_argument(
    '--list',
    action='store_true',
    help='prints array names')

parser.add_argument(
    '--lat',
    type=int,
    help='specify which array index contains latitude data')

parser.add_argument(
    '--lon',
    type=int,
    help='specify which array index contains longitude data')

parser.add_argument(
    '--ax',
    type=int,
    help='specify which array index contains acceleration x data')

parser.add_argument(
    '--ay',
    type=int,
    help='specify which array index contains acceleration y data')

parser.add_argument(
    '--az',
    type=int,
    help='specify which array index contains acceleration z data')

parser.add_argument(
    '--year',
    type=int,
    help='specify which array index contains current year')

parser.add_argument(
    '--month',
    type=int,
    help='specify which array index contains current month')

parser.add_argument(
    '--day',
    type=int,
    help='specify which array index contains current day')

parser.add_argument(
    '--hour',
    type=int,
    help='specify which array index contains current hour')

parser.add_argument(
    '--minute',
    type=int,
    help='specify which array index contains current minute')

parser.add_argument(
    '--second',
    type=int,
    help='specify which array index contains current second')

parser.add_argument(
    '--millisecond',
    type=int,
    help='specify which array index contains current millisecond')

parser.add_argument(
    'nmeafile',
    type=argparse.FileType('r'),
    nargs='+')

args = parser.parse_args()

def getINU_Samps(sentences):
    samps = []
    dt_1970 = datetime.datetime(1970, 1, 1, 0, 0, 0)
    for nmea in sentences:
        dt_samp = nmea.datetime
        delta = dt_samp - dt_1970
        samps.append({
            'lat':nmea.latitude,
            'lon':nmea.longitude,
            'speed_knots':nmea.spd_over_grnd,
            'ax':0.0,
            'ay':0.0,
            'az':0.0,
            'utc': delta.total_seconds()
        })

    return samps

for file in args.nmeafile:
    # determine input/output file names
    ifname = file.name
    name = os.path.splitext(ifname)[0]
    ofname = name + '_inu.json'

    # read data from nmea file
    print('%s' % (ifname))
    streamreader = pynmea2.NMEAStreamReader()
    line = file.readline()
    sentences = []
    while line:
        for nmea in streamreader.next(line):
            sentences.append(nmea)
        line = file.readline()

    # perform output operation
    samps = getINU_Samps(sentences)
    with open(ofname, 'w') as of:
        json.dump(samps,of)

