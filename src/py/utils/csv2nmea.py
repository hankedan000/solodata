#!/usr/bin/env python3
import csv
import pynmea2
import argparse
import os

parser = argparse.ArgumentParser(
    prog='csv2json',
    usage='%(prog)s [options] csvfile',
    description='Convert csv files to json')

parser.add_argument(
    '--list-headers',
    action='store_true',
    help='prints column headers')

parser.add_argument(
    'csvfile',
    type=argparse.FileType('r'),
    nargs='+')

args = parser.parse_args()

# prints headers in a nice numbered list format
def listHeaders(headers):
    for i in range(len(headers)):
        print("%3d: %s" % (i,headers[i]))

def makeRMC(lat,lng):
    NorS = ('N' if lat > 0 else 'S')
    EorW = ('E' if lng > 0 else 'W')
    return pynmea2.RMC('GP', 'RMC',(
        '0.0',# timestamp
        'A',# status; 'A' -> active; 'V' -> void
        str(abs(lat)),# latitude
        NorS,# latitude direction 'N' or 'S'
        str(abs(lng)),# longitude
        EorW,# longitude direction 'E' or 'W'
        '0.0',# speed over ground in knots
        '0.0',# true course angle in degress
        '060220',# date stamp 
        '0.0',# magnetic variation
        'E'))# magnetic variation direction

for file in args.csvfile:
    # determine input/output file names
    ifname = file.name
    name = os.path.splitext(ifname)[0]
    ofname = name + '.nmea'

    # read data from csv file
    print('%s' % (ifname))
    data = dict ()
    reader = csv.reader(file)
    rowCount = 0
    headerRow = None
    sampleRows = []

    lat_col_idx = 44
    lng_col_idx = 45

    nmeas = []

    for row in reader:
        if rowCount is 0:
            headerRow = row

            # add all the headers to the dictionary
            for header in headerRow:
                data[header] = []
        else:
            sampleRows.append(row)

            # add samples to the dictionary
            for i in range(len(row)):
                header = headerRow[i]
                data[header].append(row[i])
        rowCount += 1
    print('\tThere are %d row(s)' % (rowCount))
    print('\tFound %d column(s)' % (len(headerRow)))

    # perform output operation
    if args.list_headers:
        listHeaders(list(data.keys()))
    else:
        listHeaders(list(data.keys()))
        lat_col_idx = int(input('Select latitude index: '))
        lng_col_idx = int(input('Select longitude index: '))

        keys = list(data.keys())
        lat = data[keys[lat_col_idx]]
        lng = data[keys[lng_col_idx]]

        with open(ofname, 'w') as of:
            for i in range(len(lat)):
                nmea = makeRMC(float(lat[i]),float(lng[i]))
                of.write('%s\n' % nmea)
