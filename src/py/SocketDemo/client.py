#!/usr/bin/env python
import socket

HOST = 'localhost'
PORT = 1234

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect((HOST,PORT))
try:
	while True:
		msg = s.recv(1024)
		print(msg)
except KeyboardInterrupt:
	print('Goodbye!')
finally:
	s.close()
