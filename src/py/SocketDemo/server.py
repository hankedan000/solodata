#!/usr/bin/env python
import socket
import time
import threading
import ctypes
from threading import Thread
from Queue import Queue

HOST = 'localhost'
PORT = 1234

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind((HOST,PORT))

class ClientThread(Thread):
	def __init__(self,conn,addr):
		super(ClientThread,self).__init__()
		self.conn = conn
		self.addr = addr
		self.queue = Queue()

	def addMsg(self,msg):
		self.queue.put(msg)

	def run(self):
		try:
			while True:
				msg = self.queue.get(block=True)
				self.conn.send(msg)
		except socket.error as e:
			print('%s disconnected!' % (str(self.addr)))
		finally:
			self.conn.close()

print('Listening...')
s.listen(1)
workers = []

class ProducerThread(Thread):
	def run(self):
		try:
			while True:
				time.sleep(1)
				for worker in workers:
					worker.addMsg('Yello!')
		except:
			# shutdown the thread
			return

	def get_id(self):
		# returns id of the respective thread
		if hasattr(self, '_thread_id'):
			return self._thread_id
		for id, thread in threading._active.items():
			if thread is self:
				return id

	def raise_exception(self):
		thread_id = self.get_id()
		res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id,
			ctypes.py_object(SystemExit))
		if res > 1:
			ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
			print('Exception raise failure')

producer = ProducerThread()
producer.start()

try:
	while True:
		print('Accepting...')
		conn,addr = s.accept()
		print('Found a client! %s' % (str(addr)))
		worker = ClientThread(conn,addr)
		worker.start()
		workers.append(worker)
except KeyboardInterrupt:
	print('Shutting down...')

s.close()
print('Waiting for producer to join...')
producer.raise_exception()
producer.join()
print('Producer done!')
