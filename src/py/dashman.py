from wmctrl import Window
from wmctrl import Desktop

def isBlacklisted(win):
  BLACKLIST = [
    ('wm_class','desktop_window'),
    ('wm_name','desktop_window')]
    
  for bl in BLACKLIST:
    if bl[1] in getattr(win,bl[0]):
      return True
        
  return False

def blacklist(windows):
  outWins = []
  for win in windows:
    if not isBlacklisted(win):
      outWins.append(win)
  
  return outWins

wins = blacklist(Window.list())
desks = Desktop.list()

########################################

import pygtk  
pygtk.require('2.0')  
import gtk  
import wnck  

screen = wnck.screen_get_default()  

while gtk.events_pending():  
    gtk.main_iteration(False)  

for w in screen.get_windows():  
    name = w.get_name()  
    icon = w.get_icon()
    
########################################

import tkinter as tk 
from tkinter import Listbox

def onselect(evt):
    # Note here that Tkinter passes an event object to onselect()
    w = evt.widget
    index = int(w.curselection()[0])
    value = w.get(index)
    wins[index].activate()
    print 'You selected item %d: "%s"' % (index, value)

ui = tk.Tk() 
ui.title('Dash Manager') 
Lb = Listbox(ui)
i = 0
for win in wins:
  Lb.insert(i,win.wm_name)
  i += 1
Lb.pack() 
Lb.bind('<<ListboxSelect>>', onselect)
ui.mainloop() 

#while True:  
#  data = eval(raw_input("Enter a number: "))
#  wins[data].activate()

  
#print("Desktops:")
#for desk in desks:
#  print(desk)
