import json

class Reader():
	def __init__(self,sock=None):
		self.reset()
		self.sock = sock

	def reset(self):
		self.working_on_packet = False
		self.packets_have = 0
		self.buffer = ''

	def read(self):
		if self.sock:
			# reset incase we were working on an existing stream
			self.reset()

			while True:
				# this 64000 should match Writer + a little
				data, addr = self.sock.recvfrom(64000)

				packet = json.loads(data.decode('utf-8'))
				for obj in self.next(packet):
					return obj

	def next(self,packet):
		if self.working_on_packet and packet['packet'] == 0:
			print('starting new packet before finishing previous')
			self.reset()

		self.working_on_packet = True
		self.buffer += packet['obj_slice']
		self.packets_have += 1

		if self.packets_have == packet['n_packets']:
			obj = json.loads(self.buffer)
			yield obj
			
			self.reset()

	__next__ = next

	def __iter__(self):
		'''
		Support the iterator protocol.
		'''
		return self