#!/usr/bin/env python3
import socket
import argparse
import time
import queue
import json
from Reader import Reader
from threading import Thread

parser = argparse.ArgumentParser(
    prog='dispserver',
    usage='%(prog)s [options]',
    description='Services a socket connection to compute fips vectors')

args = parser.parse_args()

HOST = '127.0.0.1'
PORT = 1230

s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
try:
	s.connect((HOST,PORT))
except:
	s.connect(('localhost',PORT))

# poke the streaming server to let it know we're here
s.sendto('poke'.encode('utf-8'),(HOST,PORT))

print('Listening...')
try:
	reader = Reader(s)
	while True:
		obj = reader.read()
		if obj:
			print(obj)

except KeyboardInterrupt:
	print('Shutting down...')

# notify server that we're leaving
s.sendto('quit'.encode('utf-8'),(HOST,PORT))

s.close()
