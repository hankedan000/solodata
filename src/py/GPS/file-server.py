#!/usr/bin/env python3
import socket
import pynmea2
import queue
import argparse
import time
import datetime as dt
import proto.NavSample_pb2 as NavSample_pb2
from tqdm import tqdm
from jou.Writer import Writer
from threading import Thread

parser = argparse.ArgumentParser(
    prog='file-server',
    usage='%(prog)s [options] nmeafile',
    description='Reads NMEA strings from a file and plays them back over socket connection')

parser.add_argument(
	'--loop',
	action='store_true',
	default=False)

parser.add_argument(
    'nmeafile',
    type=argparse.FileType('r'))

args = parser.parse_args()

HOST = '192.168.1.178'
PORT = 1230

s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
try:
	s.bind((HOST,PORT))
	print('bound to %s:%d' % (HOST,PORT))
except:
	s.bind(('localhost',PORT))
	print('bound to %s:%d' % ('localhost',PORT))

class WorkerThread(Thread):
	def __init__(self,sock,addr):
		super(WorkerThread,self).__init__()
		self.writer = Writer(sock,addr)
		self.addr = addr
		self.queue = queue.Queue()
		self.alive = True
		self._stayAlive = True

	def addMsg(self,msg):
		self.queue.put(msg)

	def quit(self):
		print('Requested worker for %s to quit...' % str(self.addr))
		self._stayAlive = False

	def run(self):
		try:
			while self._stayAlive:
				msg = self.queue.get(block=True)
				self.writer.write(msg,0)
		except socket.error as e:
			print('caught exception in worker %s' % (str(self.addr)))
			print(e)

		print('Worker for %s stopped.' % str(self.addr))
		self.alive = False

workers = {}

class ProducerThread(Thread):
	def __init__(self):
		super(ProducerThread,self).__init__()
		self.alive = True
		self._stayAlive = True
		self._prevTimestamp = None
		self._ticTimestamp = dt.datetime.now()

	def quit(self):
		print('Requested producer to quit...')
		self._stayAlive = False

	def notifyWorkers(self,msg):
		delete_me = []

		for addr,worker in workers.items():
			if worker.alive:
				worker.addMsg(msg)
			else:
				print('Worker no longer alive... removing from list')
				delete_me.append(addr)

		for addr in delete_me:
			del workers[addr]

	def nmea2dict(self,nmea):
		nmea_dict = {}
		for talker_field in nmea.fields:
			field_name = talker_field[1]
			field_val = getattr(nmea,field_name)
			if isinstance(field_val,dt.date):
				# convert date to format "yyyy-mm-dd"
				field_val = str(field_val)
			elif isinstance(field_val,dt.time):
				# convert time to format "hh:mm:ss.ssssss"
				field_val = str(field_val)
			nmea_dict[field_name] = field_val
		return nmea_dict

	def tic(self):
		self._ticTimestamp = dt.datetime.now()

	def toc(self):
		return dt.datetime.now() - self._ticTimestamp

	def run(self):
		try:
			streamreader = pynmea2.NMEAStreamReader()

			# read how many lines are in the file
			num_lines = sum(1 for line in args.nmeafile)
			args.nmeafile.seek(0)

			# make a progress bar
			pbar = tqdm(total=num_lines)

			while self._stayAlive:
				# read a line form the nmea file
				line = args.nmeafile.readline()
				pbar.update(1)

				# attempt to parse NMEA string and send to workers
				for nmea in streamreader.next(line):
					# check the timestamp to see if we should pause to
					# simulate the real playback rate
					if nmea.sentence_type == 'RMC':
						if self._prevTimestamp:
							delta = dt.datetime.combine(dt.date.min,nmea.timestamp) - self._prevTimestamp

							# be nice to the CPU and sleep until next timestamp
							while self.toc() < delta:
								sleeptime = delta.total_seconds() / 4.0
								time.sleep(sleeptime)

						self.tic()
						self._prevTimestamp = dt.datetime.combine(dt.date.min,nmea.timestamp)

					ns = NavSample_pb2.NavSample()
					print(self.nmea2dict(nmea))

					# broadcast the NMEA to all workers
					self.notifyWorkers({
						'nmea':str(nmea),
						'sentence_type':nmea.sentence_type,
						'nmea_dict':self.nmea2dict(nmea)
						})

				# if we reach end of file, handle accordingly
				if line == '':
					pbar.refresh()
					pbar.reset()
					if args.loop:
						# loop back to beginning of file and playback
						args.nmeafile.seek(0)
					else:
						self.quit()
		except Exception as e:
			print(e)
			# shutdown the thread

		print("Producer shutdown complete.")
		self.alive = False

producer = ProducerThread()
producer.start()

try:
	print('Playing back...')
	while True:
		data, addr = s.recvfrom(1024)

		data = data.decode('utf-8')
		if data == 'quit':
			if addr in workers:
				workers[addr].quit()
		elif data == 'poke':
			print('Found a new client! %s' % (str(addr)))
			worker = WorkerThread(s,addr)
			worker.start()
			workers[addr] = worker
		else:
			print('Unhandled data \'%s\'' % data)

except KeyboardInterrupt:
	print('Shutting down...')

# notify all workers to shutdown
for worker in workers.values():
	worker.quit()

# notify producer to shutdown
producer.quit()

# wait for all workers to join
for worker in workers.values():
	worker.join()

# wait for producer to join
producer.join()

# close the socket now that everyone joined
s.close()
