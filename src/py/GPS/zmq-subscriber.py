#!/usr/bin/env python3
from NavSample_pb2 import *
import zmq

HOST = 'localhost'
PORT = 6060

context = zmq.Context()
socket = context.socket(zmq.SUB)
try:
	socket.connect("tcp://%s:%d" % (HOST,PORT))
	socket.subscribe("")
	print('connected to %s:%d' % (HOST,PORT))
except:
	print('Failed to connect to %s:%d' % (HOST,PORT))
	exit(1)

try:
	print('listening...')
	ns = NavSample()
	while True:
		msg = socket.recv()
		ns.ParseFromString(msg)
		print(ns)

except KeyboardInterrupt:
	print('Shutting down...')
