#!/usr/bin/env python3
import pynmea2
import argparse
import time
import datetime as dt
import zmq
from NavSample_pb2 import *
from tqdm import tqdm
from threading import Thread

parser = argparse.ArgumentParser(
    prog='zmq-file-server',
    usage='%(prog)s [options] nmeafile',
    description='Reads NMEA strings from a file and plays them back over socket connection')

parser.add_argument(
	'--loop',
	action='store_true',
	default=False)

parser.add_argument(
    'nmeafile',
    type=argparse.FileType('r'))

args = parser.parse_args()

HOST = '*'
PORT = 6060

context = zmq.Context()
socket = context.socket(zmq.PUB)
try:
	socket.bind("tcp://%s:%d" % (HOST,PORT))
	print('bound to %s:%d' % (HOST,PORT))
except:
	print('Failed to bind to %s:%d' % (HOST,PORT))
	exit(1)

# convert decimal degrees minutes coordinate to decimal degrees
def ddm_to_dd(ddm):
	degs = int(ddm / 100)
	mins = ddm - degs * 100
	return degs + mins / 60.0

def h_to_sign(h):
	if h.upper() in ["N","E"]:
		return +1
	elif h.upper() in ["S","W"]:
		return -1
	else:
		raise RuntimeError("Invalid heading '%s'" % h)

class ProducerThread(Thread):
	def __init__(self,socket):
		super(ProducerThread,self).__init__()
		self.alive = True
		self._socket = socket
		self._stayAlive = True
		self._prevTimestamp = None
		self._ticTimestamp = dt.datetime.now()

	def quit(self):
		print('Requested producer to quit...')
		self._stayAlive = False

	def nmea2dict(self,nmea):
		nmea_dict = {}
		for talker_field in nmea.fields:
			field_name = talker_field[1]
			field_val = getattr(nmea,field_name)
			if isinstance(field_val,dt.date):
				# convert date to format "yyyy-mm-dd"
				field_val = str(field_val)
			elif isinstance(field_val,dt.time):
				# convert time to format "hh:mm:ss.ssssss"
				field_val = str(field_val)
			nmea_dict[field_name] = field_val
		return nmea_dict

	def nmea2utc(self,nmea):
		ts = dt.datetime(
			nmea.datestamp.year,
			nmea.datestamp.month,
			nmea.datestamp.day,
			nmea.timestamp.hour,
			nmea.timestamp.minute,
			nmea.timestamp.second,
			nmea.timestamp.microsecond)
		return ts.timestamp()

	def tic(self):
		self._ticTimestamp = dt.datetime.now()

	def toc(self):
		return dt.datetime.now() - self._ticTimestamp

	def run(self):
		try:
			streamreader = pynmea2.NMEAStreamReader()

			# read how many lines are in the file
			num_lines = sum(1 for line in args.nmeafile)
			args.nmeafile.seek(0)

			# make a progress bar
			pbar = tqdm(total=num_lines)

			while self._stayAlive:
				# read a line form the nmea file
				line = args.nmeafile.readline()
				pbar.update(1)

				# attempt to parse NMEA string and send to workers
				for nmea in streamreader.next(line):
					# check the timestamp to see if we should pause to
					# simulate the real playback rate
					if nmea.sentence_type == 'RMC':
						if self._prevTimestamp:
							delta = dt.datetime.combine(dt.date.min,nmea.timestamp) - self._prevTimestamp

							# be nice to the CPU and sleep until next timestamp
							while self.toc() < delta:
								sleeptime = delta.total_seconds() / 4.0
								time.sleep(sleeptime)

						self.tic()
						self._prevTimestamp = dt.datetime.combine(dt.date.min,nmea.timestamp)

						# print(self.nmea2dict(nmea))

						ns = NavSample()
						ns.utc_us = int(self.nmea2utc(nmea) * 1000000)
						ns.gps.fixed = nmea.status == 'A'
						ns.gps.speed_knots = nmea.spd_over_grnd
						try:
							ns.gps.true_course_deg = float(nmea.true_course)
						except:
							pass
						ns.gps.position.lat_dd = ddm_to_dd(float(nmea.lat)) * h_to_sign(nmea.lat_dir)
						ns.gps.position.lon_dd = ddm_to_dd(float(nmea.lon)) * h_to_sign(nmea.lon_dir)

						# publish the NavSample
						self._socket.send(ns.SerializeToString())

				# if we reach end of file, handle accordingly
				if line == '':
					pbar.refresh()
					pbar.reset()
					if args.loop:
						# loop back to beginning of file and playback
						args.nmeafile.seek(0)
					else:
						self.quit()
		except Exception as e:
			print(e)
			# shutdown the thread

		print("Producer shutdown complete.")
		self.alive = False

producer = ProducerThread(socket)
producer.start()

try:
	print('Playing back...')
	while True:
		time.sleep(1)

except KeyboardInterrupt:
	print('Shutting down...')

# notify producer to shutdown
producer.quit()

# wait for producer to join
producer.join()
