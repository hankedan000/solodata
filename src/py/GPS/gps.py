#!/usr/bin/env python
import serial
import ublox.ubx as ubx
import pynmea2

# create a ublox stream class for configuring GPS module
def configGPS(serial):
  okay = True
  print('Initializing GPS...')
  
  try:
    x = ubx.UbxStream(serial)
    x.baudrate = 115200
    x.disable_NMEA()
    x.cfg_rate(100)# 10Hz
    x.enable_message(240,4)# enable RMC
    print('GPS configured!')
  except:
    print('Failed to configure GPS.')
    okay = False
    
  return okay

# get connection to GPS serial port
ser = serial.Serial ("/dev/ttyS0")    #Open named port 
ser.baudrate = 9600                   #Set baud rate to 9600
if configGPS(ser):
  try:
    streamreader = pynmea2.NMEAStreamReader()
    while True:
      data = ser.read(64);
      for msg in streamreader.next(data):
        print(msg)
      
  except KeyboardInterrupt:
	    pass	
    
ser.close()
