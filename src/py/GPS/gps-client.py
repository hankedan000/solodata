#!/usr/bin/env python
import socket
import pickle
import pynmea2

HOST = '192.168.1.178'
PORT = 1234

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
try:
	s.connect((HOST,PORT))
except:
	s.connect(('localhost',PORT))
try:
	while True:
		msg = s.recv(1024)
		nmea = pickle.loads(msg)
		print(nmea)
except KeyboardInterrupt:
	print('Goodbye!')
finally:
	s.close()
