#!/usr/bin/env python3
import pynmea2
import serial
import ublox.ubx as ubx
import argparse
import time
import datetime as dt
import zmq
from NavSample_pb2 import *
from threading import Thread

parser = argparse.ArgumentParser(
    prog='zmq-serial-server',
    usage='%(prog)s [options] serial_device',
    description='Reads NMEA strings from a GPS module connected to serial port')

parser.add_argument(
    'serial_device',
    type=str)

args = parser.parse_args()

# convert decimal degrees minutes coordinate to decimal degrees
def ddm_to_dd(ddm):
	degs = int(ddm / 100)
	mins = ddm - degs * 100
	return degs + mins / 60.0

def h_to_sign(h):
	if h.upper() in ["N","E"]:
		return +1
	elif h.upper() in ["S","W"]:
		return -1
	else:
		raise RuntimeError("Invalid heading '%s'" % h)

def nmea2utc(nmea):
	ts = dt.datetime(
		nmea.datestamp.year,
		nmea.datestamp.month,
		nmea.datestamp.day,
		nmea.timestamp.hour,
		nmea.timestamp.minute,
		nmea.timestamp.second,
		nmea.timestamp.microsecond)
	return ts.timestamp()

# create a ublox stream class for configuring GPS module
def configGPS(serial):
	okay = True
	print('Initializing GPS...')

	try:
		x = ubx.UbxStream(serial)
		x.baudrate = 115200
		x.disable_NMEA()
		x.cfg_rate(100)# 10Hz
		x.enable_message(240,4)# enable RMC
		x.enable_message(240,3)# enable GSV
	except:
		print('Failed to configure GPS.')
		okay = False

	return okay

# get connection to GPS serial port
ser = serial.Serial(args.serial_device)
ser.baudrate = 9600
if configGPS(ser):
	print('GPS configured!')

HOST = '*'
PORT = 6060

context = zmq.Context()
socket = context.socket(zmq.PUB)
try:
	socket.bind("tcp://%s:%d" % (HOST,PORT))
	print('bound to %s:%d' % (HOST,PORT))
except:
	print('Failed to bind to %s:%d' % (HOST,PORT))
	exit(1)

try:
	streamreader = pynmea2.NMEAStreamReader()
	while True:
		data = ser.read(64).decode('utf-8')

		for nmea in streamreader.next(data):
			# print(nmea)
			
			if nmea.sentence_type == 'RMC':
				ns = NavSample()
				ns.utc_us = int(nmea2utc(nmea) * 1000000)
				ns.gps.fixed = nmea.status == 'A'
				ns.gps.speed_knots = nmea.spd_over_grnd
				try:
					ns.gps.true_course_deg = float(nmea.true_course)
				except:
					pass
				ns.gps.position.lat_dd = ddm_to_dd(float(nmea.lat)) * h_to_sign(nmea.lat_dir)
				ns.gps.position.lon_dd = ddm_to_dd(float(nmea.lon)) * h_to_sign(nmea.lon_dir)

				# publish the NavSample
				socket.send(ns.SerializeToString())

except Exception as e:
	print(e)
	# shutdown the thread
