#!/usr/bin/env python3
import socket
import serial
import ublox.ubx as ubx
import pynmea2
import queue
import datetime
from jou.Writer import Writer
from threading import Thread

HOST = '192.168.1.178'
PORT = 1230

s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
try:
	s.bind((HOST,PORT))
	print('bound to %s:%d' % (HOST,PORT))
except:
	s.bind(('localhost',PORT))
	print('bound to %s:%d' % ('localhost',PORT))

class WorkerThread(Thread):
	def __init__(self,sock,addr):
		super(WorkerThread,self).__init__()
		self.writer = Writer(sock,addr)
		self.addr = addr
		self.queue = queue.Queue()
		self.alive = True
		self._stayAlive = True

	def addMsg(self,msg):
		self.queue.put(msg)

	def quit(self):
		print('Requested worker for %s to quit...' % str(self.addr))
		self._stayAlive = False

	def run(self):
		try:
			while self._stayAlive:
				msg = self.queue.get(block=True)
				self.writer.write(msg,0)
		except socket.error as e:
			print('caught exception in worker %s' % (str(self.addr)))
			print(e)

		print('Worker for %s stopped.' % str(self.addr))
		self.alive = False

workers = {}

# create a ublox stream class for configuring GPS module
def configGPS(serial):
	okay = True
	print('Initializing GPS...')

	try:
		x = ubx.UbxStream(serial)
		x.baudrate = 115200
		x.disable_NMEA()
		x.cfg_rate(100)# 10Hz
		x.enable_message(240,4)# enable RMC
		x.enable_message(240,3)# enable GSV
	except:
		print('Failed to configure GPS.')
		okay = False

	return okay

# get connection to GPS serial port
ser = serial.Serial('/dev/ttyS0')
ser.baudrate = 9600
if configGPS(ser):
	print('GPS configured!')

class ProducerThread(Thread):
	def __init__(self):
		super(ProducerThread,self).__init__()
		self.alive = True
		self._stayAlive = True

	def quit(self):
		print('Requested producer to quit...')
		self._stayAlive = False

	def notifyWorkers(self,msg):
		delete_me = []

		for addr,worker in workers.items():
			if worker.alive:
				worker.addMsg(msg)
			else:
				print('Worker no longer alive... removing from list')
				delete_me.append(addr)

		for addr in delete_me:
			del workers[addr]

	def nmea2dict(self,nmea):
		nmea_dict = {}
		for talker_field in nmea.fields:
			field_name = talker_field[1]
			field_val = getattr(nmea,field_name)
			if isinstance(field_val,datetime.date):
				# convert date to format "yyyy-mm-dd"
				field_val = str(field_val)
			elif isinstance(field_val,datetime.time):
				# convert time to format "hh:mm:ss.ssssss"
				field_val = str(field_val)
			nmea_dict[field_name] = field_val
		return nmea_dict

	def run(self):
		try:
			streamreader = pynmea2.NMEAStreamReader()
			while self._stayAlive:
				data = ser.read(64).decode('utf-8')

				for nmea in streamreader.next(data):
					self.notifyWorkers({
						'nmea':str(nmea),
						'sentence_type':nmea.sentence_type,
						'nmea_dict':self.nmea2dict(nmea)
						})
		except Exception as e:
			print(e)
			# shutdown the thread

		print("Producer shutdown complete.")
		self.alive = False

producer = ProducerThread()
producer.start()

try:
	print('Listening...')
	while True:
		data, addr = s.recvfrom(1024)

		data = data.decode('utf-8')
		if data == 'quit':
			if addr in workers:
				workers[addr].quit()
		elif data == 'poke':
			print('Found a new client! %s' % (str(addr)))
			worker = WorkerThread(s,addr)
			worker.start()
			workers[addr] = worker
		else:
			print('Unhandled data \'%s\'' % data)

except KeyboardInterrupt:
	print('Shutting down...')

# notify all workers to shutdown
for worker in workers.values():
	worker.quit()

# notify producer to shutdown
producer.quit()

# wait for all workers to join
for worker in workers.values():
	worker.join()

# wait for producer to join
producer.join()

# close the socket and serial connection now that everyone joined
s.close()
ser.close()
