extends Spatial

onready var car = $SimpleCar
onready var client = $NavClient
onready var gravity_arrow = $Gravity

var zeroing = true
var base = null

func _input(event):
	if event is InputEventKey:
		if event.pressed:
			zeroing = true

func get_quat(q):
	return Quat(q.x,q.y,q.z,q.w)

func _on_NavClient_new_sample(sample_json):
	var json_obj = JSON.parse(sample_json)
	if json_obj.error == OK:
		var nav_samp = json_obj.result
		if len(nav_samp.imuSamp) > 0:
			var quat = get_quat(nav_samp.imuSamp[0].quat)
			var gravity = NavUtils.get_gravity(quat)
			gravity_arrow.look_at(gravity,Vector3.UP)
			var basis = Basis(quat)
			if zeroing:
				base = basis
				zeroing = false
			
			var zeroed_basis = basis * base.inverse()
			car.transform.basis = zeroed_basis

func _on_NavClient_connect_success():
	print('NavClient successfully connected!')
