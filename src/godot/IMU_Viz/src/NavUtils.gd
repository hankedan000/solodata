extends Node

# returns a vector pointing in the direction of gravity
func get_gravity(q: Quat) -> Vector3:
	var v = Vector3()
	v.x = -1 * (2 * (q.x*q.z - q.w*q.y));
	v.y = -1 * (2 * (q.w*q.x + q.y*q.z));
	v.z = -1 * (q.w*q.w - q.x*q.x - q.y*q.y + q.z*q.z);
	return v
