add_library(NavClientNode SHARED NavClientNode.cpp)
target_link_libraries(NavClientNode ${GodotCpp_LIBRARIES} NavClient_static)
set_target_properties(NavClientNode
	PROPERTIES
		CXX_STANDARD 11
		CXX_STANDARD_REQUIRED YES
		CXX_EXTENSIONS NO
		LIBRARY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/../bin"
)
