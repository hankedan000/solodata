#pragma once

#include <Godot.hpp>
#include <Node.hpp>
#include <String.hpp>
#include "NavClient.h"

namespace godot
{

	class NavClientNode : public Node
	{
		GODOT_CLASS(NavClientNode, Node)

	public:
		static void _register_methods();

		NavClientNode();
		~NavClientNode();

		void
		_init(); // our initializer called by Godot

		bool
		start(
			String host,
			unsigned int port);

		void
		on_nav_sample(
			const solodata::NavSample &samp);

	private:
		solodata::NavClient client_;

	};

}
