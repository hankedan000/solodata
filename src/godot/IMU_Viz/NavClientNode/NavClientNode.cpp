#include "NavClientNode.h"
#include <google/protobuf/util/json_util.h>

namespace godot
{
	void NavClientNode::_register_methods()
	{
		register_method("start", &NavClientNode::start);

		register_signal<NavClientNode>((char *)"new_sample", "sample_json", GODOT_VARIANT_TYPE_STRING);
	}

	NavClientNode::NavClientNode()
	{
	}

	NavClientNode::~NavClientNode()
	{
		// add your cleanup here
		client_.stop();
	}

	void
	NavClientNode::_init()
	{
		// initialize any variables here
		solodata::NavClient::SampleListener callback = std::bind(
			&godot::NavClientNode::on_nav_sample,
			this,
			std::placeholders::_1);
		client_.add_listener("NavClientNode",callback);
	}

	bool
	NavClientNode::start(
		String host,
		unsigned int port)
	{
		return client_.start(host.alloc_c_string(),port);
	}

	void
	NavClientNode::on_nav_sample(
		const solodata::NavSample &samp)
	{
		std::string sample_json;
		google::protobuf::util::MessageToJsonString(samp,&sample_json);
		emit_signal("new_sample", sample_json.c_str());
	}
}

extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o)
{
	godot::Godot::gdnative_init(o);
}

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o)
{
	godot::Godot::gdnative_terminate(o);
}

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle)
{
	godot::Godot::nativescript_init(handle);

	godot::register_class<godot::NavClientNode>();
}