extends Node

signal connect_success()
signal connect_failed()
signal new_sample(samp)

onready var NavClientNode = preload("res://bin/NavClientNode.gdns")

export var hostname = 'navpi.local'
export var port = 6060
var client = null

func _ready():
	client = NavClientNode.new()
	client.connect("new_sample",self,"_on_NavClient_new_sample")
	if client.start(hostname,port):
		emit_signal("connect_success")
		add_child(client)
	else:
		emit_signal("connect_failed")
	
func _on_NavClient_new_sample(samp):
	emit_signal("new_sample",samp)
