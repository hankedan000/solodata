tool
extends Spatial

export var fill = Color() setget _set_fill

onready var body = $Body
var _ready = false

func _ready():
	_ready = true
	
func _set_fill(color):
	fill = color
	
	if not _ready():
		yield(self,"ready")
	body.material = SpatialMaterial.new()
	body.material.albedo_color = fill
	
