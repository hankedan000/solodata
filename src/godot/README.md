# Building Godot export template for ARM
scons CC=clang CXX=clang++ CCFLAGS="-mcpu=cortex-a7 -mfpu=neon-vfpv4 -mfloat-abi=hard" platform=x11 tools=no target=release bits=32 -j 4

At time of writing this, I was using the following version of clang/clang++
clang version 3.8.1-24+rpi1 (tags/RELEASE_381/final)
