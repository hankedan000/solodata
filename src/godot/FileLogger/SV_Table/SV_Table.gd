extends VBoxContainer

var sv_list = []
var SV_Row = preload("res://SV_Table/SV_Row.tscn")

func getSV(gsv,sv_num):
	return {
		"prn": gsv["sv_prn_num_%d" % sv_num],
		"el": gsv["elevation_deg_%d" % sv_num],
		"az": gsv["azimuth_%d" % sv_num],
		"snr": gsv["snr_%d" % sv_num]
	}

# gsv -> NMEA GSV sentence dictionary
func on_gsv_sentence(gsv):
	var MAX_SV_PER_VIEW = 4
	var msg_num = int(gsv["msg_num"])
	var msg_cnt = int(gsv["num_messages"])
	
	# clear list if first GSV packet
	if msg_num == 1:
		sv_list.clear()
		
	# parse the SV that are in the GSV packet
	var num_sv_before_me = (msg_num - 1) * MAX_SV_PER_VIEW
	var num_sv_in_me = int(gsv["num_sv_in_view"]) - num_sv_before_me
	if num_sv_in_me > MAX_SV_PER_VIEW:
		num_sv_in_me = MAX_SV_PER_VIEW
		
	for i in range(num_sv_in_me):
		var sv = getSV(gsv,i+1)
		sv_list.append(sv)
	
	# if last packet, update the SV table
	if msg_num == msg_cnt:
		# clear table
		for child in get_children():
			child.queue_free()
			
		for sv in sv_list:
			var sv_row = SV_Row.instance()
			sv_row.set_sv(sv)
			add_child(sv_row)
			