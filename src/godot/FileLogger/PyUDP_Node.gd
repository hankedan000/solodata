extends Node

signal recv_obj(obj)
signal server_error(error_info)
signal request_timeout(req_uid)

export (String) var SERVER_IP = "127.0.0.1"
export (String) var CLIENT_IP = "127.0.0.1"
export (int) var SERVER_PORT = 1230
export (int) var CLIENT_PORT = 1240

var client_conn := PacketPeerUDP.new()
var _current_request_uid = 0

# variable for the clients socket thread
var thread := Thread.new()
var stay_alive = true

# these variables are used to build a packetized response
# from the server. when
var _num_packets_expected = -1
var _num_packets_received = 0
var _current_obj = ""

var _response_mutex := Mutex.new()
var _available_responses = []
var _error_mutex := Mutex.new()
var _available_errors = []

func _ready():
	client_conn.set_dest_address(SERVER_IP,SERVER_PORT)
	var error = client_conn.listen(CLIENT_PORT,CLIENT_IP)
	if error != OK:
		push_error("Caught error %d when trying to listen on socket" % error)
	
	thread.start(self,"_listen_for_packets")
	
	# kick start data stream by poking the server
	client_conn.put_packet("poke".to_utf8())
	$RequestTimeout.start(0.5)

func _process_obj_slice(res):
	var okay = true
	
	if okay and _current_request_uid == -1:
		push_error("Received unexpected response packet! %s" % res)
		okay = false
		
	if okay and _current_request_uid != res["req_uid"]:
		push_error(
			"Got a response UID of %d while expecting UID of %d" %
			[res["res_uid"],_current_request_uid])
		okay = false
		
	if okay and _num_packets_expected != -1:
		# make sure the expected number of packets doesn't change
		# half way through the sequence
		if _num_packets_expected != res["n_packets"]:
			push_error(
				"Received mislead n_packets for request UID %d. Previously expecting %d, but this packet is now claiming there are %d." %
				[_current_request_uid,_num_packets_expected,res["n_packets"]])
			okay = false
			
	if okay:
		_current_obj += res["obj_slice"]
		_num_packets_expected = res["n_packets"]
		_num_packets_received += 1
		
		if _num_packets_received == _num_packets_expected:
			$RequestTimeout.stop()
			var obj_json := JSON.parse(_current_obj)
			
			if obj_json.error == OK:
				_response_mutex.lock()
				_available_responses.push_back(obj_json.result)
				_response_mutex.unlock()
			
			# clean up and prepare for next request
#			_current_request_uid = -1
			_num_packets_expected = -1
			_num_packets_received = 0
			_current_obj = ""
			
	return okay

func _process_error_res(res):
	# clean up and prepare for next request
#	_current_request_uid = -1
	$RequestTimeout.stop()
	
	_error_mutex.lock()
	_available_errors.push_back(res["error_string"])
	_error_mutex.unlock()

func _listen_for_packets(data):
	while stay_alive:
		var error = client_conn.wait()
		
		if error == OK:
			while client_conn.get_available_packet_count() > 0:
				var resp_str = client_conn.get_packet().get_string_from_utf8()
				resp_str = resp_str.replace("NaN","null")
				var resp_json := JSON.parse(resp_str)
				
				if resp_json.error == OK:
					var res = resp_json.result
					var is_dict = res is Dictionary
					if is_dict and res.has("error_string"):
						_process_error_res(res)
					elif is_dict and res.has("obj_slice"):
						_process_obj_slice(res)
					else:
						push_error("unhandled response '%s'" % res)
				else:
					push_error(
						"Received JSON parse error code %d. %s" %
						[resp_json.error,resp_json.error_string])
		else:
			push_error("Received a socket error. %d" % error)
			stay_alive = false

func _process(delta):
	# check for responses from server
	while _available_responses.size() > 0:
		_response_mutex.lock()
		var res = _available_responses.pop_front()
		_response_mutex.unlock()
		
		var is_dict = res is Dictionary
		if is_dict:
			emit_signal("recv_obj",res)
		else:
			push_error("Unsupported response! %s" % str(res))
	
	# check for errors from server
	while _available_errors.size() > 0:
		_error_mutex.lock()
		var err_str = _available_errors.pop_front()
		_error_mutex.unlock()
		
		emit_signal("server_error",err_str)

func _on_RequestTimeout_timeout():
	if _current_request_uid >= 0:
		emit_signal("request_timeout",_current_request_uid)
#		_current_request_uid = -1
		
func _exit_tree():
	# tell the thread to die, and wait for it join
	stay_alive = false
	
	# FIXME PacketPeerUDP::wait() doens't have a timeout, so this
	# wait_to_finish() will never unblock
#	thread.wait_to_finish()
	
	# notify server that client is leaving session
	client_conn.put_packet("quit".to_utf8())
	
	# clean up socket connections
	client_conn.close()
