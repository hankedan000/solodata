extends PanelContainer

var recording = false
var file = null

func _ready():
	$VBox/Buttons/Stop.hide()

func getFileName():
	var buttonGroup = $VBox/RunSelector/Run1.group
	var selectedButton = buttonGroup.get_pressed_button()
	
	return selectedButton.name.to_lower() + ".nmea"

func _on_PyUDP_Node_recv_obj(obj):
	$VBox/TabContainer/Console.text = obj["nmea"]
	
	match obj["sentence_type"]:
		"GSV":
			$VBox/TabContainer/SateliteViews/SV_Table.on_gsv_sentence(obj["nmea_dict"])
		"RMC":
			$VBox/TabContainer/LiveMap.on_coord_dd(GPS_Utils.rmc_to_coord(obj["nmea_dict"]))
	
	if recording and file != null:
		file.store_line(obj["nmea"])

func _on_Start_pressed():
	$VBox/Buttons/Start.hide()
	$VBox/Buttons/Stop.show()
	var filename = getFileName()
	
	file = File.new()
	if file.open(getFileName(), File.WRITE) != 0:
		print("Error opening file")
	else:
		recording = true

func _on_Stop_pressed():
	$VBox/Buttons/Stop.hide()
	$VBox/Buttons/Start.show()
	recording = false
	if file:
		file.close()
