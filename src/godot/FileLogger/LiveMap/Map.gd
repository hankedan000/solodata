tool
extends ViewportContainer

export (float) var zoom_speed = 0.2
export (float) var init_zoom = 1.0

const USA_Center = Vector2(40.3061681,-97.9351707)
const ConcourseCenter = Vector2(39.208847,-76.686964)
# 0.00001843908 is about the width of a car in coordinate degrees
const PX_PER_DEG = Vector2(1000000,1200000)

var drag = false
var initPosCam = false
var initPosMouse = false
var initPosNode = false
var show_fps setget set_show_fps

func _ready():
	self.show_fps = false
	
	$Viewport/Camera2D.position = ShapeUtils.coord_to_xy(ConcourseCenter) * PX_PER_DEG
	$Viewport/Camera2D.zoom = Vector2(1,1) * init_zoom
	
	_on_Map_resized()
	
func set_show_fps(value: bool):
	show_fps = value
	$Overlays/HBoxes/Top/FPS.visible = value
	
func on_coord_dd(coord_dd: Vector2):
	print_debug("xy = %s" % str(ShapeUtils.coord_to_xy(coord_dd)))
	print_debug("xy_pixel = %s" % str(ShapeUtils.coord_to_xy(coord_dd) * PX_PER_DEG))
	$Viewport/Line.add_point(ShapeUtils.coord_to_xy(coord_dd) * PX_PER_DEG)

func map(value, istart, istop, ostart, ostop):
	return ostart + (ostop - ostart) * ((value - istart) / (istop - istart))
	
func _on_Overlays_gui_input(event):	
	var zoom = $Viewport/Camera2D.zoom
	
	if event is InputEventMouseMotion:
		if drag:
			var mouse_pos = get_viewport().get_mouse_position()
	
			var dist_x = initPosMouse.x - mouse_pos.x
			var dist_y = initPosMouse.y - mouse_pos.y
	
			var nx = initPosNode.x + dist_x * zoom.x
			var ny = initPosNode.y + dist_y * zoom.y
			
			$Viewport/Camera2D.position = Vector2(nx,ny)
	
		elif not drag:
			pass
	
	if event is InputEventMouseButton:
		var zoom_dir = Vector2()
		if event.button_index == BUTTON_WHEEL_UP:
	        # print("wheel up (event)")
			zoom_dir = Vector2(-1,-1)
		if event.button_index == BUTTON_WHEEL_DOWN:
	        # print("wheel down (event)")
			zoom_dir = Vector2(1,1)
		if event.button_index == BUTTON_LEFT:
			if Input.is_mouse_button_pressed(BUTTON_LEFT) :
				initPosMouse = get_viewport().get_mouse_position()
				initPosNode = $Viewport/Camera2D.position
				drag = true
			else:
				drag = false
				
		zoom += zoom_dir * zoom_speed
		zoom.x = max(zoom.x,0.001)
		zoom.y = max(zoom.y,0.001)
		$Viewport/Camera2D.zoom = zoom
		
func _process(delta):
	$Overlays/HBoxes/Top/FPS.text = ("%0.1f FPS" % Engine.get_frames_per_second())

func _on_Map_resized():
	$Viewport.size = rect_size
	$Overlays.rect_size = rect_size
	