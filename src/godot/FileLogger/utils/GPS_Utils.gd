extends Object
class_name GPS_Utils

static func ddm_to_dd(ddm: float):
	var degs = int(ddm / 100)
	var mins = ddm - degs * 100
	return degs + mins / 60.0

static func h_to_sign(h: String):
	if h.to_upper() in ["N","E"]:
		return +1
	elif h.to_upper() in ["S","W"]:
		return -1
	else:
		push_error("Invalid heading '%s'" % h)
	
static func make_coord_ll(lat: float, lon: float):
	return Vector2(lat,lon)
		
static func rmc_to_coord(rmc: Dictionary):
	var lat = ddm_to_dd(float(rmc["lat"])) * h_to_sign(rmc["lat_dir"])
	var lon = ddm_to_dd(float(rmc["lon"])) * h_to_sign(rmc["lon_dir"])
	return make_coord_ll(lat,lon)

static func coord_to_xy(coord: Vector2) -> Vector2:
	return Vector2(coord.y,coord.x * -1)
