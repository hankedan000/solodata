extends Object
class_name ShapeUtils
		
static func coord_to_xy(coord: Vector2) -> Vector2:
	return Vector2(coord.y,coord.x * -1)

static func arr_slice(arr,start,end):
	var res = []
	for i in range(start,end):
		res.append(arr[i])
	return res

static func rdp(PointList, epsilon):
	# Find the point with the maximum distance
	var dmax = 0
	var index = 0
	var end = PointList.size() - 1
	for i in range(1,end - 1):
		var d = perpendicularDistance(PointList[i], PointList[0], PointList[end])
		if (d > dmax):
			index = i
			dmax = d
    
	var ResultList := []

	# If max distance is greater than epsilon, recursively simplify
	if dmax > epsilon:
		# Recursive call
		var recResults1 = rdp(arr_slice(PointList,0,index), epsilon)
		var recResults2 = rdp(arr_slice(PointList,index,end), epsilon)

		# Build the result list
		ResultList = recResults1 + recResults2
	else:
		ResultList = [PointList[0], PointList[end]]
	
	# Return the result
	return ResultList

class Line:
	# y = mx + b
	var m = 0.0
	var b = 0.0
  
	func _init(a : Vector2, b : Vector2):
		self.m = (b.y - a.y) / (b.x - a.x);
		self.b = a.y - m * a.x;

# @param[in] p
# a point in 2D space to compute the perpendicular distance to
#
# @param[in] a
# a point in 2D space that forms one endpoint of the line
#
# @param[in] b
# a point in 2D space that forms the other endpoint of the line
#
# @return
# the perpendicular distance from point 'p' to the line formed
# by points 'a' and 'b'
static func perpendicularDistance(p : Vector2, a : Vector2, b : Vector2) -> float:
	# https://www.intmath.com/plane-analytic-geometry/perpendicular-distance-point-line.php
	var numer = abs((b.y - a.y) * p.x - (b.x - a.x) * p.y + b.x * a.y - b.y * a.x)
	var denom = sqrt(pow(b.y - a.y,2) + pow(b.x - a.x,2))
	
	if denom == 0:
		return 100000000000000000000000.0
	else:
  		return numer/denom