#!/bin/bash

#git clone --recursive https://github.com/GodotNativeTools/godot-cpp -b 3.2

pushd godot-cpp/
	git submodule update --init --recursive
	scons platform=linux generate_bindings=yes -j$(nproc)
popd