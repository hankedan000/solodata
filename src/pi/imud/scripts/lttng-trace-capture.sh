#!/usr/bin/bash
lttng create imud-session --output=/tmp/imud-session
# lttng enable-event --userspace imud_ust:i2c_*
lttng enable-event --userspace imud_ust:imu_*
lttng enable-event --userspace imud_ust:imud_*

# for getting function call information...
# run application like this
# LD_PRELOAD=liblttng-ust-cyg-profile.so:liblttng-ust-dl.so ./my_app
# lttng enable-event --userspace lttng_ust_cyg_profile:'*'
# lttng enable-event --userspace lttng_ust_dl:'*'
# lttng enable-event --userspace lttng_ust_statedump:'*'
# lttng add-context -u -t vpid -t ip -t procname

lttng start

echo "Run your application. Press enter when you want to stop the trace."
read

lttng stop
lttng destroy
