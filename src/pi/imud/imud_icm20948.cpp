#include "I2Cdev.h"
#include <DynamicMemoryGuard.h>
#include "fs_utils.h"
#include <Logging.h>
#include <LoggingObject.h>
#include <Icm20948.h>
#include <Icm20948Defs.h>
#include "Icm20948MPUFifoControl.h"
#include "Icm20948Transport.h"
#include "imud_config.h"
#include <MultiStopwatch.h>
#include <ResourcePool.h>
#include <SensorTypes.h>

#include <atomic>
#include <csignal>
#include <fstream>
#include <getopt.h>
#include <mutex>
#include <sched.h>
#include <sys/stat.h>
#include <thread>
#include <unistd.h>
#include <wiringPi.h>
#include <zmq.h>

#include <google/protobuf/arena.h>
#include "IMU_Sample.pb.h"

// for LTTng tracing
#define TRACEPOINT_DEFINE
#include "imud_tracepoints.h"

SET_LOGGING_CFG_NAME("solodata_logging.cfg");

uint8_t I2C_Address = 0x69;

static const uint8_t dmp3_image[] = 
{
	#include "icm20948_img.dmp3a.h"
};

// used to shutdown the process when interrupt signal is received
volatile bool stay_alive = true;

class Boilerplate : public LoggingObject
{
public:
	Boilerplate()
	: LoggingObject("imud_icm20948")
	{}
};

Boilerplate *boilerplate = nullptr;

// mutex is used to lock icm_device from being used within ISR while
// the watchdog is resetting the device.
std::mutex icm_mutex;
inv_icm20948_t icm_device;

// MPU control/status vars
unsigned long irq_count = 0;
unsigned long i2c_writes = 0;
unsigned long i2c_reads = 0;
unsigned long gyro_sample_count = 0;
unsigned long acc_sample_count = 0;
unsigned long mag_sample_count = 0;
unsigned long quat_sample_count = 0;
unsigned long sync_sample_count = 0;
unsigned long watchdog_engaged_count = 0;
unsigned long pool_exhaustion_count = 0;
std::atomic<unsigned int> watchdog_counter(0);
std::atomic<bool> samples_stable(false);

// ZMQ connection related stuff
bool connected = false;
void *publisher = nullptr;
void *buffer = nullptr;

// Stopwatch variables
MultiStopwatch msw;
struct Records
{
	static int SAMPLE_BUILD;
	static int SAMPLE_PUB;
	static int SENSOR_EVENT;
};
int Records::SAMPLE_BUILD = -1;
int Records::SAMPLE_PUB = -1;
int Records::SENSOR_EVENT = -1;

// IMU sample
solodata::IMU_Sample *imu_sample = nullptr;

struct Scorecard
{
	Scorecard()
	{
		reset();
	}

	void
	reset()
	{
		hasMag = false;
		hasAcc = false;
		hasGyro = false;
		hasQuat = false;
	}

	bool
	ready(
		bool expectGyro,
		bool expectAcc,
		bool expectMag,
		bool expectQuat)
	{
		return 
			( ! expectGyro || (expectGyro && hasGyro)) &&
			( ! expectAcc  || (expectAcc  && hasAcc))  &&
			( ! expectMag  || (expectMag  && hasMag))  &&
			( ! expectQuat || (expectQuat && hasQuat));
	}

	// timestamp of the first received sensor data in the scorecard
	std::chrono::time_point<std::chrono::system_clock> first_data_ts;

	// magnetometer data [x,y,z]
	float mv[3];
	bool hasMag;

	// accelerometer data [x,y,z]
	float av[3];
	bool hasAcc;

	// gyroscope data [x,y,z]
	float gv[3];
	bool hasGyro;

	// quaternion data [w,x,y,z]
	float quat[4];
	bool hasQuat;

	// counter that gets monotonically increased each time a new IMU sample
	// groups was received from the hardware
	uint32_t sample_seq_num;
};

using IMU_DataPool = concrt::ResourcePool<Scorecard, concrt::PC_Model::SPSC>;
static IMU_DataPool *pool;
static Scorecard *curr_data = nullptr;

// publisher status
unsigned long samples_published = 0;

int rc = 0;
#define THREE_AXES 3
static int unscaled_bias[THREE_AXES * 2];

#define AK0991x_DEFAULT_I2C_ADDR  0x0C  /* The default I2C address for AK0991x Magnetometers */
#define AK0991x_SECONDARY_I2C_ADDR  0x0E  /* The secondary I2C address for AK0991x Magnetometers */

// Mounting matrix configuration applied for Accel, Gyro and Mag
static const float cfg_mounting_matrix[9] = {
	1.f, 0, 0,
	0, 1.f, 0,
	0, 0, 1.f
};

solodata::imud_config_t config;

void
send_sample(
	const solodata::IMU_Sample &datum,
	const solodata::imud_config_t::zmq_t &zmq_cfg)
{
	msw.start(Records::SAMPLE_PUB);
	// serialize datum into buffer
	size_t size = datum.ByteSizeLong();
	if (size > zmq_cfg.buff_size_bytes)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"Failed to serialize IMU_Sample of size " << size <<
			" because it exceeds max of " << zmq_cfg.buff_size_bytes << ". This"
			" can occur due to high serialization overhead. Sample dropped.");
		return;
	}
	datum.SerializeToArray(buffer,size);

	int rc = zmq_send(publisher,buffer,size,ZMQ_DONTWAIT);
	if (rc < 0)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"zmq_send failed for message '" << datum.DebugString() << "'");
		return;
	}
	msw.stop(Records::SAMPLE_PUB);
}

// periodically writes debug information to disk
void
status_thread(
	solodata::imud_config_t::status_t stat_cfg)
{
	LOG4CXX_INFO(boilerplate->getLogger(),
		"status thread started!");

	try
	{
		fs_utils::mkdir(stat_cfg.file);
	}
	catch (const std::exception &e)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"Failed to create status directory!\n"
			" file: " << stat_cfg.file <<
			" what(): " << e.what());
		return;
	}

	std::ofstream ofs;
	ofs.open(stat_cfg.file, std::ofstream::out | std::ofstream::trunc);
	if ( ! ofs.is_open())
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"Failed to create status file!\n"
			" file: " << stat_cfg.file);
		return;
	}

	while (stay_alive)
	{
		ofs.seekp(std::ios_base::beg);// go back to beginning of file

		ofs << "irq_count: " << irq_count << std::endl;
		ofs << "i2c_writes: " << i2c_writes << std::endl;
		ofs << "i2c_reads: " << i2c_reads << std::endl;
		ofs << "gyro_sample_count: " << gyro_sample_count << std::endl;
		ofs << "acc_sample_count:  " << acc_sample_count << std::endl;
		ofs << "mag_sample_count:  " << mag_sample_count << std::endl;
		ofs << "quat_sample_count: " << quat_sample_count << std::endl;
		ofs << "sync_sample_count: " << sync_sample_count << std::endl;
		ofs << "watchdog_engaged_count: " << watchdog_engaged_count << std::endl;
		ofs << "pool_exhaustion_count: " << pool_exhaustion_count << std::endl;
		ofs << "samples_stable: " << std::boolalpha << samples_stable << std::endl;
		ofs << "samples_published: " << samples_published << std::endl;

		ofs.flush();// write everything to disk

		usleep(stat_cfg.interval_ms * 1000);
	}
}

// package and sends a complete IMU sample over a zmq socket
void
publisher_thread(
	bool log_sample_rate,
	bool log_samples,
	solodata::imud_config_t::zmq_t zmq_cfg)
{
	Scorecard *res;

	while (stay_alive)
	{
		int rc = pool->consume_wait(res,1);// 1s timeout
		if (rc == concrt::ERR_TIMEOUT)
		{
			// try again or shutdown thread
			continue;
		}
		else if (rc != concrt::OK)
		{
			LOG4CXX_ERROR(boilerplate->getLogger(),
				"failed to consumer resource rc = " << rc);
			continue;
		}

		tracepoint(imud_ust,imud_sample_consume,res->sample_seq_num);
		msw.start(Records::SAMPLE_BUILD);
		static bool first_sample = true;
		static std::chrono::time_point<std::chrono::system_clock> prev_time;
		const auto &samp_time = res->first_data_ts;
		if (log_sample_rate && ! first_sample)
		{
			auto diff_time = std::chrono::duration_cast<std::chrono::microseconds>(
				samp_time - prev_time);

			LOG4CXX_INFO(boilerplate->getLogger(),
				"sample rate = " << (1.0/(diff_time.count() / 1.0e6)) << "Hz");
		}
		prev_time = samp_time;
		if (first_sample)
		{
			LOG4CXX_INFO(boilerplate->getLogger(),
				"First stable sample received! IMU samples are streaming.");
			// DynamicMemoryGuard::thread_enable();
			first_sample = false;
		}

		// assign sample timestamp
		auto samp_time_us = std::chrono::time_point_cast<std::chrono::microseconds>(samp_time);
		auto epoch = samp_time_us.time_since_epoch();
		auto timestamp_us = std::chrono::duration_cast<std::chrono::microseconds>(epoch);
		imu_sample->set_timestamp(timestamp_us.count());

		// assign sensor data
		imu_sample->mutable_quat()->set_w(res->quat[0]);
		imu_sample->mutable_quat()->set_x(res->quat[1]);
		imu_sample->mutable_quat()->set_y(res->quat[2]);
		imu_sample->mutable_quat()->set_z(res->quat[3]);
		imu_sample->mutable_mag()->set_x(res->mv[0]);
		imu_sample->mutable_mag()->set_y(res->mv[1]);
		imu_sample->mutable_mag()->set_z(res->mv[2]);
		imu_sample->mutable_acc()->set_x(res->av[0]);
		imu_sample->mutable_acc()->set_y(res->av[1]);
		imu_sample->mutable_acc()->set_z(res->av[2]);
		imu_sample->mutable_gyro()->set_x(res->gv[0]);
		imu_sample->mutable_gyro()->set_y(res->gv[1]);
		imu_sample->mutable_gyro()->set_z(res->gv[2]);

		msw.stop(Records::SAMPLE_BUILD);

		// publish sample
		tracepoint(imud_ust,imud_sample_publish,res->sample_seq_num);
		send_sample(*imu_sample,zmq_cfg);
		samples_published++;
		if (log_samples)
		{
			LOG4CXX_INFO(boilerplate->getLogger(),imu_sample->DebugString());
		}

		tracepoint(imud_ust,imud_sample_release,res->sample_seq_num);
		pool->release(res);
	}
}

static uint8_t convert_to_generic_ids[INV_ICM20948_SENSOR_MAX] = {
	INV_SENSOR_TYPE_ACCELEROMETER,
	INV_SENSOR_TYPE_GYROSCOPE,
	INV_SENSOR_TYPE_RAW_ACCELEROMETER,
	INV_SENSOR_TYPE_RAW_GYROSCOPE,
	INV_SENSOR_TYPE_UNCAL_MAGNETOMETER,
	INV_SENSOR_TYPE_UNCAL_GYROSCOPE,
	INV_SENSOR_TYPE_BAC,
	INV_SENSOR_TYPE_STEP_DETECTOR,
	INV_SENSOR_TYPE_STEP_COUNTER,
	INV_SENSOR_TYPE_GAME_ROTATION_VECTOR,
	INV_SENSOR_TYPE_ROTATION_VECTOR,
	INV_SENSOR_TYPE_GEOMAG_ROTATION_VECTOR,
	INV_SENSOR_TYPE_MAGNETOMETER,
	INV_SENSOR_TYPE_SMD,
	INV_SENSOR_TYPE_PICK_UP_GESTURE,
	INV_SENSOR_TYPE_TILT_DETECTOR,
	INV_SENSOR_TYPE_GRAVITY,
	INV_SENSOR_TYPE_LINEAR_ACCELERATION,
	INV_SENSOR_TYPE_ORIENTATION,
	INV_SENSOR_TYPE_B2S,
	INV_SENSOR_TYPE_FSYNC_EVENT
};

// forward declare ISR
void
dmp_data_ready();

int
i2c_master_write_register(
	uint8_t address,
	uint8_t reg,
	uint32_t len,
	const uint8_t *data)
{
	tracepoint(imud_ust,i2c_write,address,len,true);
	i2c_writes++;
	// I2Cdev wants a non-const pointer... :(
	uint8_t * unsafe_data = const_cast<uint8_t *>(data);
	int res = I2Cdev::writeBytes(address,reg,len,unsafe_data) ? 0 : -1;
	tracepoint(imud_ust,i2c_write,address,len,false);
	return res;
}

int
i2c_master_read_register(
	uint8_t address,
	uint8_t reg,
	uint32_t len,
	uint8_t *buff)
{
	tracepoint(imud_ust,i2c_read,address,len,true);
	i2c_reads++;
	int res = I2Cdev::readBytes(address,reg,len,buff) ? 0 : -1;
	tracepoint(imud_ust,i2c_read,address,len,false);
	return res;
}

//---------------------------------------------------------------------

int
idd_io_hal_read_reg(
	void *context,
	uint8_t reg,
	uint8_t *rbuffer,
	uint32_t rlen)
{
	return i2c_master_read_register(I2C_Address, reg, rlen, rbuffer);
}

//---------------------------------------------------------------------

int
idd_io_hal_write_reg(
	void *context,
	uint8_t reg,
	const uint8_t *wbuffer,
	uint32_t wlen)
{
	return i2c_master_write_register(I2C_Address, reg, wlen, wbuffer);
}

//---------------------------------------------------------------------

static void
icm20948_apply_mounting_matrix()
{
	for (unsigned int ii = 0; ii < INV_ICM20948_SENSOR_MAX; ii++)
	{
		inv_icm20948_set_matrix(&icm_device, cfg_mounting_matrix, (inv_icm20948_sensor)ii);
	}
}

//---------------------------------------------------------------------

static void
icm20948_set_fsr(
	const solodata::imud_config_t::imu_t &imu_cfg)
{
	LOG4CXX_INFO(boilerplate->getLogger(),
		"Configuring full sensor range."
		" acc = " << imu_cfg.acc_fsr << "g; "
		" gyr = " << imu_cfg.gyr_fsr << "deg/s; ");
	inv_icm20948_set_fsr(&icm_device, INV_ICM20948_SENSOR_RAW_ACCELEROMETER, (const void *)&imu_cfg.acc_fsr);
	inv_icm20948_set_fsr(&icm_device, INV_ICM20948_SENSOR_ACCELEROMETER, (const void *)&imu_cfg.acc_fsr);
	inv_icm20948_set_fsr(&icm_device, INV_ICM20948_SENSOR_RAW_GYROSCOPE, (const void *)&imu_cfg.gyr_fsr);
	inv_icm20948_set_fsr(&icm_device, INV_ICM20948_SENSOR_GYROSCOPE, (const void *)&imu_cfg.gyr_fsr);
	inv_icm20948_set_fsr(&icm_device, INV_ICM20948_SENSOR_GYROSCOPE_UNCALIBRATED, (const void *)&imu_cfg.gyr_fsr);
}

//---------------------------------------------------------------------

static enum
inv_icm20948_sensor idd_sensortype_conversion(
	int sensor)
{
	switch (sensor)
	{
		case INV_SENSOR_TYPE_RAW_ACCELEROMETER:
			return INV_ICM20948_SENSOR_RAW_ACCELEROMETER;
		case INV_SENSOR_TYPE_RAW_GYROSCOPE:
			return INV_ICM20948_SENSOR_RAW_GYROSCOPE;
		case INV_SENSOR_TYPE_ACCELEROMETER:
			return INV_ICM20948_SENSOR_ACCELEROMETER;
		case INV_SENSOR_TYPE_GYROSCOPE:
			return INV_ICM20948_SENSOR_GYROSCOPE;
		case INV_SENSOR_TYPE_UNCAL_MAGNETOMETER:
			return INV_ICM20948_SENSOR_MAGNETIC_FIELD_UNCALIBRATED;
		case INV_SENSOR_TYPE_UNCAL_GYROSCOPE:
			return INV_ICM20948_SENSOR_GYROSCOPE_UNCALIBRATED;
		case INV_SENSOR_TYPE_BAC:
			return INV_ICM20948_SENSOR_ACTIVITY_CLASSIFICATON;
		case INV_SENSOR_TYPE_STEP_DETECTOR:
			return INV_ICM20948_SENSOR_STEP_DETECTOR;
		case INV_SENSOR_TYPE_STEP_COUNTER:
			return INV_ICM20948_SENSOR_STEP_COUNTER;
		case INV_SENSOR_TYPE_GAME_ROTATION_VECTOR:
			return INV_ICM20948_SENSOR_GAME_ROTATION_VECTOR;
		case INV_SENSOR_TYPE_ROTATION_VECTOR:
			return INV_ICM20948_SENSOR_ROTATION_VECTOR;
		case INV_SENSOR_TYPE_GEOMAG_ROTATION_VECTOR:
			return INV_ICM20948_SENSOR_GEOMAGNETIC_ROTATION_VECTOR;
		case INV_SENSOR_TYPE_MAGNETOMETER:
			return INV_ICM20948_SENSOR_GEOMAGNETIC_FIELD;
		case INV_SENSOR_TYPE_SMD:
			return INV_ICM20948_SENSOR_WAKEUP_SIGNIFICANT_MOTION;
		case INV_SENSOR_TYPE_PICK_UP_GESTURE:
			return INV_ICM20948_SENSOR_FLIP_PICKUP;
		case INV_SENSOR_TYPE_TILT_DETECTOR:
			return INV_ICM20948_SENSOR_WAKEUP_TILT_DETECTOR;
		case INV_SENSOR_TYPE_GRAVITY:
			return INV_ICM20948_SENSOR_GRAVITY;
		case INV_SENSOR_TYPE_LINEAR_ACCELERATION:
			return INV_ICM20948_SENSOR_LINEAR_ACCELERATION;
		case INV_SENSOR_TYPE_ORIENTATION:
			return INV_ICM20948_SENSOR_ORIENTATION;
		case INV_SENSOR_TYPE_B2S:
			return INV_ICM20948_SENSOR_B2S;
		default:
			return INV_ICM20948_SENSOR_MAX;
	}//switch
}//enum sensortyp_conversion

//--------------------------------------------------------------------

bool
set_sensor_rate(
	int sensor,
	unsigned int rate_hz)
{
	bool okay = true;

	std::string sensor_str = "UNKNOWN_SENSOR";
	switch (sensor)
	{
		case INV_SENSOR_TYPE_GYROSCOPE:
			sensor_str = "GYROSCOPE";
			break;
		case INV_SENSOR_TYPE_ACCELEROMETER:
			sensor_str = "ACCELEROMETER";
			break;
		case INV_SENSOR_TYPE_MAGNETOMETER:
			sensor_str = "MAGNETOMETER";
			break;
		case INV_SENSOR_TYPE_GAME_ROTATION_VECTOR:
			sensor_str = "ROTATION";
			break;
		default:
			sensor_str = "***UNKNOWN_SENSOR***";
			sensor_str += " (" + std::to_string(sensor) + ")";
			break;
	}

	bool enable = false;
	if (rate_hz > 0)
	{
		enable = true;
		int period_rc = inv_icm20948_set_sensor_period(
			&icm_device,
			idd_sensortype_conversion(sensor),
			round(1000.0 / rate_hz));

		if (period_rc != 0)
		{
			LOG4CXX_ERROR(boilerplate->getLogger(),
				"Failed to configure " << sensor_str <<
				" rate to " << rate_hz << "Hz."
				" return code = " << period_rc);
			okay = false;
		}
	}

	if (okay)
	{
		int enable_rc = inv_icm20948_enable_sensor(
			&icm_device,
			idd_sensortype_conversion(sensor),
			enable ? 1 : 0);

		if (enable_rc != 0)
		{
			LOG4CXX_ERROR(boilerplate->getLogger(),
				"Failed to " << (enable ? "enable " : "disable ") <<
				sensor_str << ". return code = " << rc);
			okay = false;
		}
	}

	return okay;
}

bool
icm20948_sensor_setup(
	const solodata::imud_config_t::imu_t &imu_cfg)
{
	uint8_t i, whoami = 0xff;

	// Just get the whoami
	if (inv_icm20948_get_whoami(&icm_device, &whoami) != 0)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"Failed to verify 'whoami' register!");
		return false;
	}

	// ------------------------

	LOG4CXX_INFO(boilerplate->getLogger(),"Initiating IMU soft reset...");

	if (inv_icm20948_soft_reset(&icm_device) != 0)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"Soft reset failed!");
		return false;
	}

	// ------------------------

	LOG4CXX_INFO(boilerplate->getLogger(),"Initializing DMP...");

	// Setup accel and gyro mounting matrix and associated angle for current board
	inv_icm20948_init_matrix(&icm_device);

	if (inv_icm20948_initialize(&icm_device, dmp3_image, sizeof(dmp3_image)) != 0)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"Failed to write DMP image! (" << sizeof(dmp3_image) << "bytes)");
		return false;
	}

	// ------------------------

	LOG4CXX_INFO(boilerplate->getLogger(),"Configuring AUX compass...");

	/* Initialize auxiliary sensors */
	inv_icm20948_register_aux_compass(
		&icm_device,
		INV_ICM20948_COMPASS_ID_AK09916,
		AK0991x_DEFAULT_I2C_ADDR);

	if (inv_icm20948_initialize_auxiliary(&icm_device) != 0)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"AUX compass not detected at"
			" address 0x" << std::hex << AK0991x_DEFAULT_I2C_ADDR);
		return false;
	}

	icm20948_apply_mounting_matrix();
	icm20948_set_fsr(imu_cfg);

	/* re-initialize base state structure */
	inv_icm20948_init_structure(&icm_device);

	return true;
} //sensor_setup

//---------------------------------------------------------------------
// SETUP
//---------------------------------------------------------------------

union InvGyroCfg1
{
	struct InvGyroCfg1_bits
	{
		unsigned int fchoise  : 1;
		unsigned int fs_sel   : 2;
		unsigned int dlpf_cfg : 3;
		unsigned int reserved : 2;
	} bits;
	uint8_t word;
};

union InvAccelCfg
{
	struct InvAccelCfg_bits
	{
		unsigned int fchoise  : 1;
		unsigned int fs_sel   : 2;
		unsigned int dlpf_cfg : 3;
		unsigned int reserved : 2;
	} bits;
	uint8_t word;
};

void
dumpIMU_SetupInfo()
{
	const char *GYRO_FS_SEL_STRS[] = {
		"+/-250dps",
		"+/-500dps",
		"+/-1000dps",
		"+/-2000dps",
	};

	InvGyroCfg1 gyro_cfg1;
	inv_icm20948_read_mems_reg(&icm_device,REG_GYRO_CONFIG_1,1,&gyro_cfg1.word);
	uint8_t gyro_cfg2;
	inv_icm20948_read_mems_reg(&icm_device,REG_GYRO_CONFIG_2,1,&gyro_cfg2);
	uint8_t gyro_smplrt_div;
	inv_icm20948_read_mems_reg(&icm_device,REG_GYRO_SMPLRT_DIV,1,&gyro_smplrt_div);

	float gyro_rate_hz = 9600;
	if (gyro_cfg1.bits.fchoise)
	{
		gyro_rate_hz = 1125.0 / (1 + gyro_smplrt_div);
	}

	// -----------------
	const char *ACCEL_FS_SEL_STRS[] = {
		"+/-2g",
		"+/-4g",
		"+/-8g",
		"+/-16g",
	};
	const char *ACCEL_DEC3_CFG_STRS[] = {
		"average 1 or 4 samples",
		"average 8 samples",
		"average 16 samples",
		"average 32 samples",
	};

	uint8_t accel_smplrt_div_data[2];
	inv_icm20948_read_mems_reg(&icm_device,REG_ACCEL_SMPLRT_DIV_1,2,accel_smplrt_div_data);
	uint16_t accel_smplrt_div = (accel_smplrt_div_data[0] & 0xf) << 8 | accel_smplrt_div_data[1];
	InvAccelCfg accel_cfg;
	inv_icm20948_read_mems_reg(&icm_device,REG_ACCEL_CONFIG,1,&accel_cfg.word);
	uint8_t accel_cfg2;
	inv_icm20948_read_mems_reg(&icm_device,REG_ACCEL_CONFIG_2,1,&accel_cfg2);

	float accel_rate_hz = 4500;
	if (accel_cfg.bits.fchoise)
	{
		accel_rate_hz = 1125.0 / (1 + accel_smplrt_div);
	}

	LOG4CXX_INFO(boilerplate->getLogger(),
		__func__ << " - \n"
		"GYRO_CONFIG_1: 0x" << std::hex << (uint)gyro_cfg1.word << "; \n" << std::dec <<
		"  GYRO_DLPCFG:  " << gyro_cfg1.bits.dlpf_cfg << "; \n"
		"  GYRO_FS_SEL:  " << gyro_cfg1.bits.fs_sel << " (" << GYRO_FS_SEL_STRS[gyro_cfg1.bits.fs_sel] << "); \n"
		"  GYRO_FCHOICE: " << (gyro_cfg1.bits.fchoise ? "DLPF enabled" : "DLPF bypassed") << "; \n"
		"GYRO_CONFIG_2: 0x" << std::hex << (uint)gyro_cfg2 << "; \n" << std::dec <<
		"  GYRO_AVGCFG: " << (1U << (gyro_cfg2&0x7)) << "x averaging; \n"
		"GYRO_SMPLRT_DIV: " << (uint)(gyro_smplrt_div) << " (" << gyro_rate_hz << "Hz); \n"
		"--------------------------------------\n"
		"ACCEL_CONFIG: 0x" << std::hex << (uint)accel_cfg.word << "; \n" << std::dec <<
		"  ACCEL_DLPCFG:  " << accel_cfg.bits.dlpf_cfg << "; \n"
		"  ACCEL_FS_SEL:  " << accel_cfg.bits.fs_sel << " (" << ACCEL_FS_SEL_STRS[accel_cfg.bits.fs_sel] << "); \n"
		"  ACCEL_FCHOICE: " << (accel_cfg.bits.fchoise ? "DLPF enabled" : "DLPF bypassed") << "; \n"
		"ACCEL_CONFIG_2: 0x" << std::hex << (uint)accel_cfg2 << "; \n" << std::dec <<
		"  ACCEL_AVGCFG: " << ACCEL_DEC3_CFG_STRS[accel_cfg2&0x3] << "; \n"
		"ACCEL_SMPLRT_DIV: " << (uint)(accel_smplrt_div) << " (" << accel_rate_hz << "Hz); \n");
}

bool
init_imu(
	const solodata::imud_config_t::imu_t &imu_cfg)
{
	bool okay = true;

	LOG4CXX_INFO(boilerplate->getLogger(),
		"\n======== ENABLED SENSORS ========\n"
		"GYRO   | " << (imu_cfg.gyr_rate_hz > 0  ? "ON " : "OFF") << " | " << imu_cfg.gyr_rate_hz  << "Hz\n"
		"ACCEL  | " << (imu_cfg.acc_rate_hz > 0  ? "ON " : "OFF") << " | " << imu_cfg.acc_rate_hz  << "Hz\n"
		"MAG    | " << (imu_cfg.mag_rate_hz > 0  ? "ON " : "OFF") << " | " << imu_cfg.mag_rate_hz  << "Hz\n"
		"ORIENT | " << (imu_cfg.quat_rate_hz > 0 ? "ON " : "OFF") << " | " << imu_cfg.quat_rate_hz << "Hz\n"
		"=================================");

	struct inv_icm20948_serif icm20948_serif;
	icm20948_serif.context   = 0; /* no need */
	icm20948_serif.read_reg  = idd_io_hal_read_reg;
	icm20948_serif.write_reg = idd_io_hal_write_reg;
	icm20948_serif.max_read  = 1024 * 16; /* maximum number of bytes allowed per serial read */
	icm20948_serif.max_write = 1024 * 16; /* maximum number of bytes allowed per serial write */
	icm20948_serif.is_spi = false;

	// driver only needs to be reset once
	if (icm20948_instance == 0)
	{
		inv_icm20948_reset_states(&icm_device, &icm20948_serif);
	}

	okay = okay && icm20948_sensor_setup(config.imu);

	if (okay && icm_device.selftest_done && !icm_device.offset_done)
	{
		// If we've run selftes and not already set the offset.
		inv_icm20948_set_offset(&icm_device, unscaled_bias);
		icm_device.offset_done = 1;
	}

	// enable FSYNC delay measurement (1PPS is connected to FYSNC line)
	const bool ENABLE_FSYNC = false;
	if (ENABLE_FSYNC && inv_icm20948_enable_fsync_delay(&icm_device) != 0)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"Failed to enable FYSNC delay measurement");
		okay = false;
	}

	// configure data rate and enable sensors
	okay = okay && set_sensor_rate(INV_SENSOR_TYPE_GYROSCOPE,imu_cfg.gyr_rate_hz);
	okay = okay && set_sensor_rate(INV_SENSOR_TYPE_ACCELEROMETER,imu_cfg.acc_rate_hz);
	okay = okay && set_sensor_rate(INV_SENSOR_TYPE_MAGNETOMETER,imu_cfg.mag_rate_hz);
	okay = okay && set_sensor_rate(INV_SENSOR_TYPE_GAME_ROTATION_VECTOR,imu_cfg.quat_rate_hz);

	dumpIMU_SetupInfo();

	return okay;
} //Setup

static uint8_t
icm20948_get_grv_accuracy()
{
	uint8_t accel_accuracy;
	uint8_t gyro_accuracy;

	accel_accuracy = (uint8_t)inv_icm20948_get_accel_accuracy();
	gyro_accuracy = (uint8_t)inv_icm20948_get_gyro_accuracy();
	return std::min(accel_accuracy, gyro_accuracy);
}

uint64_t prev_timestamp = 0;
uint32_t sample_seq_num = -1;
const uint32_t SAMPLES_TILL_STABLE = 10;
void
build_sensor_event_data(
	void *context,
	enum inv_icm20948_sensor sensortype,
	uint64_t timestamp,
	const void *data,
	const void *arg)
{
	const bool LOG_SAMPLE_RATES = false;

	// timestamp data as earlier as possible
	auto ts = std::chrono::system_clock::now();

	// incremement sample sequence # when we detect increase in
	// the sample timestamp that invensense library provides
	if (timestamp > prev_timestamp)
	{
		sample_seq_num++;
	}
	prev_timestamp = timestamp;

	if (sample_seq_num > config.imu.junk_filter)
	{
		samples_stable = true;
	}
	if ( ! samples_stable)
	{
		// wait till interrupt events become stable before handling
		return;
	}

	msw.start(Records::SENSOR_EVENT);

	if (curr_data == nullptr)
	{
		if (pool->acquire_busy(curr_data,100) != 0)
		{
			LOG4CXX_ERROR(boilerplate->getLogger(),
				"acquire failed! IMU data pool is exhausted. sensor data being dropped.");
			pool_exhaustion_count++;
			curr_data = nullptr;
			return;
		}
		curr_data->reset();
		curr_data->sample_seq_num = sample_seq_num;
		curr_data->first_data_ts = ts;
	}

	uint8_t sensor_id = convert_to_generic_ids[sensortype];

	const float *data_vect = (const float *)data;
	const uint8_t *accuracy_flag = (const uint8_t *)arg;
	switch (sensor_id)
	{
		case INV_SENSOR_TYPE_GYROSCOPE:
		{
			tracepoint(imud_ust,imu_sample_gyro,timestamp,sample_seq_num);
			gyro_sample_count++;
			memcpy(curr_data->gv,data_vect,sizeof(curr_data->gv));
			if (curr_data->hasGyro)
			{
				LOG4CXX_WARN(boilerplate->getLogger(),
					"Scorecard already had gyro data! Over wrote existing"
					" data. Seems like an interrupt might have been dropped...");
			}

			if (LOG_SAMPLE_RATES)
			{
				static uint64_t __prev_timestamp = -1;
				static bool logged = false;
				if (__prev_timestamp != -1 && ! logged)
				{
					logged = true;
					float rate_hz = 1000000.0f / (timestamp - __prev_timestamp);
					LOG4CXX_INFO(boilerplate->getLogger(),
						"gyro rate: " << rate_hz << "Hz");
				}
				__prev_timestamp = timestamp;
			}

			curr_data->hasGyro = true;
			break;
		}
		case INV_SENSOR_TYPE_ACCELEROMETER:
		{
			tracepoint(imud_ust,imu_sample_accel,timestamp,sample_seq_num);
			acc_sample_count++;
			memcpy(curr_data->av,data_vect,sizeof(curr_data->av));
			if (curr_data->hasAcc)
			{
				LOG4CXX_WARN(boilerplate->getLogger(),
					"Scorecard already had accelerometer data! Over wrote existing"
					" data. Seems like an interrupt might have been dropped...");
			}

			if (LOG_SAMPLE_RATES)
			{
				static uint64_t __prev_timestamp = -1;
				static bool logged = false;
				if (__prev_timestamp != -1 && ! logged)
				{
					logged = true;
					float rate_hz = 1000000.0f / (timestamp - __prev_timestamp);
					LOG4CXX_INFO(boilerplate->getLogger(),
						"accel rate: " << rate_hz << "Hz");
				}
				__prev_timestamp = timestamp;
			}

			curr_data->hasAcc = true;
			break;
		}
		case INV_SENSOR_TYPE_MAGNETOMETER:
		{
			tracepoint(imud_ust,imu_sample_mag,timestamp,sample_seq_num);
			mag_sample_count++;
			memcpy(curr_data->mv,data_vect,sizeof(curr_data->mv));
			if (curr_data->hasMag)
			{
				LOG4CXX_WARN(boilerplate->getLogger(),
					"Scorecard already had magnetmoeter data! Over wrote existing"
					" data. Seems like an interrupt might have been dropped...");
			}

			if (LOG_SAMPLE_RATES)
			{
				static uint64_t __prev_timestamp = -1;
				static bool logged = false;
				if (__prev_timestamp != -1 && ! logged)
				{
					logged = true;
					float rate_hz = 1000000.0f / (timestamp - __prev_timestamp);
					LOG4CXX_INFO(boilerplate->getLogger(),
						"mag rate: " << rate_hz << "Hz");
				}
				__prev_timestamp = timestamp;
			}

			curr_data->hasMag = true;
			break;
		}
		case INV_SENSOR_TYPE_GAME_ROTATION_VECTOR:
		{
			tracepoint(imud_ust,imu_sample_quat,timestamp,sample_seq_num);
			quat_sample_count++;
			memcpy(curr_data->quat,data_vect,sizeof(curr_data->quat));
			if (curr_data->hasQuat)
			{
				LOG4CXX_WARN(boilerplate->getLogger(),
					"Scorecard already had quaternion data! Over wrote existing"
					" data. Seems like an interrupt might have been dropped...");
			}

			if (LOG_SAMPLE_RATES)
			{
				static uint64_t __prev_timestamp = -1;
				static bool logged = false;
				if (__prev_timestamp != -1 && ! logged)
				{
					logged = true;
					float rate_hz = 1000000.0f / (timestamp - __prev_timestamp);
					LOG4CXX_INFO(boilerplate->getLogger(),
						"quat rate: " << rate_hz << "Hz");
				}
				__prev_timestamp = timestamp;
			}

			curr_data->hasQuat = true;
			break;
		}
		case INV_SENSOR_TYPE_FSYNC_EVENT:
			tracepoint(imud_ust,imu_sample_fsync,timestamp,data_vect[0],sample_seq_num);
			sync_sample_count++;
			LOG4CXX_DEBUG(boilerplate->getLogger(),
				"FSYNC delay time = " << data_vect[0] << "us");
			break;
		default:
			LOG4CXX_WARN(boilerplate->getLogger(),
				"Received data for unexpected sensor_id " << sensor_id);
			return;
	}

	if (icm_device.fsync_info.error != 0)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"Received FSYNC error " << icm_device.fsync_info.error);
	}

	if (curr_data->ready(
		config.imu.gyr_rate_hz,
		config.imu.acc_rate_hz,
		config.imu.mag_rate_hz,
		config.imu.quat_rate_hz))
	{
		tracepoint(imud_ust,imud_sample_produce,curr_data->sample_seq_num);
		pool->produce(curr_data);
		curr_data = nullptr;
	}
	msw.stop(Records::SENSOR_EVENT);
}

// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

void
dmp_data_ready()
{
	tracepoint(imud_ust,imu_interrupt);
	if ( ! stay_alive)
	{
		// ignore samples while we're in the middle of shutting down.
		// really wish wiringPi provided a way to gracefully unregister
		// an ISR function...
		return;
	}

	// reset watchdog counter back to zero
	watchdog_counter = 0;
	irq_count++;

	// service the interrupt
	std::lock_guard<std::mutex> guard(icm_mutex);
	int rv = inv_icm20948_poll_sensor(&icm_device, (void *)0, build_sensor_event_data);
}

bool
init_zmq(
	void *zmq_context,
	const solodata::imud_config_t::zmq_t &zmq_cfg)
{
	bool okay = true;

	// configure ZMQ publisher
	publisher = zmq_socket(zmq_context,ZMQ_PUB);
	int rc = zmq_bind(publisher,zmq_cfg.hostname.c_str());
	if (rc < 0)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"Failed to bind socket to " << zmq_cfg.hostname << "."
			" error = " << zmq_strerror(errno));
		okay = false;
	}
	if (okay && zmq_cfg.hostname.find("ipc://") == 0)
	{
		// allow all users to connect and receive data from socket
		chmod(&zmq_cfg.hostname.c_str()[6],0777);
	}

	// seems to help with dropped messages at startup
	usleep(10000);

	// allocate memory to sample buffer
	buffer = malloc(zmq_cfg.buff_size_bytes);

	return okay;
}

void
shutdown_signal(
	int signal)
{
	DynamicMemoryGuard::thread_disable();
	LOG4CXX_INFO(boilerplate->getLogger(),"shutdown requested!");
	stay_alive = false;
}

static
void
skeleton_daemon()
{
	pid_t pid;

	/* Fork off the parent process */
	Logging::before_fork();
	pid = fork();
	Logging::after_fork();

	/* An error occurred */
	if (pid < 0)
		exit(EXIT_FAILURE);

	/* Success: Let the parent terminate */
	if (pid > 0)
		exit(EXIT_SUCCESS);

	/* On success: The child process becomes session leader */
	if (setsid() < 0)
		exit(EXIT_FAILURE);
}

void
display_usage()
{
	printf("imud [options]\n");
	printf(" -f                : path to config file\n");
	printf(" --daemon          : run as daemon process\n");
	printf(" --stopwatch       : enable stopwatching of critical functions\n");
	printf(" --log-sample-rate : prints measured sample rate of IMU data\n");
	printf(" --log-samples     : prints IMU sample data to console\n");
	printf(" --help            : display this menu\n");
}

int
main(int argc, char **argv)
{
	int daemon_flag = 0;
	int enable_sw = 0;
	int log_sample_rate = 0;
	int log_samples = 0;
	std::string cfg_filepath = "";
	static struct option long_options[] =
	{
		{"daemon"         , no_argument      , &daemon_flag    , 1  },
		{"stopwatch"      , no_argument      , &enable_sw      , 1  },
		{"log-sample-rate", no_argument      , &log_sample_rate, 1  },
		{"log-samples"    , no_argument      , &log_samples    , 1  },
		{"help"           , no_argument      , 0               , 'h'},
		{0, 0, 0, 0}
	};

	while (true)
	{
		int option_index = 0;
		int c = getopt_long(
			argc,
			argv,
			"hf:",
			long_options,
			&option_index);

		// detect the end of the options
		if (c == -1)
		{
			break;
		}

		switch (c)
		{
			case 0:
				// flag setting
				break;
			case 'f':
				cfg_filepath = optarg;
				break;
			case 'h':
			case '?':
				display_usage();
				exit(0);
				break;
			default:
				printf("Unsupported option '%c'\n",(char)c);
				display_usage();
				exit(1);
				break;
		}
	}

	// start the daemon in another process
	if (daemon_flag)
	{
		skeleton_daemon();
	}
	boilerplate = new Boilerplate();
	/* Catch, ignore and handle signals */
	signal(SIGINT, shutdown_signal);
	signal(SIGHUP, shutdown_signal);

	if (cfg_filepath != "")
	{
		LOG4CXX_INFO(boilerplate->getLogger(),
			"parsing config from '" << cfg_filepath << "'");
		YAML::Node cfg_node = YAML::LoadFile(cfg_filepath);
		config = cfg_node["imud"].as<solodata::imud_config_t>();
	}

	bool okay = true;

	// setup realtime scheduling policy/priorities
	struct sched_param sched_p;
	sched_p.sched_priority = config.sched.priority;
	LOG4CXX_INFO(boilerplate->getLogger(),
		"setting scheduler policy to 'SCHED_FIFO' with"
		" priority = " << config.sched.priority);
	if (sched_setscheduler(0,SCHED_FIFO,&sched_p) < 0)
	{
		if (errno == EPERM)
		{
			LOG4CXX_WARN(boilerplate->getLogger(),
				"unable to set scheduling policy to 'SCHED_FIFO'."
				" root permissions are required. defaulting to 'SCHED_OTHER' policy.");
		}
		else
		{
			LOG4CXX_ERROR(boilerplate->getLogger(),
				"uncaught sched_setscheduler() error!"
				" errno = " << errno << "(" << strerror(errno) << ")");
		}
	}

	if (config.sched.cpu_affinity >= 0)
	{
		LOG4CXX_INFO(boilerplate->getLogger(),
			"setting CPU affinity to core " << config.sched.cpu_affinity);
		cpu_set_t cpu_set;
		CPU_ZERO(&cpu_set);
		CPU_SET(config.sched.cpu_affinity,&cpu_set);
		if (sched_setaffinity(0,sizeof(cpu_set),&cpu_set) < 0)
		{
			LOG4CXX_ERROR(boilerplate->getLogger(),
				"uncaught sched_setaffinity() error!"
				" errno = " << errno << "(" << strerror(errno) << ")");
		}
	}

	// setup stopwatch
	Records::SAMPLE_BUILD = msw.addRecord("SampleBuild",1000);
	Records::SAMPLE_PUB = msw.addRecord("SamplePublish",1000);
	Records::SENSOR_EVENT = msw.addRecord("SensorEvent",10000);
	if (enable_sw)
	{
		LOG4CXX_WARN(boilerplate->getLogger(),"stopwatch is enabled");
		msw.enabled(enable_sw);
	}

	google::protobuf::ArenaOptions opt;
	opt.start_block_size = 4092;
	google::protobuf::Arena pb_arena(opt);
	imu_sample = google::protobuf::Arena::CreateMessage<solodata::IMU_Sample>(&pb_arena);

	void *context = zmq_ctx_new();
	std::thread pub_thread;
	std::thread stat_thread;
	pool = new IMU_DataPool(config.samp_pool_size);

	connected = init_zmq(context,config.zmq);
	if (connected)
	{
		pub_thread = std::thread(
			&publisher_thread,
			log_sample_rate,
			log_samples,
			config.zmq);
	}

	if (config.stat.enabled)
	{
		stat_thread = std::thread(
			status_thread,
			config.stat);
	}

	okay = okay && init_imu(config.imu);
	if (okay)
	{
		LOG4CXX_INFO(boilerplate->getLogger(),
			"IMU initialized successfully! Enabling interrupt handler.");

		wiringPiSetup();
		wiringPiISR(config.imu.intr_pin, INT_EDGE_RISING, &dmp_data_ready);
	}
	else
	{
		// IMU is not setup, to request all threads to shutdown
		stay_alive = false;
	}

	// ------------------
	// main event loop
	// ------------------
	while (okay && stay_alive && connected)
	{
		// be nice to CPU and service interrupt when needed
		usleep(100000);

		watchdog_counter++;
		if (watchdog_counter > config.imu.watchdog_limit)
		{
			LOG4CXX_ERROR(boilerplate->getLogger(),
				"IMU watchdog engaged! re-initializing IMU...");
			watchdog_engaged_count++;

			// hold lock while resetting (prevents ISR from using device)
			std::lock_guard<std::mutex> guard(icm_mutex);
			init_imu(config.imu);

			watchdog_counter = 0;
		}
	}

	pub_thread.join();
	if (config.stat.enabled)
	{
		stat_thread.join();
	}

	if (msw.enabled())
	{
		LOG4CXX_INFO(boilerplate->getLogger(),
			msw.getSummary());
	}

	// ------------------
	// clean stuff up
	// ------------------

	if (buffer != nullptr)
	{
		free(buffer);
		buffer = nullptr;
	}

	if (connected)
	{
		zmq_close(publisher);
	}
	zmq_ctx_destroy(context);

	if (boilerplate != nullptr)
	{
		free(boilerplate);
		boilerplate = nullptr;
	}

	return okay ? 0 : -1;
}
