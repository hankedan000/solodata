#undef TRACEPOINT_PROVIDER
#define TRACEPOINT_PROVIDER imud_ust

#undef TRACEPOINT_INCLUDE
#define TRACEPOINT_INCLUDE "./imud_tracepoints.h"

#if !defined(_TP_H) || defined(TRACEPOINT_HEADER_MULTI_READ)
#define _TP_H

#include <lttng/tracepoint.h>

//---------------------------------------------------------
// IMU Sample Events
//---------------------------------------------------------

TRACEPOINT_EVENT_CLASS(
	/* Tracepoint provider name */
	imud_ust,

	/* Tracepoint class name */
	imu_sample_class,

	/* Input arguments */
	TP_ARGS(
		uint64_t, inv_timestamp,
		uint32_t, sample_seq_num
	),

	/* Output event fields */
	TP_FIELDS(
		ctf_integer(uint64_t, inv_timestamp, inv_timestamp)
		ctf_integer(uint32_t, sample_seq_num, sample_seq_num)
	)
)

TRACEPOINT_EVENT_INSTANCE(
	imud_ust,
	imu_sample_class,
	imu_sample_gyro,
	TP_ARGS(
		uint64_t, inv_timestamp,
		uint32_t, sample_seq_num
	)
)
TRACEPOINT_EVENT_INSTANCE(
	imud_ust,
	imu_sample_class,
	imu_sample_accel,
	TP_ARGS(
		uint64_t, inv_timestamp,
		uint32_t, sample_seq_num
	)
)
TRACEPOINT_EVENT_INSTANCE(
	imud_ust,
	imu_sample_class,
	imu_sample_mag,
	TP_ARGS(
		uint64_t, inv_timestamp,
		uint32_t, sample_seq_num
	)
)
TRACEPOINT_EVENT_INSTANCE(
	imud_ust,
	imu_sample_class,
	imu_sample_quat,
	TP_ARGS(
		uint64_t, inv_timestamp,
		uint32_t, sample_seq_num
	)
)

TRACEPOINT_EVENT(
	imud_ust,
	imu_sample_fsync,
	TP_ARGS(
		uint64_t, inv_timestamp,
		float, offset_us,
		uint32_t, sample_seq_num
	),
	TP_FIELDS(
		ctf_integer(uint64_t, inv_timestamp, inv_timestamp)
		ctf_float(float, offset_us, offset_us)
		ctf_integer(uint32_t, sample_seq_num, sample_seq_num)
	)
)

//---------------------------------------------------------
// i2c trace events
//---------------------------------------------------------

TRACEPOINT_EVENT_CLASS(
	/* Tracepoint provider name */
	imud_ust,

	/* Tracepoint class name */
	i2c_event_class,

	/* Input arguments */
	TP_ARGS(
		uint8_t, addr,
		uint32_t, size,
		uint8_t, start
	),

	/* Output event fields */
	TP_FIELDS(
		ctf_integer(uint8_t, addr, addr)
		ctf_integer(uint32_t, size, size)
		ctf_integer(uint8_t, start, start)
	)
)

TRACEPOINT_EVENT_INSTANCE(
	imud_ust,
	i2c_event_class,
	i2c_read,
	TP_ARGS(
		uint8_t, addr,
		uint32_t, size,
		uint8_t, start
	)
)

TRACEPOINT_EVENT_INSTANCE(
	imud_ust,
	i2c_event_class,
	i2c_write,
	TP_ARGS(
		uint8_t, addr,
		uint32_t, size,
		uint8_t, start
	)
)

//---------------------------------------------------------
// imud general events
//---------------------------------------------------------

TRACEPOINT_EVENT(
	imud_ust,
	imu_interrupt,
	TP_ARGS(
	),
	TP_FIELDS(
	)
)

TRACEPOINT_EVENT_CLASS(
	/* Tracepoint provider name */
	imud_ust,

	/* Tracepoint class name */
	imud_sample_event_class,

	/* Input arguments */
	TP_ARGS(
		uint32_t, sample_seq_num
	),

	/* Output event fields */
	TP_FIELDS(
		ctf_integer(uint32_t, sample_seq_num, sample_seq_num)
	)
)

TRACEPOINT_EVENT_INSTANCE(
	imud_ust,
	imud_sample_event_class,
	imud_sample_produce,
	TP_ARGS(
		uint32_t, sample_seq_num
	)
)

TRACEPOINT_EVENT_INSTANCE(
	imud_ust,
	imud_sample_event_class,
	imud_sample_consume,
	TP_ARGS(
		uint32_t, sample_seq_num
	)
)

TRACEPOINT_EVENT_INSTANCE(
	imud_ust,
	imud_sample_event_class,
	imud_sample_release,
	TP_ARGS(
		uint32_t, sample_seq_num
	)
)

TRACEPOINT_EVENT_INSTANCE(
	imud_ust,
	imud_sample_event_class,
	imud_sample_publish,
	TP_ARGS(
		uint32_t, sample_seq_num
	)
)

#endif /* _TP_H */

#include <lttng/tracepoint-event.h>
