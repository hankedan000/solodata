#include <csignal>
#include <Logging.h>
#include <LoggingObject.h>
#include "MPU6050_6Axis_MotionApps20.h"
#include "IMU_Sample.pb.h"
#include <thread>
#include <unistd.h>
#include <wiringPi.h>
#include <zmq.h>

SET_LOGGING_CFG_NAME("solodata_logging.cfg");
#define Device_INT_PIN 0// GPIO 17 -> Pin 0 (wiringPi API)
#define WATCHDOG_COUNTER_MAX 10
#define LOG_SAMPLE_RATE false// will log MPU interrupt event rate
#define MAX_BUFFER_SIZE 1000// used serial MPU samples to protobuf
#define HOSTNAME "ipc:///var/run/imud.sock"

// used to shutdown the process when interrupt signal is received
volatile bool stay_alive = true;

class Boilerplate : public LoggingObject
{
public:
	Boilerplate()
	: LoggingObject("imud")
	{}
};

Boilerplate *boilerplate = nullptr;
MPU6050 mpu(0x68);

// MPU control/status vars
volatile unsigned int watchdogCounter = 0;
bool mpuCommsOkay = true;
uint16_t packetSize;   // expected DMP packet size (default is 42 bytes)
uint8_t fifoBuffer[64];// FIFO storage buffer

// orientation/motion vars
Quaternion q;  // [w, x, y, z]         quaternion container
VectorInt16 aa;// [x, y, z]            accel sensor measurements
VectorInt16 gg;// [x, y, z]            gyro sensor measurements

// ZMQ connection related stuff
bool connected = false;
void *publisher = nullptr;
void *buffer = nullptr;
solodata::VectorFloat av;
solodata::VectorFloat gv;
solodata::Quaternion quat;

void
send_sample(
	const solodata::IMU_Sample &datum)
{
	// serialize datum into buffer
	size_t size = datum.ByteSizeLong();
	if (size > MAX_BUFFER_SIZE)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"Failed to serialize IMU_Sample of size " << size <<
			" because it exceeds max of " << MAX_BUFFER_SIZE << ". This"
			" can occur due to high serialization overhead. Sample dropped.");
		return;
	}
	datum.SerializeToArray(buffer,size);

	int rc = zmq_send(publisher,buffer,size,ZMQ_DONTWAIT);
	if (rc < 0)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"zmq_send failed for message '" << datum.DebugString() << "'");
		return;
	}
}

// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

void
dmp_data_ready()
{
	// reset watchdog counter back to zero
	watchdogCounter = 0;

	static bool first_interrupt = true;
	static std::chrono::time_point<std::chrono::system_clock> prev_time;
	auto samp_time = std::chrono::high_resolution_clock::now();
	if (LOG_SAMPLE_RATE && ! first_interrupt)
	{
		auto diff_time = std::chrono::duration_cast<std::chrono::microseconds>(
			samp_time - prev_time);

		LOG4CXX_INFO(boilerplate->getLogger(),
			"sample rate = " << (1.0/(diff_time.count() / 1.0e6)) << "Hz");
	}
	prev_time = samp_time;
	if (first_interrupt)
	{
		LOG4CXX_INFO(boilerplate->getLogger(),
			"First interrupt received! IMU samples are streaming.");
		first_interrupt = false;
	}

	// service the interrupt
	uint8_t mpuIntStatus = mpu.getIntStatus();

	// get current FIFO count
	uint16_t fifoCount = mpu.getFIFOCount();

	// check for overflow (this should never happen unless our code is too inefficient)
	if ((mpuIntStatus & (0x01 << MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount == 1024)
	{
		// reset so we can continue cleanly
		mpu.resetFIFO();
		LOG4CXX_WARN(boilerplate->getLogger(),"FIFO overflow!");
	}
	else if (mpuIntStatus & (0x01 << MPU6050_INTERRUPT_DMP_INT_BIT))
	{
		// wait for correct available data length, should be a VERY short wait
		while (fifoCount < packetSize)
		{
			fifoCount = mpu.getFIFOCount();
		}

		// read a packet from FIFO
		mpu.getFIFOBytes(fifoBuffer, packetSize);
		
		// track FIFO count here in case there is > 1 packet available
		// (this lets us immediately read more without waiting for an interrupt)
		fifoCount -= packetSize;

		// -------------------------
		// BUILD AND SEND SAMPLE
		// -------------------------

		// get info from sample buffer
		mpu.dmpGetQuaternion(&q, fifoBuffer);
		mpu.dmpGetAccel(&aa, fifoBuffer);
		mpu.dmpGetGyro(&gg, fifoBuffer);

		solodata::IMU_Sample samp;

		// assign sample timestamp
		auto samp_time_us = std::chrono::time_point_cast<std::chrono::microseconds>(samp_time);
		auto epoch = samp_time_us.time_since_epoch();
		auto timestamp_us = std::chrono::duration_cast<std::chrono::microseconds>(epoch);
		samp.set_timestamp(timestamp_us.count());

		// assign quanternion
		quat.set_w(q.w);
		quat.set_x(q.x);
		quat.set_y(q.y);
		quat.set_z(q.z);
		samp.set_allocated_quat(&quat);

		// assign acceleration vector (+1g = +8192)
		av.set_x((float)(aa.x) / 8192.0);
		av.set_y((float)(aa.y) / 8192.0);
		av.set_z((float)(aa.z) / 8192.0);
		samp.set_allocated_acc(&av);

		// assign gyro vector (+1000deg/sec = +8192)
		gv.set_x((float)(gg.x) * 1000.0 / 8192.0);
		gv.set_y((float)(gg.y) * 1000.0 / 8192.0);
		gv.set_z((float)(gg.z) * 1000.0 / 8192.0);
		samp.set_allocated_gyro(&gv);

		// publish sample to main thread
		send_sample(samp);
		LOG4CXX_TRACE(boilerplate->getLogger(),samp.DebugString());

		// release values we assigned
		samp.release_quat();
		samp.release_acc();
		samp.release_gyro();
	}
}

// ================================================================
// ===                      INITIAL SETUP                       ===
// ================================================================

bool
init_mpu()
{
	bool okay = true;

	// NOTE: 8MHz or slower host processors, like the Teensy @ 3.3v or Ardunio
	// Pro Mini running at 3.3v, cannot handle this baud rate reliably due to
	// the baud timing being too misaligned with processor ticks. You must use
	// 38400 or slower in these cases, or use some kind of external separate
	// crystal solution for the UART timer.

	// initialize device
	LOG4CXX_DEBUG(boilerplate->getLogger(),"Initializing MPU device...");
	mpu.initialize();
	mpu.setRate(4); 								// Sample Rate (200Hz = 1Hz Gyro SR / 4+1)
	mpu.setDLPFMode(MPU6050_DLPF_BW_20);			// Low Pass filter 20hz
	mpu.setFullScaleGyroRange(MPU6050_GYRO_FS_250);	// 250? / s
	mpu.setFullScaleAccelRange(MPU6050_ACCEL_FS_2);	// +-2g
	
	// verify connection
	LOG4CXX_DEBUG(boilerplate->getLogger(),"Testing device connections...");
	if (mpu.testConnection())
	{
		LOG4CXX_INFO(boilerplate->getLogger(),"MPU6050 connection successful");
	}
	else
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),"MPU6050 connection failed!");
		okay = false;
	}

	// load and configure the DMP
	LOG4CXX_DEBUG(boilerplate->getLogger(),"Initializing DMP...");
	uint8_t devStatus = mpu.dmpInitialize();
	
	// make sure it worked (returns 0 if so)
	if (devStatus == 0)
	{
		// turn on the DMP, now that it's ready
		LOG4CXX_DEBUG(boilerplate->getLogger(),"Enabling DMP...");
		mpu.setDMPEnabled(true);

		// setup MPU6050 interrupt service routine
		LOG4CXX_DEBUG(boilerplate->getLogger(),"Enabling interrupt detection...");
		wiringPiSetup();
		wiringPiISR(Device_INT_PIN, INT_EDGE_RISING, &dmp_data_ready);

		// set our DMP Ready flag so the main loop() function knows it's okay to use it
		LOG4CXX_INFO(boilerplate->getLogger(),"DMP ready! Waiting for first interrupt...");

		// clear interrupt status
		mpu.getIntStatus();

		// get expected DMP packet size for later comparison
		packetSize = mpu.dmpGetFIFOPacketSize();
	}
	else
	{
		// ERROR!
		// 1 = initial memory load failed
		// 2 = DMP configuration updates failed
		// (if it's going to break, usually the code will be 1)
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"DMP Initialization failed (code " << devStatus << ")");
		okay = false;
	}

	return okay;
}

bool
init_zmq(
	void *zmq_context)
{
	bool okay = true;

	// configure ZMQ publisher
	publisher = zmq_socket(zmq_context,ZMQ_PUB);
	int rc = zmq_bind(publisher,HOSTNAME);
	if (rc < 0)
	{
		LOG4CXX_ERROR(boilerplate->getLogger(),
			"Failed to bind socket to " << HOSTNAME << "."
			" error = " << zmq_strerror(errno));
		okay = false;
	}

	// seems to help with dropped messages at startup
	usleep(10000);

	// allocate memory to sample buffer
	buffer = malloc(MAX_BUFFER_SIZE);

	return okay;
}

void
shutdown_signal(
	int signal)
{
	LOG4CXX_INFO(boilerplate->getLogger(),"shutdown requested!");
	stay_alive = false;
}

static
void
skeleton_daemon()
{
	pid_t pid;

	/* Fork off the parent process */
	Logging::before_fork();
	pid = fork();
	Logging::after_fork();

	/* An error occurred */
	if (pid < 0)
		exit(EXIT_FAILURE);

	/* Success: Let the parent terminate */
	if (pid > 0)
		exit(EXIT_SUCCESS);

	/* On success: The child process becomes session leader */
	if (setsid() < 0)
		exit(EXIT_FAILURE);

	/* Catch, ignore and handle signals */
	//TODO: Implement a working signal handler */
	signal(SIGINT, shutdown_signal);
	signal(SIGHUP, shutdown_signal);
}

int
main()
{
	// start the daemon in another process
	skeleton_daemon();
	boilerplate = new Boilerplate();

	bool okay = true;

	void *context = zmq_ctx_new();

	connected = init_zmq(context);
	mpuCommsOkay = init_mpu();

	// ------------------
	// main event loop
	// ------------------
	while (stay_alive && connected)
	{
		if ( ! mpuCommsOkay)
		{
			LOG4CXX_WARN(boilerplate->getLogger(),
				"MPU6050 communication failure detected. Resetting MPU6050...");
			usleep(1000000);
			mpuCommsOkay = init_mpu();
		}

		// be nice to CPU and service interrupt when needed
		usleep(100000);

		watchdogCounter++;
		if (watchdogCounter > WATCHDOG_COUNTER_MAX)
		{
			LOG4CXX_WARN(boilerplate->getLogger(),
				"MPU watchdog engadged");
			mpuCommsOkay = false;
		}
	}

	// ------------------
	// clean stuff up
	// ------------------

	if (buffer != nullptr)
	{
		free(buffer);
		buffer = nullptr;
	}

	if (connected)
	{
		zmq_close(publisher);
	}
	zmq_ctx_destroy(context);

	if (boilerplate != nullptr)
	{
		free(boilerplate);
		boilerplate = nullptr;
	}

	return okay ? 0 : -1;
}