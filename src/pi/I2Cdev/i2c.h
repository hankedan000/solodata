#ifndef I2C_H_
#define I2C_H_

#include <stdint.h>
#include <string.h>

// Returns a new file descriptor for communicating with the I2C bus:
int
i2c_init(
	const char *i2c_fname);

void
i2c_close(
	int i2c_fd);

// Write to an I2C slave device's register:
int
i2c_write8(
	int i2c_fd,
	uint8_t slave_addr,
	uint8_t reg,
	uint8_t length,
	const uint8_t *data);

// Write 16bits words to an I2C slave device's register:
int
i2c_write16(
	int i2c_fd,
	uint8_t slave_addr,
	uint8_t reg,
	uint8_t length,
	const uint16_t *data);

// Read the given I2C slave device's register and return the read value in `*result`:
int
i2c_read8(
	int i2c_fd,
	uint8_t slave_addr,
	uint8_t reg,
	uint8_t length,
	uint8_t *data);

#endif