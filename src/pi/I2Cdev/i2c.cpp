#include "i2c.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <linux/i2c-dev.h>
// Terrible portability hack between arm-linux-gnueabihf-gcc on Mac OS X and native gcc on raspbian.
#ifndef I2C_M_RD
#include <linux/i2c.h>
#endif

// Returns a new file descriptor for communicating with the I2C bus:
int
i2c_init(
	const char *i2c_fname)
{
	int i2c_fd = -1;
	if ((i2c_fd = open(i2c_fname, O_RDWR)) < 0)
	{
		char err[200];
		sprintf(err, "open('%s') in i2c_init", i2c_fname);
		perror(err);
		return -1;
	}

	// NOTE we do not call ioctl with I2C_SLAVE here because we always use the I2C_RDWR ioctl operation to do
	// writes, reads, and combined write-reads. I2C_SLAVE would be used to set the I2C slave address to communicate
	// with. With I2C_RDWR operation, you specify the slave address every time. There is no need to use normal write()
	// or read() syscalls with an I2C device which does not support SMBUS protocol. I2C_RDWR is much better especially
	// for reading device registers which requires a write first before reading the response.

	return i2c_fd;
}

void
i2c_close(
	int i2c_fd)
{
	close(i2c_fd);
}

// Write to an I2C slave device's register:
int
i2c_write8(
	int i2c_fd,
	uint8_t slave_addr,
	uint8_t reg,
	uint8_t length,
	const uint8_t *data)
{
	int retval = 0;
	uint8_t *buff = (uint8_t*)malloc(length + 1);

	struct i2c_msg msgs[1];
	struct i2c_rdwr_ioctl_data msgset[1];

	buff[0] = reg;
	memcpy((void*)(buff+1),(void*)data,length);

	msgs[0].addr = slave_addr;
	msgs[0].flags = 0;
	msgs[0].len = length + 1;
	msgs[0].buf = buff;

	msgset[0].msgs = msgs;
	msgset[0].nmsgs = 1;

	int res = ioctl(i2c_fd, I2C_RDWR, &msgset);
	if (res < 0)
	{
		perror("ioctl(I2C_RDWR) in i2c_write8");
		retval = -1;
	}

	free(buff);

	return retval;
}

// Write to an I2C slave device's register:
int
i2c_write16(
	int i2c_fd,
	uint8_t slave_addr,
	uint8_t reg,
	uint8_t length,
	const uint16_t *data)
{
	int retval = 0;
	size_t buff_len = sizeof(uint16_t) * length + 1;
	uint16_t *buff = (uint16_t*)malloc(buff_len);

	struct i2c_msg msgs[1];
	struct i2c_rdwr_ioctl_data msgset[1];

	buff[0] = reg;
	for (unsigned int i=0; i<length; i++)
	{
		buff[1+i+0] = data[i] >> 8;// MSB
		buff[1+i+1] = data[i];     // LSB
	}
	memcpy((void*)(buff+1),(void*)data,length);

	msgs[0].addr = slave_addr;
	msgs[0].flags = 0;
	msgs[0].len = buff_len;
	msgs[0].buf = (uint8_t*)buff;

	msgset[0].msgs = msgs;
	msgset[0].nmsgs = 1;

	int res = ioctl(i2c_fd, I2C_RDWR, &msgset);
	if (res < 0)
	{
		perror("ioctl(I2C_RDWR) in i2c_write16");
		retval = -1;
	}

	free(buff);

	return retval;
}

// Read the given I2C slave device's register and return the read value in `*result`:
int
i2c_read8(
	int i2c_fd,
	uint8_t slave_addr,
	uint8_t reg,
	uint8_t length,
	uint8_t *data)
{
	int retval = 0;
	struct i2c_msg msgs[2];
	struct i2c_rdwr_ioctl_data msgset[1];

	msgs[0].addr = slave_addr;
	msgs[0].flags = 0;
	msgs[0].len = 1;
	msgs[0].buf = &reg;

	msgs[1].addr = slave_addr;
	msgs[1].flags = I2C_M_RD | I2C_M_NOSTART;
	msgs[1].len = length;
	msgs[1].buf = data;

	msgset[0].msgs = msgs;
	msgset[0].nmsgs = 2;

	if (ioctl(i2c_fd, I2C_RDWR, &msgset) < 0)
	{
		perror("ioctl(I2C_RDWR) in i2c_read8");
		retval = -1;
	}

	return retval;
}