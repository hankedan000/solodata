add_library(I2Cdev SHARED I2Cdev.cpp i2c.cpp)
target_include_directories(I2Cdev PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)
target_link_libraries(I2Cdev wiringPi ArduinoWrapper)
set_target_properties(
	I2Cdev PROPERTIES
		PUBLIC_HEADER "include/I2Cdev.h"
)
install(
	TARGETS I2Cdev
	LIBRARY
		DESTINATION lib
)