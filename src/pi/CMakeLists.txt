add_definitions(-DRASPBERRY_PI)

# fixes issue with using synchronization instructions for ARMv7 (raspberry pi)
if ("${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "armv7l")
	add_definitions("-march=armv7-a")
endif()

add_subdirectory(ArduinoWrapper)
add_subdirectory(I2Cdev)
add_subdirectory(ICM20948)
add_subdirectory(MPU6050)
add_subdirectory(imud)
add_subdirectory(imumon)
