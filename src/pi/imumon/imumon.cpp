#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <unistd.h>
#include <ZMQ_Boilerplate.h>
#include <Reactor.h>

#include "NavMath.h"

// include protobuf header files
#include "IMU_Sample.pb.h"

// include ZeroMQ headers
#include <zmq.h>

// TODO move these to a shared header with imud
#define HOSTNAME "ipc:///var/run/imud.sock"

ZMQ_Boilerplate boilerplate("imumon");
Reactor reactor;

bool saw_first_sample = false;
solodata::IMU_Sample first_sample;
solodata::VectorFloat gravity;
solodata::VectorFloat lin_acc;
solodata::VectorFloat pos;

solodata::Quaternion
operator-(
	const solodata::Quaternion &lhs,
	const solodata::Quaternion &rhs)
{
	solodata::Quaternion res;
	res.set_w(lhs.w() - rhs.w());
	res.set_x(lhs.x() - rhs.x());
	res.set_y(lhs.y() - rhs.y());
	res.set_z(lhs.z() - rhs.z());
	return res;
}

void
on_imu_sample(
	const solodata::IMU_Sample &sample)
{
	LOG4CXX_DEBUG(boilerplate.getLogger(),sample.DebugString());

	if (saw_first_sample)
	{
		const float alpha = 0.9;
		gravity.set_x(alpha * gravity.x() + (1.0 - alpha) * sample.acc().x());
		gravity.set_y(alpha * gravity.y() + (1.0 - alpha) * sample.acc().y());
		gravity.set_z(alpha * gravity.z() + (1.0 - alpha) * sample.acc().z());

		lin_acc = sample.acc() - gravity;

		solodata::VectorFloat g(solodata::get_gravity(sample.quat()));

		const float SAMPLE_RATE_HZ = 100.0;
		float dt = 1.0 / SAMPLE_RATE_HZ;
		solodata::VectorFloat acc_drift(sample.acc() - first_sample.acc());
		solodata::Quaternion quat_drift(sample.quat() - first_sample.quat());
		pos = pos + 0.5 * lin_acc * dt * dt;
		LOG4CXX_INFO(boilerplate.getLogger(),"gravity: " << solodata::vf_to_str(gravity));
		LOG4CXX_INFO(boilerplate.getLogger(),"g: " << solodata::vf_to_str(g));
		LOG4CXX_INFO(boilerplate.getLogger(),"lin_acc: " << solodata::vf_to_str(lin_acc));
		LOG4CXX_INFO(boilerplate.getLogger(),"acc_drift: " << solodata::vf_to_str(acc_drift));
		LOG4CXX_INFO(boilerplate.getLogger(),"quat_drift: " << solodata::quat_to_str(quat_drift));
		LOG4CXX_INFO(boilerplate.getLogger(),"pos: " << solodata::vf_to_str(pos));
	}
	else
	{
		LOG4CXX_INFO(boilerplate.getLogger(),"Got the first sample!");
		first_sample = sample;
		saw_first_sample = true;
	}
}

int
main(
	int argc,
	char **argv)
{
	bool okay = true;

	// reduce default logging level
	boilerplate.getLogger()->setLevel(log4cxx::Level::getInfo());

	// set class logger prefixes
	reactor.setLoggerPrefix(boilerplate.getLoggerNameFull());

	void *subscriber = boilerplate.create_socket(ZMQ_SUB);
	if (okay && zmq_connect(subscriber,HOSTNAME) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to connect socket to " << HOSTNAME << "."
			" error = " << zmq_strerror(errno));
		okay = false;
	}
	if (okay && zmq_setsockopt(subscriber,ZMQ_SUBSCRIBE,"",0) < 0)
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to create subscriber."
			" error = " << zmq_strerror(errno));
		okay = false;
	}
	if (okay && ! reactor.add_pb_listener(subscriber,on_imu_sample))
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"Failed to add IMU sample listener to reactor.");
		okay = false;
	}

	// start aggregating logs on this node
	if (okay && ! reactor.start())
	{
		LOG4CXX_ERROR(boilerplate.getLogger(),
			"failed to start reactor!");
		okay = false;
	}

	return okay ? EXIT_SUCCESS : EXIT_FAILURE;
}