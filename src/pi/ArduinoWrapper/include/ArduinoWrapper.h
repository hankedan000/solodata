#ifndef ARDUINOWRAPPER_H_
#define ARDUINOWRAPPER_H_

//Standard Libraries
#include <stdint.h>
#include <sstream>
#include <iostream>
#include <unistd.h>
#include <chrono>
#include <algorithm>
#include <cmath>
#include <wiringPi.h>

// ----- BEGIN avr/pgmspace.h wrappers -----
typedef uint8_t prog_uchar;

#define PROGMEM
#define F(expression) expression
#define pgm_read_byte(expression) *(expression)
// ----- END avr/pgmspace.h wrappers -----

#define delay(expression) usleep((expression) * 1000)
#define round(val) std::round(val)

#define PI M_PI

long
map(
	long x,
	long in_min,
	long in_max,
	long out_min,
	long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

template<typename T>
T
min(
	T lhs,
	T rhs)
{
	return std::min(lhs,rhs);
}

template<typename T>
T
max(
	T lhs,
	T rhs)
{
	return std::max(lhs,rhs);
}

#endif /* ARDUINOWRAPPER_H_ */