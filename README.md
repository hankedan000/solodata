# Getting Started

## Dependencies
 * [cpp-can-parser](https://github.com/hankedan000/cpp-can-parser)
 * libzip
 * gpsd
  - See section on building gpsd from source
 * can-utils
  - ```sudo apt install can-utils```

### Build gpsd From Source
The version of gpsd that Raspbian provides (version 3.16 at the time) wasn't allowing me to control the GPS rate via the `gpsctl -c <rate>` command. I also fixed some items related to statusing the GPS rate. That is fixed as of gspd release-3.23. Here's how to build gpsd from source on the Pi. I recommend building release-3.23.1 of gpsd because in release-3.23 the lat/lon has a tendency to get stuck at a fixed value out of gpsd (wasn't a libgps issue).

You'll need the `scons` build system installed which you can get by `sudo apt install scons`.

```bash
git clone https://gitlab.com/gpsd/gpsd.git
cd gpsd
git checkout release-3.23.1
scons
scons udev-install
```

## Building Raspberry PI Source
```
sudo apt update
sudo apt install libcppunit-dev libzmq3-dev libprotobuf-dev libaprutil1-dev libapr1-dev liblog4cxx-dev
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install
```

## Telemetry Server Setup
This section describes how to configure the Raspberry Pi based telemetry server. The Telemetry server has three hardware devices wired to it.

| Device        | Chipset  | Interface Inputs | Supplementary Inputs   |
|---------------|----------|------------------|------------------------|
| GPS           | Ublox U7 | UART 0 (ttyAMA0) | pps: BCM 4             |
| IMU           | ICM20948 | I2C              | int: BCM 27            |
| CAN interface | MCP2515  | SPI              | int: BCM 17, cs: BCM 8 |

### CAN Setup
Follow this guide to configure the CAN device. Reference above table for GPIO pins.
https://www.raspberrypi.org/forums/viewtopic.php?t=141052

# Network Services
| Service            | Socket Type       | Transport                | Port |
|--------------------|-------------------|--------------------------|------|
| cand               | ZMQ PUB/SUB       | TCP                      | 7070 |
| imud               | ZMQ PUB/SUB       | ipc:///var/run/imud.sock | N/A  |
| gpsd               | BSD Client/Server | ?                        | 2947 |
| NavServer          | ZMQ PUB/SUB       | TCP                      | 6060 |
| DataLogger::Status | ZMQ PUB/SUB       | TCP                      | 5050 |

# Setup NTP with GPS
Here are some good resources to reference.
 * http://unixwiz.net/techtips/raspberry-pi3-gps-time.html
 * https://gpsd.gitlab.io/gpsd/gpsd-time-service-howto.html
 * https://www.rapid7.com/blog/post/2015/07/27/adding-a-gps-time-source-to-ntpd/

Basic steps to follow:
1. get gpsd running with PPS
 - verify with `gpsmon`
 - see that there are PPS event in the event log
2. run `sudo ntpshmmon`
 - see that there are PPS samples displayed
3. configure ntp to listen to GPS time source
 - follow [this guide](https://www.rapid7.com/blog/post/2015/07/27/adding-a-gps-time-source-to-ntpd/)
4. run `nptq -p`
 - verify that the GPS and PPS sources are in the list of time sources feeding ntpd

# Logging
The C++ apps that run on the raspberry use log4cxx logging. All solodata apps will share the same logging config file installed into `/usr/local/etc/solodata_logging.cfg`. The default logging config creates 3 appenders: ConsoleAppender, FileAppender, and ZMQ_Appender.

The console appender allows each app to log directly to stdout. This appender is really only useful when running apps manually from commandline (ie. when developing).

The file appender configures all apps to log to `/var/log/solodata/solodata.log`. This log file is useful for seeing logs in the deployed system. A logrotate config file is installed into `/etc/logrotate.d/solodata` that can rotate the logs based on size (10M default size rotation).

The ZMQ appender will send logs over an IPC ZMQ socket connection to the LogAggregator process. The aggregator process will stream the logs over a TCP socket to a potential chainsaw endpoint. This again, is really only useful for development because these logs are not store persistently.