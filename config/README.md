# solodata.conf
This file is installed to ```/etc/ld.so.conf.d``` on the target system.
It configures LD_LIBRARY_PATH properly so that shared objects are found in ```/usr/local/lib```.
**NOTE:** You may need to run ```sudo ldconfig``` after install for new shared objects to get picked up correctly.