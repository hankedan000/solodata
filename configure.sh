#!/bin/bash
# source repository environment variables
source env.sh

if [[ -f "src/java/lib/JMapViewer-2.9/JMapViewer.jar" ]]; then
	echo "JMapViewer.jar already installed."
else
	echo "Getting JMapViewer..."
	wget https://svn.openstreetmap.org/applications/viewer/jmapviewer/releases/2.9/JMapViewer-2.9.zip
	mkdir -p src/java/lib/JMapViewer-2.9
	unzip -n JMapViewer-2.9.zip -d src/java/lib/JMapViewer-2.9
	rm JMapViewer-2.9.zip
	# mvn install:install-file -Dfile=src/java/lib/JMapViewer-2.9/JMapViewer.jar -DgroupId=org.openstreetmap.gui -DartifactId=jmapviewer -Dversion=2.9 -Dpackaging=jar
fi

if [[ -f "src/java/lib/TunerStudioPluginAPI-1.0/TunerStudioPluginAPI.jar" ]]; then
	echo "TunerStudioPluginAPI already installed."
else
	echo "Getting TunerStudioPluginAPI..."
	wget http://www.efianalytics.com/TunerStudio/plugins/TunerStudioPluginAPI.jar
	mkdir -p src/java/lib/TunerStudioPluginAPI-1.0
	mv TunerStudioPluginAPI.jar src/java/lib/TunerStudioPluginAPI-1.0/
fi

echo "Creating generated source dirs..."
# create directory for generated protobuf python source files
mkdir -p $PROTO_DIR_PY
touch $PROTO_DIR_PY/__init__.py
# create directory for generated protobuf java source files
mkdir -p $PROTO_DIR_JAVA
ln -sfn $PROTO_DIR_JAVA/solodata $REPO_ROOT/src/java/SoloDataCommon/src/main/java/

# generate protobuf source code
echo "Generating protobuf source code..."
PROTO_SRC=$(find src/proto -name "*.proto")
for proto_file in $PROTO_SRC; do
	echo "  $proto_file"
	protoc -I=src/proto --python_out=$PROTO_DIR_PY $proto_file
	protoc -I=src/proto --java_out=$PROTO_DIR_JAVA $proto_file
done

echo "Complete!"